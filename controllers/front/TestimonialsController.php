<?php

class TestimonialsController extends FrontController{

	public function initContent()
    {
        parent::initContent();
		
		$errMsg="";
		if(Tools::getValue('nume')!=""){
		//if(isset(Tools::getValue('g-recaptcha-response')){
			if(!($gcaptcha = (int)(Tools::getValue('g-recaptcha-response')))){
				$errMsg = 'Apasa pe checkboxul de verificare!';
			}else{
				$secret = '6LePZ10UAAAAAGdQmOjSqmEoNX7kd9O6xpGb-Vlq';
			 	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.Tools::getValue('g-recaptcha-response'));
		        $responseData = json_decode($verifyResponse);
		        if($responseData->success){
		            //save to database
		        	Db::getInstance(_PS_USE_SQL_SLAVE_)->insert('jmstestimonials', array(
					    'author' => pSQL(Tools::getValue('nume')),
					    'posttime' => pSQL(date('Y-m-d H:i:s')),
					    'image' => pSQL(""),
					    'active' => 0
					));

		        	
					
					Db::getInstance(_PS_USE_SQL_SLAVE_)->insert('jmstestimonials_lang', array(
					    'id_testimonial' => (int)Db::getInstance()->Insert_ID(),
					    'id_lang' => (int)1,
					    'office' => pSQL(Tools::getValue('oras')),
					    'comment' => 'DE LA: '.pSQL(Tools::getValue('email').' |+| '.Tools::getValue('marturie'))
					));

		        	// echo Db::getInstance()->getMsgError();
		        	// die();
		        	Tools::redirect('testimonials');
		        } else {
		            $errMsg = 'Apasa pe checkboxul de verificare!';
		        }
			}

		}
		
		



	    if (Tools::getValue('add_new')=='true'){
	    	$this->setTemplate('add-testimonials-index');
	    	$this->context->smarty->assign([
		       'error' => $errMsg
		    ]);
	    }else{
			$this->setTemplate('testimonials-index');
			$testimonials=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
				'SELECT * FROM ps_jmstestimonials pb LEFT JOIN ps_jmstestimonials_lang pbl ON (pbl.`id_testimonial`=pb.`id_testimonial`) WHERE pbl.`comment` !="" AND pb.`active` = "1" ORDER BY pb.posttime DESC LIMIT 0,100');
			$this->context->smarty->assign([
		       'testimonials' => $testimonials
		    ]);
		}
    }
}
