<?php  
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

class CautaController extends FrontController {
	  //public $php_self = 'reaparitii';

    /**
     * @inheritdoc
     */
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate('cauta-index');

        $theQuery = Tools::getValue('s');
        $thecat = Tools::getValue('cat');
        $queryProd = explode(" ", $theQuery);

        $sql = new DbQuery();
        $sql->select('pl.`name`, p.id_product, p.`active`, pl.*, p.*');
        $sql->from('product', 'p');
        $sql->leftJoin('product_lang', 'pl', 'p.`id_product` = pl.`id_product` ');

        $i=0;
        $where = '';
        foreach($queryProd as $query) {
            if (strlen($query) > 2) {
                $i++;
                if ($i > 1) {
                    $where .= ' AND ';
                }
                $where .= '(pl.`name` LIKE \'%' . pSQL($query) . '%\' OR p.`supplier_reference` LIKE \'%' . pSQL($query) . '%\')';
            }
        }

        if ($thecat){
            $sql->leftJoin('category_lang', 'cl', 'cl.id_category = p.id_category_default');
            $where .= ' AND (cl.`name` LIKE \'%'.$thecat.'%\')';
        }

        $sql->orderBy('p.`active` DESC');
        $sql->limit(200);

        if ($where != ''){
            $where .= ' AND p.`active`=1';
        } else {
            $where .= ' p.`active`=1';
        }
        $sql->where($where."");


        $products = Db::getInstance()->executeS($sql);



        if (count($products) === 0){
            $sql1 = new DbQuery();
            $sql1->select('pl.`name`, p.id_product, p.`active`, pl.*, p.*');
            $sql1->from('product', 'p');
            $sql1->leftJoin('product_lang', 'pl', 'p.`id_product` = pl.`id_product` ');

            $i=0;
            $where = '';
            foreach($queryProd as $query) {
                if (strlen($query) > 2) {
                    $i++;
                    if ($i > 1) {
                        $where .= ' OR ';
                    }
                    $where .= '(pl.`name` LIKE \'%' . pSQL($query) . '%\' OR p.`supplier_reference` LIKE \'%' . pSQL($query) . '%\')';
                }
            }

            if ($thecat){
                $sql->leftJoin('category_lang', 'cl', 'cl.id_category = p.id_category_default');
                $where .= ' AND (cl.`name` LIKE \'%'.$thecat.'%\')';
            }

            $sql1->orderBy('p.`active` DESC');
            $sql1->limit(200);

            $sql1->where($where);

            $products = Db::getInstance()->executeS($sql1);
        }



		$productss=array();
		foreach($products as $key => $prod){
			$p = new Product($prod['id_product']);
			$link = new Link();
			$url = $link->getProductLink($p);

			$cover=Product::getCover($prod['id_product']);
			if($cover["id_image"]){
			   $link_rewrite=is_array($p->link_rewrite)?$p->link_rewrite[1]:$p->link_rewrite;
			   $link=new Link();
			   $image_link=Tools::getShopProtocol().$link->getImageLink($link_rewrite, $cover["id_image"], 'home_default');
			}
			$thearray=[$prod['id_product'],$prod['name'], number_format($prod['price'],2,',','.').' RON',$url,$image_link];
			array_push($productss, $thearray);

		}

//		 echo '<pre>';
//		 print_r($productss);
//		 die();
		$this->context->smarty->assign([
	       'products' => $productss,
            'count' => count($products),
            'search'=>$theQuery
	    ]);
    }


   
    
}
