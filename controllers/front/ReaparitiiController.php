<?php  
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

class ReaparitiiController extends FrontController {
	  //public $php_self = 'reaparitii';

    /**
     * @inheritdoc
     */
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate('reaparitii-index');
    	
    	 $sql = 'SELECT p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name
				FROM `'._DB_PREFIX_.'product` p
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`) 
                    AND product_shop.`visibility` IN ("both", "catalog") 
                    AND product_shop.`active` = 1 
                    AND p.quantity > 0
				ORDER BY p.date_upd DESC LIMIT 0,100';


        $products = Db::getInstance()->executeS($sql);
		
		$productss=array();
		foreach($products as $key => $prod){
			$p = new Product($prod['id_product']);
			$link = new Link();
			$url = $link->getProductLink($p);

			$cover=Product::getCover($prod['id_product']);
			if($cover["id_image"]){
			   $link_rewrite=is_array($p->link_rewrite)?$p->link_rewrite[1]:$p->link_rewrite;
			   $link=new Link();
			   $image_link=Tools::getShopProtocol().$link->getImageLink($link_rewrite, $cover["id_image"], 'home_default');
			}

			$thearray=[$prod['id_product'],$prod['name'], number_format($prod['price'],2,',','.').' RON',$url,$image_link];
			array_push($productss, $thearray);
			
		}

		// echo '<pre>';
		// print_r($productss);
		// die();
		$this->context->smarty->assign([
	       'products' => $productss
	    ]);
    }


   
    
}
