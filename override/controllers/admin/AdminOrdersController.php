<?php

class AdminOrdersController  extends AdminOrdersControllerCore{

    protected function doEditProductValidation(OrderDetail $order_detail, Order $order, OrderInvoice $order_invoice = null)
    {
        if (!Validate::isLoadedObject($order_detail)) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('The Order Detail object could not be loaded.', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

        if (!empty($order_invoice) && !Validate::isLoadedObject($order_invoice)) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('The invoice object cannot be loaded.', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

        if (!Validate::isLoadedObject($order)) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('The order object cannot be loaded.', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

        if ($order_detail->id_order != $order->id) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('You cannot edit the order detail for this order.', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

         //We can't edit a delivered order
//        if ($order->hasBeenDelivered()) {
//            die(json_encode(array(
//                'result' => false,
//                'error' => $this->trans('You cannot edit a delivered order.', array(), 'Admin.Orderscustomers.Notification')
//            )));
//        }

        if (!empty($order_invoice) && $order_invoice->id_order != Tools::getValue('id_order')) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('You cannot use this invoice for the order', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

        // Clean price
        $product_price_tax_incl = str_replace(',', '.', Tools::getValue('product_price_tax_incl'));
        $product_price_tax_excl = str_replace(',', '.', Tools::getValue('product_price_tax_excl'));

        if (!Validate::isPrice($product_price_tax_incl) || !Validate::isPrice($product_price_tax_excl)) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('Invalid price', array(), 'Admin.Orderscustomers.Notification')
            )));
        }

        if (!is_array(Tools::getValue('product_quantity')) && !Validate::isUnsignedInt(Tools::getValue('product_quantity'))) {
            die(json_encode(array(
                'result' => false,
                'error' => $this->trans('Invalid quantity', array(), 'Admin.Orderscustomers.Notification')
            )));
        } elseif (is_array(Tools::getValue('product_quantity'))) {
            foreach (Tools::getValue('product_quantity') as $qty) {
                if (!Validate::isUnsignedInt($qty)) {
                    die(json_encode(array(
                        'result' => false,
                        'error' => $this->trans('Invalid quantity', array(), 'Admin.Orderscustomers.Notification')
                    )));
                }
            }
        }
    }
}