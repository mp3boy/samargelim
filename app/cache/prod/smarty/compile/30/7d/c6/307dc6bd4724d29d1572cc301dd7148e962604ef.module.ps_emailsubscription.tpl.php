<?php /* Smarty version Smarty-3.1.19, created on 2020-05-21 09:47:58
         compiled from "module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8851785755ec6241e847462-37373343%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl',
      1 => 1525600552,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '8851785755ec6241e847462-37373343',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'urls' => 0,
    'msg' => 0,
    'nw_error' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ec6241e85b2d0_20233824',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec6241e85b2d0_20233824')) {function content_5ec6241e85b2d0_20233824($_smarty_tpl) {?>

<!-- Block Newsletter module-->
<div id="newsletter_block_left" class="block newsletter_block">	
		<div class="addon-title">
			<h3><?php echo smartyTranslate(array('s'=>'Shop By Style','d'=>'Shop.Theme'),$_smarty_tpl);?>
</h3>
		</div>
	<div class="block_content">		
		<div class="newsletter_title">
			<span><?php echo smartyTranslate(array('s'=>'Sign up to Newsletter and receive $20 coupon for first shopping','d'=>'Shop.Theme'),$_smarty_tpl);?>
</span>
		</div>

		<div class="news_content newsletter_content">
	         <div  class="block_content block_c_right">
				<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
#footer" method="post">
						<div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['msg']->value)&&$_smarty_tpl->tpl_vars['msg']->value) {?> <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>form-error<?php } else { ?>form-ok<?php }?><?php }?>" >
							<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo smartyTranslate(array('s'=>'Enter your email','d'=>'Shop.Forms.Labels'),$_smarty_tpl);?>
" />
							
							<button type="submit" name="submitNewsletter" class="btn_newsletter btn-default btn">
								<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
								<span><?php echo smartyTranslate(array('s'=>'Subscribe','d'=>'Shop.Theme'),$_smarty_tpl);?>
</span>
							</button>
							<input type="hidden" name="action" value="0" />
						</div>
					</form>
				</div>
	       </div>
	</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
    <div class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

    </div>
<?php }?><?php }} ?>
