<?php /* Smarty version Smarty-3.1.19, created on 2020-05-12 06:44:36
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/cart-detailed.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13266670755eba1ba4cdcf51-89189780%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d83746fdb32e303fd362b4aff05f5024b0f204b' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/cart-detailed.tpl',
      1 => 1543346741,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13266670755eba1ba4cdcf51-89189780',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cart' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5eba1ba4cf4639_50144177',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eba1ba4cf4639_50144177')) {function content_5eba1ba4cf4639_50144177($_smarty_tpl) {?>
 


<div class="cart-overview js-cart" data-refresh-url="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->getUrlSmarty(array('entity'=>'cart','params'=>array('ajax'=>true,'action'=>'refresh')),$_smarty_tpl);?>
">
  <?php if ($_smarty_tpl->tpl_vars['cart']->value['products']) {?>
  <div style="height: 30px;padding-top: 5px;background-color: #eff8d3;    border: 1px solid #f9e919;">
    <div class="product-line-grid-left col-md-2 col-xs-4 left">Imagine</div>
    <div class="product-line-grid-left col-md-4 col-xs-8 left">Denumire</div>
    <div class="product-line-grid-left col-md-2 col-xs-4 left">Pret</div>
    <div class="product-line-grid-left col-md-2 col-xs-4 left">Cantitate</div>
    <div class="product-line-grid-left col-md-2 col-xs-4 left">Total</div>
  </div>
  <ul class="cart-items">
    <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
      <li class="cart-item"><?php echo $_smarty_tpl->getSubTemplate ('checkout/_partials/cart-detailed-product-line.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>
</li>
      <?php if (count($_smarty_tpl->tpl_vars['product']->value['customizations'])>1) {?>
      <hr>
      <?php }?>
      
      <?php if ($_smarty_tpl->tpl_vars['product']->value['stock_quantity']<$_smarty_tpl->tpl_vars['product']->value['cart_quantity']) {?>
        <div class="alert alert-warning" role="alert">Produsul <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
 are doar <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['stock_quantity'], ENT_QUOTES, 'UTF-8');?>
 bucati in stoc.</div>
      <?php }?>
    <?php } ?>
  </ul>
  <?php } else { ?>
    <span class="no-items" style="margin-bottom:40px;display: block;"><?php echo smartyTranslate(array('s'=>'There are no more items in your cart','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</span>
  <?php }?>
</div>
<?php }} ?>
