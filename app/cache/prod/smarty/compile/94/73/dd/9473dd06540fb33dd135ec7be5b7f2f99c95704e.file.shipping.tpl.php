<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 17:08:14
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/steps/shipping.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1238928665ebbff4e462385-65421015%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9473dd06540fb33dd135ec7be5b7f2f99c95704e' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/steps/shipping.tpl',
      1 => 1543758215,
      2 => 'file',
    ),
    'dbdc201d8fbd87fa5898461d3116b25e8a201688' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/steps/checkout-step.tpl',
      1 => 1543059827,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1238928665ebbff4e462385-65421015',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'identifier' => 0,
    'position' => 0,
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ebbff4e4a05f2_01174016',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ebbff4e4a05f2_01174016')) {function content_5ebbff4e4a05f2_01174016($_smarty_tpl) {?>
<section  id    = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['identifier']->value, ENT_QUOTES, 'UTF-8');?>
"
          class = "<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['classnames'][0][0]->smartyClassnames(array('checkout-step'=>true,'-current'=>true,'-reachable'=>true,'-complete'=>true,'js-current-step'=>true)), ENT_QUOTES, 'UTF-8');?>
"
>
	<div class="step-box1">
		 <h1 class="step-title">
			<i class="fa fa-check-circle-o done"></i>
			<span class="step-number"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['position']->value, ENT_QUOTES, 'UTF-8');?>
</span>
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>

			<span class="step-edit text-muted"><i class="fa fa-pencil-square-o edit"></i> edit</span>
		</h1>
		<div class="content">
			
  <div id="hook-display-before-carrier">
    <?php echo $_smarty_tpl->tpl_vars['hookDisplayBeforeCarrier']->value;?>

  </div>

  <div class="delivery-options-list">
      
      <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value, ENT_QUOTES, 'UTF-8');?>
" id="currentcartid">
    <?php if (count($_smarty_tpl->tpl_vars['delivery_options']->value)) {?>
      <form
        class="clearfix"
        id="js-delivery"
        data-url-update="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->getUrlSmarty(array('entity'=>'order','params'=>array('ajax'=>1,'action'=>'selectDeliveryOption')),$_smarty_tpl);?>
"
        method="post"
      >
        <hr>
        <h1 class="testimonialss" style="float:none;display:block">Modalitati de livrare</h1>   
        <div class="form-fields">
          
          <?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['customer']->value['addresses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
?>
            
                <?php if (mb_strtolower($_smarty_tpl->tpl_vars['address']->value['city'], 'UTF-8')=="bucuresti"||mb_strtolower($_smarty_tpl->tpl_vars['address']->value['city'], 'UTF-8')=="bucurești") {?>
                  <?php $_smarty_tpl->tpl_vars['thetransport'] = new Smarty_variable("transport_bucuresti", null, 0);?> 
                <?php }?>
                <?php if (mb_strtolower($_smarty_tpl->tpl_vars['address']->value['city'], 'UTF-8')!="bucuresti"&&mb_strtolower($_smarty_tpl->tpl_vars['address']->value['city'], 'UTF-8')!="bucurești") {?>
                  <?php $_smarty_tpl->tpl_vars['thetransport'] = new Smarty_variable("transport_tara", null, 0);?> 
                <?php }?>
            
          <?php } ?>
            <div class="delivery-options <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thetransport']->value, ENT_QUOTES, 'UTF-8');?>
">
              <?php  $_smarty_tpl->tpl_vars['carrier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carrier']->_loop = false;
 $_smarty_tpl->tpl_vars['carrier_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carrier']->key => $_smarty_tpl->tpl_vars['carrier']->value) {
$_smarty_tpl->tpl_vars['carrier']->_loop = true;
 $_smarty_tpl->tpl_vars['carrier_id']->value = $_smarty_tpl->tpl_vars['carrier']->key;
?>
                <?php if ($_smarty_tpl->tpl_vars['carrier']->value['id']==23&&$_smarty_tpl->tpl_vars['thetransport']->value=="transport_tara") {?>
                <?php } elseif ($_smarty_tpl->tpl_vars['carrier']->value['id']==25&&$_smarty_tpl->tpl_vars['thetransport']->value=="transport_bucuresti") {?>
                <?php } else { ?>
                  <div class="delivery-option " >
                  
                    <label for="delivery_option_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="col-sm-12 delivery-option-2" style="margin-left:20px;">
                      <div class="row">
                        <div class="col-sm-2 col-xs-12">
                          <div class="row">
                           
                            <div class="col-xs-12">
                                <span class="custom-radio pull-xs-left">
                                    <input type="radio" name="delivery_option[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_address']->value, ENT_QUOTES, 'UTF-8');?>
]" id="delivery_option_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['id'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier_id']->value, ENT_QUOTES, 'UTF-8');?>
"<?php if ($_smarty_tpl->tpl_vars['delivery_option']->value==$_smarty_tpl->tpl_vars['carrier_id']->value) {?> checked<?php }?>>
                                    
                                  </span>
                              <span class="h6 carrier-name bodyText2"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                          <span class="carrier-delay labelsl"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['delay'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>
                        <div class="col-sm-1 col-xs-12">
                          <span class="carrier-price smgg2"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['price'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>
                      </div>
                    </label>
                    <div class="col-md-12 carrier-extra-content"<?php if ($_smarty_tpl->tpl_vars['delivery_option']->value!=$_smarty_tpl->tpl_vars['carrier_id']->value) {?> style="display:none;"<?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['carrier']->value['extraContent'];?>

                    </div>
                    <div class="clearfix"></div>
                  </div>
                <?php }?>

              <?php } ?>
            </div>
          
          <div class="order-options">
            <?php if ($_smarty_tpl->tpl_vars['recyclablePackAllowed']->value) {?>
              <label>
                <input type="checkbox" name="recyclable" value="1" <?php if ($_smarty_tpl->tpl_vars['recyclable']->value) {?> checked <?php }?>>
                <span><?php echo smartyTranslate(array('s'=>'I would like to receive my order in recycled packaging.','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</span>
              </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['gift']->value['allowed']) {?>
              <span class="custom-checkbox">
                <input
                  class="js-gift-checkbox"
                  name="gift"
                  type="checkbox"
                  value="1"
                  <?php if ($_smarty_tpl->tpl_vars['gift']->value['isGift']) {?>checked="checked"<?php }?>
                >
                <span><i class="material-icons checkbox-checked">&#xE5CA;</i></span>
                <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gift']->value['label'], ENT_QUOTES, 'UTF-8');?>
</label >
              </span>

              <div id="gift" class="collapse<?php if ($_smarty_tpl->tpl_vars['gift']->value['isGift']) {?> in<?php }?>">
                <label for="gift_message"><?php echo smartyTranslate(array('s'=>'If you\'d like, you can add a note to the gift:','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</label>
                <textarea rows="2" cols="120" id="gift_message" name="gift_message"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gift']->value['message'], ENT_QUOTES, 'UTF-8');?>
</textarea>
              </div>

            <?php }?>
          </div>
        </div>

       
      
        

        <button type="submit" class="continue btn btn-primary button-small pull-xs-right finalizeaza_comanda" name="confirmDeliveryOption" value="1">
          <?php echo smartyTranslate(array('s'=>'Continue','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>

        </button>
      </form>
    <?php } else { ?>
      <p class="alert alert-danger"><?php echo smartyTranslate(array('s'=>'Unfortunately, there are no carriers available for your delivery address.','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</p>
    <?php }?>
  </div>

  <div id="hook-display-after-carrier">
    <?php echo $_smarty_tpl->tpl_vars['hookDisplayAfterCarrier']->value;?>

  </div>

  <div id="extra_carrier"></div>


		</div>
	</div>
</section>
<?php }} ?>
