<?php /* Smarty version Smarty-3.1.19, created on 2020-05-14 07:01:41
         compiled from "modules/loyaltyrewardpoints/views/templates/front/checkout/display_shopping_cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11069296635ebcc2a5bd4dc6-70752632%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e8ae31c33cda035c5633c0de99e957bf8c2f9bae' => 
    array (
      0 => 'modules/loyaltyrewardpoints/views/templates/front/checkout/display_shopping_cart.tpl',
      1 => 1543430061,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11069296635ebcc2a5bd4dc6-70752632',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'delivery_message' => 0,
    'points_redeemed' => 0,
    'points' => 0,
    'points_redeemed_value' => 0,
    'baseDir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ebcc2a5bf3c53_66718747',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ebcc2a5bf3c53_66718747')) {function content_5ebcc2a5bf3c53_66718747($_smarty_tpl) {?>



<div id="lrp-points" class="card-block">
<div class="nuafisaincart"> 
    <hr>
    <h1 class="testimonialss" style="float:none;display:block">Factura</h1>
    <input type="checkbox" id="factura" onchange="if(this.checked == 1){ document.getElementById('hiddfields').style.display='block';}else{ document.getElementById('hiddfields').style.display='none'; }"/> <label for="factura">Doresc factura pe firma</label>
    
    <div id="hiddfields">
        <label class="inputLabel" for="camp1">Cumparator*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp1" id="camp1" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp2">Nr. Ord. Reg. Com.*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp2" id="camp2" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp3">AF / CUI*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp3" id="camp3" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp4">Sediul*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp4" id="camp4" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp5">Judet*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp5" id="camp5" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp6">Cont</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp6" id="camp6" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp7">Banca</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp7" id="camp7" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
    </div>
    
    <hr>
    <h1 class="testimonialss" style="float:none;display:block">Modalitate de plata</h1>
    <div class="bodyText2">Ramburs</div>
    
     <hr>
     <h1 class="testimonialss" style="float:none;display:block">Instructiuni speciale sau detalii comanda</h1>
     
     <div id="delivery">
        <textarea style="width:100%;height:100px;background: #f6e12f;resize:none;padding:10px;border:1px solid #000;" id="delivery_message" name="delivery_message"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_message']->value, ENT_QUOTES, 'UTF-8');?>
</textarea>
    </div>    
    <hr>
    <h1 class="testimonialss" style="float:none;display:block">Puncte de fidelitate</h1>   

<script src="/modules/loyaltyrewardpoints/views/js/front/LRPFrontCheckoutController.js"></script>

	<?php if ($_smarty_tpl->tpl_vars['points_redeemed']->value==0) {?>

		<?php if ($_smarty_tpl->tpl_vars['points']->value>0) {?>

			<span id="lrp-summary" class="label">

				<i class="material-icons" style="margin-top: -2px;vertical-align: middle;">card_giftcard</i>

				<?php echo smartyTranslate(array('s'=>'Ai in cont %d puncte disponibile valorand %d lei.','sprintf'=>array($_smarty_tpl->tpl_vars['points']->value,$_smarty_tpl->tpl_vars['points']->value*0.5),'mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>


				<a id="lrp-redeem-link" href="#lrp-redeem"><?php echo smartyTranslate(array('s'=>'','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</a>

			</span>

			<div id="lrp-redeem-form">

				<input name="points" type="number" value="0" size="6" maxlength="6">

				<span class="points-label label"><?php echo smartyTranslate(array('s'=>'puncte','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span>

				<a href="#lrp-redeem" id="btn-lrp-redeem" class="btn btn-secondary"><?php echo smartyTranslate(array('s'=>'Foloseste','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</a>

			</div>

		<?php }?>

	<?php } else { ?>

		<strong><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['points_redeemed']->value,'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
 <?php echo smartyTranslate(array('s'=>'puncte folosite','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
 (<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['points_redeemed_value']->value,'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
)</strong>

		<a href="#" id="lrp-points-clear" class="material-icons" style="margin-top: -2px;vertical-align: middle;">clear</a>

	<?php }?>
</div>

</div>



<script>

	document.addEventListener("DOMContentLoaded", function (event) {

		$(function () {

			baseDir = "<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['baseDir']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
";

			let lrp_front_checkout_controller = new LRPFrontCheckoutController("#lrp-points");

		});

	});

</script>

<?php }} ?>
