<?php /* Smarty version Smarty-3.1.19, created on 2020-05-14 18:20:00
         compiled from "/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/customer/_partial_history_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20173907375ebd61a06fcd96-42454006%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13efe21456f8beca6df87ceccdb5cd6cd5b1043a' => 
    array (
      0 => '/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/customer/_partial_history_list.tpl',
      1 => 1538930614,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20173907375ebd61a06fcd96-42454006',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'history' => 0,
    'history_item' => 0,
    'pagination' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ebd61a0731543_41225939',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ebd61a0731543_41225939')) {function content_5ebd61a0731543_41225939($_smarty_tpl) {?>

<table class="table">
	<thead>
	<tr>
		<th><span class="title_box "><?php echo smartyTranslate(array('s'=>'Order','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box "><?php echo smartyTranslate(array('s'=>'Points','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box "><?php echo smartyTranslate(array('s'=>'Point Value','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box "><?php echo smartyTranslate(array('s'=>'Type','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box "><?php echo smartyTranslate(array('s'=>'Source','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span></th>
	</tr>
	</thead>
	<tbody>
	<?php if ($_smarty_tpl->tpl_vars['history']->value) {?>
		<?php  $_smarty_tpl->tpl_vars['history_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['history_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['history']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['history_item']->key => $_smarty_tpl->tpl_vars['history_item']->value) {
$_smarty_tpl->tpl_vars['history_item']->_loop = true;
?>
			<tr>
				<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['history_item']->value->reference,'htmlall','UTF-8');?>
</td>
				<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['history_item']->value->points,'htmlall','UTF-8');?>
</td>
				<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['history_item']->value->point_value,'htmlall','UTF-8');?>
</td>
				<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['history_item']->value->type,'htmlall','UTF-8');?>
</td>
				<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['history_item']->value->source,'htmlall','UTF-8');?>
</td>
			</tr>
		<?php } ?>
	<?php }?>
	</tbody>
</table>

<?php if ($_smarty_tpl->tpl_vars['pagination']->value) {?>
	<?php if ($_smarty_tpl->tpl_vars['pagination']->value['page_total']>1) {?>
		<div id="lrp-history-pagination" class="lrp-pagination">
			<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['pagination']->value['page_total']+1 - (1) : 1-($_smarty_tpl->tpl_vars['pagination']->value['page_total'])+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
				<a href="" class="page <?php if ($_smarty_tpl->tpl_vars['pagination']->value['current_page']==$_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?>" data-page="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['i']->value,'htmlall','UTF-8');?>
"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['i']->value,'htmlall','UTF-8');?>
</a>
			<?php }} ?>
		</div>
	<?php }?>
<?php }?>
<?php }} ?>
