<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7405776905ebc4cc7929ba6-68849848%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7058813d066d30ed2a9d832f7281568d2e0c2575' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/contact.tpl',
      1 => 1532363956,
      2 => 'file',
    ),
    'b6baec9af587ea151d3c353388ef3603c2b6079c' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/page.tpl',
      1 => 1529424302,
      2 => 'file',
    ),
    '4f61b1e25633f6fbfcebc5b966a16fa15085184d' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/layouts/layout-left-column.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
    '77a7f8c9b03159a9dcd359a3c3bfa9e202e6e399' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/layouts/layout-both-columns.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
    '6a8ffe7d95b98cedc261448a80606c46ff6da369' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/stylesheets.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
    'ba55be1bb2665c71d40c5aedef9cd9b8928889f4' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/javascript.tpl',
      1 => 1532348602,
      2 => 'file',
    ),
    '71e65422a3b18ef457ba5d7b11dca1803ba9dd44' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/head.tpl',
      1 => 1532070079,
      2 => 'file',
    ),
    '732a8be44c53ef29e0a8e0656707a3abd9c14fad' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/catalog/_partials/product-activation.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
    'c52551d93d65479c34d0e3a6e7da515a28317785' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/header.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
    'cd7660410511d243f92e365d967897662d2033b1' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/breadcrumb.tpl',
      1 => 1532365377,
      2 => 'file',
    ),
    '827a74007ec33be5108b5d46e43a8870e0de3d39' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/_partials/footer.tpl',
      1 => 1525600549,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7405776905ebc4cc7929ba6-68849848',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'layout' => 0,
    'language' => 0,
    'page' => 0,
    'jpb_homeclass' => 0,
    'jpb_rtl' => 0,
    'jpb_loadingstyle' => 0,
    'jpb_mobilemenu' => 0,
    'javascript' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ebc4cc7a1c611_12784917',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ebc4cc7a1c611_12784917')) {function content_5ebc4cc7a1c611_12784917($_smarty_tpl) {?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">
  <head>
    
      <?php /*  Call merged included template "_partials/head.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc794ba83_79219643($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/head.tpl" */?>
    
  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['classnames'][0][0]->smartyClassnames($_smarty_tpl->tpl_vars['page']->value['body_classes']), ENT_QUOTES, 'UTF-8');?>
 <?php if (isset($_smarty_tpl->tpl_vars['jpb_homeclass']->value)&&$_smarty_tpl->tpl_vars['jpb_homeclass']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['jpb_homeclass']->value, ENT_QUOTES, 'UTF-8');?>
<?php }?> <?php if ($_smarty_tpl->tpl_vars['jpb_rtl']->value) {?> rtl<?php }?>">
	<?php if ($_smarty_tpl->tpl_vars['jpb_loadingstyle']->value) {?>
		<div class="preloader">
			<div class="spinner<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['jpb_loadingstyle']->value, ENT_QUOTES, 'UTF-8');?>
">
				<div class="dot1"></div>
				<div class="dot2"></div>
			    <div class="bounce1"></div>
			    <div class="bounce2"></div>
			    <div class="bounce3"></div>
			</div>
		</div>
	<?php }?>
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl);?>

	<?php if ($_smarty_tpl->tpl_vars['jpb_mobilemenu']->value) {?>
		<div class="menu-wrap hidden-lg hidden-md">
			<button id="close-button" class="close-button"> <?php echo smartyTranslate(array('s'=>'Menu','d'=>'Shop.Theme'),$_smarty_tpl);?>
</button>
				<nav id="off-canvas-menu">					
					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayTopColumn'),$_smarty_tpl);?>

				</nav>				
				
			</div>
	<?php }?>
    <div class="main-site">
      
        <?php /*  Call merged included template "catalog/_partials/product-activation.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc79b2331_04927053($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "catalog/_partials/product-activation.tpl" */?>
      
      <header id="header">
        
          <?php /*  Call merged included template "_partials/header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc79bc670_79044833($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/header.tpl" */?>
        
      </header>
      
     
      
		<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']!='index') {?>
			
			   <?php /*  Call merged included template "_partials/breadcrumb.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc79c4da9_73233877($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/breadcrumb.tpl" */?>
			
		<?php }?>
        <div id="wrapper" <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']!='index') {?>class="container"<?php }?>> 
		<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']!='index') {?>
		<div class="row">
		<?php }?>
          
  <div id="left-column" class="col-xs-12 col-sm-3">
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0][0]->smartyWidget(array('name'=>"ps_contactinfo",'hook'=>'displayLeftColumn'),$_smarty_tpl);?>

  </div>


          
  <div id="content-wrapper" class="left-column col-sm-12 col-md-8 col-lg-9 col-xs-12">
    

  <section id="main">
	
    
      <section id="content" class="page-content card card-block row">
        
        
	<h1>Contact</h1>
<div class="content_samargelim">
<div class="col-sm-6">
	Poti ridica comanda sau CUMPARA DIRECT de la sediul din Bucuresti: Str. Ernest Juvara, nr.24 - langa Gradina Botanica. <br /><em><b>In incinta Cafenelei Kuber, cladirea din spate.</b></em> <br /><br /><b>Program:</b><br /><em>Luni - Vineri:</em> 10:30 - 19:00 | <em>Sambata:</em> 10:00 - 14:00 | <em>Duminica:</em> Inchis<br /><br /><b>Cum ajungi?</b> <br /> Cu troleibuzele 62, 93 si 96, autobuzul 601. De la oricare din statiile de metrou Eroilor sau Grozavesti (6 minute de mers)<br /><br /><b>Contacteaza-ne:</b>
	<br /><br />
	<div class="col-sm-10"> 
	<!--<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0][0]->smartyWidget(array('name'=>"contactform"),$_smarty_tpl);?>
-->
	<form action="http://samargelim1.frincu.info/contact" method="post" enctype="multipart/form-data" id="cfcontact">
		<input type="hidden" name="id_contact" value="2" />

		<div>
			<label>Nume:</label>
			<input type="text" name="nume" required/>
		</div>
		<div>
			<label>Email:</label>
			<input type="email" name="from" required/>
		</div>
		<div>
			<label>Telefon:</label>
			<input type="text" name="telefon" required/>
		</div>
		<div>
			<label>Mesaj:</label>
			<textarea name="message" required></textarea>
		</div>
		<div class="g-recaptcha" data-sitekey="6LePZ10UAAAAAClZ6Tq5rqTeRo8p_XwCDncpC0r9"></div>
		<?php if ($_smarty_tpl->tpl_vars['error']->value!='') {?>
		<div class="cferror"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['error']->value, ENT_QUOTES, 'UTF-8');?>
</div>
		<?php }?>
		<div>
			<input type="submit" class="trimite_cf" name="submitMessage" />
		</div>
	</form>
	</div>
	<div style="clear:both;"></div>
<p><span class="edInfo_tel"><img src="http://samargelim1.frincu.info/img/cms/phone1.png" alt="" width="20" height="20" />  0720 241 480 / 0749 394 968</span><br class="clearBoth" /><span class="edInfo_adr"><img src="http://samargelim1.frincu.info/img/cms/email_contact.jpg" alt="" width="25" height="20" /> EDORA Corporation SRL<br />        RO18577681<br />        J40/6125/2006<br /><img src="http://samargelim1.frincu.info/img/cms/edora_email.png" alt="" width="119" height="27" /><br /><br />Bucuresti, Str. Ernest Juvara nr 24.</span> <span class="edInfo_ema"></span> 
</div>
<div class="col-sm-6">
<a class="llcolor" target="_blank" href="http://samargelim1.frincu.info/includes/templates/edora/images/templ2012/harta.jpg"> Harta sediu<br /></a><br /><a href="http://samargelim1.frincu.info/img/cms/harta.jpg" target="_blank"><img src="http://samargelim1.frincu.info/img/cms/harta.jpg" alt="" width="285" height="158" /></a><br /><br /><iframe width="285" height="285" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ro/maps?f=d&source=s_d&saddr=Strada+Ernest+Juvara&daddr=Strada+Ernest+Juvara&hl=ro&geocode=FdQUpgIdwb6NAQ%3BFdUUpgIdvr6NAQ&sll=44.438838,26.06696&sspn=0.000965,0.002411&mra=dme&mrsp=1&sz=19&ie=UTF8&t=m&ll=44.438836,26.066995&spn=0.010725,0.018239&z=15&output=embed"></iframe><br /><small><a href="https://maps.google.ro/maps?f=d&source=embed&saddr=Strada+Ernest+Juvara&daddr=Strada+Ernest+Juvara&hl=ro&geocode=FdQUpgIdwb6NAQ%3BFdUUpgIdvr6NAQ&sll=44.438838,26.06696&sspn=0.000965,0.002411&mra=dme&mrsp=1&sz=19&ie=UTF8&t=m&ll=44.438836,26.066995&spn=0.010725,0.018239&z=15" style="color: #0000ff; text-align: left;">Vizualizare harta marita</a></small></p>
</div>
</div>
 


      </section>
    

    
     
    
  </section>


  </div>


          
			
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']!='index') {?>
		</div>
		<?php }?>
      </div>

      <footer id="footer">
        
          <?php /*  Call merged included template "_partials/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc7a151b9_66913184($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/footer.tpl" */?>
        
      </footer>

    </div>

    
      <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc79839d2_53371674($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>
    

    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl);?>

	<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='cms') {?>		 
		 <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv9p77jYCRrlLa3xx1hwemtEjUyH3YcZo&callback=initMap">
        </script>
		<?php }?>
  </body>

</html>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/head.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc794ba83_79219643')) {function content_5ebc4cc794ba83_79219643($_smarty_tpl) {?>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">


  <title>
    
      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>

    
  </title>
  <meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">
  <meta name="keywords" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['keywords'], ENT_QUOTES, 'UTF-8');?>
">
  <?php if ($_smarty_tpl->tpl_vars['page']->value['meta']['robots']!=='index') {?>
    <meta name="robots" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['robots'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['page']->value['canonical']) {?>
    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['canonical'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>


<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/product.css" />
<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/checkout.css" />
<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/checkout.min.css" />

<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">

  <?php /*  Call merged included template "_partials/stylesheets.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/stylesheets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('stylesheets'=>$_smarty_tpl->tpl_vars['stylesheets']->value), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc796c495_95958457($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/stylesheets.tpl" */?>



  <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['head'],'vars'=>$_smarty_tpl->tpl_vars['js_custom_vars']->value), 0, '7405776905ebc4cc7929ba6-68849848');
content_5ebc4cc79839d2_53371674($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>



  <?php echo $_smarty_tpl->tpl_vars['HOOK_HEADER']->value;?>


<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/stylesheets.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc796c495_95958457')) {function content_5ebc4cc796c495_95958457($_smarty_tpl) {?>
 <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
 <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/neueeinstellung/neueeinstellung.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
	<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
  <style>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['content'], ENT_QUOTES, 'UTF-8');?>

  </style>
<?php } ?>

<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,700,700i" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/line-awesome.css" />

 <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/responsive.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
/css/responsive-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['jpb_homeclass']->value, ENT_QUOTES, 'UTF-8');?>
.css" />

 <?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/javascript.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc79839d2_53371674')) {function content_5ebc4cc79839d2_53371674($_smarty_tpl) {?>
<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
  <script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['attribute'], ENT_QUOTES, 'UTF-8');?>
></script>
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
  <script type="text/javascript">
    <?php echo $_smarty_tpl->tpl_vars['js']->value['content'];?>

  </script>
<?php } ?>

<?php if (isset($_smarty_tpl->tpl_vars['vars']->value)&&count($_smarty_tpl->tpl_vars['vars']->value)) {?>
  <script type="text/javascript">
    <?php  $_smarty_tpl->tpl_vars['var_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['var_value']->_loop = false;
 $_smarty_tpl->tpl_vars['var_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['var_value']->key => $_smarty_tpl->tpl_vars['var_value']->value) {
$_smarty_tpl->tpl_vars['var_value']->_loop = true;
 $_smarty_tpl->tpl_vars['var_name']->value = $_smarty_tpl->tpl_vars['var_value']->key;
?>
    var <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
 = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['var_value']->value);?>
;
    <?php } ?>
  </script>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['jpb_homepage']->value==8) {?>
<script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
themes/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['themename']->value, ENT_QUOTES, 'UTF-8');?>
/assets/js/jquery.multiscroll.extensions.min.js"></script>
<script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
themes/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['themename']->value, ENT_QUOTES, 'UTF-8');?>
/assets/js/jquery.multiscroll.js"></script>
<?php }?>

<script src="https://www.google.com/recaptcha/api.js?hl=ro" async defer></script><?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/catalog/_partials/product-activation.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc79b2331_04927053')) {function content_5ebc4cc79b2331_04927053($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['page']->value['admin_notifications']) {?>
  <div class="alert alert-warning row" role="alert">
    <div class="container">
      <div class="row">
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['page']->value['admin_notifications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <div class="col-sm-12">
            <i class="material-icons pull-xs-left">&#xE001;</i>
            <p class="alert-text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value['message'], ENT_QUOTES, 'UTF-8');?>
</p>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php }?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc79bc670_79044833')) {function content_5ebc4cc79bc670_79044833($_smarty_tpl) {?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayTop'),$_smarty_tpl);?>

<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/breadcrumb.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc79c4da9_73233877')) {function content_5ebc4cc79c4da9_73233877($_smarty_tpl) {?>
 <div class="breadcrumb">
<div class="breadcrumb-box container">
<!-- <span class="title_meta"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
</span> -->
	<div data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['breadcrumb']->value['count'], ENT_QUOTES, 'UTF-8');?>
" class="breadcrumb-inner hidden-sm-down">
  <ol itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php  $_smarty_tpl->tpl_vars['path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['breadcrumb']->value['links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['breadcrumb']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['path']->key => $_smarty_tpl->tpl_vars['path']->value) {
$_smarty_tpl->tpl_vars['path']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['breadcrumb']['iteration']++;
?>
      <?php if ($_smarty_tpl->tpl_vars['page']->value['meta']['title']!=$_smarty_tpl->tpl_vars['path']->value['title']) {?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
             <span itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
          </a>
          <meta itemprop="position" content="<?php echo htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['breadcrumb']['iteration'], ENT_QUOTES, 'UTF-8');?>
">
        </li>
      <?php } else { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
          <meta itemprop="position" content="<?php echo htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['breadcrumb']['iteration'], ENT_QUOTES, 'UTF-8');?>
">
        </li>
      <?php }?>
    <?php } ?>
  </ol>
</div>
</div>
</div>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:38:47
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/_partials/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5ebc4cc7a151b9_66913184')) {function content_5ebc4cc7a151b9_66913184($_smarty_tpl) {?>
 <div class="back-to-top" id="back-to-top">
			<span class="fa fa-angle-up"></span>
</div>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayFooter'),$_smarty_tpl);?>


<?php }} ?>
