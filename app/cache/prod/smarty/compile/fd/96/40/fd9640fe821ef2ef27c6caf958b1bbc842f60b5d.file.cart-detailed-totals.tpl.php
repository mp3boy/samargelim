<?php /* Smarty version Smarty-3.1.19, created on 2020-05-12 06:44:36
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/cart-detailed-totals.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11802616685eba1ba4d88da5-81426810%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd9640fe821ef2ef27c6caf958b1bbc842f60b5d' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/checkout/_partials/cart-detailed-totals.tpl',
      1 => 1542744059,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11802616685eba1ba4d88da5-81426810',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cart' => 0,
    'subtotal' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5eba1ba4db2392_94315136',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eba1ba4db2392_94315136')) {function content_5eba1ba4db2392_94315136($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['cart']->value['products']) {?>

<div class="cart-detailed-totals" style="width:50%;float:right;margin-top:-125px;">
<style>

    .estegol{display:none;}

</style>
  <div class="card-block">
    <?php  $_smarty_tpl->tpl_vars["subtotal"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["subtotal"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value['subtotals']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["subtotal"]->key => $_smarty_tpl->tpl_vars["subtotal"]->value) {
$_smarty_tpl->tpl_vars["subtotal"]->_loop = true;
?>
      <?php if ($_smarty_tpl->tpl_vars['subtotal']->value['value']&&$_smarty_tpl->tpl_vars['subtotal']->value['type']!=='tax') {?>
        <div class="cart-summary-line" id="cart-subtotal-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['type'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="label<?php if ('products'===$_smarty_tpl->tpl_vars['subtotal']->value['type']) {?> js-subtotal<?php }?>">
            <?php if ('products'==$_smarty_tpl->tpl_vars['subtotal']->value['type']) {?>
              <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['summary_string'], ENT_QUOTES, 'UTF-8');?>

            <?php } else { ?>
              <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['label'], ENT_QUOTES, 'UTF-8');?>

            <?php }?>
          </span>
          <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['value'], ENT_QUOTES, 'UTF-8');?>
</span>
          <?php if ($_smarty_tpl->tpl_vars['subtotal']->value['type']==='shipping') {?>
              <div><small class="value"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayCheckoutSubtotalDetails','subtotal'=>$_smarty_tpl->tpl_vars['subtotal']->value),$_smarty_tpl);?>
</small></div>
          <?php }?>
        </div>
      <?php }?>
    <?php } ?>
  </div>

  

 

  <div class="card-block">
    <div class="cart-summary-line cart-total" style="text-align:right;">
      <span class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['label'], ENT_QUOTES, 'UTF-8');?>
</span>
      <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'], ENT_QUOTES, 'UTF-8');?>
</span>
    </div>

    <div class="cart-summary-line">
      <small class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['subtotals']['tax']['label'], ENT_QUOTES, 'UTF-8');?>
</small>
      <small class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['subtotals']['tax']['value'], ENT_QUOTES, 'UTF-8');?>
</small>
    </div>
  </div>

 
</div>



<div class="ppppp card-block cart-summary-line">
  <div style="clear:both;"></div>
<div class="order-f">
  
  <div class="m15px buttonRow back" style="float:left;">
    <a href="/"><font color="#3f608a"><b>&lt;&lt; Inapoi la lista</b></font></a>
  </div>
  <div class="m15px buttonRow forward" style="color:#3f608a;font-size:12px;float: right;font-family: trebuchet ms;font-weight: bold;">
  <?php if ((60>$_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'])) {?>
  Comanda de inca <?php echo htmlspecialchars((intval(60)-intval($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'])), ENT_QUOTES, 'UTF-8');?>
 RON si ai livrarea gratuita!
  <?php } else { ?>
    
  <?php }?>
  </div>
  <div style="clear:both;margin-top:10px;"></div>
  <div class="buttonRow forward" style="float: right;">
    <a href="/comanda"><img src="/themes/jms_freshshop/assets/img/goto_casa.jpg"></a>
  </div>

  <div style="color:#F26039;font-size:15px;"><a href="/discounturi-24" style="color:#ff0000;font-size:15px;" <em="">» <em>Cauti un produs anume si nu l-ai gasit pe site?  Suna-ne!</em></a></div>

</div>


<div style="padding:10px;font-size:12px;line-height:24px;font-family: Trebuchet ms;font-weight: bold;margin-top:50px;" >

<table>

  <tbody><tr>

    <td width="85%">

      <span style="color:#f7c61d;font-size:18px; font-weight:bold;font-family:trebuchet ms;">Comanda cu incredere de pe SaMargelim.ro!</span><br><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Comanda in siguranta!</span> Comenzile tale pe SaMargelim.ro sunt sigure.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Rapiditate.</span> Coletul pleaca spre tine in ziua comenzii*. Te sunam inainte!</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#garantie" style="color:#636363;"><span style="color: #8dca07;">Garantie.</span> Vei fi complet satisfacuta de alegerea facuta.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#returnare" style="color:#636363;"><span style="color: #8dca07;">Politica de returnare.</span> Daca nu esti multumita, poti returna produsele.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/politica-de-confidentialitate-26" style="color:#636363;"><span style="color: #8dca07;">Confidentialitate.</span> Respectam confidentialitatea datelor tale personale.</a>

    </td>

    <td><br><br><img src="/themes/jms_freshshop/assets/img/garantat.jpg"></td>

  </tr>

</tbody></table>

</div>



</div>

<?php } else { ?>
<style>

    .order-f,#cart-points-summary{ display:none; }
    .showit{display:block !important;}

</style>
<a href="/" style="color:#fff;background: #FFEB3B;padding: 10px;" class="noshow">Intoarce-te in magazin</a>
<div style="padding:10px;font-size:12px;line-height:24px;font-family: Trebuchet ms;font-weight: bold;margin-top:50px;" class="showit">

<table>

  <tbody><tr>

    <td width="85%">

      <span style="color:#f7c61d;font-size:18px; font-weight:bold;font-family:trebuchet ms;">Comanda cu incredere de pe SaMargelim.ro!</span><br><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Comanda in siguranta!</span> Comenzile tale pe SaMargelim.ro sunt sigure.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Rapiditate.</span> Coletul pleaca spre tine in ziua comenzii*. Te sunam inainte!</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#garantie" style="color:#636363;"><span style="color: #8dca07;">Garantie.</span> Vei fi complet satisfacuta de alegerea facuta.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#returnare" style="color:#636363;"><span style="color: #8dca07;">Politica de returnare.</span> Daca nu esti multumita, poti returna produsele.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/politica-de-confidentialitate-26" style="color:#636363;"><span style="color: #8dca07;">Confidentialitate.</span> Respectam confidentialitatea datelor tale personale.</a>

    </td>

    <td><br><br><img src="/themes/jms_freshshop/assets/img/garantat.jpg"></td>

  </tr>

</tbody></table>

</div>
<?php }?> <?php }} ?>
