<?php /* Smarty version Smarty-3.1.19, created on 2020-04-22 08:50:06
         compiled from "/home/margele/public_html/themes/jms_freshshop/modules/jmsadvsearch/views/templates/hook/jmsadvsearch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3336850975e9fdb0ecce8c4-08631945%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d3e85c6f12f3acd0f627b06c0b06c4754d90ee8' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/modules/jmsadvsearch/views/templates/hook/jmsadvsearch.tpl',
      1 => 1525600552,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3336850975e9fdb0ecce8c4-08631945',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'jmsCategTree' => 0,
    'child' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5e9fdb0eced9d4_55685343',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5e9fdb0eced9d4_55685343')) {function content_5e9fdb0eced9d4_55685343($_smarty_tpl) {?>
<div class="jms-advsearch">
	<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('search');?>
" class="input-group" id="searchbox">
		<div class="input-group-addon icon-drop-down advsearch_categories">
			<select name="id_category" id="selector_cat" class="">
			<option value="0"><?php echo smartyTranslate(array('s'=>'All Category','mod'=>'jmsadvsearch'),$_smarty_tpl);?>
</option>
				<?php  $_smarty_tpl->tpl_vars['child'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['child']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['jmsCategTree']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['child']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['child']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['child']->key => $_smarty_tpl->tpl_vars['child']->value) {
$_smarty_tpl->tpl_vars['child']->_loop = true;
 $_smarty_tpl->tpl_vars['child']->iteration++;
 $_smarty_tpl->tpl_vars['child']->last = $_smarty_tpl->tpl_vars['child']->iteration === $_smarty_tpl->tpl_vars['child']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['jmsCategTree']['last'] = $_smarty_tpl->tpl_vars['child']->last;
?>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['jmsCategTree']['last']) {?>
						<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['branche_tpl_path']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('node'=>$_smarty_tpl->tpl_vars['child']->value,'last'=>'true'), 0);?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['branche_tpl_path']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('node'=>$_smarty_tpl->tpl_vars['child']->value), 0);?>

					<?php }?>
				<?php } ?>
			</select>
		</div>
			<input type="hidden" name="fc" value="module" />
			<input type="hidden" name="module" value="jmsadvsearch" />
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="order" value="product.position.asc" />			
		<div class="input-group keyword-group jms_ajax_search">
			<input type="text" id="ajax_advsearch" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Type your product name you want to buy ','mod'=>'jmsadvsearch'),$_smarty_tpl);?>
" class="input-search" />
			<span class="input-group-addon input-group-search btn_advsearch">
				<button class="">search</button>
			</span>
		</div>
	</form>
	<div id="advsearch_result">
	</div>
</div>
<?php }} ?>
