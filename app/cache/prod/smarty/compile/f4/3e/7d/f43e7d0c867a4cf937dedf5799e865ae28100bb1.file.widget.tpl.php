<?php /* Smarty version Smarty-3.1.19, created on 2020-05-17 04:37:10
         compiled from "/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/front/product/widget.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7797536345ec09546e6f448-17177472%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f43e7d0c867a4cf937dedf5799e865ae28100bb1' => 
    array (
      0 => '/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/front/product/widget.tpl',
      1 => 1538930614,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7797536345ec09546e6f448-17177472',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'points' => 0,
    'points_money_value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ec09546eb4c66_33821548',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec09546eb4c66_33821548')) {function content_5ec09546eb4c66_33821548($_smarty_tpl) {?>

<div id="lrp-product-widget" class="card" style="padding: 20px;">
	<?php echo smartyTranslate(array('s'=>"Earn [1]%d points[/1] when you buy me!",'sprintf'=>array($_smarty_tpl->tpl_vars['points']->value),'tags'=>array('<strong>'),'mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
<br>
	<?php echo smartyTranslate(array('s'=>"That's worth [1]%s[/1]",'sprintf'=>array($_smarty_tpl->tpl_vars['points_money_value']->value),'tags'=>array('<strong>'),'mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
<br>
	<i class="material-icons">card_giftcard</i>
</div><?php }} ?>
