<?php /* Smarty version Smarty-3.1.19, created on 2020-05-13 22:04:20
         compiled from "/home/margele/public_html/themes/jms_freshshop/templates/customer/_partials/customer-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15343055865ebc44b44dd770-15893349%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfa65e8897c8c8402c8502eaddfeddc68ba7f819' => 
    array (
      0 => '/home/margele/public_html/themes/jms_freshshop/templates/customer/_partials/customer-form.tpl',
      1 => 1568321250,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15343055865ebc44b44dd770-15893349',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'errors' => 0,
    'action' => 0,
    'formFields' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ebc44b44fa717_48598511',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ebc44b44fa717_48598511')) {function content_5ebc44b44fa717_48598511($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ('_partials/form-errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('errors'=>$_smarty_tpl->tpl_vars['errors']->value['']), 0);?>


<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');?>
" id="customer-form" class="js-customer-form" method="post" style="display:none">
  <section>
    
      <?php  $_smarty_tpl->tpl_vars["field"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["field"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['formFields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["field"]->key => $_smarty_tpl->tpl_vars["field"]->value) {
$_smarty_tpl->tpl_vars["field"]->_loop = true;
?>
        
          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['form_field'][0][0]->smartyFormField(array('field'=>$_smarty_tpl->tpl_vars['field']->value),$_smarty_tpl);?>

        
      <?php } ?>
    
  </section>

  <footer class="form-footer clearfix">
    <input type="hidden" name="submitCreate" value="1">
    
      <button class="btn btn-primary form-control-submit pull-xs-right" data-link-action="save-customer" type="submit">
        <?php echo smartyTranslate(array('s'=>'Save','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>

      </button>
    
  </footer>

</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div id="nowayss">



<?php if ($_GET['pass']=="change_pass") {?>
<style>
label.inputLabel {
    width: 12em;
    float: left;
    color: #9cb159;
    font-size: 11px;
    padding:5px 0 0 0;
}
.accountInput {
    width: 170px;
    margin: 0;
    background: #f6e12f;
    border: 1px solid #000 !important;
}
</style>
<script type="text/Javascript">
$(document).ready(function($){
    $('[name="psgdpr"]').prop('checked',true);
    $('#samargelim_pass1').on('change',function(){ $('.form-control[name="password"]').val($('#samargelim_pass1').val()); });
    $('#samargelim_pass2').on('change',function(){ $('.form-control[name="new_password"]').val($('#samargelim_pass2').val()); });
    $('.savethecontactinfo').click(function(){
        if (jQuery('#samargelim_pass1').val()==""||jQuery('#samargelim_pass2').val()==""||jQuery('#samargelim_pass3').val()==""){
            alert('Toate campurile sunt obligatorii!');
            return;
        }else{
            if (jQuery('#samargelim_pass2').val()!=jQuery('#samargelim_pass3').val()){
                alert('Cele doua parole noi trebuie sa coincida!');
                return;
            } else {
                $('#customer-form').submit();
            }
        }
    });
});
</script>
<h4>Schimba parola</h4>
<label class="inputLabel" for="samargelim_pass">Parola veche *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass1" id="samargelim_pass1" value="" class="accountInput" autocomplete="old-password1"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<label class="inputLabel" for="samargelim_pass">Parola noua *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass2" id="samargelim_pass2" value="" class="accountInput" autocomplete="old-password2"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<label class="inputLabel" for="samargelim_pass">Repeta parola noua *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass3" id="samargelim_pass3" value="" class="accountInput" autocomplete="old-password3"></div><div class="input_wrap_e"></div>
<br class="clearBoth">


<br class="clearBoth">
<div class="buttonUpdate back btnAcc"><input type="button" class="savethecontactinfo"></div>
</fieldset>

<?php } else { ?>

<style>
label.inputLabel {
    width: 12em;
    float: left;
    color: #9cb159;
    font-size: 11px;
    padding:5px 0 0 0;
}
.accountInput {
    width: 170px;
    margin: 0;
    background: #f6e12f;
    border: 1px solid #000 !important;
}
select{
    background: #f6e12f;
    border: 1px solid #000 !important;
    width: 170px;
    margin: 0;
    padding: 5px;
}
</style>

<fieldset>


<label class="inputLabel" for="firstname">Prenume *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text"  id="samargelim_firstname" class="accountInput"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<label class="inputLabel" for="lastname">Nume *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" id="samargelim_lastname" class="accountInput"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<label class="inputLabel" for="email-address">Adresa de mail *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="email_address" id="samargelim_email_address" class="accountInput"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<div class="parolele">
<label class="inputLabel" for="pass1">Parola *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass1" id="samargelim_pass1" value="" class="accountInput" autocomplete="new-password"></div><div class="input_wrap_e"></div>
<br class="clearBoth">

<label class="inputLabel" for="pass2">Confirma parola *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass2" id="samargelim_pass2" value="" class="accountInput" autocomplete="new-password"></div><div class="input_wrap_e"></div>
<br class="clearBoth">
</div>

<label class="inputLabel" for="dob">Data nasterii *</label>
<select name="dob_day" id="samargelim_dob_day">
  <option >zi</option>
  <option value="01">01</option>
  <option value="02">02</option>
  <option value="03">03</option>
  <option value="04">04</option>
  <option value="05">05</option>
  <option value="06">06</option>
  <option value="07">07</option>
  <option value="08">08</option>
  <option value="09">09</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
  <option value="13">13</option>
  <option value="14" >14</option>
  <option value="15">15</option>
  <option value="16">16</option>
  <option value="17">17</option>
  <option value="18">18</option>
  <option value="19">19</option>
  <option value="20">20</option>
  <option value="21">21</option>
  <option value="22">22</option>
  <option value="23">23</option>
  <option value="24">24</option>
  <option value="25">25</option>
  <option value="26">26</option>
  <option value="27">27</option>
  <option value="28">28</option>
  <option value="29">29</option>
  <option value="30">30</option>
  <option value="31">31</option>
</select>
<select name="dob_month" id="samargelim_dob_month">
  <option >luna</option>
  <option value="01">ianuarie</option>
  <option value="02">februarie</option>
  <option value="03">martie</option>
  <option value="04" >aprilie</option>
  <option value="05">mai</option>
  <option value="06">iunie</option>
  <option value="07">iulie</option>
  <option value="08">august</option>
  <option value="09">septembrie</option>
  <option value="10">octombrie</option>
  <option value="11">noiembrie</option>
  <option value="12">decembrie</option>
</select>
<select name="dob_year" id="samargelim_dob_year">
  <option >an</option>
  <option value="1940">1940</option>
  <option value="1941">1941</option>
  <option value="1942">1942</option>
  <option value="1943">1943</option>
  <option value="1944">1944</option>
  <option value="1945">1945</option>
  <option value="1946">1946</option>
  <option value="1947">1947</option>
  <option value="1948">1948</option>
  <option value="1949">1949</option>
  <option value="1950">1950</option>
  <option value="1951">1951</option>
  <option value="1952">1952</option>
  <option value="1953">1953</option>
  <option value="1954">1954</option>
  <option value="1955">1955</option>
  <option value="1956">1956</option>
  <option value="1957">1957</option>
  <option value="1958">1958</option>
  <option value="1959">1959</option>
  <option value="1960">1960</option>
  <option value="1961">1961</option>
  <option value="1962">1962</option>
  <option value="1963">1963</option>
  <option value="1964">1964</option>
  <option value="1965">1965</option>
  <option value="1966">1966</option>
  <option value="1967">1967</option>
  <option value="1968">1968</option>
  <option value="1969">1969</option>
  <option value="1970">1970</option>
  <option value="1971">1971</option>
  <option value="1972">1972</option>
  <option value="1973">1973</option>
  <option value="1974">1974</option>
  <option value="1975">1975</option>
  <option value="1976">1976</option>
  <option value="1977">1977</option>
  <option value="1978">1978</option>
  <option value="1979">1979</option>
  <option value="1980">1980</option>
  <option value="1981">1981</option>
  <option value="1982">1982</option>
  <option value="1983">1983</option>
  <option value="1984">1984</option>
  <option value="1985">1985</option>
  <option value="1986">1986</option>
  <option value="1987">1987</option>
  <option value="1988">1988</option>
  <option value="1989">1989</option>
  <option value="1990">1990</option>
  <option value="1991">1991</option>
  <option value="1992">1992</option>
  <option value="1993">1993</option>
  <option value="1994">1994</option>
  <option value="1995">1995</option>
  <option value="1996">1996</option>
  <option value="1997">1997</option>
  <option value="1998">1998</option>
  <option value="1999">1999</option>
  <option value="2000">2000</option>
  <option value="2001">2001</option>
  <option value="2002">2002</option>
  <option value="2003">2003</option>
  <option value="2004">2004</option>
  <option value="2005">2005</option>
  <option value="2006">2006</option>
  <option value="2007">2007</option>
  <option value="2008">2008</option>
  <option value="2009">2009</option>
  <option value="2010">2010</option>
  <option value="2011">2011</option>
  <option value="2012">2012</option>
  <option value="2013">2013</option>
  <option value="2014">2014</option>
  <option value="2015">2015</option>
  <option value="2016">2016</option>
  <option value="2017">2017</option>
  <option value="2018">2018</option>
</select>
<div class="pull_space"></div><br class="clearBoth">

<input type="checkbox" name="newsletter" value="1" id="samargelim_newsletter_checkbox"><label class="checkboxLabel" for="samargelim_newsletter_checkbox">Doresc sa aflu ocazional noutatile de la Samargelim.ro</label><input type="hidden" name="email_format"><br class="clearBoth">


<!--Factura-->
<div class="thefactura">
<input type="checkbox" id="factura" name="factura" onchange="displ();">
<label class="checkboxLabel" for="factura">Doresc factura pe firma</label>
<br class="clearBoth">


<div id="hiddfields" style="padding: 0px 50px; display: none;">
<label class="inputLabel" for="camp1">Cumparator*</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp1" id="camp1" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp2">Nr. Ord. Reg. Com.*</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp2" id="camp2" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp3">AF / CUI*</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp3" id="camp3" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp4">Sediul*</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp4" id="camp4" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp5">Judet*</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp5" id="camp5" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp6">Cont</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp6" id="camp6" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
<label class="inputLabel" for="camp7">Banca</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp7" id="camp7" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
<br class="clearBoth">
</div>
</div>

<!--Factura-->

<input type="checkbox" name="iagree" value="1" id="iagree"><label class="checkboxLabel" for="iagree">Sunt de acord cu termenii si conditiile siteului Samargelim.ro. </label><input type="hidden" name="email_format"><br class="clearBoth">
<div id="afteriagree"></div>


<br><br><br>

<div class="parolatosave">
<h5>Pentru a modifica datele contului tau, trebuie sa introduci parola!</h5>
<label class="inputLabel" for="lastname">Parola *</label>
<div class="input_wrap_b"></div><div class="input_wrap"><input type="password" name="samargelim_pass" id="samargelim_pass" value="" class="accountInput" autocomplete="new-password"></div><div class="input_wrap_e"></div>
<br class="clearBoth">
</div>



<br class="clearBoth">
<div class="buttonUpdate back btnAcc"><input type="button" class="savethecontactinfo"></div>
</fieldset>
<?php }?>

</div>

<script>
$(document).ready(function($){
    
if ($('.help-block').length){
    var errorstring = $('.help-block').html();
    if (errorstring.indexOf('este deja utilizat') !== -1){
        alert('Adresa de email este folosita! Alege alta!');
    }
}
    
    if (location.search.includes("create_account=1")){  //create account
            $('.parolatosave').remove();
            $('#afteriagree').html('<button class="btn btn-primary form-control-submit pull-xs-right" data-link-action="save-customer" id="creaza-cont-user" type="submit">Salveaza</button>');
            $('.savethecontactinfo').remove();
    
  $('#samargelim_firstname').on('change',function(){ $('.form-control[name="firstname"]').val($('#samargelim_firstname').val()); });
  $('#samargelim_lastname').on('change',function(){ $('.form-control[name="lastname"]').val($('#samargelim_lastname').val()); });
  $('#samargelim_email_address').on('change',function(){ $('.form-control[name="email"]').val($('#samargelim_email_address').val()); });
  $('#samargelim_pass1').on('change',function(){ $('.form-control[name="password"]').val($('#samargelim_pass1').val()); });
  $('#samargelim_dob_day,#samargelim_dob_month,#samargelim_dob_year').on('change',function(){
      let coo=$('#samargelim_dob_year').val()+'-'+$('#samargelim_dob_month').val()+'-'+$('#samargelim_dob_day').val();
      $('.form-control[name="birthday"]').val(coo);
  });  
  $('#samargelim_newsletter_checkbox').on('change',function(){
      if($(this).is(':checked')){
          $('[name="newsletter"]').prop('checked',true);
      }else{
          $('[name="newsletter"]').prop('checked',false); 
      }
  });
  $('#iagree').on('change',function(){
      if($(this).is(':checked')){
          $('[name="psgdpr"]').prop('checked',true);
      }else{
          $('[name="psgdpr"]').prop('checked',false); 
      }
  });  
            
    var smerror=false;        
    $('#creaza-cont-user').click(function(){
    if ( $('#samargelim_firstname').val() == "") { alert('Prenumele este obligatoriu!');smerror=true;}
else if ( $('#samargelim_lastname').val() == "") { alert('Numele este obligatoriu!');smerror=true;}
else if ( $('#samargelim_email_address').val() == "") { alert('Adresa de email este obligatorie!');smerror=true;}

else if ( !validateEmail($('#samargelim_email_address').val())) { alert('Adresa de email nu este valida!');smerror=true;}
else if ( $('#samargelim_pass1').val() == "") { alert('Parola este obligatorie');smerror=true;}
else if ( $('#samargelim_pass2').val() == "") { alert('Confirma parola!');smerror=true;}
else if ( $('#samargelim_pass1').val() != $('#samargelim_pass2').val()) { alert('Parolele nu coincid!');smerror=true;}
else if ( $('#samargelim_dob_year option:selected').text() == "an" || $('#samargelim_dob_month option:selected' ).text() == "luna" || $('#samargelim_dob_day option:selected' ).text() == "zi") { alert('Data de nastere este obligatorie!');smerror=true;} 
else if( $('#factura').is(':checked') ){
   if(jQuery('#camp1').val()==""||jQuery('#camp2').val()==""||jQuery('#camp3').val()==""||jQuery('#camp4').val()==""||jQuery('#camp5').val()==""){
       alert('Completati campurile obligatorii de facturare!');
       smerror=true;
   }
}
else if (!$('#iagree').prop('checked')) { alert('Trebuie sa fii de acord cu termenii si conditiile Samargelim.ro!');smerror=true;}
else { smerror=false;}
if (smerror === false){
       $('#customer-form').submit();
}
});

    } else {
        
        $('.parolele').remove();
        //not 
    $('#samargelim_firstname').val($('.form-control[name="firstname"]').val());
    $('#samargelim_lastname').val($('.form-control[name="lastname"]').val());
    if ($('.form-control[name="birthday"]').val()!=""){
        var dob=$('.form-control[name="birthday"]').val().split('-');
        $('#samargelim_dob_day option[value="' + dob[2] + '"]').prop('selected',true);
        $('#samargelim_dob_month option[value="' + dob[1] + '"]').prop('selected',true); 
        $('#samargelim_dob_year option[value="' + dob[0] + '"]').prop('selected',true); 
    }
    $('#samargelim_email_address').val($('.form-control[name="email"]').val());
    if ($('[name="newsletter"]').is(':checked')){
        $('#samargelim_newsletter_checkbox').prop('checked',true);
    }
    $('[name="psgdpr"]').prop('checked',true);
    
    //make jquery call to get the factura
    $.ajax({
        url:"/printareposta/check_factura_adresa.php",
        method:"POST",
        data:'userid='+$('input[name="id_customer"]').val()+'&req=get',
        success:function(data){
            var obiect = JSON.parse(data);
            if (obiect.factura==1){
                $('#factura').prop('checked',true);
                $('#camp1').val(obiect.buyer);
                $('#camp2').val(obiect.nrreg); 
                $('#camp3').val(obiect.cui); 
                $('#camp4').val(obiect.sediu); 
                $('#camp5').val(obiect.judet); 
                $('#camp6').val(obiect.cont); 
                $('#camp7').val(obiect.banca); 
                $('#hiddfields').show();
            } else{
                $('#hiddfields').hide();
            }
        }
  });
  
  $('#samargelim_firstname').on('change',function(){ $('.form-control[name="firstname"]').val($('#samargelim_firstname').val()); });
  $('#samargelim_lastname').on('change',function(){ $('.form-control[name="lastname"]').val($('#samargelim_lastname').val()); });
  $('#samargelim_email_address').on('change',function(){ $('.form-control[name="email"]').val($('#samargelim_email_address').val()); });
  $('#samargelim_pass').on('change',function(){ $('.form-control[name="password"]').val($('#samargelim_pass').val()); });
  $('#samargelim_dob_day,#samargelim_dob_month,#samargelim_dob_year').on('change',function(){
      let coo=$('#samargelim_dob_year').val()+'-'+$('#samargelim_dob_month').val()+'-'+$('#samargelim_dob_day').val();
      $('.form-control[name="birthday"]').val(coo);
  });  
  $('#samargelim_newsletter_checkbox').on('change',function(){
      if($(this).is(':checked')){
          $('[name="newsletter"]').prop('checked',true);
      }else{
          $('[name="newsletter"]').prop('checked',false); 
      }
  });
  
  $('#iagree').on('change',function(){
      if($(this).is(':checked')){
          $('[name="psgdpr"]').prop('checked',true);
      }else{
          $('[name="psgdpr"]').prop('checked',false); 
      }
  });

$('.savethecontactinfo').click(function(){
    if( $('#factura').is(':checked') ){
       if(jQuery('#camp1').val()==""||jQuery('#camp2').val()==""||jQuery('#camp3').val()==""||jQuery('#camp4').val()==""||jQuery('#camp5').val()==""){
           alert('Completati campurile obligatorii de facturare!');
           return;
       } else {
            $.ajax({
                url:"/printareposta/check_factura_adresa.php",
                method:"POST",
                data:{
                    userid:$('input[name="id_customer"]').val(),
                    req:'save',
                    factura:1,
                    buyer:jQuery('#camp1').val(),
                    nrreg:jQuery('#camp2').val(),
                    cui:jQuery('#camp3').val(),
                    sediu:jQuery('#camp4').val(),
                    judet:jQuery('#camp5').val(),
                    cont:jQuery('#camp6').val(),
                    banca:jQuery('#camp7').val()
                },
                success:function(data){
                   $('#customer-form').submit();
                }  
            });
       }
    }else{
        $.ajax({
                url:"/printareposta/check_factura_adresa.php",
                method:"POST",
                data:{
                    userid:$('input[name="id_customer"]').val(),
                    req:'save',
                    factura:0,
                    buyer:jQuery('#camp1').val(),
                    nrreg:jQuery('#camp2').val(),
                    cui:jQuery('#camp3').val(),
                    sediu:jQuery('#camp4').val(),
                    judet:jQuery('#camp5').val(),
                    cont:jQuery('#camp6').val(),
                    banca:jQuery('#camp7').val()
                },
                success:function(data){
                   $('#customer-form').submit();
                }  
            });
    } 
});

        
}
    

function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

});

function displ(){
    var xx=document.getElementById("factura");
    var ele = document.getElementById("hiddfields");
    if (xx.checked == 1) {
       ele.style.display= "block";
       } else {
       ele.style.display= "none";       
              }     
}
</script>

<?php }} ?>
