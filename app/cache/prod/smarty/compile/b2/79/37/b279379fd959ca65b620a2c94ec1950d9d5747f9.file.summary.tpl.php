<?php /* Smarty version Smarty-3.1.19, created on 2020-05-17 04:37:21
         compiled from "/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/order/summary.tpl" */ ?>
<?php /*%%SmartyHeaderCode:358499695ec0955136c823-68446499%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b279379fd959ca65b620a2c94ec1950d9d5747f9' => 
    array (
      0 => '/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/order/summary.tpl',
      1 => 1538930614,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '358499695ec0955136c823-68446499',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'redeemed_points' => 0,
    'redeemed_value' => 0,
    'redeemed_point_value' => 0,
    'rewarded_points' => 0,
    'rewarded_point_value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ec09551385721_22675141',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec09551385721_22675141')) {function content_5ec09551385721_22675141($_smarty_tpl) {?>

<div id="lrp-admin-order-summary-panel" class="panel">
	<div class="panel-heading">
    	<i class="icon-money"></i>
    	<?php echo smartyTranslate(array('s'=>'Loyalty Reward Points','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>

    </div>

	<h4><?php echo smartyTranslate(array('s'=>'Redeemed','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</h4>
	<dl class="well list-detail <?php if ($_smarty_tpl->tpl_vars['redeemed_points']->value==0) {?>disabled<?php }?>">
		<div class="lrp-box">
			<dt><?php echo smartyTranslate(array('s'=>'Points Redeemed','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</dt>
			<dd><span class="badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['redeemed_points']->value,'htmlall','UTF-8');?>
</span></dd>
		</div>

		<div class="lrp-box">
			<dt><?php echo smartyTranslate(array('s'=>'Discount','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</dt>
			<dd><span class="badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['redeemed_value']->value,'htmlall','UTF-8');?>
</span></dd>
		</div>

		<div class="lrp-box">
			<dt><?php echo smartyTranslate(array('s'=>'Point Value','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</dt>
			<dd><span class="badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['redeemed_point_value']->value,'htmlall','UTF-8');?>
</span></dd>
		</div>
	</dl>

	<h4><?php echo smartyTranslate(array('s'=>'Rewarded','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</h4>
	<dl class="well list-detail">
		<div class="lrp-box">
			<dt><?php echo smartyTranslate(array('s'=>'Points Rewarded','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</dt>
			<dd><span class="badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['rewarded_points']->value,'htmlall','UTF-8');?>
</span></dd>
		</div>

		<div class="lrp-box">
			<dt><?php echo smartyTranslate(array('s'=>'Point Value','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</dt>
			<dd><span class="badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['rewarded_point_value']->value,'htmlall','UTF-8');?>
</span></dd>
		</div>
	</dl>
</div><?php }} ?>
