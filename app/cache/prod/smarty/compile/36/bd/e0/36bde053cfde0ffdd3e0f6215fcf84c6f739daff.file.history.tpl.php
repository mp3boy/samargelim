<?php /* Smarty version Smarty-3.1.19, created on 2020-05-17 02:18:30
         compiled from "/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/customer/history.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3742347545ec074c62975f5-12823781%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36bde053cfde0ffdd3e0f6215fcf84c6f739daff' => 
    array (
      0 => '/home/margele/public_html/modules/loyaltyrewardpoints/views/templates/admin/customer/history.tpl',
      1 => 1543356663,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3742347545ec074c62975f5-12823781',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'points' => 0,
    'currencies' => 0,
    'currency' => 0,
    'id_customer' => 0,
    'module_config_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ec074c62a4888_45294780',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec074c62a4888_45294780')) {function content_5ec074c62a4888_45294780($_smarty_tpl) {?>

<div class="col-lg-6">
	<div id="lrp-customer-history" class="panel">
		<div class="panel-heading">
			<i class="icon-eye"></i>
			<?php echo smartyTranslate(array('s'=>'Loyalty Reward Points','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>

		</div>

		<div>
			<strong><?php echo smartyTranslate(array('s'=>'Customer Points','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
 : <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['points']->value,'htmlall','UTF-8');?>
</strong>
			<form id="lrp-customer-points-form">
				<select id="type" name="type" style="width: 100px; float: left;">
					<option value="add"><?php echo smartyTranslate(array('s'=>'Add','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</option>
					<option value="subtract"><?php echo smartyTranslate(array('s'=>'Subtract','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</option>
				</select>

				<select id="currency_iso_code" name="currency_iso_code" style="width: 100px; float: left;">
					<?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value) {
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
						<option value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['currency']->value['iso_code'],'htmlall','UTF-8');?>
"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['currency']->value['iso_code'],'htmlall','UTF-8');?>
</option>
					<?php } ?>
				</select>

				<div class="input-group" style="width: 280px; float: left; margin-left: 10px;">
					<input type="text" id="points" name="points" value="0" size="20">
					<span class="input-group-addon"><?php echo smartyTranslate(array('s'=>'points','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>
</span>
					<button type="submit" id="btn-lrp-update" class="btn btn-default pull-right" style="margin-left: 10px;">
						<i class="icon-save"></i>
						<?php echo smartyTranslate(array('s'=>'Update','mod'=>'loyaltyrewardpoints'),$_smarty_tpl);?>

					</button>
				</div>

			</form>
		</div>
		<div style="clear:both"></div>

		<div id="lrp-history-list">

		</div>

	</div>
</div>

<script>
	id_customer = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['id_customer']->value,'htmlall','UTF-8');?>
;
	module_config_url = '<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_config_url']->value,'quotes','UTF-8');?>
';
</script><?php }} ?>
