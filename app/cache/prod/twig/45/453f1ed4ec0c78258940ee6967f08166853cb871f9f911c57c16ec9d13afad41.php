<?php

/* __string_template__626b3f434c46cecc58efa88db20e728d68cf2685bfd410bd301d9dad51a58e2a */
class __TwigTemplate_900302ec0a625d2edd50b86ccdf1853fb4c12516f870ed315b18901bd2e32977 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ro\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produse • Samargelim</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'ro';
    var lang_is_rtl = '0';
    var full_language_code = 'ro-ro';
    var full_cldr_language_code = 'ro-RO';
    var country_iso_code = 'RO';
    var _PS_VERSION_ = '1.7.3.2';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'A fst plasata o comanda noua in magazin.';
    var order_number_msg = 'Numarul comenzii: ';
    var total_msg = 'Total: ';
    var from_msg = 'Din ';
    var see_order_msg = 'Vezi comanda';
    var new_customer_msg = 'S-a inregistrat un client nou in magazin.';
    var customer_name_msg = 'Numele clientului: ';
    var new_msg = 'A fost postat un mesaj nou in magazin.';
    var see_msg = 'Citire mesaj';
    var token = '1d8f0c5945b7700fb56b389f1b524a6a';
    var token_admin_orders = 'e34450487a1938169c961ac38afc54e9';
    var token_admin_customers = '128b2821f4f2d688f9014fbbc116a3be';
    var token_admin_customer_threads = 'a173095b964349e5e6c08fb597595bda';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = 'ae6a16a529b2abead8fb02388789a197';
    var choose_language_translate = 'Alege limba';
    var default_language = '1';
    var admin_modules_link = '/admin540adq7um/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Actualizare reusita';
    var errorLogin = 'PrestaShop nu putut efectua autentificarea in Addons. Te rugam sa verifici datele de autentificare si conexiunea la internet.';
    var search_product_msg = 'Cauta un produs';
  </script>

      <link href=\"/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin540adq7um/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin540adq7um/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  \t<link href=\"/admin540adq7um/themes/default/css/overrides.css\" rel=\"stylesheet\" type=\"text/css\"/>
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin540adq7um\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"RON\",\"sign\":\"RON\",\"name\":\"leu rom\\u00e2nesc\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/admin540adq7um/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/admin540adq7um/themes/default/public/custom.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.3.2\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.3.2\"></script>
<script type=\"text/javascript\" src=\"/admin540adq7um/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin540adq7um/themes/default/js/vendor/nv.d3.min.js\"></script>


  <script>
\t\t\t\tvar ids_ps_advice = new Array();
\t\t\t\tvar admin_gamification_ajax_url = 'http://samargelim.vip/admin540adq7um/index.php?controller=AdminGamification&token=bad979ff0fc6c1ee52511239f722cbd2';
\t\t\t\tvar current_id_tab = 10;
\t\t\t</script>

";
        // line 84
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-ro adminproducts\">



<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

    
    

    
    <i class=\"material-icons float-left px-1 js-mobile-menu d-md-none\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminDashboard&amp;token=0d26881d9cfe6163c55fe20448e14f5d\"></a>

    <div class=\"component d-none d-md-flex\" id=\"quick-access-container\">
    
    <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acces rapid
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCategories&amp;addcategory&amp;token=7cc7c8ec56aab11ef7e583fd78f946aa\"
                 data-item=\"Categorie noua\"
      >Categorie noua</a>
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminOrders&amp;token=e34450487a1938169c961ac38afc54e9\"
                 data-item=\"Comenzi\"
      >Comenzi</a>
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=837981da56cda8bf39095d717d7a1edc\"
                 data-item=\"Cupon nou\"
      >Cupon nou</a>
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=bc9c313fee5a2430f4517ce549882e69\"
                 data-item=\"Evaluarea catalogului\"
      >Evaluarea catalogului</a>
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php/module/manage?token=452cb869a4c816e7ab7fef0481009371\"
                 data-item=\"Module instalate\"
      >Module instalate</a>
          <a class=\"dropdown-item\"
         href=\"http://samargelim.vip/admin540adq7um/index.php/product/new?token=452cb869a4c816e7ab7fef0481009371\"
                 data-item=\"Produs nou\"
      >Produs nou</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"90\"
        data-icon=\"icon-AdminCatalog\"
        data-method=\"add\"
        data-url=\"index.php/product/catalog/0/20/position/asc\"
        data-post-link=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminQuickAccesses&token=ddfa660f18551fc5adbc510fe48eb5d5\"
        data-prompt-text=\"Te rog numeste aceasta scurtatura:\"
        data-link=\"Produse - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Adauga pagina curenta la lista de Acces rapid
      </a>
        <a class=\"dropdown-item\" href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminQuickAccesses&token=ddfa660f18551fc5adbc510fe48eb5d5\">
      <i class=\"material-icons\">settings</i>
      Administreaza linkurile de Acces Rapid
    </a>
  </div>
</div>


    </div>
    <div class=\"component d-none d-md-inline-block col-md-4\" id=\"header-search-container\">
<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/admin540adq7um/index.php?controller=AdminSearch&amp;token=0e1a9e4785d1e271cfe0502c82a13098\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Cauta (de exemplu, referinta produsului, nume client...)\">
    <div class=\"input-group-btn\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Oriunde
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Oriunde\" href=\"#\" data-value=\"0\" data-placeholder=\"Ce anume cautati?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Oriunde</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catalog de produse\" href=\"#\" data-value=\"1\" data-placeholder=\"Numele produsului, unitatea de stoc (SKU), referinta...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catalog de produse</a>
        <a class=\"dropdown-item\" data-item=\"Clienti dupa nume\" href=\"#\" data-value=\"2\" data-placeholder=\"E-mail, nume...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clienti dupa nume</a>
        <a class=\"dropdown-item\" data-item=\"Clienti dupa adresa ip\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clienti dupa adresa IP</a>
        <a class=\"dropdown-item\" data-item=\"Comenzi\" href=\"#\" data-value=\"3\" data-placeholder=\"ID comanda\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Comenzi</a>
        <a class=\"dropdown-item\" data-item=\"Facturi\" href=\"#\" data-value=\"4\" data-placeholder=\"Numar factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Facturi</a>
        <a class=\"dropdown-item\" data-item=\"Cosuri\" href=\"#\" data-value=\"5\" data-placeholder=\"ID cos\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Cosuri</a>
        <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Nume modul\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Module</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">CAUTA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>


<div class=\"intopmenu\">
  <a href=\"?controller=AdminCititor\">Cititor</a>
  <div>Gestiune
    <ul style=\"\">
      <li><a href=\"/admin540adq7um/index.php?controller=Adminfurnizori\">Furnizori</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminNiruri\">NIR-uri</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminNirnou\">Deschide NIR nou</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminFacturichitante\">Facturi si chitante</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminSetaridocumente\">Setari documente</a></li>
    </ul>
  </div>
  <div>Reports
    <ul style=\"\">
      <li><a href=\"/admin540adq7um/index.php/product/catalog/0/20/sav_quantity/asc\">Product Low Stock</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminRaportstocuri\">Raport Stocuri</a></li>
    </ul>
  </div>
  <!-- <div>Rectificare
    <ul style=\"\">
      <li><a href=\"http://www.samargelim.ro/admiandr31ut/furnizori.php\">Rectificare</a></li>
      <li><a href=\"http://www.samargelim.ro/admiandr31ut/lista_nir.php\">Rectificare 2</a></li>
    </ul>
  </div> -->
  <div>Posta
    <ul style=\"\">
      <li><a href=\"/admin540adq7um/index.php?controller=AdminBorderoucache\">Borderou Cache</a></li>
      <li><a href=\"/admin540adq7um/index.php?controller=AdminBorderoucont\">Borderou Cont</a></li>
    </ul>
  </div>
</div>

</div>

            <div class=\"component d-none d-md-inline-block\">  <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://samargelim.vip/\" target= \"_blank\">Samargelim</a>
  </div>
</div>
          <div class=\"component\"><div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <div class=\"notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Comenzi<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clienti<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mesaje<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Nici o comanda noua pentru moment :(<br>
              Ti-ai verificat <strong><a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCarts&token=2f4829bfe4ac1eab4b1b944f1b9d72e0&action=filterOnlyAbandonedCarts\">cosurile abandonate</a></strong>?<br>Urmatoarea ta comanda s-ar putea ascunde acolo!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Nici un client nou pentru moment :(<br>
              Te-ai gandit sa vinzi in marketplace-uri?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Nu sunt mesaje noi pana acum.<br>
              Nicio veste e o veste buna, nu?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de_la <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - inregistrati <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component -norightmargin d-none d-md-inline-block\"><div class=\"employee-dropdown dropdown\">
      <div class=\"person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">account_circle</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-xs-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/frincu_pety%40yahoo.com.jpg\" /><br>
      <span>Test Test</span>
    </div>
    <div>
      <a class=\"employee-link profile-link\" href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminEmployees&amp;token=ae6a16a529b2abead8fb02388789a197&amp;id_employee=1&amp;updateemployee\">
        <i class=\"material-icons\">settings_applications</i> Profilul tau
      </a>
    </div>
    <div>
      <a class=\"employee-link\" id=\"header_logout\" href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminLogin&amp;token=9254682e0204221876a8d394049f1368&amp;logout\">
        <i class=\"material-icons\">power_settings_new</i> Deconectare
      </a>
    </div>
  </div>
</div>
</div>

    
  </nav>
  </header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminDashboard&amp;token=0d26881d9cfe6163c55fe20448e14f5d\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Tablou de bord</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block -active\" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vinde</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminOrders&amp;token=e34450487a1938169c961ac38afc54e9\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i>
                    <span>
                    Comenzi
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminOrders&amp;token=e34450487a1938169c961ac38afc54e9\" class=\"link\"> Comenzi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminInvoices&amp;token=80f58b2e51c3b158dc08a4bf284f1a70\" class=\"link\"> Facturi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminSlip&amp;token=cb050a2ef0771a136bed4c662c67f5a1\" class=\"link\"> Note de credit
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminDeliverySlip&amp;token=a90fc259e94e22c9af7fffc9738e23a8\" class=\"link\"> Avize de livrare
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCarts&amp;token=2f4829bfe4ac1eab4b1b944f1b9d72e0\" class=\"link\"> Cosuri de cumparaturi
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/admin540adq7um/index.php/product/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\">
                    <i class=\"material-icons\">store</i>
                    <span>
                    Catalog de produse
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/admin540adq7um/index.php/product/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Produse
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCategories&amp;token=7cc7c8ec56aab11ef7e583fd78f946aa\" class=\"link\"> Categorii
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminTracking&amp;token=dafded9dba1b919b80d226d8715ec9dd\" class=\"link\"> Monitorizare
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminAttributesGroups&amp;token=a14c1e68504d1641891c68668843fd5e\" class=\"link\"> Atribute si caracteristici
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminManufacturers&amp;token=d5e81be2be1e09b2968182b63c8f5826\" class=\"link\"> Marci si furnizori
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminAttachments&amp;token=1bcefc98e4eed940a65e1e6d03e1d001\" class=\"link\"> Fisiere
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCartRules&amp;token=837981da56cda8bf39095d717d7a1edc\" class=\"link\"> Reduceri
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/admin540adq7um/index.php/stock/?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCustomers&amp;token=128b2821f4f2d688f9014fbbc116a3be\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i>
                    <span>
                    Clienti
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCustomers&amp;token=128b2821f4f2d688f9014fbbc116a3be\" class=\"link\"> Clienti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminAddresses&amp;token=9b56075a19b4a0edd1f574f0fde2fd86\" class=\"link\"> Adrese
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCustomerThreads&amp;token=a173095b964349e5e6c08fb597595bda\" class=\"link\">
                    <i class=\"material-icons\">chat</i>
                    <span>
                    Asistenta pentru clienti
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCustomerThreads&amp;token=a173095b964349e5e6c08fb597595bda\" class=\"link\"> Asistenta pentru clienti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminOrderMessage&amp;token=704e4b229f8dfb76947be416c630ad0a\" class=\"link\"> Mesaje la comenzi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminReturn&amp;token=c7e0bd3d88b3bfee27b5a9448d32c856\" class=\"link\"> Returnari de produse
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminStats&amp;token=bc9c313fee5a2430f4517ce549882e69\" class=\"link\">
                    <i class=\"material-icons\">assessment</i>
                    <span>
                    Statistici
                                        </span>

                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Imbunatateste</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/admin540adq7um/index.php/module/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\">
                    <i class=\"material-icons\">extension</i>
                    <span>
                    Module
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/admin540adq7um/index.php/module/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Module si servicii
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"46\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/admin540adq7um/index.php/module/addons-store?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Catalog de module
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"139\" id=\"subtab-trustedshopsintegration\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminTrustedshopsintegrationHome&amp;token=a7ce7fb19061a3ba79637d6801254c6b\" class=\"link\"> Trusted Shops Reviews Toolkit
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"47\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminThemes&amp;token=6897317145c2c90a8850afde774d74f2\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i>
                    <span>
                    Design
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-47\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminThemes\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminThemes&amp;token=6897317145c2c90a8850afde774d74f2\" class=\"link\"> Tema si sigla
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminThemesCatalog&amp;token=2dc87edca44892ceda43b90f583e62f4\" class=\"link\"> Catalog de teme
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCmsContent&amp;token=c7913047b43bdd37c7df60e50d6b0280\" class=\"link\"> Pagini
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminModulesPositions&amp;token=9a3ea1c5afc3aed139c001dce8359eb9\" class=\"link\"> Pozitii
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminImages\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminImages&amp;token=18226faf1dccfe917c98c0e56a9086de\" class=\"link\"> Optiuni pentru imagini
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminLinkWidget&amp;token=f4ccb94e7c72843d520b424b638a5993\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"53\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCarriers&amp;token=0e26c922339353c2fb29a7ec87fe154f\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i>
                    <span>
                    Livrare
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCarriers&amp;token=0e26c922339353c2fb29a7ec87fe154f\" class=\"link\"> Transportatori
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminShipping\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminShipping&amp;token=3cc146453593c506d834a1e15ceb5eb7\" class=\"link\"> Preferinte
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"56\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPayment&amp;token=02af49f4fea8ab410a400738be94b90b\" class=\"link\">
                    <i class=\"material-icons\">payment</i>
                    <span>
                    Plata
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminPayment\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPayment&amp;token=02af49f4fea8ab410a400738be94b90b\" class=\"link\"> Modalitati de plata
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPaymentPreferences&amp;token=d85efaaba8f1fe8779466eec0d697828\" class=\"link\"> Preferinte
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"59\" id=\"subtab-AdminInternational\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminLocalization&amp;token=ac26ea2e072822eaf2467e13b03387bd\" class=\"link\">
                    <i class=\"material-icons\">language</i>
                    <span>
                    International
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminLocalization&amp;token=ac26ea2e072822eaf2467e13b03387bd\" class=\"link\"> Localizare
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminZones&amp;token=a9da803f86aeefb9f595ff1a79b99101\" class=\"link\"> Locatii
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"69\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminTaxes&amp;token=ef6aba1c8504f51db319e78c171e99ad\" class=\"link\"> Taxe
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminTranslations&amp;token=d32d2c897d916d14f6199fea41066b86\" class=\"link\"> Traduceri
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"73\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configureaza</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"74\" id=\"subtab-ShopParameters\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPreferences&amp;token=119ff96529474f7f2658382bd0f5b6be\" class=\"link\">
                    <i class=\"material-icons\">settings</i>
                    <span>
                    Parametri ai magazinului
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-74\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPreferences&amp;token=119ff96529474f7f2658382bd0f5b6be\" class=\"link\"> Parametri generali
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminOrderPreferences&amp;token=d2fa5f867dd5163f560b49c2942df99d\" class=\"link\"> Comenzi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminPPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminPPreferences&amp;token=463e2dd6b89bf26983b9d088ee72ce9e\" class=\"link\"> Produse
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCustomerPreferences&amp;token=913d557620ac1ef39f16f69d75ef3ee7\" class=\"link\"> Clienti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"86\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminContacts&amp;token=2c258d32bce8dc1195ba6f714d61ef81\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminMeta&amp;token=c42f3d454e9904076245fdbbc37549b8\" class=\"link\"> Trafic si SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminSearchConf&amp;token=915bb5084d062579ae5e2289b18feb03\" class=\"link\"> Cauta
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"119\" id=\"subtab-AdminGamification\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminGamification&amp;token=bad979ff0fc6c1ee52511239f722cbd2\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"96\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/admin540adq7um/index.php/configure/advanced/system_information?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i>
                    <span>
                    Parametri avansați
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-96\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\" id=\"subtab-AdminInformation\">
                              <a href=\"/admin540adq7um/index.php/configure/advanced/system_information?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Informatii
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\" id=\"subtab-AdminPerformance\">
                              <a href=\"/admin540adq7um/index.php/configure/advanced/performance?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\" class=\"link\"> Performanta
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminAdminPreferences&amp;token=09800c92d6097c630e3f1d5c443ebd6c\" class=\"link\"> Administrare
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminEmails\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminEmails&amp;token=1508457b035b599ac491130d4a759861\" class=\"link\"> Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminImport\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminImport&amp;token=e9b2739f19c0a824e9cee50f30153cd7\" class=\"link\"> Importa
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminEmployees&amp;token=ae6a16a529b2abead8fb02388789a197\" class=\"link\"> Angajati
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminRequestSql&amp;token=0233f0b3a97449a5008724d80096b46c\" class=\"link\"> Baza de date
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminLogs\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminLogs&amp;token=bd4e25905b35ae4681db8776dbf35a92\" class=\"link\"> Jurnale
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"110\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminWebservice&amp;token=0ed3e49a66be528298cc05a3347af4c3\" class=\"link\"> Servicii Web
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"114\" id=\"tab-DEFAULT\">
              <span class=\"title\">Mai multe</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"138\" id=\"subtab-AdminSelfUpgrade\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminSelfUpgrade&amp;token=afbbf8a425eb14d9df89cae841a556b2\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    1-Click Upgrade
                                        </span>

                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"121\" id=\"tab-JMS-MODULES\">
              <span class=\"title\">Jms Modules</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"122\" id=\"subtab-AdminJmsmegamenuDashboard\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsmegamenuManager&amp;token=77e78784d5f662d4d7dbe76a77bd5f6a\" class=\"link\">
                    <i class=\"material-icons\">menu</i>
                    <span>
                    Jms MegaMenu
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-122\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"123\" id=\"subtab-AdminJmsmegamenuManager\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsmegamenuManager&amp;token=77e78784d5f662d4d7dbe76a77bd5f6a\" class=\"link\"> Menu Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"124\" id=\"subtab-AdminJmsmegamenuStyle\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsmegamenuStyle&amp;token=123a3f6d734a906f408477e1df752329\" class=\"link\"> Menu Style
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"125\" id=\"subtab-AdminJmsblogDashboard\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsblogCategories&amp;token=874f6c29df37580d5387014189eeeb7b\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    Jms Blog
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-125\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-AdminJmsblogCategories\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsblogCategories&amp;token=874f6c29df37580d5387014189eeeb7b\" class=\"link\"> Categorii
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"127\" id=\"subtab-AdminJmsblogPost\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsblogPost&amp;token=1e112de475873ef69d5d5b7b6cdda19b\" class=\"link\"> Post
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"128\" id=\"subtab-AdminJmsblogComment\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsblogComment&amp;token=3482242ea9e16065f2bc3cbd51f262cf\" class=\"link\"> Comments
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"129\" id=\"subtab-AdminJmsblogSetting\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsblogSetting&amp;token=782d9e9f8d52a913e5fd95e3ca2dd674\" class=\"link\"> Setting
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"130\" id=\"subtab-AdminJmspagebuilderDashboard\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmspagebuilderHomepages&amp;token=c69173f23de412d30e25dc2917065fc0\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    Jms Page Builder
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-130\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"131\" id=\"subtab-AdminJmspagebuilderHomepages\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmspagebuilderHomepages&amp;token=c69173f23de412d30e25dc2917065fc0\" class=\"link\"> Home Pages
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"133\" id=\"subtab-AdminJmspagebuilderSetting\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmspagebuilderSetting&amp;token=d0c088a7228f79dd3f3fe97db80a8c89\" class=\"link\"> Setting
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"134\" id=\"subtab-AdminJmsvermegamenuDashboard\">
                  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsvermegamenuManager&amp;token=2565366a009490e7dc1f9584876132d5\" class=\"link\">
                    <i class=\"material-icons\">menu</i>
                    <span>
                    Jms Vertical MegaMenu
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-134\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"135\" id=\"subtab-AdminJmsvermegamenuManager\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsvermegamenuManager&amp;token=2565366a009490e7dc1f9584876132d5\" class=\"link\"> Menu Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"136\" id=\"subtab-AdminJmsvermegamenuStyle\">
                              <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminJmsvermegamenuStyle&amp;token=f3168aee05bd214d59c595e73d8d1d85\" class=\"link\"> Menu Style
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
            </ul>
  
  <span class=\"menu-collapse d-none d-md-block\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  
</nav>


<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">

  
    <nav class=\"breadcrumb\">

                        <a class=\"breadcrumb-item\" href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminCatalog&amp;token=680f621d53f6f9ac2d1b0bd969e83d13\">Catalog de produse</a>
              
                        <a class=\"breadcrumb-item active\" href=\"/admin540adq7um/index.php/product/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\">Produse</a>
              
    </nav>
  

  
    <h2 class=\"title\">
      Produse    </h2>
  

  
    <div class=\"toolbar-icons\">
      
                                      
          <a
            class=\"mx-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-add\"
            href=\"/admin540adq7um/index.php/product/new?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\"            title=\"Creeaza un produs nou: CTRL+P\"            data-toggle=\"pstooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">add_circle_outline</i>
            <span class=\"title\">Produs nou</span>
          </a>
                            
        <a
          class=\"toolbar-button toolbar_btn\"
          id=\"page-header-desc-configuration-modules-list\"
          href=\"/admin540adq7um/index.php/module/catalog?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\"          title=\"Module si servicii recomandate\"
                  >
                      <i class=\"material-icons\">extension</i>
                    <span class=\"title\">Module si servicii recomandate</span>
        </a>
            
                  <a class=\"toolbar-button btn-help btn-sidebar\" href=\"#\"
             title=\"Ajutor\"
             data-toggle=\"sidebar\"
             data-target=\"#right-sidebar\"
             data-url=\"/admin540adq7um/index.php/common/sidebar/http%253A%252F%252Fhelp.prestashop.com%252Fro%252Fdoc%252FAdminProducts%253Fversion%253D1.7.3.2%2526country%253Dro/Ajutor?_token=xJlBY3x4OZGr8S2SPAKiC7COQFS0Jf3EJtLT3jF6bzw\"
             id=\"product_form_open_help\"
          >
            <i class=\"material-icons\">help</i>
            <span class=\"title\">Ajutor</span>
          </a>
                  </div>
  
      
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-RO&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"alert alert-warning\">
\t\t\t\tDaca doresti sa folosesti pe deplin panoul de administrare a modulelor si sa ai acces la module gratuite, ar trebui sa activezi urmatoarea configureazae pe server:
\t\t\t\t<br />
\t\t\t\t- Activeaza setarea PHP allow_url_fopen<br />\t\t\t\t\t\t\t</div>
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div \">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 1271
        $this->displayBlock('content_header', $context, $blocks);
        // line 1272
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1273
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1274
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1275
        echo "
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh nu!</h1>
  <p class=\"mt-3\">
    Versiunea mobila a acestei pagini nu este inca disponibila.
  </p>
  <p class=\"mt-2\">
    Te rugam sa utilizezi un calculator de tip desktop pentru a accesa aceasta pagina, pana cand este adaptata pentru mobil.
  </p>
  <p class=\"mt-2\">
    Multumesc.
  </p>
  <a href=\"http://samargelim.vip/admin540adq7um/index.php?controller=AdminDashboard&amp;token=0d26881d9cfe6163c55fe20448e14f5d\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Inapoi
  </a>
</div>
<div class=\"mobile-layer\"></div>



  <div id=\"footer\" class=\"bootstrap\">
    
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-RO&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"alert alert-warning\">
\t\t\t\tDaca doresti sa folosesti pe deplin panoul de administrare a modulelor si sa ai acces la module gratuite, ar trebui sa activezi urmatoarea configureazae pe server:
\t\t\t\t<br />
\t\t\t\t- Activeaza setarea PHP allow_url_fopen<br />\t\t\t\t\t\t\t</div>
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1334
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 84
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1271
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1272
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1273
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1274
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1334
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__626b3f434c46cecc58efa88db20e728d68cf2685bfd410bd301d9dad51a58e2a";
    }

    public function getDebugInfo()
    {
        return array (  1413 => 1334,  1408 => 1274,  1403 => 1273,  1398 => 1272,  1393 => 1271,  1384 => 84,  1376 => 1334,  1315 => 1275,  1312 => 1274,  1309 => 1273,  1306 => 1272,  1304 => 1271,  113 => 84,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__626b3f434c46cecc58efa88db20e728d68cf2685bfd410bd301d9dad51a58e2a", "");
    }
}
