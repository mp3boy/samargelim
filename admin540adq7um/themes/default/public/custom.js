jQuery(document).ready(function(){

    //CATEGORIES
if (jQuery('body').hasClass('admincategories')){
    jQuery('#table-category tbody tr').each(function(){
        var catID  = jQuery(this).children('td:nth-child(2)').html();
        var isParentCat = jQuery(this).children('td:nth-child(7)').find('a').attr('title');
        if (isParentCat == 'Modifica') {
            var tokenProd = admin_modules_link.split('_token=');
            var linkProd = '/admin540adq7um/index.php/product/catalog?_token=' + tokenProd[1];

            var inputHidden = '<input type="hidden" name="filter_category" value="' + jQuery.trim(catID) + '" />';
            inputHidden += '<input type="hidden" name="filter_column_id_product" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_name" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_reference" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_name_category" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_price" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_sav_quantity" value="" />';
            inputHidden += '<input type="hidden" name="filter_column_active" value="" />';
            inputHidden += '<input type="hidden" name="paginator_jump_page" value="1" />';
            inputHidden += '<input type="hidden" name="paginator_select_page_limit" value="1000" />';

            var produseSubmit = '<td><form action="' + linkProd + '" method="POST"><input type="submit" value="Produse" style="display: inline-block;margin-top: 10px;"/>' + inputHidden + '</form></td>';
            jQuery(this).append(produseSubmit);
        }
    });
}


if (jQuery('body').hasClass('adminproducts')){
   if (jQuery('.products-catalog').length){
        var selectedCategory = jQuery('[name="form[choice_tree][tree]"]:checked').val();
        if (selectedCategory != '10001'){
            var toolbarIcons = $('.toolbar-icons').html();
            $('.toolbar-icons').remove();
            $('#product_catalog_category_tree_filter').append('<div class="newProductOnList">' + toolbarIcons + '</div>');
            localStorage.setItem('clickedCategoryForProduct', selectedCategory);
        } else {
            localStorage.removeItem('clickedCategoryForProduct');
        }

    } else {
        //product page
       var categoryId = localStorage.getItem('clickedCategoryForProduct');
       if (categoryId) {
           setTimeout(function () {
               if (!jQuery('[name="form[step1][categories][tree][]"]').is(':checked')) {
                   jQuery('[name="form[step1][categories][tree][]"][value="' + categoryId + '"]').trigger("click");
                   jQuery('[name="form[step1][categories][tree][]"][value="' + categoryId + '"]').siblings('input[type="radio"]').trigger("click");
               } else {

                   var xoxo = jQuery('[name="form[step1][categories][tree][]"]:checked').val();
                   if (xoxo === '10001') {
                       jQuery('[name="form[step1][categories][tree][]"][value="' + categoryId + '"]').trigger("click");
                       jQuery('[name="form[step1][categories][tree][]"][value="' + categoryId + '"]').siblings('input[type="radio"]').trigger("click");
                   } else {
                       jQuery('[name="form[step1][categories][tree][]"][value="' + xoxo + '"]').trigger("click");
                       jQuery('[name="form[step1][categories][tree][]"][value="' + xoxo + '"]').siblings('input[type="radio"]').trigger("click");
                   }
               }
           }, 1000);
       }

       //set other values in product page
       if($('#form_step1_piftva_1').val()==""){$('#form_step1_piftva_1').val(0);}
       if($('#form_step1_picutva_1').val()==""){$('#form_step1_picutva_1').val(0);}
       if($('#form_step1_adaos_1').val()==""){$('#form_step1_adaos_1').val(0);}

       var harray={
           // 105: "Accesorii hartie",
           // 6 : "Snur",
           // 76 : "Ambalaje",
           // 78 : "Bijuterii fantezie",
           // 97 : "Accesorii bijuterii",
           // 82 : "Accesorii bijuterii",
           // 90 : "Accesorii bijuterii",
           // 65 : "Cursuri",
           // 69 : "Margele",
           // 71 : "Margele",
           // 70 : "Accesorii coliere",
           // 64 : "Ustensile margelit",
           // 104 : "Fetru (filtz)",
           // 107 : "Organza",
           // 109 : "Accesorii bijuterii",
           // 112 : "Sarma",
           // 113 : "Coliere",
           // 115 : "Margele",
           // 119 : "Margele",
           // 163 : "Suporturi Bijou",
           // 123 : "Pietre",
           // 160 : "Pietre",
           // 161 : "Lant",
           // 162 : "Pietre",
           // 170 : "Plastelina FIMO"
           105 : "Accesorii hartie",
           97 : "Accesorii bijuterii",
           4 : "Margele",
           64 : "Ustensile margelit",
           123 : "Pietre",
           6 : "Snur",
           95 : "Accesorii bijuterii",
           213 : "Margele",
           214 : "Margele",
           215 : "Margele",
           216 : "Margele",
           217 : "Margele",
           208 : "Pietre",
           209 : "Accesorii bijuterii",
           76 : "Ambalaje",
           8 : "Margele",
           63 : "Margele",
           74 : "Sarma",
           78 : "Bijuterii fantezie",
           79 : "Plastelina FIMO",
           94 : "Accesorii bijuterii",
           82 : "Accesorii bijuterii",
           83 : "Accesorii bijuterii",
           84 : "Accesorii bijuterii",
           85 : "Accesorii bijuterii",
           86 : "Accesorii bijuterii",
           87 : "Accesorii bijuterii",
           89 : "Margele",
           90 : "Accesorii bijuterii",
           91 : "Accesorii bijuterii",
           92 : "Accesorii bijuterii",
           93 : "Accesorii bijuterii",
           72 : "Margele",
           65 : "Cursuri",
           68 : "Margele",
           69 : "Margele",
           70 : "Accesorii coliere",
           71 : "Margele",
           218 : "Pietre",
           219 : " Pietre",
           220 : "Pietre",
           100 : "Accesorii bijuterii",
           101 : "Accesorii bijuterii",
           102 : "Accesorii bijuterii",
           103 : "Accesorii bijuterii",
           104 : "Fetru (filtz)",
           107 : "Organza",
           108 : "Accesorii bijuterii",
           109 : "Accesorii bijuterii",
           110 : "Accesorii bijuterii",
           158 : "Sarma",
           163 : "Suporturi Bijou",
           111 : "Accesorii bijuterii",
           112 : "Sarma",
           113 : "Coliere",
           114 : "Accesorii bijuterii",
           115 : "Margele",
           116 : "Margele",
           117 : "Margele",
           221 : "Pietre",
           119 : "Margele",
           120 : "Perle",
           121 : "Perle",
           124 : "Pietre",
           125 : "Pietre",
           126 : "Pietre",
           127 : "Pietre",
           128 : "Pietre",
           129 : "Pietre",
           130 : "Pietre",
           156 : "Pietre",
           132 : "Pietre",
           133 : "Pietre",
           134 : "Pietre",
           135 : "Pietre",
           136 : "Pietre",
           137 : "Pietre",
           138 : "Pietre",
           139 : "Pietre",
           140 : "Pietre",
           141 : "Pietre",
           142 : "Pietre",
           143 : "Pietre",
           144 : "Pietre",
           145 : "Pietre",
           146 : "Pietre",
           147 : "Pietre",
           148 : "Pietre",
           149 : "Pietre",
           150 : "Pietre",
           151 : "Pietre",
           157 : "Pietre",
           153 : "Pietre",
           154 : "Pietre",
           155 : "Pietre",
           169 : "Ustensile FIMO",
           159 : "Pietre",
           160 : "Pietre",
           161 : "Lant",
           162 : "Pietre",
           164 : "Pietre",
           165 : "Pietre",
           166 : "Pietre",
           167 : "Pietre",
           168 : "Pietre",
           170 : "Plastelina FIMO",
           207 : "Bijuterii fantezie",
           175 : "Pietre",
           176 : "Pietre",
           178 : "Sarma",
           222 : "Pietre",
           180 : "Pietre",
           181 : "Pietre",
           182 : "Pietre",
           183 : "Pietre",
           197 : "Margele",
           185 : "Pietre",
           186 : "Pietre",
           187 : "Pietre",
           189 : "Pietre",
           190 : "Pietre",
           191 : "Pietre",
           193 : "Pietre",
           196 : "Snur",
           198 : "Accesorii bijuterii",
           199 : "Pietre",
           200 : "Snur",
           201 : "Margele",
           202 : "Margele",
           203 : "Margele",
           204 : "Margele",
           205 : "Margele",
           206 : "Margele",
           211 : "Pietre",
           212 : "Margele"

       };

       var xarray = [
           "Accesorii argint",
           "Accesorii bijuterii",
           "Accesorii coliere",
           "Accesorii hartie",
           "Ambalaje",
           "Bijuterii fantezie",
           "Coliere",
           "Cursuri",
           "Fetru (filtz)",
           "Lant",
           "Margele",
           "Organza",
           "Perle",
           "Pietre",
           "Plastelina FIMO",
           "Sarma",
           "Snur",
           "Suporturi Bijou",
           "Transport Refacturat",
           "Ustensile FIMO",
           "Ustensile margelit",
       ];

            //set product gift
            if ($('#form_step1_gift_1').attr('value')){} else { $('#form_step1_gift_1').val("--none--");}
            var gift=$('#form_step1_gift_1').val();
            var harray1=['Cercei handmade','O bratara handmade','O brosa handmade','Un colier handmade','Un inel handmade','Un pandantiv handmade'];
            var selectgift='<select id="form_step1_gift_1000" class="custom-select form-control" multiple="multiple" style="height:70px;">';
            var selectd;
            var selectedGift;
            if (gift.includes("--none--")) { selectedGift="selected";}else{seletd1="";}
            selectgift +='<option value="--none--" '+selectedGift+'>--none--</option>';
                $.each(harray1,function(index, value ){
                    if (gift.includes(index)){selectd="selected";}else{selectd="";}
                    selectgift +='<option value="'+index+'" '+selectd+'>'+value+'</option>';
                });
            selectgift +='</select>';
            $('.gift').after(selectgift);

            $('#form_step1_gift_1000').change(function() {
                var lll="";
                $.each($(this).val(),function(index, value ){
                    lll+=","+value;
                });

                $('#form_step1_gift_1').val(lll.substring(1));
            });

            //cod2
            var defaultCod2 = $('#form_step1_cod2_1').val();
            var selectedCod2 = '';
            $('#form_step1_cod2_1').remove();
            var selectcod2 = '<select name="form[step1][cod2][1]" id="form_step1_cod2_1" class="custom-select form-control">';
            selectcod2 += '<option value="Alege codul produsului">Alege codul produsului</option>';
            $.each(xarray, function (index, value) {
                if (value === defaultCod2) {selectedCod2 = 'selected';}
                selectcod2 += '<option value="' + value + '" ' + selectedCod2 + '>' + value + '</option>';
            });
            selectcod2 += '</select>';
            $('.cod2').after(selectcod2);

            //set product model
            var productId = $('#form_id_product').val();

            $('input[name="form[step1][categories][tree][]"]').click(function() {
                var parentCategory ='';
                var childCategory ='';
                if ( $(this).parent().parent().parent().parent().parent().children('.checkbox:first-child').children('label').length ) {
                    pops = $(this).parent().parent().parent().parent().parent().children('.checkbox:first-child').children('label').children('input[type=checkbox]').val()
                    if (pops != "10001"){
                        parentCategory += pops;
                    }
                }
                childCategory += $(this).val();

                if ($('#form_step1_productmodel_1').val() == '') {
                    var productModel = parentCategory + childCategory + productId;
                    $('#form_step1_productmodel_1').val(productModel);
                }

                if ($('#form_step1_cod2_1').val() == 'Alege codul produsului'){
                    var xCode2 = (parentCategory == "10001" || parentCategory == "")? childCategory : parentCategory;
                    $("#form_step1_cod2_1 option[value='"+harray[xCode2]+"']").prop("selected", true);
                }
            });

            setTimeout(function(){
                $("#ps_categoryTags .pstaggerTag:first-child .pstaggerClosingCross").click();
            },2000);

    }

}

    //Refacere Meniu
    var subMenu = '<li class="active link-leveltwo" id="subtab-{id}" data-submenu="4"><a href="{href}" class="title link">{title}</a></li>';
    var facturiChitante = subMenu.replace('{id}','facturiChitante').replace('{href}','/admin540adq7um/index.php?controller=AdminFacturichitante').replace('{title}','Facturi si chitante');
    var setariDocumente = subMenu.replace('{id}','setariDocumente').replace('{href}','/admin540adq7um/index.php?controller=AdminSetaridocumente').replace('{title}','Setari documente');
    var cosCumparaturi = subMenu.replace('{id}','cosCumparaturi').replace('{href}','/admin540adq7um/index.php?controller=AdminCarts').replace('{title}','Cos cumparaturi');

    $('#subtab-AdminOrders').after(facturiChitante);
    $('#subtab-facturiChitante').after(setariDocumente);
    $('#subtab-setariDocumente').after(cosCumparaturi);
    $('#subtab-AdminCarts').remove();
    $('#subtab-AdminInvoices a, #subtab-AdminDeliverySlip a, #subtab-AdminSlip a').append('*');

    var adminProduse = subMenu.replace('{id}','adminProduse').replace('{href}','/admin540adq7um/index.php/product/catalog').replace('{title}','Produse *');
    var adminAtribute = subMenu.replace('{id}','adminAtribute').replace('{href}','/admin540adq7um/index.php?controller=AdminAttributesGroups').replace('{title}','Atribute si caracteristici');
    var adminCartRules = subMenu.replace('{id}','adminCartRules').replace('{href}','/admin540adq7um/index.php?controller=AdminCartRules').replace('{title}','Reduceri');
    var adminRaportStocuri = subMenu.replace('{id}','adminRaportStocuri').replace('{href}','/admin540adq7um/index.php?controller=AdminRaportstocuri').replace('{title}','Raport Stocuri');
    var adminLowStock = subMenu.replace('{id}','adminLowStock').replace('{href}','/admin540adq7um/index.php/product/catalog/0/20/sav_quantity/asc').replace('{title}','Product Low Stock');
    var adminStocks = subMenu.replace('{id}','adminStocks').replace('{href}','/admin540adq7um/index.php/stock/').replace('{title}','Stocks');
    var adminMarci = subMenu.replace('{id}','adminMarci').replace('{href}','/admin540adq7um/index.php?controller=AdminManufacturers').replace('{title}','Marci si furnizori *');


    $('#subtab-AdminProducts,#subtab-AdminParentAttributesGroups,#subtab-AdminParentCartRules, #subtab-AdminStockManagement').remove();
    $('#subtab-AdminAttachments').after(adminProduse);
    $('#subtab-AdminCategories').after(adminAtribute);
    $('#subtab-adminAtribute').after(adminCartRules);
    $('#subtab-adminCartRules').after(adminRaportStocuri);
    $('#subtab-adminRaportStocuri').after(adminLowStock);
    $('#subtab-adminLowStock').after(adminStocks);
    $('#subtab-adminProduse').after(adminMarci);

    var adminTestimoniale = subMenu.replace('{id}','adminTestimoniale').replace('{href}','/admin540adq7um/index.php?controller=AdminModules&configure=jmstestimonials').replace('{title}','Testimoniale');
    var adminFidelitate = subMenu.replace('{id}','adminFidelitate').replace('{href}','/admin540adq7um/index.php?controller=AdminModules&configure=loyaltyrewardpoints').replace('{title}','Puncte de fidelitate');
    var adminBannere = subMenu.replace('{id}','adminBannere').replace('{href}','/admin540adq7um/index.php?controller=AdminModules&configure=jmsslider').replace('{title}','Bannere');
    $('#subtab-AdminAddresses').after(adminTestimoniale);
    $('#subtab-adminTestimoniale').after(adminFidelitate);
    $('#subtab-AdminThemes').before(adminBannere);

    var adminFurnizori = subMenu.replace('{id}','adminFurnizori').replace('{href}','/admin540adq7um/index.php?controller=Adminfurnizori').replace('{title}','Furnizori');
    var adminNiruri = subMenu.replace('{id}','adminNiruri').replace('{href}','/admin540adq7um/index.php?controller=AdminNiruri').replace('{title}','Niruri');
    var adminNirNou = subMenu.replace('{id}','adminNirNou').replace('{href}','/admin540adq7um/index.php?controller=AdminNirnou').replace('{title}','Nir Nou');

    var adminGestiune = '<li class="maintab has_submenu link-levelone" id="subtab-adminGestiune" data-submenu="143">'+
        '<a href="/admin540adq7um/index.php?controller=Adminfurnizori" class="title link has_submenu">'+
            '<i class="material-icons hidden-xs">extension</i>'+
            '<span>Gestiune</span>'+
            '<i class="material-icons pull-right">keyboard_arrow_down</i>'+
        '</a>'+
        '<ul id="collapse-43" class="submenu list-group panel-collapse">'+
        adminFurnizori + adminNiruri + adminNirNou +
        '</ul></li>';

    $('#subtab-AdminInternational').after(adminGestiune);

});
