/**
 * Default layout instanciation
 */
$(document).ready(function() {
  var $this = $(this);
  var $ajaxSpinner = $('.ajax-spinner');
  $('[data-toggle="tooltip"]').tooltip();
  rightSidebar.init();
  /** spinner loading */
  $this.ajaxStart(function () {
    $ajaxSpinner.show();
  });
  $this.ajaxStop(function () {
    $ajaxSpinner.hide();
  });
  $this.ajaxError(function () {
    $ajaxSpinner.hide();
  });
});

var rightSidebar = (function() {
    return {
        'init': function() {
            $('.btn-sidebar').on('click', function initLoadQuickNav() {
                $('div.right-sidebar-flex').removeClass('col-lg-12').addClass('col-lg-9');

                /** Lazy load of sidebar */
                var url = $(this).data('url');
                var target = $(this).data('target');

                if (url) {
                    rightSidebar.loadQuickNav(url,target);
                }
            });
            $(document).on('hide.bs.sidebar', function(e) {
                $('div.right-sidebar-flex').removeClass('col-lg-9').addClass('col-lg-12');
            });
        },
        'loadQuickNav': function(url, target) {
            /** Loads inner HTML in the sidebar container */
            $(target).load(url, function() {
                $(this).removeAttr('data-url');
                $('ul.pagination > li > a[href]', this).on('click', function(e) {
                    e.preventDefault();
                    rightSidebar.navigationChange($(e.target).attr('href'), $(target));
                });
                $('ul.pagination > li > input[name="paginator_jump_page"]', this).on('keyup', function(e) {
                    if (e.which === 13) { // ENTER
                        e.preventDefault();
                        var val = parseInt($(e.target).val());
                        var limit = $(e.target).attr('pslimit');
                        var url = $(this).attr('psurl').replace(/999999/, (val-1)*limit);
                        rightSidebar.navigationChange(url, $(target));
                    }
                });
            });
        },
        'navigationChange': function(url, sidebar) {
            rightSidebar.loadQuickNav(url, sidebar);
        }
    };
})();

/**
 *  BO Events Handler
 */
var BOEvent = {

    on: function(eventName, callback, context) {

        document.addEventListener(eventName, function(event) {
            if (typeof context !== 'undefined') {
                callback.call(context, event);
            } else {
                callback(event);
            }
        });
    },

    emitEvent: function(eventName, eventType) {
        var _event = document.createEvent(eventType);
        // true values stand for: can bubble, and is cancellable
        _event.initEvent(eventName, true, true);
        document.dispatchEvent(_event);
    }
};


$(document).ready(function(){

    $('#imprima_eticheta').click(function(){
       var num=$('#form_step1_name_1').val();
       var price=$('#form_step1_price_ttc_shortcut').val();
       var model=$('#form_step1_productmodel_1').val();

       var links="/imprimare/imprimare2013.php?s1="+num+"&s2="+price+"&s3="+model; //alert(num);     
       //alert('daa');
      
       window.open(links,'mywindow','width=380,height=250,left=600,top=100,screenX=0,screenY=100');   
    });

     $('#addnir').click(function(){


     	if($('#prodnr').val()==""){
     		$('#prodnr').focus();
     	}else{
		        var cevar=$('#prodnr').val();
		        var altc= $('#form_id_product').val();
		        var pvf=document.getElementById("form_step1_price_ttc_shortcut");
		        var pif=document.getElementById("form_step1_piftva_1");
		        var picu=document.getElementById("form_step1_picutva_1");
		        var adaos=document.getElementById("form_step1_adaos_1");
		        var linkys="/imprimare/adaugare_nir.php?s1="+altc+"&s2="+cevar+"&pvf="+pvf.value+"&pif="+pif.value+"&picu="+picu.value+"&ada="+adaos.value;
		        //alert(linkys);     
		        window.open(linkys,'mywindow','width=1000,height=500,left=600,top=100,screenX=0,screenY=100');
		}
     });


     $('#form_step1_price_shortcut').change(function(){
     	change_adaos();
     });

      $('#form_step1_picutva_1').change(function(){
     	var pret2=document.getElementById("form_step1_picutva_1"); 
		var pric=(100*pret2.value)/(100+19);
		//var pric=(100-19)/100*pret2.value;
		var prit=pric.toFixed(2);
		document.getElementById('form_step1_piftva_1').value = prit;
		change_adaos();
     }); 
      $('#form_step1_piftva_1').change(function(){
     	var pret1=document.getElementById("form_step1_piftva_1"); 
		var pric=parseFloat(pret1.value)*19/100+parseFloat(pret1.value);
		var prit=pric.toFixed(2);
		document.getElementById('form_step1_picutva_1').value = prit;
		change_adaos();
	 
     });

    function change_adaos() {
		var pret1=document.getElementById("form_step1_price_ttc_shortcut");
		var pret2=document.getElementById("form_step1_picutva_1"); 
		var adaos=(pret1.value-pret2.value)*100/pret2.value;
		console.log(pret1.value+" - "+pret2.value+" - "+adaos);
		document.getElementById('form_step1_adaos_1').value = adaos.toFixed(2);
	}


  
      

});
