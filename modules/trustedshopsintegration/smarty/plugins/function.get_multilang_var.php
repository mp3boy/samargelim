<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * Gets multilang variable.
 * @usage {$tsid|getGeneralHelpLink}
 * @param string $varName variable name
 * @param string $tsid Trusted Shops ID
 * @return string
*/

if (!function_exists('smarty_function_get_multilang_var')) {
    function smarty_function_get_multilang_var($params, &$smarty)
    {
        $varName = isset($params['varName']) ? $params['varName'] : '';
        $langID  = isset($params['langID']) ? (int) $params['langID'] : 0;
        $tsid    = isset($params['tsid']) ? $params['tsid'] : '';

        $availableVarName = array('phone-number', 'phone-number-test', 'test-link', 'trustbadge-link','review-link', 'price-link', 'help-link', 'upgrade-link', 'review-sticker-link', 'forgotten-password-link', 'seo-profile-link', 'sign-in-image', 'contact-link', 'cgu-link');

        if (!in_array($varName, $availableVarName)) {
            return '#';
        }

        $multilang_vars = TSParameters::get();
        $context = Context::getContext();
        $lang_iso = ($langID == true) ? Language::getIsoById($langID) : $context->language->iso_code;

        // If lang is not supported, default to EN instead.
        if (!isset($multilang_vars[$varName][$lang_iso])) {
            if (isset($multilang_vars[$varName]['en'])) {
                $lang_iso = 'en';
            } else {
                return '#';
            }
        }
        $output = $multilang_vars[$varName][$lang_iso];

        // Search for dynamic parts in the output and replace them.
        $arrSearch = array('{tsid}', '{iso_lang}', '{ps_version}', '{plugin_version}');
        $arrReplace = array($tsid, $context->language->iso_code, Configuration::get('PS_INSTALL_VERSION'), '');
        $output = str_replace($arrSearch, $arrReplace, $output);

        return $output;
    }
}
