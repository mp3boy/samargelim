<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * Smarty function to display color input with colorpicker.
 * @usage {input_color name='my_input_name' value=true}
 * @param.value string
 * @param.name string
 */

if (!function_exists('smarty_function_input_color')) {
    function smarty_function_input_color($params, &$smarty)
    {
        $name  = $params['name'];
        $value = $params['value'];

        $output = '<div class="input-group color-picker fixed-width-lg">';
            $output .= '<input
                            class="mColorPicker"
                            id="'. $name . '"
                            data-hex="true"
                            type="text"
                            value="' . $value . '"
                            name="'. $name . '"
                            style="background-color: ' . $value . ';"
                  >';
            $output .= '<span
                    id="icp_'. $name . '"
                    class="mColorPickerTrigger input-group-addon"
                    style="cursor: pointer;"
                    data-mcolorpicker="true"
                  >';
                $output .= '<img src="'. _MODULE_DIR_ . '/trustedshopsintegration/views/img/color.png">';
            $output .= '</span>';
        $output .= '</div>';

        return $output;
    }
}
