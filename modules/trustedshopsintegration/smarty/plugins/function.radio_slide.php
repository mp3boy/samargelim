<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * Smarty function to display Prestashop switch.
 * @usage {radio_slide name='my_input_name' value=true dataAttr=''}
 * @param.value boolean
 * @param.name string
 * @param.dataAttr string optional
 */

if (!function_exists('smarty_function_radio_slide')) {
    function smarty_function_radio_slide($params, &$smarty)
    {
        $name  = $params['name'];
        $value = $params['value'];
        if (!isset($params['on'])) {
            $params['on'] = 'On';
        }
        if (!isset($params['off'])) {
            $params['off'] = 'Off';
        }
        if (isset($params['dataAttr'])) {
            $attr  = $params['dataAttr'];
        } else {
            $attr = false;
        }

        $output = '<span class="switch prestashop-switch fixed-width-lg" ' . ($attr ? 'data-' . $attr . '' : '')  . '>';
            $output .= '<input type="radio" name="' . $name . '" id="' . $name . '_on" value="1"' . ($value ? 'checked="checked"' : '')  . '>';
            $output .= '<label for="' . $name . '_on" class="radioCheck">'.$params['on'].'</label>';

            $output .= '<input type="radio" name="' . $name . '" id="' . $name . '_off" value="0"' . (!$value ? 'checked="checked"' : '')  . '>';
            $output .= '<label for="' . $name . '_off" class="radioCheck">'.$params['off'].'</label>';

            $output .= '<a class="slide-button btn"></a>';
        $output .= '</span>';

        return $output;
    }
}
