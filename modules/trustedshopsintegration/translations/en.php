<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{trustedshopsintegration}prestashop>trustedshopsintegration_83a7c9f25d8534546586bd6098840869'] = 'Trusted Shops Reviews';
$_MODULE['<{trustedshopsintegration}prestashop>trustedshopsintegration_6ec3e1560bd1543dffe188e26255fc65'] = 'This module integrates Trusted Shops into your Prestashop installation.';
$_MODULE['<{trustedshopsintegration}prestashop>trustedshopsintegration_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Are you sure you want to uninstall this module?';
$_MODULE['<{trustedshopsintegration}prestashop>ts_info_hint_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Information';
$_MODULE['<{trustedshopsintegration}prestashop>ts_info_hint_3868a5da793ea679f6b36a959f0ce6a6'] = 'More Traffic, fewer returns: Make sure to unlock unlimited Product Reviews in your Trusted Shops plan.';
$_MODULE['<{trustedshopsintegration}prestashop>ts_info_hint_d3d2e617335f08df83599665eef8a418'] = 'Close';
$_MODULE['<{trustedshopsintegration}prestashop>login_4192a09369d96711266c3b766a66da72'] = 'By clicking on [b]Start free trial[/b] I agree to the [a]terms and conditions[/a].';
$_MODULE['<{trustedshopsintegration}prestashop>login_38c731cd1548211c7bb3fd19d7bf1228'] = 'Talk to us';
$_MODULE['<{trustedshopsintegration}prestashop>index_2de83a9b6ba96f0a4470c416f47f2644'] = 'Trusted Shops sends invite emails automatically 7 days after a customer has placed an order. Activating [b]Automated Invites[/b] according to PrestaShop status enables you to send invite emails based on the order status of PrestaShop that you specified here (eg: if you specified \'delivered\' as the order status, the email will be sent after the order has reached the \'delivered\' status). This enables you to send out your invite emails at the best time possible to maximize your changes of receiving a great review.';
$_MODULE['<{trustedshopsintegration}prestashop>index_93cba07454f06a4a960172bbd6e2a435'] = 'Yes';
$_MODULE['<{trustedshopsintegration}prestashop>index_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{trustedshopsintegration}prestashop>index_bc69351634b347628d52e058c7861407'] = 'Activate this setting in order to collect product reviews and display them in your shop.';
$_MODULE['<{trustedshopsintegration}prestashop>invites_step4_51b1dc851b0391e2dd320d25139c9ab2'] = 'invite is ready to be sent.';
$_MODULE['<{trustedshopsintegration}prestashop>admintrustedshopsintegrationaccountcontroller_a1650419df1042dc424bfdf5cd1373fc'] = 'We are sorry. A trial account for this shop already exists.';
$_MODULE['<{trustedshopsintegration}prestashop>admintrustedshopsintegrationaccountcontroller_0ecf2b4967411c2cbdcaa1e7d09deb7b'] = 'We are sorry. A trial account for this email address already exists.';
$_MODULE['<{trustedshopsintegration}prestashop>index_a7434c1cd40387b768f3a8dd48c259cf'] = 'Please note that in order for this option to work properly, you need to change the standard settings in your My Trusted Shops account. Log in to your My Trusted Shops account and go to Reviews > Settings > Collecting reviews and activate \"Review Trigger API\".';
