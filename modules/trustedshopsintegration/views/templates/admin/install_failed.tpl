{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $lang == 'fr'}
    <span>
        Cher(e) utilisateur(trice),<br/>
        Malheureusement, vous utilisez une version PHP non compatible. Pour un fonctionnement optimal, ce module nécessite d’utiliser une version PHP 5.5 ou ultérieure. Si vous souhaitez poursuivre avec une ancienne version, voici
        <a href="{$failed_link|escape:'htmlall'}" target="blank">un guide</a> sur la façon de procéder.
    </span>
{elseif $lang == 'de'}
    <span>
        Lieber Nutzer,<br/>
        leider verwenden Sie eine nicht kompatible PHP Version. Das Modul setzt mindestens eine Version von PHP 5.5 voraus. Wenn Sie Ihre PHP Version nicht updaten können, finden Sie
        <a href="{$failed_link|escape:'htmlall'}" target="blank">hier</a> eine Anleitung, wie Sie weiter vorgehen können.
    </span>
{elseif $lang == 'pl'}
    <span>
        Drogi Użytkowniku,<br/>
        niestety nie korzystasz z kompatybilnej wersji PHP. Moduł wymaga co najmniej wersji 5.5. Jeśli nie możesz zaktualizować swojej wersji PHP, tu
        <a href="{$failed_link|escape:'htmlall'}" target="blank">znajdziesz instrukcje</a>, jak dalej postąpić.
    </span>
{else}
    <span>
        Dear user,<br/>
        unfortunately, you are using a non-compatible PHP version. The module requires at least PHP 5.5. If you cannot update your PHP version, you can find instructions on how to proceed
        <a href="{$failed_link|escape:'htmlall'}" target="blank">here</a>.
    </span>
{/if}
