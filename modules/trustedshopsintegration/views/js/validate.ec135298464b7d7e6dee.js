/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

/* unicode_hack.js
*    Copyright (C) 2010-2012  Marcelo Gibson de Castro GonÃ§alves. All rights reserved.
*
*    Copying and distribution of this file, with or without modification,
*    are permitted in any medium without royalty provided the copyright
*    notice and this notice are preserved.  This file is offered as-is,
*    without any warranty.
*/


var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var unicodeCategories = undefined;
var unicode_category = undefined;

var unicode_hack = (function () {
  /* Regexps to match characters in the BMP according to their Unicode category.
     Extracted from Unicode specification, version 5.0.0, source:
     http://unicode.org/versions/Unicode5.0.0/
  */
  unicodeCategories = {
    Pi: '[«‘‛“‟‹⸂⸄⸉⸌⸜]',
    Sk: '[^`¨¯´¸˂-˅˒-˟˥-˭˯-˿ʹ͵΄΅᾽᾿-῁῍-῏῝-῟῭-`´῾゛゜꜀-꜖꜠꜡＾｀￣]',
    Sm: '[+<->|~¬±×÷϶⁄⁒⁺-⁼₊-₌⅀-⅄⅋←-↔↚↛↠↣↦↮⇎⇏⇒⇔⇴-⋿⌈-⌋⌠⌡⍼⎛-⎳⏜-⏡▷◁◸-◿♯⟀-⟄⟇-⟊⟐-⟥⟰-⟿⤀-⦂⦙-⧗⧜-⧻⧾-⫿﬩﹢﹤-﹦＋＜-＞｜～￢￩-￬]',
    So: '[¦§©®°¶҂؎؏۩۽۾߶৺୰௳-௸௺ೱೲ༁-༃༓-༗༚-༟༴༶༸྾-࿅࿇-࿌࿏፠᎐-᎙᥀᧠-᧿᭡-᭪᭴-᭼℀℁℃-℆℈℉℔№-℘℞-℣℥℧℩℮℺℻⅊⅌⅍↕-↙↜-↟↡↢↤↥↧-↭↯-⇍⇐⇑⇓⇕-⇳⌀-⌇⌌-⌟⌢-⌨⌫-⍻⍽-⎚⎴-⏛⏢-⏧␀-␦⑀-⑊⒜-ⓩ─-▶▸-◀◂-◷☀-♮♰-⚜⚠-⚲✁-✄✆-✉✌-✧✩-❋❍❏-❒❖❘-❞❡-❧➔➘-➯➱-➾⠀-⣿⬀-⬚⬠-⬣⳥-⳪⺀-⺙⺛-⻳⼀-⿕⿰-⿻〄〒〓〠〶〷〾〿㆐㆑㆖-㆟㇀-㇏㈀-㈞㈪-㉃㉐㉠-㉿㊊-㊰㋀-㋾㌀-㏿䷀-䷿꒐-꓆꠨-꠫﷽￤￨￭￮￼�]',
    Po: '[!-#%-\'*,./:;?@\\¡·¿;·՚-՟։־׀׃׆׳״،؍؛؞؟٪-٭۔܀-܍߷-߹।॥॰෴๏๚๛༄-༒྅࿐࿑၊-၏჻፡-፨᙭᙮᛫-᛭᜵᜶។-៖៘-៚᠀-᠅᠇-᠊᥄᥅᧞᧟᨞᨟᭚-᭠‖‗†-‧‰-‸※-‾⁁-⁃⁇-⁑⁓⁕-⁞⳹-⳼⳾⳿⸀⸁⸆-⸈⸋⸎-⸖、-〃〽・꡴-꡷︐-︖︙︰﹅﹆﹉-﹌﹐-﹒﹔-﹗﹟-﹡﹨﹪﹫！-＃％-＇＊，．／：；？＠＼｡､･]',
    Mn: '[̀-ͯ҃-֑҆-ׇֽֿׁׂׅׄؐ-ًؕ-ٰٞۖ-ۜ۟-۪ۤۧۨ-ܑۭܰ-݊ަ-ް߫-߳ँं़ु-ै्॑-॔ॢॣঁ়ু-ৄ্ৢৣਁਂ਼ੁੂੇੈੋ-੍ੰੱઁં઼ુ-ૅેૈ્ૢૣଁ଼ିୁ-ୃ୍ୖஂீ்ా-ీె-ైొ-಼్ౕౖಿೆೌ್ೢೣു-ൃ്්ි-ුූัิ-ฺ็-๎ັິ-ູົຼ່-ໍཱ༹༘༙༵༷-ཾྀ-྄྆྇ྐ-ྗྙ-ྼ࿆ိ-ူဲံ့္ၘၙ፟ᜒ-᜔ᜲ-᜴ᝒᝓᝲᝳិ-ួំ៉-៓៝᠋-᠍ᢩᤠ-ᤢᤧᤨᤲ᤹-᤻ᨘᨗᬀ-ᬃ᬴ᬶ-ᬺᬼᭂ᭫-᭳᷀-᷊᷿᷾⃐-⃥⃜⃡-〪⃯-゙゚꠆〯ꠋꠥꠦﬞ︀-️︠-︣]',
    Ps: '[([{༺༼᚛‚„⁅⁽₍〈❨❪❬❮❰❲❴⟅⟦⟨⟪⦃⦅⦇⦉⦋⦍⦏⦑⦓⦕⦗⧘⧚⧼〈《「『【〔〖〘〚〝﴾︗︵︷︹︻︽︿﹁﹃﹇﹙﹛﹝（［｛｟｢]',
    Cc: '[\u0000-\u001f-]',
    Cf: '[­؀-؃۝܏឴឵​-‏‪-‮⁠-⁣⁪-⁯﻿￹-￻]',
    Ll: '[a-zªµºß-öø-ÿāăąćĉċčďđēĕėęěĝğġģĥħĩīĭįıĳĵķĸĺļľŀłńņňŉŋōŏőœŕŗřśŝşšţťŧũūŭůűųŵŷźżž-ƀƃƅƈƌƍƒƕƙ-ƛƞơƣƥƨƪƫƭưƴƶƹƺƽ-ƿǆǉǌǎǐǒǔǖǘǚǜǝǟǡǣǥǧǩǫǭǯǰǳǵǹǻǽǿȁȃȅȇȉȋȍȏȑȓȕȗșțȝȟȡȣȥȧȩȫȭȯȱȳ-ȹȼȿɀɂɇɉɋɍɏ-ʓʕ-ʯͻ-ͽΐά-ώϐϑϕ-ϗϙϛϝϟϡϣϥϧϩϫϭϯ-ϳϵϸϻϼа-џѡѣѥѧѩѫѭѯѱѳѵѷѹѻѽѿҁҋҍҏґғҕҗҙқҝҟҡңҥҧҩҫҭүұҳҵҷҹһҽҿӂӄӆӈӊӌӎӏӑӓӕӗәӛӝӟӡӣӥӧөӫӭӯӱӳӵӷӹӻӽӿԁԃԅԇԉԋԍԏԑԓա-ևᴀ-ᴫᵢ-ᵷᵹ-ᶚḁḃḅḇḉḋḍḏḑḓḕḗḙḛḝḟḡḣḥḧḩḫḭḯḱḳḵḷḹḻḽḿṁṃṅṇṉṋṍṏṑṓṕṗṙṛṝṟṡṣṥṧṩṫṭṯṱṳṵṷṹṻṽṿẁẃẅẇẉẋẍẏẑẓẕ-ẛạảấầẩẫậắằẳẵặẹẻẽếềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹἀ-ἇἐ-ἕἠ-ἧἰ-ἷὀ-ὅὐ-ὗὠ-ὧὰ-ώᾀ-ᾇᾐ-ᾗᾠ-ᾧᾰ-ᾴᾶᾷιῂ-ῄῆῇῐ-ΐῖῗῠ-ῧῲ-ῴῶῷⁱⁿℊℎℏℓℯℴℹℼℽⅆ-ⅉⅎↄⰰ-ⱞⱡⱥⱦⱨⱪⱬⱴⱶⱷⲁⲃⲅⲇⲉⲋⲍⲏⲑⲓⲕⲗⲙⲛⲝⲟⲡⲣⲥⲧⲩⲫⲭⲯⲱⲳⲵⲷⲹⲻⲽⲿⳁⳃⳅⳇⳉⳋⳍⳏⳑⳓⳕⳗⳙⳛⳝⳟⳡⳣⳤⴀ-ⴥﬀ-ﬆﬓ-ﬗａ-ｚ]',
    Lm: '[ʰ-ˁˆ-ˑˠ-ˤˮͺՙـۥۦߴߵߺๆໆჼៗᡃᴬ-ᵡᵸᶛ-ᶿₐ-ₔⵯ々〱-〵〻ゝゞー-ヾꀕꜗ-ꜚｰﾞﾟ]',
    Lo: '[ƻǀ-ǃʔא-תװ-ײء-غف-يٮٯٱ-ۓەۮۯۺ-ۼۿܐܒ-ܯݍ-ݭހ-ޥޱߊ-ߪऄ-हऽॐक़-ॡॻ-ॿঅ-ঌএঐও-নপ-রলশ-হঽৎড়ঢ়য়-ৡৰৱਅ-ਊਏਐਓ-ਨਪ-ਰਲਲ਼ਵਸ਼ਸਹਖ਼-ੜਫ਼ੲ-ੴઅ-ઍએ-ઑઓ-નપ-રલળવ-હઽૐૠૡଅ-ଌଏଐଓ-ନପ-ରଲଳଵ-ହଽଡ଼ଢ଼ୟ-ୡୱஃஅ-ஊஎ-ஐஒ-கஙசஜஞடணதந-பம-ஹఅ-ఌఎ-ఐఒ-నప-ళవ-హౠౡಅ-ಌಎ-ಐಒ-ನಪ-ಳವ-ಹಽೞೠೡഅ-ഌഎ-ഐഒ-നപ-ഹൠൡඅ-ඖක-නඳ-රලව-ෆก-ะาำเ-ๅກຂຄງຈຊຍດ-ທນ-ຟມ-ຣລວສຫອ-ະາຳຽເ-ໄໜໝༀཀ-ཇཉ-ཪྈ-ྋက-အဣ-ဧဩဪၐ-ၕა-ჺᄀ-ᅙᅟ-ᆢᆨ-ᇹሀ-ቈቊ-ቍቐ-ቖቘቚ-ቝበ-ኈኊ-ኍነ-ኰኲ-ኵኸ-ኾዀዂ-ዅወ-ዖዘ-ጐጒ-ጕጘ-ፚᎀ-ᎏᎠ-Ᏼᐁ-ᙬᙯ-ᙶᚁ-ᚚᚠ-ᛪᜀ-ᜌᜎ-ᜑᜠ-ᜱᝀ-ᝑᝠ-ᝬᝮ-ᝰក-ឳៜᠠ-ᡂᡄ-ᡷᢀ-ᢨᤀ-ᤜᥐ-ᥭᥰ-ᥴᦀ-ᦩᧁ-ᧇᨀ-ᨖᬅ-ᬳᭅ-ᭋℵ-ℸⴰ-ⵥⶀ-ⶖⶠ-ⶦⶨ-ⶮⶰ-ⶶⶸ-ⶾⷀ-ⷆⷈ-ⷎⷐ-ⷖⷘ-ⷞ〆〼ぁ-ゖゟァ-ヺヿㄅ-ㄬㄱ-ㆎㆠ-ㆷㇰ-ㇿ㐀䶵一龻ꀀ-ꀔꀖ-ꒌꠀꠁꠃ-ꠅꠇ-ꠊꠌ-ꠢꡀ-ꡳ가힣豈-鶴侮-頻並-龎יִײַ-ﬨשׁ-זּטּ-לּמּנּסּףּפּצּ-ﮱﯓ-ﴽﵐ-ﶏﶒ-ﷇﷰ-ﷻﹰ-ﹴﹶ-ﻼｦ-ｯｱ-ﾝﾠ-ﾾￂ-ￇￊ-ￏￒ-ￗￚ-ￜ]',
    Co: '[]',
    Nd: '[0-9٠-٩۰-۹߀-߉०-९০-৯੦-੯૦-૯୦-୯௦-௯౦-౯೦-೯൦-൯๐-๙໐-໙༠-༩၀-၉០-៩᠐-᠙᥆-᥏᧐-᧙᭐-᭙０-９]',
    Lt: '[ǅǈǋǲᾈ-ᾏᾘ-ᾟᾨ-ᾯᾼῌῼ]',
    Lu: '[A-ZÀ-ÖØ-ÞĀĂĄĆĈĊČĎĐĒĔĖĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹĻĽĿŁŃŅŇŊŌŎŐŒŔŖŘŚŜŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƁƂƄƆƇƉ-ƋƎ-ƑƓƔƖ-ƘƜƝƟƠƢƤƦƧƩƬƮƯƱ-ƳƵƷƸƼǄǇǊǍǏǑǓǕǗǙǛǞǠǢǤǦǨǪǬǮǱǴǶ-ǸǺǼǾȀȂȄȆȈȊȌȎȐȒȔȖȘȚȜȞȠȢȤȦȨȪȬȮȰȲȺȻȽȾɁɃ-ɆɈɊɌɎΆΈ-ΊΌΎΏΑ-ΡΣ-Ϋϒ-ϔϘϚϜϞϠϢϤϦϨϪϬϮϴϷϹϺϽ-ЯѠѢѤѦѨѪѬѮѰѲѴѶѸѺѼѾҀҊҌҎҐҒҔҖҘҚҜҞҠҢҤҦҨҪҬҮҰҲҴҶҸҺҼҾӀӁӃӅӇӉӋӍӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸӺӼӾԀԂԄԆԈԊԌԎԐԒԱ-ՖႠ-ჅḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠḢḤḦḨḪḬḮḰḲḴḶḸḺḼḾṀṂṄṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦṨṪṬṮṰṲṴṶṸṺṼṾẀẂẄẆẈẊẌẎẐẒẔẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸἈ-ἏἘ-ἝἨ-ἯἸ-ἿὈ-ὍὙὛὝὟὨ-ὯᾸ-ΆῈ-ΉῘ-ΊῨ-ῬῸ-Ώℂℇℋ-ℍℐ-ℒℕℙ-ℝℤΩℨK-ℭℰ-ℳℾℿⅅↃⰀ-ⰮⱠⱢ-ⱤⱧⱩⱫⱵⲀⲂⲄⲆⲈⲊⲌⲎⲐⲒⲔⲖⲘⲚⲜⲞⲠⲢⲤⲦⲨⲪⲬⲮⲰⲲⲴⲶⲸⲺⲼⲾⳀⳂⳄⳆⳈⳊⳌⳎⳐⳒⳔⳖⳘⳚⳜⳞⳠⳢＡ-Ｚ]',
    Cs: '[���􏰀�]',
    Zl: '[\u2028]',
    Nl: '[ᛮ-ᛰⅠ-ↂ〇〡-〩〸-〺]',
    Zp: '[\u2029]',
    No: '[²³¹¼-¾৴-৹௰-௲༪-༳፩-፼៰-៹⁰⁴-⁹₀-₉⅓-⅟①-⒛⓪-⓿❶-➓⳽㆒-㆕㈠-㈩㉑-㉟㊀-㊉㊱-㊿]',
    Zs: '[   ᠎ -   　]',
    Sc: '[$¢-¥؋৲৳૱௹฿៛₠-₵﷼﹩＄￠￡￥￦]',
    Pc: '[_‿⁀⁔︳︴﹍-﹏＿]',
    Pd: '[-֊᠆‐-―⸗〜〰゠︱︲﹘﹣－]',
    Pe: '[)]}༻༽᚜⁆⁾₎〉❩❫❭❯❱❳❵⟆⟧⟩⟫⦄⦆⦈⦊⦌⦎⦐⦒⦔⦖⦘⧙⧛⧽〉》」』】〕〗〙〛〞〟﴿︘︶︸︺︼︾﹀﹂﹄﹈﹚﹜﹞）］｝｠｣]',
    Pf: '[»’”›⸃⸅⸊⸍⸝]',
    Me: '[҈҉۞⃝-⃠⃢-⃤]',
    Mc: '[ःा-ीॉ-ौংঃা-ীেৈোৌৗਃਾ-ੀઃા-ીૉોૌଂଃାୀେୈୋୌୗாிுூெ-ைொ-ௌௗఁ-ఃు-ౄಂಃಾೀ-ೄೇೈೊೋೕೖംഃാ-ീെ-ൈൊ-ൌൗංඃා-ෑෘ-ෟෲෳ༾༿ཿာေးၖၗាើ-ៅះៈᤣ-ᤦᤩ-ᤫᤰᤱᤳ-ᤸᦰ-ᧀᧈᧉᨙ-ᨛᬄᬵᬻᬽ-ᭁᭃ᭄ꠂꠣꠤꠧ]'
  };
  /* Also supports the general category (only the first letter) */
  var firstLetters = {};
  for (var p in unicodeCategories) {
    if (firstLetters[p[0]]) {
      firstLetters[p[0]] = unicodeCategories[p].substring(0, unicodeCategories[p].length - 1) + firstLetters[p[0]].substring(1);
    } else {
      firstLetters[p[0]] = unicodeCategories[p];
    }
  }
  for (var p in firstLetters) {
    unicodeCategories[p] = firstLetters[p];
  }

  /* Gets a regex written in a dialect that supports unicode categories and
    translates it to a dialect supported by JavaScript. */
  return function (regexpString, classes) {
    var modifiers = "";
    if (regexpString instanceof RegExp) {
      modifiers = (regexpString.global ? "g" : "") + (regexpString.ignoreCase ? "i" : "") + (regexpString.multiline ? "m" : "");
      regexpString = regexpString.source;
    }
    regexpString = regexpString.replace(/\\p\{(..?)\}/g, function (match, group) {
      var unicode_categorie = unicodeCategories[group];
      if (!classes) {
        unicode_category = unicode_categorie.replace(/\[(.*?)\]/g, "$1");
      }

      return unicode_category || match;
    });

    return new RegExp(regexpString, modifiers);
  };
})();

var validate_functions = [];

validate_functions['validate_isName'] = function (s) {
  var reg = /^[^0-9!<>,;?=+()@#"°{}_$%:]+$/;
  return reg.test(s);
};

validate_functions['validate_isGenericName'] = function (s) {
  var reg = /^[^<>={}]+$/;
  return reg.test(s);
};

validate_functions['validate_isAddress'] = function (s) {
  var reg = /^[^!<>?=+@{}_$%]+$/;
  return reg.test(s);
};

validate_functions['validate_isPostCode'] = function (s, pattern, iso_code) {
  if (typeof iso_code === 'undefined' || iso_code == '') {
    iso_code = '[A-Z]{2}';
  }

  if (typeof pattern == 'undefined' || pattern.length == 0) {
    pattern = '[a-zA-Z 0-9-]+';
  } else {
    var replacements = {
      ' ': '(?:\ |)',
      '-': '(?:-|)',
      'N': '[0-9]',
      'L': '[a-zA-Z]',
      'C': iso_code
    };

    for (var new_value in replacements) {
      pattern = pattern.split(new_value).join(replacements[new_value]);
    }
  }

  var reg = new RegExp('^' + pattern + '$');
  return reg.test(s);
};

validate_functions['validate_isCityName'] = function (s) {
  var reg = /^[^!<>;?=+@#"°{}_$%]+$/;
  return reg.test(s);
};

validate_functions['validate_isMessage'] = function (s) {
  var reg = /^[^<>{}]+$/;
  return reg.test(s);
};

validate_functions['validate_isPhoneNumber'] = function (s) {
  var reg = /^[+0-9. ()-]+$/;
  return reg.test(s);
};

validate_functions['validate_isDniLite'] = function (s) {
  var reg = /^[0-9a-z-.]{1,16}$/i;
  return reg.test(s);
};

validate_functions['validate_isEmail'] = function (s) {
  var reg = unicode_hack(/^[a-z\p{L}0-9!#$%&'*+\/=?^`{}|~_-]+[.a-z\p{L}0-9!#$%&'*+\/=?^`{}|~_-]*@[a-z\p{L}0-9]+[._a-z\p{L}0-9-]*\.[a-z\p{L}0-9]+$/i, false);
  return reg.test(s);
};

/**
 * Check password, must contain at least :
 * 6 characters (254 max)
 * 1 uppercase
 * 1 digit
 */
validate_functions['validate_isPasswd'] = function (s) {
  var reg = /^(?=.*\d)(?=.*[A-Z])[0-9a-zA-Z]{6,254}$/;
  return reg.test(s);
};

function validate_field(that) {
  if ($(that).hasClass('is_required') || $(that).val().length) {
    if ($(that).attr('data-validate') == 'isPostCode') {
      var selector = '#id_country';
      if ($(that).attr('name') == 'postcode_invoice') {
        selector += '_invoice';
      }

      var id_country = $(selector + ' option:selected').val();

      if (typeof countriesNeedZipCode[id_country] != 'undefined' && typeof countries[id_country] != 'undefined') {
        var result = validate_functions['validate_' + $(that).attr('data-validate')]($(that).val(), countriesNeedZipCode[id_country], countries[id_country]['iso_code']);
      }
    } else if ($(that).attr('data-validate')) {
      var result = validate_functions['validate_' + $(that).attr('data-validate')]($(that).val());
    }

    var $helpBlock = $(that).parent().find('.help-block');
    var originalContent = $helpBlock.data('original-content');

    if (result) {
      if (typeof originalContent !== 'undefined' && originalContent.length) {
        $helpBlock.html(originalContent);
        $helpBlock.removeAttr('data-original-content');
      } else {
        $helpBlock.remove();
      }

      $(that).parent().removeClass('form-error').addClass('form-ok');
    } else {
      $(that).parent().addClass('form-error').removeClass('form-ok');

      if (typeof originalContent !== 'undefined' && originalContent.length) {
        $helpBlock.html(originalContent);
        $helpBlock.removeAttr('data-original-content');
      }
    }
  }
}

$(document).on('focusout', 'input.validate, textarea.validate', function () {
  validate_field(this);
});

/**
 * Localised error feedback
 */
$(document).ready(function () {
  validatePage();
});

// Dispatch error stacks to their corresponding form.
function validatePage() {
  if (typeof errorsStack === 'undefined' || !errorsStack) {
    return;
  }
  errorsStack = JSON.parse(errorsStack);

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = errorsStack[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var errors = _step.value;

      validateForm(errors);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator['return']) {
        _iterator['return']();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}

// Inside a form, dispatch errors to their corresponding inputs.
function validateForm(errors) {
  if (typeof errors.form === 'undefined' || !errors.form) {
    console.warn('validateForm: missing form argument.');
    return;
  }

  var $form = $('[name="' + errors.form + '"]').closest('form');

  if (!$form.length) {
    console.warn('validateForm: form does not exist.');
    return;
  }

  // Set all inputs as OK.
  var $inputs = $form.find('input[type!="hidden"][type!="submit"]').each(function () {
    $(this).closest('.form-group').addClass('form-ok');
  });

  // Then validated inputs associated with errors.
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = Object.entries(errors)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var _step2$value = _slicedToArray(_step2.value, 2);

      var input = _step2$value[0];
      var error = _step2$value[1];

      if (input == 'form') {
        continue;
      }
      validateInput(input, error, $form);
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2['return']) {
        _iterator2['return']();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }
}

// Add proper class and display error on inputs.
function validateInput(input, error, $form) {
  var $input = $form.find('[name="' + input + '"]');
  if (!$input.length) {
    console.warn('validateInput: input ' + input + ' does not exist.');
    return;
  }

  var $formGroup = $input.closest('.form-group');
  if (!$formGroup.length) {
    console.warn('validateInput: input ' + input + ' does not have a form group parent. Please follow bootstrap form structure.');
    return;
  }

  $formGroup.removeClass('form-ok').addClass('form-error');

  var $helpBlock = $formGroup.find('.help-block');
  if ($helpBlock.length) {
    // input already has an associated help block
    var content = $helpBlock.html();
    $helpBlock.data('original-content', content); // store original content in data-original-content
    $helpBlock.html(error); // place error
  } else {
      $helpBlock = $('<div>', { 'class': 'help-block small' });
      $helpBlock.html(error);
      $input.after($helpBlock);
    }
}

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })

/******/ });
//# sourceMappingURL=validate.ec135298464b7d7e6dee.js.map