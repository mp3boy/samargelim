<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('TS_LOG')) {
    define('TS_LOG', false);
}
if (!defined('TSAPI_URL')) {
    define('TSAPI_URL', 'https://api.trustedshops.com');
}
if (!defined('WIDGET_DOMAIN')) {
    define('WIDGET_DOMAIN', 'widgets.trustedshops.com');
}
if (!defined('TSAPI_KEY')) {
    define('TSAPI_KEY', 'b7141bb85d47973f2cc6f235a200bb0612a194985def2e549ff7f26e1f0696a0');
}
