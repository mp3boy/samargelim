<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once 'TSApiWrapperAbstract.php';

/**
 * @desc: API Client of Trusted Shops
 */
class TSApiWrapperTrigger extends TSApiWrapperAbstract implements TSApiWrapperInterface
{

    protected $uri = '/rest/restricted/v2/shops/<TS_ID>/reviews/trigger.json';

    protected $method = 'POST';

    /**
     * @desc: uri of the data
     *
     * @return: uri of the api
     */
    public function getUri()
    {
        $uri = str_replace('<TS_ID>', $this->tsId, $this->uri);

        return $uri;
    }

    /**
     * @desc: check if wrapper is correctly configurated
     *
     * @return boolean
     */
    public function check()
    {
        if ($this->input == null || $this->tsId == null || $this->credentials == null) {
            return false;
        }

        return true;
    }

    /**
     * @desc: check if wrapper is correctly configurated
     * @param: $response TSApiResponse Response object
     * @param: $data     string        json response
     *
     * @return this
     */
    public function parseReponse(TSApiResponse $response, $data)
    {
        $data = json_decode($data, true);
        $errors = array(400 => 'BAD_REQUEST',
                        403 => 'UNAUTHORIZED',
                        404 => 'RESOURCE_NOT_FOUND');

        if (isset($errors[$data['response']['code']])) {
            $response->setStatus('fail');
            $response->addError($errors);

            return $this;
        }
        $response->setContent($data['response']['data']);

        return $this;
    }
}
