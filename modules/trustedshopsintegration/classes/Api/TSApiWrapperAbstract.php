<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require 'TSApiWrapperInterface.php';

/**
 * @desc: abstract wrapper of a webservice
 */
abstract class TSApiWrapperAbstract implements TSApiWrapperInterface
{
    protected $credentials;

    protected $uri;

    protected $method = 'GET';

    protected $tsId;

    protected $input;

    /**
     * @desc: set credential
     * @param: string $url
     *
     * @return this
     */
    public function setCredentials($credentials)
    {
        $this->credentials = $credentials;

        return $this;
    }

    /**
     * @desc: get credential
     * @param: string $url
     *
     * @return this
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @desc: set API url (prod or qa)
     * @param: string $url
     *
     * @return this
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @desc: set request tsId
     * @param: string $tsId
     *
     * @return this
     */
    public function setTsId($tsId)
    {
        $this->tsId = $tsId;

        return $this;
    }

    /**
     * @desc: set request method
     * @param: string $method
     *
     * @return this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @desc: get request method
     *
     * @return this
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @desc: set request body
     * @param: string $json
     *
     * @return this
     */
    public function setInput($json)
    {
        $this->input = $json;

        return $this;
    }

    /**
     * @desc: get request body
     *
     * @return this
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @desc: check if wrapper is correctly configurated
     *
     * @return this
     */
    public function check()
    {
        return false;
    }

    /**
     * @desc: check if wrapper is correctly configurated
     * @param: $response TSApiResponse Response object
     * @param: $data     string        json response
     *
     * @return this
     */
    public function parseReponse(TSApiResponse $response, $data)
    {
        $data = json_decode($data, true);
        $response->setContent($data);

        return $this;
    }
}
