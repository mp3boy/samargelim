<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @desc: interface wrapper of a webservice
 */
interface TSApiWrapperInterface
{

    /**
     * @desc: set credential
     * @param: string $url
     *
     * @return this
     */
    public function setCredentials($credentials);

    /**
     * @desc: get credential
     * @param: string $url
     *
     * @return this
     */
    public function getCredentials();

    /**
     * @desc: set API url (prod or qa)
     * @param: string $url
     *
     * @return this
     */
    public function getUri();

    /**
     * @desc: set request method
     * @param: string $method
     *
     * @return this
     */
    public function setMethod($method);

    /**
     * @desc: get request method
     *
     * @return this
     */
    public function getMethod();

    /**
     * @desc: set request body
     * @param: string $json
     *
     * @return this
     */
    public function setInput($json);

    /**
     * @desc: get request body
     *
     * @return this
     */
    public function getInput();

    /**
     * @desc: check if wrapper is correctly configurated
     *
     * @return this
     */
    public function check();
}
