<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @desc Manage login an create account page
 */
class AdminTrustedshopsintegrationAccountController extends ModuleAdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->name = 'trustedshopsintegration';

        $this->client = new TSApiClient();

        $this->actionPath = $this->context->link->getAdminLink('AdminTrustedshopsintegrationHome');
        $this->accountUrl = $this->context->link->getAdminLink('AdminTrustedshopsintegrationAccount');
        $this->template = 'login.tpl';
        $this->bootstrap = true;
        $this->meta_title = $this->l('Sign in');
        $this->context->smarty->assign('ts_page', 'login');
    }

    public function initContent()
    {
        $this->context->smarty->addPluginsDir(realpath(dirname(__FILE__) .'/../../smarty/plugins'));

        if (version_compare(phpversion(), '5.5', '<')) {
            $multilang_vars = TSParameters::get();

            $this->context->smarty->assign('lang', $this->context->language->iso_code);
            if (false === isset($multilang_vars['php-version-min'][$this->context->language->iso_code])) {
                $multilang_vars['php-version-min'][$this->context->language->iso_code] = $multilang_vars['php-version-min']['en'];
            }
            $this->context->smarty->assign('failed_link', $multilang_vars['php-version-min'][$this->context->language->iso_code]);
            $this->errors[] = $this->context->smarty->fetch(_PS_MODULE_DIR_. 'trustedshopsintegration/views/templates/admin/install_failed.tpl');
        }

        if (Tools::getValue('ts_logout') == true) {
            // LOGOUT
            Configuration::deleteByName('TRUSTEDSHOPS_MEMBER_CREDENTIALS');
            Configuration::updateValue('TRUSTEDSHOPS_MEMBER_FAILED', 1);
            //Configuration::deleteByName('TRUSTEDSHOPS_MEMBER_UUID');
            if (isset($_SESSION['TRUSTEDSHOPS_SHOPS'])) {
                unset($_SESSION['TRUSTEDSHOPS_SHOPS']);
            }
            Configuration::deleteByName('TRUSTEDSHOPS_CURRENT_ID_CONFIG');
            Tools::redirectAdmin($this->accountUrl);
        } elseif (Tools::isSubmit('submit_free_trial')) {
            // create account form
            $this->context->smarty->assign('ts_page', 'create');
            $errors = array();
            if (Tools::getValue('free_trial_shop_lang') == false) {
                $errors['free_trial_shop_lang'] = $this->l('Please select a right language');
            } else {
                $shopLang = explode('_', Tools::getValue('free_trial_shop_lang'));
                $id_shop = (int) $shopLang[0];
                $id_lang = (int) $shopLang[1];

                $langCountry = array(
                    "de" => "DEU",
                    "en" => "GBR",
                    "fr" => "FRA",
                    "es" => "ESP",
                    "pl" => "POL",
                    "nl" => "NLD",
                    "da" => "AUT",
                    "sv" => "CHE",
                    "it" => "ITA",
                    "pt" => "PRT",
                    "de" => "DEU"
                );
                // check language
                $languageIso = Language::getIsoById($id_lang);
                if ($languageIso == false || !array_key_exists($languageIso, $langCountry)) {
                    $errors['free_trial_shop_lang'] = $this->l('Please select a right language');
                }
            }
            // check if field is not empty
            $free_trial_firstname = Tools::getValue('free_trial_firstname');
            if (empty($free_trial_firstname)) {
                $errors['free_trial_firstname'] = $this->l('Please tell us your firstname');
            }
            $free_trial_lastname = Tools::getValue('free_trial_lastname');
            if (empty($free_trial_lastname)) {
                $errors['free_trial_lastname'] = $this->l('Please tell us your lastname');
            }
            $free_trial_email = Tools::getValue('free_trial_email');
            if (empty($free_trial_email)) {
                $errors['free_trial_email'] = $this->l('Please enter a valid email address');
            }
            $free_trial_phone = Tools::getValue('free_trial_phone');
            if (empty($free_trial_phone)) {
                $errors['free_trial_phone'] = $this->l('Please let us know which phone number we can use to reach you');
            }
            $free_trial_password = Tools::getValue('free_trial_password');
            if (empty($free_trial_password)) {
                $errors['free_trial_password'] = $this->l('Your password must contain at least 6 characters, 1 uppercase and 1 digit');
            }
            $free_trial_company = Tools::getValue('free_trial_company');
            if (empty($free_trial_company)) {
                $errors['free_trial_company'] = $this->l('Please tell us the name of your company');
            }
            if (count($errors)) {
                $formErrors = array();
                $formErrors['form'] = 'submit_free_trial';
                foreach ($errors as $key => $error) {
                    $formErrors[$key] = $error;
                }
                $errorsStack = array();
                $errorsStack[] = $formErrors;
                $errorsStack = json_encode($errorsStack);
                Media::addJsDef(array('errorsStack' => $errorsStack));
                $this->context->smarty->assign('errors', $formErrors);

                $this->content = $this->renderView();
                return parent::initContent();
            }

            $data = array();
            $data['contact'] = array(
                                'firstname' => Tools::getValue('free_trial_firstname'),
                                'lastname' => Tools::getValue('free_trial_lastname'),
                                'email' => Tools::getValue('free_trial_email'),
                                'phone' => Tools::getValue('free_trial_phone'),
                                'password' => Tools::getValue('free_trial_password')
                                    );
            $data['company']['name'] = Tools::getValue('free_trial_company');

            $returnUrl = '';
            if (version_compare(_PS_VERSION_, '1.7', '<')) {
                $returnUrl .= (((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://') .
                            $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
            }
            $returnUrl .= $this->actionPath;
            $data['items']['domains'][] = array (
                                        "url" => ShopUrl::getMainShopDomain($id_shop),
                                        "languageISO2" => Language::getIsoById($id_lang),
                                        "countryISO3" => $langCountry[Language::getIsoById($id_lang)],
                                        "shopSoftware" => "PRESTA"
                                    );

            $data['confirmationMail']['returnUrl'] = $returnUrl;
            $data['applicationOrigin']['name'] = "PRESTA"; // Do not change this value
            $data['productOffering']['uuid'] = "2f1ec0179c50c88e489c4258d6b1f541"; // Do not change this value
            $adminTsUrl = array();
            $adminTsUrl['home'] = $this->context->link->getAdminLink('AdminTrustedshopsintegrationHome');
            $this->context->smarty->assign('adminTsUrl', $adminTsUrl);

            $webservice = $this->client->getWrapper('application')
                            ->setInput(array($data));

            $this->client->call($webservice);
            TrustedShopsIntegration::Log('==APPLICATION==');
            TrustedShopsIntegration::Log(array($data));

            $errors = $this->client->getResponse()->getErrors();
            TrustedShopsIntegration::Log('Application ERRORS');
            TrustedShopsIntegration::Log($errors);
            TrustedShopsIntegration::Log('Application CONTENT');
            TrustedShopsIntegration::Log($this->client->getResponse()->getContent());
            foreach ($errors as $error) {
                if ($error == 'SHOP_EMAIL_DUPLICATE.') {
                    $this->errors[] = $this->l('Shop email duplicate');
                } elseif ($error == 'SHOP_URL_DUPLICATE.') {
                    $this->errors[] = $this->l('Shop URL duplicate');
                } elseif ($error == 'Internal failed') {
                        $this->errors[] = $this->l('We are sorry! Our service is not available at the moment. Please try again later');
                } else {
                    $this->errors[] = $error;
                }
            }

            if ($this->client->getResponse()->isSuccess()) {
                Configuration::updateValue('TRUSTEDSHOPS_MEMBER_TRIAL', 'TRIAL');
                Configuration::updateValue('TRUSTEDSHOPS_MEMBER_CREDENTIALS', Tools::getValue('free_trial_email').':'.Tools::getValue('free_trial_password'));
                Configuration::updateValue('TRUSTEDSHOPS_MEMBER_CREATEDAT', Date('Y-m-d H:i:s'));

                $this->template = 'thankyou.tpl';
            }
        } elseif (Tools::isSubmit('submit_ts_infos')) {
            // login form
            $this->context->smarty->assign('ts_page', 'login');
            $data = array();
            $data['login'] = Tools::getValue('ts_login_email');
            $data['password'] = Tools::getValue('ts_login_password');

            $webservice = $this->client->getWrapper('memberships');
            $webservice->setCredentials($data['login'].':'.$data['password']);
            $this->client->call($webservice);
            $response = $this->client->getResponse();

            if ($response->isSuccess() == false) {
                Configuration::deleteByName('TRUSTEDSHOPS_MEMBER_CREDENTIALS');
                $errors = $this->client->getResponse()->getErrors();
                foreach ($errors as $error) {
                    if ($error == 'Authentication failed') {
                        $this->errors[] = $this->l('The credentials are not correct. Please try again or click on "Forgot your password"');
                    } elseif ($error == 'Internal failed') {
                        $this->errors[] = $this->l('We are sorry! Our service is not available at the moment. Please try again later');
                    } else {
                        $this->errors[] = $error;
                    }
                }
                $this->content = $this->renderView();
                Configuration::updateValue('TRUSTEDSHOPS_MEMBER_FAILED', 1);

                return parent::initContent();
            }

            $content = $response->getContent();
            $status = 'NO_TRIAL';
            foreach ($content['retailer']['memberships'][0]['serviceItems'] as $item) {
                if ($item['type'] == 'MEMBERSHIP_TRIAL') {
                    $status = 'TRIAL';
                    break;
                }
            }

            Configuration::updateValue('TRUSTEDSHOPS_MEMBER_CREDENTIALS', $data['login'].':'.$data['password']);
            if (!Configuration::hasKey('TRUSTEDSHOPS_MEMBER_CREATEDAT')) {
                Configuration::updateValue('TRUSTEDSHOPS_MEMBER_CREATEDAT', Date('Y-m-d H:i:s'));
            }
            Configuration::updateValue('TRUSTEDSHOPS_MEMBER_FAILED', 0);
            Configuration::updateValue('TRUSTEDSHOPS_MEMBER_TRIAL', $status);
            Configuration::updateValue('TRUSTEDSHOPS_MEMBER_UUID', $content['retailer']['memberships'][0]['uuid']);

            Tools::redirectAdmin($this->actionPath);
        }

        $this->content = $this->renderView();

        return parent::initContent();
    }

    public function renderView()
    {


        $this->context->controller->addJS(_MODULE_DIR_ . $this->name .'/'.TotWebpack::getJSByName('validate'));
        $this->context->controller->addJS(_MODULE_DIR_ . $this->name .'/'.TotWebpack::getJSByName('back'));
        $this->context->controller->addCSS(_MODULE_DIR_ . $this->name .'/'.TotWebpack::getCSSByName('back'));
        // If PS version > 1.7, hide auto generated controller menu (in the top bar).
        if ((version_compare(_PS_VERSION_, '1.7.0', '>=') === true)) {
            $this->context->controller->addCSS(_MODULE_DIR_ . '/' . $this->name . '/views/css/ps_17_specific.css');
        }
        if (Configuration::get('TRUSTEDSHOPS_MEMBER_TRIAL') == 'EXPIRED_TRIAL') {
            $this->context->smarty->assign('ts_page', 'expiredTrial');
        }

        $this->context->smarty->assign('actionURL', $this->actionPath);

        $this->context->smarty->assign('possibleShopsLangs', $this->getPossibleLanguages());
        $this->context->smarty->assign('accountUrl', $this->accountUrl);
        $this->context->smarty->assign('module_dir', _MODULE_DIR_);

        $ts_img_dir = _MODULE_DIR_ . '/' . $this->name . '/views/img/';
        $this->context->smarty->assign('ts_img_dir', $ts_img_dir);

        $iso_code = $this->context->language->iso_code;
        if (!in_array($iso_code, array('de', 'en', 'fr', 'pl'))) {
            $iso_code = 'en';
        }

        $this->context->smarty->assign('iso_lang', $iso_code);

        $tpl = $this->createTemplate($this->template);
        return $tpl->fetch();
    }

    /**
     * Gets a choosen ts link.
     * @param string $type Linktype
     * @return string URL
     */
    private function getTSLink($type = 'information')
    {
        $typeArray = array('information', 'product_review_information', 'trustbadge_options');
        $lang_iso = $this->context->language->iso_code;

        if (!in_array($lang_iso, array('de', 'en', 'fr', 'pl'))) {
            $lang_iso = 'en';
        }
        if (in_array($type, $typeArray)) {
            $key = 'TRUSTEDSHOPS_'.Tools::strtoupper($type).'_LINK_'.Tools::strtoupper($lang_iso);
            return Configuration::get($key);
        } else {
            return false;
        }
    }

    /**
     * Gets the available languages for a shop instance
     * @return mixed[] Array of choosable languages for linking a trusted shops id
     */
    private function getPossibleLanguages()
    {
        $querystr = 'SELECT s.id_shop, l.id_lang, l.name AS lang_name, s.name AS shop_name FROM '._DB_PREFIX_.'shop s
            LEFT JOIN '._DB_PREFIX_.'lang_shop ls ON ls.id_shop = s.id_shop
            LEFT JOIN '._DB_PREFIX_.'lang l ON ls.id_lang = l.id_lang';

        $shopsLangs = Db::getInstance()->executeS($querystr, true, false);

        return $shopsLangs;
    }
}
