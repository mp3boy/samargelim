<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once 'AdminTrustedshopsintegrationDefaultController.php';
require_once dirname(__FILE__) . '/../../classes/Utils.php';

class AdminTrustedshopsintegrationSiteController extends AdminTrustedshopsintegrationDefaultController
{

    public function __construct()
    {
        parent::__construct();
        $this->name = 'trustedshopsintegration';
        $this->currentPage = 'site';
        $this->bootstrap = true;
        $this->meta_title = $this->l('Trusted shops - Site reviews');

        $this->homeUrl = $this->context->link->getAdminLink('AdminTrustedshopsintegrationHome');
        $this->siteUrl = $this->context->link->getAdminLink('AdminTrustedshopsintegrationSite');

        $this->template = 'index.tpl';
    }

    public function initContent()
    {
        parent::init();
        $tsconfig = new TSID(Configuration::get('TRUSTEDSHOPS_CURRENT_ID_CONFIG'));
        $this->context->smarty->assign('tsconfig', $tsconfig);
        $this->content = $this->renderView();

        // Save Trustbadge
        if (Tools::isSubmit('trustbadge_save_and_stay') || Tools::isSubmit('trustbadge_save')) {
            $tsconfig->display_trustbadge = Tools::getValue('display_trustbadge');
            if (Tools::getValue('trustbadge_advanced_configuration') == 0) {
                $tab = array('display_trustbadge', 'variant', 'y_offset');
                $result = Utils::validateConfigurationForm($tab);
                if ($result !== true) {
                    foreach ($result as $res) {
                        $this->errors[] = $res;
                        return false;
                    }
                }

                $tsconfig->variant = Tools::getValue('variant');
                $tsconfig->y_offset = (int)Tools::getValue('y_offset');
            } else {
                if (Utils::isJson(Tools::getValue('trustbadge_code')) === true) {
                    $trustbadgeJSONArray = array(
                        'customElementId' => '',
                        'trustcardDirection' => '',
                        'customBadgeWidth' => '',
                        'disableResponsive' => '',
                        'disableTrustbadge' => '',
                        'variant' => 'reviews',
                        'yOffset' => 0
                    );

                    $trustbadge_code = Utils::compareJSON($trustbadgeJSONArray, Tools::getValue('trustbadge_code'));
                    $tsconfig->trustbadge_code = json_encode($trustbadge_code);
                } else {
                    $this->errors = $this->l('Please enter a valid JSON configuration.');
                    return false;
                }
            }
            $tsconfig->tick_configure_trustedbadge = 1;
            $tsconfig->trustbadge_advanced_configuration = Tools::getValue('trustbadge_advanced_configuration');

            $tsconfig->save();

            if (Tools::isSubmit('trustbadge_save')) {
                Tools::redirectAdmin($this->homeUrl);
            }
        }

        // Save Review sticker
        if (Tools::isSubmit('review_save_and_stay') || Tools::isSubmit('review_save')) {
            $tsconfig->display_shop_reviews = Tools::getValue('display_shop_reviews');
            if (Tools::getValue('review_advanced_configuration') == 0) {
                $tab = array('display_shop_reviews', 'review_sticker_font', 'number_of_reviews', 'maximum_rating', 'site_review_background_color');
                $result = Utils::validateConfigurationForm($tab);
                if ($result !== true) {
                    foreach ($result as $res) {
                        $this->errors[] = $res;
                        return false;
                    }
                }

                $tsconfig->review_sticker_font = Tools::getValue('review_sticker_font');
                $tsconfig->number_of_reviews = Tools::getValue('number_of_reviews');
                $tsconfig->maximum_rating = Tools::getValue('maximum_rating');
                $tsconfig->site_review_background_color = Tools::getValue('site_review_background_color');
            } else {
                if (Utils::isJson(Tools::getValue('site_review_configuration_code'))) {
                    $trustbadgeJSONArray = array(
                        'element' => '',
                        'variant' => 'testimonial',
                        'reviews' => '5',
                        'betterThan' => '3.0',
                        'richSnippets' => 'off',
                        'backgroundColor' => '#ffdc0f',
                        'linkColor' => '#000000',
                        'quotationMarkColor' => '#FFFFFF',
                        'fontFamily' => 'Arial',
                        'reviewMinLength' => '10'
                    );

                    $site_review_configuration_code = Utils::compareJSON($trustbadgeJSONArray, Tools::getValue('site_review_configuration_code'));
                    $tsconfig->site_review_configuration_code = json_encode($site_review_configuration_code);
                } else {
                    $this->errors = $this->l('Please enter a valid JSON configuration.');
                    return false;
                }
            }
            $tsconfig->review_advanced_configuration = Tools::getValue('review_advanced_configuration');

            $tsconfig->save();

            if (Tools::isSubmit('review_save')) {
                Tools::redirectAdmin($this->homeUrl);
            }
        }

        return parent::initContent();
    }

    public function renderView()
    {
        return parent::renderView();
    }
}
