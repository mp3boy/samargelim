<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once 'AdminTrustedshopsintegrationDefaultController.php';
require_once dirname(__FILE__) . '/../../classes/Utils.php';

class AdminTrustedshopsintegrationProductsController extends AdminTrustedshopsintegrationDefaultController
{

    public function __construct()
    {
        parent::__construct();
        $this->name = 'trustedshopsintegration';
        $this->currentPage = 'products';
        $this->bootstrap = true;
        $this->meta_title = $this->l('Trusted shops - Products reviews');

        $this->homeUrl = $this->context->link->getAdminLink('AdminTrustedshopsintegrationHome');

        $this->template = 'index.tpl';
    }

    public function initContent()
    {
        parent::init();
        $tsconfig = new TSID(Configuration::get('TRUSTEDSHOPS_CURRENT_ID_CONFIG'));
        $this->context->smarty->assign('tsconfig', $tsconfig);
        $this->context->smarty->assign('MPNProductFeatures', $this->getMPNProductFeatures($this->context->language->id));

        $this->content = $this->renderView();


        if (Tools::isSubmit('submitOptionsimage_type_stay') || Tools::isSubmit('submitOptionsimage_type')) {
            $tsconfig->collect_reviews = Tools::getValue('collect_reviews');
            $tsconfig->show_reviews = Tools::getValue('show_reviews');
            $tsconfig->show_rating = Tools::getValue('show_rating');
            $tsconfig->mpn_allocation = Tools::getValue('mpn_allocation');
            if (Tools::getValue('products_reviews_advanced_configuration') == 0) {
                $tab = array('collect_reviews', 'show_reviews', 'mpn_allocation', 'review_tab_border_color', 'review_tab_star_color', 'review_tab_background_color', 'review_tab_name');
                $result = Utils::validateConfigurationForm($tab);
                if ($result !== true) {
                    foreach ($result as $res) {
                        $this->errors[] = $res;
                        return false;
                    }
                }

                $tsconfig->review_tab_border_color = Tools::getValue('review_tab_border_color');
                $tsconfig->review_tab_star_color = Tools::getValue('review_tab_star_color');
                $tsconfig->review_tab_background_color = Tools::getValue('review_tab_background_color');
                $tsconfig->review_tab_name = Tools::getValue('review_tab_name');
                // hide_empty_reviews Not available due to trustedshops JS
                // $tsconfig->hide_empty_reviews = Tools::getValue('hide_empty_reviews');
            } else {
                if (Utils::isJson(Tools::getValue('product_sticker_code'))) {
                    $ProductReviewsJSONArray = array(
                        'variant' => 'productreviews',
                        'borderColor' => '#0DBEDC',
                        'backgroundColor' => '#FFFFFF',
                        'starColor' => '#FFDC0F',
                        'starSize' => '15px',
                        'richSnippets' => 'off',
                        'ratingSummary' => 'false',
                        'maxHeight' => '600px',
                        'filter' => 'true',
                        'introtext' => ''
                    );

                    $product_sticker_code = Utils::compareJSON($ProductReviewsJSONArray, Tools::getValue('product_sticker_code'));
                    $tsconfig->product_sticker_code = json_encode($product_sticker_code);
                    $tsconfig->review_tab_name = Tools::getValue('review_tab_name');
                } else {
                    $this->errors = $this->l('Please enter a valid JSON configuration.');
                    return false;
                }
            }

            if (Tools::getValue('rating_stars_advanced_configuration') == 0) {
                $tab = array('collect_reviews', 'show_reviews', 'mpn_allocation', 'show_rating', 'rating_star_color', 'rating_star_size', 'rating_font_size', 'hide_empty_ratings');
                $result = Utils::validateConfigurationForm($tab);
                if ($result !== true) {
                    foreach ($result as $res) {
                        $this->errors[] = $res;
                        return false;
                    }
                }

                $tsconfig->rating_star_color = Tools::getValue('rating_star_color');
                $tsconfig->rating_star_size = Tools::getValue('rating_star_size');
                $tsconfig->rating_font_size = Tools::getValue('rating_font_size');
                $tsconfig->hide_empty_ratings = Tools::getValue('hide_empty_ratings');
            } else {
                if (Utils::isJson(Tools::getValue('product_widget_code'))) {
                    $ProductReviewsJSONArray = array(
                        'starColor' => '#FFDC0F',
                        'starSize' => '14px',
                        'fontSize' => '12px',
                        'showRating' => 'false',
                        'scrollToReviews' => 'false',
                        'enablePlaceholder' => 'false'
                    );

                    $product_widget_code = Utils::compareJSON($ProductReviewsJSONArray, Tools::getValue('product_widget_code'));
                    //$product_widget_code = json_decode(Tools::getValue('product_widget_code'));
                    $tsconfig->product_widget_code = json_encode($product_widget_code);
                } else {
                    $this->errors = $this->l('Please enter a valid JSON configuration.');
                    return false;
                }
            }
            $tsconfig->products_reviews_advanced_configuration = Tools::getValue('products_reviews_advanced_configuration');
            $tsconfig->rating_stars_advanced_configuration = Tools::getValue('rating_stars_advanced_configuration');
            $tsconfig->tick_configure_request = 1;
            $tsconfig->save();

            if (Tools::isSubmit('submitOptionsimage_type')) {
                Tools::redirectAdmin($this->homeUrl);
            }
        }

        return parent::initContent();
    }

    private function getMPNProductFeatures($id_lang)
    {
        $sql = "SELECT id_feature, name FROM "._DB_PREFIX_."feature_lang
                WHERE id_lang=". (int) $id_lang ."
                ORDER BY id_feature ASC";

        return Db::getInstance()->executeS($sql);
    }
}
