<?php
include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');

$theStates = State::getStatesByIdCountry(36, true);

$theStatesSelect = '';
$theState = isset($_POST['idState']) ? $_POST['idState'] : '';

foreach ($theStates as $state){
    $selected = $theState === $state['name'] ? 'selected' : '';
    $theStatesSelect .= '<option value="'.$state['id_state'].'" '.$selected.'>'.$state['name'].'</option>';
}
echo $theStatesSelect;
die();
