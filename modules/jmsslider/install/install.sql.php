<?php
/**
* 2007-2017 PrestaShop
*
* Slider Layer module for prestashop
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

$query = "DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_suffix` varchar(100) NOT NULL,
  `bg_type` int(10) NOT NULL DEFAULT '1',
  `bg_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bg_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFF',
  `slide_link` varchar(100) NOT NULL,
  `order` int(10) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_slide`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides` (`id_slide`, `title`, `class_suffix`, `bg_type`, `bg_image`, `bg_color`, `slide_link`, `order`, `status`) VALUES
(7, 'Home 1 - Slider 1', '', 1, 'ac41c639a120595f68fb1ad74d61d8b7.jpg', '', '', 0, 1),
(23, 'Home 1 - Slider 2', '', 1, 'fc1c930cdb98287f0356f16c29e0c823.jpg', '', '', 0, 1),
(24, 'Home 1 - Slider 3', '', 1, 'ac41c639a120595f68fb1ad74d61d8b7.jpg', '', '', 0, 1),
(25, 'Home 1 - Slider 4', '', 1, 'fc1c930cdb98287f0356f16c29e0c823.jpg', '', '', 0, 1),
(27, 'Home 1 - Slider 5', '', 1, 'ac41c639a120595f68fb1ad74d61d8b7.jpg', '', '', 0, 1),
(28, 'Home 3 - Slider 1', '', 1, '4e504e44e89f7b177223d438aaba15b2.jpg', '', '', 0, 1),
(29, 'Home 3 - Slider 2', '', 1, '8aa90c2f380af3b1d6f4137bd61f7dbf.jpg', '', '', 0, 1),
(30, 'Home 3 - Slider 3', '', 1, '4e504e44e89f7b177223d438aaba15b2.jpg', '', '', 0, 1),
(31, 'Home 3 - Slider 4', '', 1, '8aa90c2f380af3b1d6f4137bd61f7dbf.jpg', '', '', 0, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_lang`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_lang` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_lang`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_lang` (`id_slide`, `id_lang`) VALUES
(7, 0),
(23, 0),
(24, 0),
(25, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_layers`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_layers` (
  `id_layer` int(10) NOT NULL AUTO_INCREMENT,
  `id_slide` int(10) NOT NULL,
  `data_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_class_suffix` varchar(50) NOT NULL,
  `data_fixed` int(10) NOT NULL DEFAULT '0',
  `data_delay` int(10) NOT NULL DEFAULT '1000',
  `data_time` int(10) NOT NULL DEFAULT '1000',
  `data_x` int(10) NOT NULL DEFAULT '0',
  `data_y` int(10) NOT NULL DEFAULT '0',
  `data_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'left',
  `data_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `data_ease_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_ease_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_step` int(10) NOT NULL DEFAULT '0',
  `data_special` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cycle',
  `data_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_html` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video_controls` int(10) NOT NULL DEFAULT '1',
  `data_video_muted` int(10) NOT NULL DEFAULT '0',
  `data_video_autoplay` int(10) NOT NULL DEFAULT '1',
  `data_video_loop` int(10) NOT NULL DEFAULT '1',
  `data_video_bg` int(10) NOT NULL DEFAULT '0',
  `data_font_size` int(10) NOT NULL DEFAULT '14',
  `data_line_height` int(10) NOT NULL,
  `data_style` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'normal',
  `data_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#FFFFFF',
  `data_width` int(10) NOT NULL,
  `data_height` int(10) NOT NULL,
  `data_order` int(10) NOT NULL,
  `data_status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_layer`,`id_slide`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_layers` (`id_layer`, `id_slide`, `data_title`, `data_class_suffix`, `data_fixed`, `data_delay`, `data_time`, `data_x`, `data_y`, `data_in`, `data_out`, `data_ease_in`, `data_ease_out`, `data_step`, `data_special`, `data_type`, `data_image`, `data_html`, `data_video`, `data_video_controls`, `data_video_muted`, `data_video_autoplay`, `data_video_loop`, `data_video_bg`, `data_font_size`, `data_line_height`, `data_style`, `data_color`, `data_width`, `data_height`, `data_order`, `data_status`) VALUES
(62, 7, 'season’s must have', 'text-1 text', 0, 1000, 1700, 700, 160, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>season’s must have</p>', '', 0, 0, 0, 0, 0, 14, 56, 'normal', '#f49626', 200, 50, 0, 1),
(63, 7, 'Norm Architects Wicker Basket  ', 'text text-2', 0, 1500, 1700, 700, 205, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Norm Architects Wicker Basket  </p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(64, 7, 'shopnow', 'slider_btn', 0, 1000, 1700, 700, 447, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(92, 7, 'New Norm Dinnerware is pressed to produce restaurant level qual', 'text text-3', 0, 1000, 1700, 700, 319, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>New Norm Dinnerware is pressed to produce restaurant level quality and reliability</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#666666', 200, 50, 2, 1),
(93, 7, 'from', 'text text-4', 0, 1000, 1700, 700, 381, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>from <span>$15.00<span></span></span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(94, 23, 'Summer big sale', 'text-1 text', 0, 1000, 1700, 700, 160, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Summer big sale</p>', '', 0, 0, 0, 0, 0, 14, 56, 'normal', '#f49626', 200, 50, 0, 1),
(95, 23, 'Vaio laptop clearance sale', 'text text-2', 0, 1500, 1700, 700, 205, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Vaio laptop clearance sale</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(96, 23, 'Save up to 50% on Vaio E and F series laptop white supplles last', 'text text-3', 0, 1000, 1700, 700, 319, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Save up to 50% on Vaio E and F series laptop white supplles last</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#666666', 200, 50, 2, 1),
(97, 23, 'from', 'text text-4', 0, 1000, 1700, 700, 381, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Save up to<span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(98, 23, 'shopnow', 'slider_btn', 0, 1000, 1700, 700, 447, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(99, 24, 'season’s must have', 'text-1 text', 0, 1000, 1700, 700, 160, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>season’s must have</p>', '', 0, 0, 0, 0, 0, 14, 56, 'normal', '#f49626', 200, 50, 0, 1),
(100, 24, 'Norm Architects Wicker Basket  ', 'text text-2', 0, 1500, 1700, 700, 205, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Norm Architects Wicker Basket  </p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(101, 24, 'New Norm Dinnerware is pressed to produce restaurant level qual', 'text text-3', 0, 1000, 1700, 700, 319, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>New Norm Dinnerware is pressed to produce restaurant level quality and reliability</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#666666', 200, 50, 2, 1),
(102, 24, 'from', 'text text-4', 0, 1000, 1700, 700, 381, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>from <span>$15.00<span></span></span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(103, 24, 'shopnow', '', 0, 1000, 1700, 700, 447, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(104, 25, 'Summer big sale', 'text-1 text', 0, 1000, 1700, 700, 160, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Summer big sale</p>', '', 0, 0, 0, 0, 0, 14, 56, 'normal', '#f49626', 200, 50, 0, 1),
(105, 25, 'Vaio laptop clearance sale', 'text text-2', 0, 1500, 1700, 700, 205, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Vaio laptop clearance sale</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(106, 25, 'Save up to 50% on Vaio E and F series laptop white supplles last', 'text text-3', 0, 1000, 1700, 700, 319, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Save up to 50% on Vaio E and F series laptop white supplles last</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#666666', 200, 50, 2, 1),
(107, 25, 'from', 'text text-4', 0, 1000, 1700, 700, 381, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Save up to<span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(108, 25, 'shopnow', '', 0, 1000, 1700, 700, 447, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(114, 27, 'season’s must have', 'text-1 text', 0, 1000, 1700, 700, 160, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>season’s must have</p>', '', 0, 0, 0, 0, 0, 14, 56, 'normal', '#f49626', 200, 50, 0, 1),
(115, 27, 'Norm Architects Wicker Basket  ', 'text text-2', 0, 1500, 1700, 700, 205, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Norm Architects Wicker Basket  </p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(116, 27, 'New Norm Dinnerware is pressed to produce restaurant level qual', 'text text-3', 0, 1000, 1700, 700, 319, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>New Norm Dinnerware is pressed to produce restaurant level quality and reliability</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#666666', 200, 50, 2, 1),
(117, 27, 'from', 'text text-4', 0, 1000, 1700, 700, 381, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>from <span>$15.00<span></span></span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(118, 27, 'shopnow', 'slider_btn', 0, 1000, 1700, 700, 447, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(120, 28, 'Our top chandeliers', 'text text-2', 0, 1500, 1700, 60, 100, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Our top chandeliers</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(121, 28, 'Shine & glitter', 'text text-1', 0, 1000, 1700, 60, 220, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Shine &amp; glitter</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#f49626', 200, 50, 2, 1),
(122, 28, 'from', 'text text-4', 0, 1000, 1700, 60, 250, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Under <span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(123, 28, 'shopnow', 'slider_btn', 0, 1000, 1700, 60, 310, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(124, 29, 'Our top chandeliers', 'text text-2', 0, 1500, 1700, 60, 100, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Our top chandeliers</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(125, 29, 'Shine & glitter', 'text text-1', 0, 1000, 1700, 60, 220, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Shine &amp; glitter</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#f49626', 200, 50, 2, 1),
(126, 29, 'from', 'text text-4', 0, 1000, 1700, 60, 250, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Under <span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(127, 29, 'shopnow', 'slider_btn', 0, 1000, 1700, 60, 310, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(128, 30, 'Our top chandeliers', 'text text-2', 0, 1500, 1700, 60, 100, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Our top chandeliers</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(129, 30, 'Shine & glitter', 'text text-1', 0, 1000, 1700, 60, 220, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Shine &amp; glitter</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#f49626', 200, 50, 2, 1),
(130, 30, 'from', 'text text-4', 0, 1000, 1700, 60, 250, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Under <span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(131, 30, 'shopnow', 'slider_btn', 0, 1000, 1700, 60, 310, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1),
(132, 31, 'Our top chandeliers', 'text text-2', 0, 1500, 1700, 60, 100, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Our top chandeliers</p>', '', 0, 0, 0, 0, 0, 36, 56, 'normal', '#000000', 200, 50, 1, 1),
(133, 31, 'Shine & glitter', 'text text-1', 0, 1000, 1700, 60, 220, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Shine &amp; glitter</p>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#f49626', 200, 50, 2, 1),
(134, 31, 'from', 'text text-4', 0, 1000, 1700, 60, 250, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<p>Under <span>$300.00</span></p>', '', 0, 0, 0, 0, 0, 18, 0, 'normal', '#000000', 200, 50, 3, 1),
(135, 31, 'shopnow', 'slider_btn', 0, 1000, 1700, 60, 310, 'right', 'right', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop now\" class=\"btn btn-slider btn-default\">Shop Now\r\n</a>', '', 0, 0, 0, 0, 0, 14, 26, 'normal', '#111111', 67, 19, 4, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_shop`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_shop` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_shop`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_shop` (`id_slide`, `id_shop`) VALUES
(7, 1),
(23, 1),
(24, 1),
(25, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1);
";
