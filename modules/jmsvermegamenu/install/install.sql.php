<?php
/**
* 2007-2017 PrestaShop
*
* Jms Mega Menu module for prestashop
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

$query = "DROP TABLE IF EXISTS `_DB_PREFIX_jmsvermegamenu`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jmsvermegamenu` (
  `mitem_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(30) NOT NULL,
  `value` varchar(255) NOT NULL,
  `html_content` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  `target` varchar(25) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`mitem_id`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=276 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jmsvermegamenu` (`mitem_id`, `id_shop`, `parent_id`, `type`, `value`, `html_content`, `active`, `target`, `params`, `ordering`) VALUES
(84, 1, 0, 'category', '12', '', 1, '_self', '{}', 0),
(85, 1, 0, 'category', '13', '', 1, '_self', '{\"class\":\"must-see mega-fullwidth\",\"align\":\"center\"}', 1),
(86, 1, 0, 'category', '14', '', 1, '_self', '{\"class\":\"mega-fullwidth\",\"align\":\"left\",\"sub\":{\"width\":\"680\",\"row\":[[{\"width\":\"3\",\"items\":[{\"item\":\"220\"},{\"item\":\"221\"},{\"item\":\"222\"},{\"item\":\"223\"}]},{\"width\":\"3\",\"items\":[{\"item\":\"230\"},{\"item\":\"231\"},{\"item\":\"232\"},{\"item\":\"233\"},{\"item\":\"234\"},{\"item\":\"235\"}]},{\"width\":\"6\",\"class\":\"addon-header-banner\",\"items\":[{\"item\":\"242\"}]}],[{\"width\":\"3\",\"class\":\"mega-col-nav-bottom\",\"items\":[{\"item\":\"224\"},{\"item\":\"225\"},{\"item\":\"226\"},{\"item\":\"227\"},{\"item\":\"228\"},{\"item\":\"229\"}]},{\"width\":\"3\",\"class\":\"mega-col-nav-bottom\",\"items\":[{\"item\":\"236\"},{\"item\":\"237\"},{\"item\":\"238\"},{\"item\":\"239\"},{\"item\":\"240\"},{\"item\":\"241\"}]}]]}}', 2),
(87, 1, 0, 'category', '15', '', 1, '_self', '{}', 3),
(88, 1, 0, 'category', '16', '', 1, '_self', '{}', 4),
(89, 1, 0, 'category', '17', '', 1, '_self', '{\"align\":\"left\",\"sub\":{\"width\":\"430\",\"row\":[[{\"width\":\"6\",\"items\":[{\"item\":\"243\"},{\"item\":\"244\"},{\"item\":\"245\"},{\"item\":\"246\"},{\"item\":\"247\"}]},{\"width\":\"6\",\"items\":[{\"item\":\"253\"},{\"item\":\"254\"},{\"item\":\"255\"},{\"item\":\"256\"},{\"item\":\"257\"},{\"item\":\"258\"}]}],[{\"width\":\"6\",\"items\":[{\"item\":\"248\"},{\"item\":\"249\"},{\"item\":\"250\"},{\"item\":\"251\"},{\"item\":\"252\"}]},{\"width\":\"6\",\"class\":\"addon-header-banner addon-header-banner-2\",\"items\":[{\"item\":\"259\"}]}]]}}', 5),
(199, 1, 0, 'category', '18', '', 1, '_self', '{}', 6),
(215, 1, 0, 'category', '19', '', 1, '_self', '{\"align\":\"left\",\"sub\":{\"width\":\"400\",\"row\":[[{\"width\":\"6\",\"items\":[{\"item\":\"260\"},{\"item\":\"261\"},{\"item\":\"262\"},{\"item\":\"263\"},{\"item\":\"264\"},{\"item\":\"265\"}]},{\"width\":\"6\",\"items\":[{\"item\":\"266\"},{\"item\":\"267\"},{\"item\":\"268\"},{\"item\":\"269\"},{\"item\":\"270\"},{\"item\":\"271\"}]}],[{\"width\":\"12\",\"class\":\"addon-header-banner addon-header-banner-3\",\"items\":[{\"item\":\"272\"}]}]]}}', 7),
(216, 1, 0, 'category', '20', '', 1, '_self', '{}', 8),
(217, 1, 0, 'category', '21', '', 1, '_self', '{}', 9),
(218, 1, 0, 'category', '22', '', 1, '_self', '{}', 10),
(219, 1, 0, 'category', '23', '', 1, '_self', '{}', 11),
(220, 1, 86, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 1),
(221, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 2),
(222, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 3),
(223, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 4),
(224, 1, 86, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 5),
(225, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 6),
(226, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 7),
(227, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 8),
(228, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 9),
(229, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 10),
(230, 1, 86, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 11),
(231, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 12),
(232, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 13),
(233, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 14),
(234, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 15),
(235, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 16),
(236, 1, 86, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 17),
(237, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 18),
(238, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 19),
(239, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 20),
(240, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 21),
(241, 1, 86, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 22),
(242, 1, 86, 'html', 'html_content', '<div class=\"header-banner\">\r\n<div class=\"banner-text\">\r\n<p class=\"text-1\">Update to a <span>New phone</span></p>\r\n<p class=\"text-2\">70% off select electronics</p>\r\n<a href=\"#\" class=\"btn btn-default\"><span>Shop now</span></a></div>\r\n<div class=\"banner-img\"><img src=\"themes/jms_freshshop/assets/img/header_banner.png\" width=\"276\" height=\"257\" /></div>\r\n</div>', 1, '_self', '{\"title\":\"0\"}', 23),
(243, 1, 89, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 0),
(244, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 1),
(245, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 2),
(246, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 3),
(247, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 4),
(248, 1, 89, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 5),
(249, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 6),
(250, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 7),
(251, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 8),
(252, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 9),
(253, 1, 89, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 10),
(254, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 11),
(255, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 12),
(256, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 13),
(257, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 14),
(258, 1, 89, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 15),
(259, 1, 89, 'html', 'html_content', '<div class=\"header-banner\">\r\n<div class=\"banner-text\"><a href=\"#\" class=\"btn btn-default\"><span>Shop now</span></a></div>\r\n<div class=\"banner-img\"><img src=\"themes/jms_freshshop/assets/img/header_banner_2.jpg\" alt=\"\" width=\"150\" height=\"170\" /></div>\r\n</div>', 1, '_self', '{\"title\":\"0\"}', 16),
(260, 1, 215, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 1),
(261, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 2),
(262, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 3),
(263, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 4),
(264, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 5),
(265, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 6),
(266, 1, 215, 'link', '', '', 1, '_self', '{\"class\":\"title\",\"title\":\"1\"}', 7),
(267, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 8),
(268, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 9),
(269, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 10),
(270, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 11),
(271, 1, 215, 'link', '', '', 1, '_self', '{\"title\":\"1\"}', 12),
(272, 1, 215, 'html', 'html_content', '<div class=\"header-banner\">\r\n<div class=\"banner-text\">\r\n<p class=\"text-1\">New from watch store, Introducing the</p>\r\n<p class=\"text-2\">Voyager Series</p>\r\n<a href=\"#\" class=\"btn btn-default\"><span>Shop now</span></a></div>\r\n<div class=\"banner-img\"><img src=\"themes/jms_freshshop/assets/img/header_banner_3.jpg\" width=\"348\" height=\"170\" /></div>\r\n</div>', 1, '_self', '{\"title\":\"0\"}', 13),
(273, 1, 0, 'link', '', '', 1, '_self', '{\"class\":\"btn btn-default btn-continue\"}', 14),
(274, 1, 0, 'link', '', '', 1, '_self', '', 12),
(275, 1, 0, 'link', '', '', 1, '_self', '', 13);

DROP TABLE IF EXISTS `_DB_PREFIX_jmsvermegamenu_lang`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jmsvermegamenu_lang` (
  `mitem_id` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `menulink` varchar(255) NOT NULL
) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jmsvermegamenu_lang` (`mitem_id`, `id_lang`, `name`, `menulink`) VALUES
(84, 1, 'Top 100 offers', 'indexen.php'),
(85, 1, 'New arrivals', ''),
(86, 1, 'Computers & Accessories', ''),
(87, 1, 'Cameras, audio & video', ''),
(88, 1, 'Mobiles & tablets', ''),
(89, 1, 'Movies, music & video games', 'index.php?controller=contact'),
(85, 2, 'Shop', ''),
(85, 3, 'Shop', ''),
(85, 5, 'Shop', ''),
(85, 8, 'Shop', ''),
(89, 2, 'Contact', 'index.php?controller=contact'),
(89, 3, 'Contact', 'index.php?controller=contact'),
(89, 5, 'Contact', 'index.php?controller=contact'),
(89, 8, 'Contact', 'index.php?controller=contact'),
(85, 4, 'Shop', ''),
(85, 6, 'Shop', ''),
(85, 7, 'Shop', ''),
(88, 2, 'Blog', ''),
(88, 3, 'Blog', ''),
(88, 4, 'Blog', ''),
(88, 5, 'Blog', ''),
(88, 6, 'Blog', ''),
(88, 7, 'Blog', ''),
(88, 8, 'Blog', ''),
(199, 1, 'TV & audio', '#'),
(199, 2, 'Elements', '#'),
(199, 3, 'Elements', '#'),
(199, 4, 'Elements', '#'),
(199, 5, 'Elements', '#'),
(199, 6, 'Elements', '#'),
(199, 7, 'Elements', '#'),
(199, 8, 'Elements', '#'),
(84, 2, 'Home', 'indexfr.php'),
(84, 3, 'Home', 'index.php'),
(84, 4, 'Home', 'index.php'),
(84, 5, 'Home', 'index.php'),
(84, 6, 'Home', 'index.php'),
(84, 7, 'Home', 'index.php'),
(84, 8, 'Home', 'index.php'),
(89, 4, 'Contact', 'index.php?controller=contact'),
(89, 6, 'Contact', 'index.php?controller=contact'),
(89, 7, 'Contact', 'index.php?controller=contact'),
(86, 2, 'Computers & Accessories', ''),
(86, 3, 'Computers & Accessories', ''),
(87, 2, 'Cameras, audio & video', ''),
(87, 3, 'Cameras, audio & video', ''),
(215, 1, 'Watches & eyewear', ''),
(215, 2, 'Watches & eyewear', ''),
(215, 3, 'Watches & eyewear', ''),
(216, 1, 'Car, Motorbike & Industrial', ''),
(216, 2, 'Car, Motorbike & Industrial', ''),
(216, 3, 'Car, Motorbike & Industrial', ''),
(217, 1, 'Office Solutions', ''),
(217, 2, 'Office Solutions', ''),
(217, 3, 'Office Solutions', ''),
(218, 1, 'Home & Tools', ''),
(218, 2, 'Home & Tools', ''),
(218, 3, 'Home & Tools', ''),
(219, 1, 'Accessories', ''),
(219, 2, 'Accessories', ''),
(219, 3, 'Accessories', ''),
(220, 1, 'Tablets', '#'),
(220, 2, 'Computers & Accessories 1', ''),
(220, 3, 'Computers & Accessories 1', ''),
(221, 1, 'Apple iPad', '#'),
(221, 2, 'Apple iPad', ''),
(221, 3, 'Apple iPad', ''),
(222, 1, 'Calling Tablets', '#'),
(222, 2, 'Calling Tablets', ''),
(222, 3, 'Calling Tablets', ''),
(223, 1, 'Android Tablets', '#'),
(223, 2, 'Android Tablets', ''),
(223, 3, 'Android Tablets', ''),
(224, 1, 'Popular Brands', '#'),
(224, 2, 'Popular Brands', ''),
(224, 3, 'Popular Brands', ''),
(225, 1, 'Apple', '#'),
(225, 2, 'Apple', ''),
(225, 3, 'Apple', ''),
(226, 1, 'Samsung', '#'),
(226, 2, 'Samsung', ''),
(226, 3, 'Samsung', ''),
(227, 1, 'Sony', '#'),
(227, 2, 'Sony', ''),
(227, 3, 'Sony', ''),
(228, 1, 'Micromax', '#'),
(228, 2, 'Micromax', ''),
(228, 3, 'Micromax', ''),
(229, 1, 'Intex', '#'),
(229, 2, 'Intex', ''),
(229, 3, 'Intex', ''),
(230, 1, 'Mobiles', '#'),
(230, 2, 'Mobiles', ''),
(230, 3, 'Mobiles', ''),
(231, 1, 'Smartphones', '#'),
(231, 2, 'Smartphones', ''),
(231, 3, 'Smartphones', ''),
(232, 1, 'Budget Mobiles', '#'),
(232, 2, 'Budget Mobiles', ''),
(232, 3, 'Budget Mobiles', ''),
(233, 1, '4G Phones', '#'),
(233, 2, '4G Phones', ''),
(233, 3, '4G Phones', ''),
(234, 1, 'Unboxed Mobiles', '#'),
(234, 2, 'Unboxed Mobiles', ''),
(234, 3, 'Unboxed Mobiles', ''),
(235, 1, '3G Phones', '#'),
(235, 2, '3G Phones', ''),
(235, 3, '3G Phones', ''),
(236, 1, 'Accessories', '#'),
(236, 2, 'Accessories', ''),
(236, 3, 'Accessories', ''),
(237, 1, 'Cases & Covers', '#'),
(237, 2, 'Cases & Covers', ''),
(237, 3, 'Cases & Covers', ''),
(238, 1, 'Screen Guards', '#'),
(238, 2, 'Screen Guards', ''),
(238, 3, 'Screen Guards', ''),
(239, 1, 'Chargers', '#'),
(239, 2, 'Chargers', ''),
(239, 3, 'Chargers', ''),
(240, 1, 'Hangers & Holders', '#'),
(240, 2, 'Hangers & Holders', ''),
(240, 3, 'Hangers & Holders', ''),
(241, 1, 'Extended Warranty', '#'),
(241, 2, 'Extended Warranty', ''),
(241, 3, 'Extended Warranty', ''),
(242, 1, 'banner', ''),
(242, 2, 'banner', ''),
(242, 3, 'banner', ''),
(243, 1, 'Movies & TV Shows', '#'),
(243, 2, 'Movies & TV Shows', ''),
(243, 3, 'Movies & TV Shows', ''),
(244, 1, 'All Movies & TV Shows', '#'),
(244, 2, 'All Movies & TV Shows', ''),
(244, 3, 'All Movies & TV Shows', ''),
(245, 1, 'Blu-ray', '#'),
(245, 2, 'Blu-ray', ''),
(245, 3, 'Blu-ray', ''),
(246, 1, 'All English', '#'),
(246, 2, 'All English', ''),
(246, 3, 'All English', ''),
(247, 1, 'All hindi', '#'),
(247, 2, 'All hindi', ''),
(247, 3, 'All hindi', ''),
(248, 1, 'Video games', '#'),
(248, 2, 'Video games', ''),
(248, 3, 'Video games', ''),
(249, 1, 'All consoles, game & accessories', '#'),
(249, 2, 'All consoles, game & accessories', ''),
(249, 3, 'All consoles, game & accessories', ''),
(250, 1, 'PC game', '#'),
(250, 2, 'PC game', ''),
(250, 3, 'PC game', ''),
(251, 1, 'Consoles', '#'),
(251, 2, 'Consoles', ''),
(251, 3, 'Consoles', ''),
(252, 1, 'Accessories', '#'),
(252, 2, 'Accessories', ''),
(252, 3, 'Accessories', ''),
(253, 1, 'Music', '#'),
(253, 2, 'Music', ''),
(253, 3, 'Music', ''),
(254, 1, 'All Music', '#'),
(254, 2, 'All Music', ''),
(254, 3, 'All Music', ''),
(255, 1, 'Intermationnal music', '#'),
(255, 2, 'Intermationnal music', ''),
(255, 3, 'Intermationnal music', ''),
(256, 1, 'Film songs', '#'),
(256, 2, 'Film songs', ''),
(256, 3, 'Film songs', ''),
(257, 1, 'Indian classical', '#'),
(257, 2, 'Indian classical', ''),
(257, 3, 'Indian classical', ''),
(258, 1, 'Musical instruments', '#'),
(258, 2, 'Musical instruments', ''),
(258, 3, 'Musical instruments', ''),
(259, 1, 'banner', ''),
(259, 2, 'banner', ''),
(259, 3, 'banner', ''),
(260, 1, 'Watches', '#'),
(260, 2, 'Watches', ''),
(260, 3, 'Watches', ''),
(261, 1, 'All watches', '#'),
(261, 2, 'All watches', ''),
(261, 3, 'All watches', ''),
(262, 1, 'Men\'s Watches', '#'),
(262, 2, 'Men\'s Watches', ''),
(262, 3, 'Men\'s Watches', ''),
(263, 1, 'Women\'t Watches', '#'),
(263, 2, 'Women\'t Watches', ''),
(263, 3, 'Women\'t Watches', ''),
(264, 1, 'Premium Watches', '#'),
(264, 2, 'Premium Watches', ''),
(264, 3, 'Premium Watches', ''),
(265, 1, 'Deals Watches', '#'),
(265, 2, 'Deals Watches', ''),
(265, 3, 'Deals Watches', ''),
(266, 1, 'Eyewear', '#'),
(266, 2, 'Eyewear', ''),
(266, 3, 'Eyewear', ''),
(267, 1, 'Men\'s sunglasses', '#'),
(267, 2, 'Men\'s sunglasses', ''),
(267, 3, 'Men\'s sunglasses', ''),
(268, 1, 'Women\'s sunglasses', '#'),
(268, 2, 'Women\'s sunglasses', ''),
(268, 3, 'Women\'s sunglasses', ''),
(269, 1, 'Spectacle frames', '#'),
(269, 2, 'Spectacle frames', ''),
(269, 3, 'Spectacle frames', ''),
(270, 1, 'All sunglasses', '#'),
(270, 2, 'All sunglasses', ''),
(270, 3, 'All sunglasses', ''),
(271, 1, 'Amazon fashion', '#'),
(271, 2, 'Amazon fashion', ''),
(271, 3, 'Amazon fashion', ''),
(272, 1, 'banner', ''),
(272, 2, 'banner', ''),
(272, 3, 'banner', ''),
(273, 1, 'Continue', '#'),
(273, 2, 'Continue', ''),
(273, 3, 'Continue', ''),
(274, 1, 'Accessories 2', '#'),
(274, 2, 'Accessories 2', ''),
(274, 3, 'Accessories 2', ''),
(275, 1, 'Accessories 3', '#'),
(275, 2, 'Accessories 3', ''),
(275, 3, 'Accessories 3', '');
";
