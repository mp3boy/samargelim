{*
* Project : loyalty
* @author Team EVER
* @copyright Team EVER
* @license   Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
* @link https://www.team-ever.com
*}

<div class="panel row">
    <h3><i class="icon icon-smile"></i> {l s='Loyalty module' mod='loyalty'}</h3>
    <div class="col-md-6">
        <a href="https://www.team-ever.com/contact" target="_blank"><img id="everlogo" src="{$everloyaltyimage_dir|escape:'htmlall':'UTF-8'}/ever.png" style="max-width: 120px;"></a>
        <p>
            <strong>{l s='Welcome to Loyalty !' mod='loyalty'}</strong><br />
            {l s='Thanks for using Team Ever\'s modules' mod='loyalty'}.<br />
        </p>
    </div>
    <div class="col-md-6">
        <h4>{l s='How to be first on Google pages ?' mod='loyalty'}</h4>
        <p>{l s='We have created the best SEO module, by working with huge websites and SEO societies' mod='loyalty'}</p>
        <p>
            <a href="https://www.team-ever.com/produit/prestashop-ever-ultimate-seo/" target="_blank">{l s='See the best SEO module for your Prestashop on our site' mod='loyalty'}</a>
        </p>
        <p>
            <a href="https://addons.prestashop.com/fr/seo-referencement-naturel/39489-ever-ultimate-seo.html" target="_blank">{l s='See the best SEO module Prestashop Addons' mod='loyalty'}</a>
        </p>
    </div>
</div>