<?php
class AdminSumarController extends ModuleAdminController{
    public function init(){
        parent::init();
    }

    public function initContent(){
       // parent::initContent();

        ?>

        <h2 style="margin-top:150px;">SUMAR COMENZI</h2>

 <table style="margin-top:30px;">
   <tr>
     <td width="200"><p style="line-height:30px"><strong>Ultimele 60 zile</strong></p></td>
     <td width="200"><p>Ridicare Sediu</p></td>
     <td width="200"><p>Curier Rapid</p></td>
     <td width="200"><p>Curier Rapid Bucuresti</p></td>
     <td width="200"><p>Posta Romana</p></td>
     <td width="200"><p><strong>Total</strong></p></td>
   </tr>
    <?php
        $start_day = 0;
        $j = 1;
        for($start_day = 0; $start_day <= 100; $start_day++){
           $date_start = date('Y-m-d', strtotime('-'.$start_day.' days', strtotime(date('Y-m-d'))));

           $sqll = "SELECT * FROM ps_orders  WHERE date_add LIKE '%".$date_start."%'"; //" and date_purchased <= '".$date_start." 23:59:59'  ";
           $sql_flat = Db::getInstance()->ExecuteS($sqll);


            $total_ridicare = 0;
            $total_numar_ridicare = 0;

            $total_posta = 0;
            $total_numar_posta = 0;

            $total_curier = 0;
            $total_numar_curier = 0;

            $total_curier_buc = 0;
            $total_numar_curier_buc = 0;

            $total_count = 0;
            $total_value = 0;

           foreach($sql_flat as $row){

               if($row['id_carrier'] == '24'){ //posta romana
                   $total_posta += $row['total_paid'];
                   $total_numar_posta++;

               }

               if($row['id_carrier'] == '25'){ //curier rapid
                   $total_curier += $row['total_paid'];
                   $total_numar_curier++;

               }

               if($row['id_carrier'] == '23'){ //curier rapid bucuresti
                   $total_curier_buc += $row['total_paid'];
                   $total_numar_curier_buc++;

               }

               if($row['id_carrier'] == '20'){ //ridicare sediu
                   $total_ridicare += $row['total_paid'];
                   $total_numar_ridicare++;

               }

                    $total_count++;
                    $total_value += $row['total_paid'];

           }

         ?>
         <tr  <?php if($j++%2 ==0) echo 'style="background-color:#599659; "; '; else echo 'style="background-color:white;"'; ?> >
           <td><?php echo $date_start ?></td>
           <td><strong><?php echo $total_ridicare,' lei',' [', $total_numar_ridicare,']' ?></strong></td>
           <td><strong><?php echo $total_curier,' lei',' [', $total_numar_curier,']' ?></strong></td>
           <td><strong><?php echo $total_curier_buc,' lei',' [', $total_numar_curier_buc,']' ?></strong></td>
           <td><strong><?php echo $total_posta,' lei',' [', $total_numar_posta,']' ?></strong></td>
           <td><strong><?php echo $total_value,' lei',' [', $total_count,']' ?></strong></td>
         </tr>
         <?php
        }

      ?>
   <tr>
     <td></td>
   </tr>
 </table>
 <?php

    }


}
?>
