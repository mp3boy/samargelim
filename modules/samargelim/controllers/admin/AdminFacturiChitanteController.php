<?php 
class AdminFacturichitanteController extends ModuleAdminController{
	public function init(){
		parent::init();
	}

	public function initContent(){
		parent::initContent();


		if (!isset($_GET['dels'])) {} else {
            Db::getInstance()->delete('f_facturi', 'id='.$_GET['dels'], 0,true, false);
        }

		$ccs=Db::getInstance()->ExecuteS("SELECT * FROM f_facturi ORDER BY id DESC");
		$i=1;

		$facturichitante="";
		foreach($ccs as $row) { 
	       	$facturichitante.='<tr bgcolor="#ffffff" style="color:#000000;border-top: 1px solid grey;" onMouseover="this.bgColor=\'#EEEEEE\'" onMouseout="this.bgColor=\'#FFFFFF\'">
	           <td width="50" align="left" style="padding:5px;">'.$i.'</td>
	           <td width="90" align="left" style="padding:5px;">Factura</td>
	           <td width="300" align="left" style="padding:5px;">'.$row['client'].'</td>
	           <td width="100" align="left" style="padding:5px;">'.$row['serie'].'</td>
	           <td width="90" align="left" style="padding:5px;">'.$row['nr_curent'].'</td>
	           <td width="90" align="left" style="padding:5px;">'.$row['data_factura'].'</td>
	           <td width="90" align="right" style="padding:5px;">'.$row['valoare'].'</td>
	           <td width="90" align="right" style="padding:5px;">';
	           if ($row['chitanta']=="0") { 	$facturichitante.="0";} else { 	$facturichitante.=$row['valoare']; }

	           	$facturichitante.='</td>
	           <td width="50" align="right" style="padding:5px;"><a href="/printareposta/factura_fiscala_restransa.php?idz='.$row['order_id'].'&chitanta=';
	           if ($row['chitanta']=="0") { 	$facturichitante.="0";} else { 	$facturichitante.="1";} 	
	           $facturichitante.='">Edit</a></td><td align="center" style="padding:5px;"><a href="?controller=AdminFacturichitante&dels='.$row['id'].'&token='.$this->token.'" onclick="return confirm(\'Vrei sa stegi factura fiscala pe numele '.$row['client'].'?\')">x</a></td>
	       </tr>';
	       $i++;                             
	    }


       $this->context->smarty->assign(
	        array(
	          'facturichitante' => $facturichitante,
	          'token' => $this->token
	        )
	    );

		$this->setTemplate('facturichitante.tpl');
	}

}