<?php 
class AdminSetaridocumenteController extends ModuleAdminController{
	public function init(){
		parent::init();
	}

	public function initContent(){
		parent::initContent();

		if (!isset($_POST['actualizare'])) {}
		else if ($_POST['actualizare']=="ACTUALIZEAZA") {
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['tva1']), "nume='tva1'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['tva2']), "nume='tva2'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['serief']), "nume='serief'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['seriec']), "nume='seriec'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['serien']), "nume='serien'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['urcf']), "nume='urcf'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['urcc']), "nume='urcc'", $limit = 0,false,true, false);
			Db::getInstance()->update('setari_nir', array('setare'=>$_POST['urcn']), "nume='urcn'", $limit = 0,false,true, false);
        }

		$tva1=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='tva1'");
		$tva2=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='tva2'");
		$serief=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='serief'");
		$urcf=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='urcf'");
		$seriec=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='seriec'");
		$urcc=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='urcc'");
		$serien=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='serien'");
		$urcn=Db::getInstance()->getValue("SELECT setare FROM setari_nir WHERE nume='urcn'");

		$this->context->smarty->assign(
	        array(
	          'tva1' => $tva1,
	          'tva2' => $tva2,
	          'serief' => $serief,
	          'urcf' => $urcf,
	          'seriec' => $seriec,
	          'urcc' => $urcc,
	          'serien' => $serien,
	          'urcn' => $urcn,
	          'token' => $this->token
	        )
	    );

		$this->setTemplate('setaridocumente.tpl');


	
	}

}