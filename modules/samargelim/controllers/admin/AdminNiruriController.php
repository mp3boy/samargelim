<?php 
class AdminNiruriController extends ModuleAdminController{
	public function init(){
		parent::init();
	}

	public function initContent(){
		parent::initContent();

		if (!isset($_GET['dels'])) {}  else {
	      Db::getInstance()->delete('nir', 'id='.$_GET['dels'], 0,true, false);
	      Db::getInstance()->delete('nir_products', 'id_nir='.$_GET['dels'], 0,true, false);
	    }


		$niruri=Db::getInstance()->ExecuteS("SELECT a.*,b.denumire FROM nir as a INNER JOIN furnizori as b ON b.id=a.id_furnizor AND a.activ=0 ORDER BY a.data_nir ASC");
		$this->context->smarty->assign(
	        array(
	          'niruri' => $niruri,
	          'token' => $this->token
	        )
	    );

		$this->setTemplate('niruri.tpl');
	}

}