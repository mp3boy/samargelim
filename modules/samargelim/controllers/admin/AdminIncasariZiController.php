<?php
class AdminIncasariZiController extends ModuleAdminController{
    public function init(){
        parent::init();
    }

    public function initContent(){
        parent::initContent();

        if (isset($_GET['incasari']) && $_GET['incasari'] === 'card'){
            if (isset($_POST['suma'])){

                Db::getInstance()->insert('incasari_card_total', array(
                    'dataadaugare' => date('Y-m-d'),
                    'suma'      => $_POST['suma'],
                    'activ'      => 1
                ),false,true,Db::INSERT,false);

                Db::getInstance()->update('incasari_cache',array(
                    'activ'=> 0
                ), 'cache=0',0,false, true, false);

                echo 'Incasarile card au fost resetate cu succes!';
                die();
            }

            $incasari_zi=Db::getInstance()->ExecuteS("SELECT * FROM incasari_cache WHERE cache = 0 AND activ = 1");

            $this->context->smarty->assign(
                array(
                    'data'  => date('d.m.Y'),
                    'incasari' => $incasari_zi,
                    'token' => $this->token,
                )
            );
            $this->setTemplate('incasari_zi_card.tpl');

        } else {
            if (isset($_POST['suma'])){

                Db::getInstance()->insert('incasari_cache_total', array(
                    'dataadaugare' => date('Y-m-d'),
                    'suma'      => $_POST['suma'],
                    'activ'      => 1
                ),false,true,Db::INSERT,false);

                Db::getInstance()->update('incasari_cache',array(
                    'activ'=> 0
                ), 'cache=1',0,false, true, false);

                echo 'Incasarile cache au fost resetate cu succes!';
                die();
            }

            $incasari_zi=Db::getInstance()->ExecuteS("SELECT * FROM incasari_cache WHERE cache = 1 AND activ = 1");

            $this->context->smarty->assign(
                array(
                    'data'  => date('d.m.Y'),
                    'incasari' => $incasari_zi,
                    'token' => $this->token
                )
            );

            $this->setTemplate('incasari_zi.tpl');
        }


    }

}
