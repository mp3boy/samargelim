<?php
class AdminIncasariTotalController extends ModuleAdminController{
    public function init(){
        parent::init();
    }

    public function initContent(){
        parent::initContent();

        if (isset($_POST['typeRequest'])){

            Db::getInstance()->delete('incasari_cache_total','',0,true, false);

            echo 'Incasarile cache au fost resetate cu succes!';
            die();
        }
        if (isset($_POST['typeRequestCard'])){

            Db::getInstance()->delete('incasari_card_total','',0,true, false);

            echo 'Incasarile card au fost resetate cu succes!';
            die();
        }

        $incasari_cache=Db::getInstance()->ExecuteS("SELECT * FROM incasari_cache_total WHERE activ = 1 ORDER BY id DESC");
        $incasari_card=Db::getInstance()->ExecuteS("SELECT * FROM incasari_card_total WHERE activ = 1 ORDER BY id DESC");

        $this->context->smarty->assign(
            array(
                'data_cache'  => date('d.m.Y'),
                'incasari_cache' => $incasari_cache,
                'incasari_card' => $incasari_card,
                'token' => $this->token
            )
        );

        $this->setTemplate('incasari_total.tpl');
    }

}
