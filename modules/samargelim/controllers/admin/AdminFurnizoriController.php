<?php 
class AdminFurnizoriController extends ModuleAdminController{
	public function init(){
		parent::init();
	}

	public function initContent(){
		parent::initContent();

		if (!isset($_POST['adaugaf'])) {}else if ($_POST['adaugaf']=="Adauga Furnizor") {
            $dats=date("Ymd");
			Db::getInstance()->insert('furnizori', array(
			    'denumire' => $_POST['denumire'],
			    'localitate'      => $_POST['localitate'],
			    'judet'      => $_POST['judet'],
			    'adresa'      => $_POST['adresa'],
			    'cf'      => $_POST['codfiscal'],
			    'nrc'      => $_POST['nrregcom'],
			    'banca'      => $_POST['banca'],
			    'localitate'      => $_POST['cont'],
			    'data'      => $dats,
			),false,true,Db::INSERT,false);
		}   

		 if (!isset($_GET['dels'])) {}
		 else { 
		 	Db::getInstance()->delete('furnizori','id ='.$_GET['dels'],0,true, false);
		 }    


		$furnizori=Db::getInstance()->ExecuteS("SELECT * FROM furnizori ORDER BY data DESC");
		
		 $this->context->smarty->assign(
	        array(
	          'furnizori' => $furnizori,
	          'token' => $this->token
	        )
	    );

		$this->setTemplate('furnizori.tpl');
	}

}
