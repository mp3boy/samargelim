<?php 
if(!defined('_PS_VERSION_'))
	exit;
class Samargelim extends Module{

	public function __construct(){
		$this->name='samargelim';
		$this->version='1.0.0';
		$this->author='Pety Frincu';
		$this->tab='administration';
		$this->secure_key=Tools::encrypt($this->name);
		$this->need_instance=0;
		$this->controllers=array('samargelim');
		$this->bootstrap=true;
		parent::__construct();
		$this->displayName=$this->l('Samargelim');
		$this->description=$this->l('Module to implement for George Apreutesei');
		$this->confirmUninstall=$this->l('Are you sure you want to uninstal this module?');
		$this->ps_version_compliancy=array('min'=>'1.6.0.0','max'=>_PS_VERSION_);
	}

	public function install(){
		return parent::install();
	}

	public function uninstall(){
		return parent::uninstall();
	}

	public function installModuleTab(){
		$tab=new Tab;
		$langs=language::getLanguages();
		foreach($langs as $lang)
			$tab->name[$lang['id_lang']] = 'Samargelim';
		$tab->module=$this->name;
		$tab->id_parent=0;
		$tab->class_name='AdminSam';
		return $tab->save;
	}

	public function uninstallModuleTab(){
		$id_tab = Tab::getIdFromClassName('AdminSam');
		if($id_tab){
			$tab=new Tab($id_tab);
			return $tab->delete;
		}
		return true;
	}
}
?>