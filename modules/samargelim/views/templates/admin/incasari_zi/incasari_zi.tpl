<style type="text/css">
    #orders {
        border-collapse: collapse;
        width: 50%;
    }
    #orders td,
    #orders th {
        border: 1px solid #ddd;
        padding: 8px;
        font-weight:bold;
    }
    #orders tr:nth-child(even){
        background-color: #f2f2f2;
    }
    #orders tr:hover {
        background-color: #ddd;
    }
    #orders th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    @media print {
        .header + table,
        #navbar,
        #buttoane{
            display:none;
        }
        #orders{
            color-adjust: exact;
            -webkit-print-color-adjust: exact;
            print-color-adjust: exact;
        }

        #orders tr:nth-child(even){
            background-color: #f2f2f2 !important;
        }
        #orders tr:hover {
            background-color: #ddd !important;
        }
        #orders th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50 !important;
            color: white !important;
        }
    }
</style>

{if !empty($incasari)}
    {assign var=val value=0}

<h1 style="margin-top:50px;margin-bottom:20px;">Incasari cache in data de {$data}</h1>
<table id="orders">
    <tr>
        <th>Nr comanda</th>
        <th>Felul comenzii</th>
        <th>Suma incasata</th>
    </tr>

    {foreach $incasari as $incasare}
    <tr>
        <td>{$incasare.idcomanda}</td>
        <td>{if $incasare.type == 0} Ridicare Sediu {else} Din Magazin {/if}</td>
        <td>{$incasare.suma} LEI</td>
    </tr>
        {assign var=val value=$val+$incasare.suma}
    {/foreach}

    <tr style="margin-top:20px;">
        <td style="border:0px;">&nbsp;</td>
        <td>Total incasat</td>
        <td>{$val} LEI</td>
    </tr>

    <tr style="margin-top:20px;background:transparent;" id="buttoane">
        <td style="border:0px;text-align:center;color:blue;cursor:pointer;padding-top:50px;" id="printthis">Printeaza</td>
        <td style="border:0px;background:transparent;">&nbsp;</td>
        <td style="border:0px;text-align:center;color:blue;cursor:pointer;padding-top:50px;" id="resetthis">Reseteaza si Salveaza</td>
    </tr>
</table>


{else}

<h1 style="margin-top:50px;margin-bottom:20px;">Azi nu ai nici o incasare cache</h1>

{/if}

<script>
    $('#printthis').click(function(){
        window.print();
    });

    $('#resetthis').click(function(){
        $.ajax({
            method: 'POST',
            url: window.location.href+'&token={$token}',
            data: {
                suma: {$val}
            },
            success: function(data){
                alert(data);
                window.location.reload();
            }
        });
    });
</script>
