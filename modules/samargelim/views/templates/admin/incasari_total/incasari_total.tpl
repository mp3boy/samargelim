<style type="text/css">
    #orders {
        border-collapse: collapse;
        width: 50%;
    }

    #orders td, #orders th {
        border: 1px solid #ddd;
        padding: 8px;
        font-weight:bold;
    }

    #orders tr:nth-child(even){
        background-color: #f2f2f2;
    }

    #orders tr:hover {
        background-color: #ddd;
    }

    #orders th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

    #resetform,
    #resetformcard{
        margin-left: 200px;
        color: blue;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
    }
</style>

{if !empty($incasari_cache)}
    {assign var=val value=0}

    <h1 style="margin-top:50px;margin-bottom:20px;">Incasari cache sediu <span id="resetform">Sterge Formular</span></h1>
    <table id="orders">
        <tr>
            <th>DATA</th>
            <th>SUMA</th>
        </tr>
        {foreach $incasari_cache as $incasare}
        <tr>
            <td>{$incasare.dataadaugare}</td>
            <td>{$incasare.suma} LEI</td>
        </tr>
            {assign var=val value=$val+$incasare.suma}
        {/foreach}

        <tr style="margin-top:20px;">
            <td style="border:0px;">&nbsp;</td>
            <td>Total incasat cache: {$val} LEI</td>
        </tr>

    </table>

{else}

    <h1 style="margin-top:50px;margin-bottom:20px;">Nu sunt incasari cache!</h1>

{/if}


{if !empty($incasari_card)}
    {assign var=val value=0}

    <h1 style="margin-top:50px;margin-bottom:20px;">Incasari card sediu <span id="resetformcard">Sterge Formular</span></h1>
    <table id="orders">
        <tr>
            <th>DATA</th>
            <th>SUMA</th>
        </tr>
        {foreach $incasari_card as $incasare}
            <tr>
                <td>{$incasare.dataadaugare}</td>
                <td>{$incasare.suma} LEI</td>
            </tr>
            {assign var=val value=$val+$incasare.suma}
        {/foreach}

        <tr style="margin-top:20px;">
            <td style="border:0px;">&nbsp;</td>
            <td>Total incasat card: {$val} LEI</td>
        </tr>

    </table>

{else}

    <h1 style="margin-top:50px;margin-bottom:20px;">Nu sunt incasari card!</h1>

{/if}




<script>
    $('#resetform').click(function(){
        if (confirm('Esti sigur ca vrei sa stergi datele?')){

            $.ajax({
                method: 'POST',
                url: window.location.href+'&token={$token}',
                data: {
                    typeRequest: 'reset'
                },
                success: function(data){
                    alert(data);
                    window.location.reload();
                }
            });

        }
    });
    $('#resetformcard').click(function(){
        if (confirm('Esti sigur ca vrei sa stergi datele?')){

            $.ajax({
                method: 'POST',
                url: window.location.href+'&token={$token}',
                data: {
                    typeRequestCard: 'reset'
                },
                success: function(data){
                    alert(data);
                    window.location.reload();
                }
            });

        }
    });
</script>
