<script type="text/javascript">
	function toggless1() {
		var ele = document.getElementById("addfurnizor");
		var text = document.getElementById("formfurnizor");
	    		text.style.display = "block";
	        ele.style.display = "none";
		} 
	function toggless2() {
		var ele = document.getElementById("addfurnizor");
		var text = document.getElementById("formfurnizor");
	    		text.style.display = "none";
	        ele.style.display = "block";
	} 
</script>
<style type="text/css">
	.oks tr:nth-child(2n+1):not(:first-child){
		background-color:#ffffff;
	}
	.oks tr:nth-child(2n){
		background-color:#CEEEF5;
	}

	.oks tr:hover{
		background-color:#EDEDED;
	}
	.oks tr td{
		padding:5px 10px;
		border: 1px solid #dddddd;
	}
</style>
<table class="oks" width="100%" cellpadding="2" cellspacing="2" style="background-color:#cedff6;">
	<tr bgcolor="#accdef" style="color:#10257f;">
	  <td align="left" width="260">Nume</td>
	  <td align="left" width="100">Localitate</td>
	  <td align="left" width="360">Adresa</td>
	  <td align="left" width="100">Judet</td>
	  <td align="left" width="120">Cod Fiscal</td>
	  <td align="left" width="120">Nr. Reg. Com.</td>
	  <td align="left" width="40">Sterge</td>
	</tr>

{foreach $furnizori as $furnizor}
<tr>
	<td>{$furnizor.denumire}</td>
	<td>{$furnizor.localitate}</td>
	<td>{$furnizor.adresa}</td>
	<td>{$furnizor.judet}</td>
	<td>{$furnizor.cf}</td>
	<td>{$furnizor.nrc}</td>
	<td align="center"><a href="?controller=Adminfurnizori&token={$token}&dels={$furnizor.id}" onclick="return confirm('Vrei sa stegi furnizorul?')" class="delete">x</a></td>
</tr>
{/foreach}



</table>
<br /><br />
<a href="javascript:toggless1();" id="addfurnizor">Adauga furnizor</a>
<div id="formfurnizor" style="display: none">
<form action="?controller=Adminfurnizori&token={$token}" method="post">
<table width="450" cellpadding="2" cellspacing="2" style="background-color:#e3f1fe;padding:20px;" align="left">
       <tr><td colspan="2" align="center"><b>FORMULAR DE ADAUGARE FURNIZOR</b></td></tr>
       <tr>
              <td width="30%" align="left">Denumire</td>
              <td width="70%" align="left"><input type="text" name="denumire" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Localitate</td>
              <td align="left"><input type="text" name="localitate" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Judet</td>
              <td align="left"><input type="text" name="judet" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Adresa</td>
              <td align="left"><input type="text" name="adresa" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Cod fiscal</td>
              <td align="left"><input type="text" name="codfiscal" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Nr. Reg. Com.</td>
              <td align="left"><input type="text" name="nrregcom" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Banca</td>
              <td align="left"><input type="text" name="banca" size="50"/></td>
       </tr>
       <tr>
              <td align="left">Cont</td>
              <td align="left"><input type="text" name="cont" size="50"/></td>
       </tr>
       <tr>
       <td colspan="2">&nbsp;</td>
       </tr>
       <tr>
       <td align="left"><a href="javascript:toggless2();">Inchide</a></td>
       <td align="center"><input type="submit" name="adaugaf" value="Adauga Furnizor"/></td>
       </tr>
</table>
<form>