<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

class LRPCustomerHelper
{

    /**
     * Update total customer points based on points transaction history
     * @param $id_customer
     * @throws Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function calculateAndUpdatePointsTotal($id_customer)
    {
        if ($id_customer == 0) {
            return false;
        }
        $lrp_history = new LRPHistoryModel();
        $lrp_history = $lrp_history->getByCustomerID($id_customer);
        $lrp_customer = new LRPCustomerModel();
        $lrp_customer->loadByCustomerID(Context::getContext()->customer->id);

        $points = 0;
        $lrp_history['result'] = array_reverse($lrp_history['result']);

        foreach ($lrp_history['result'] as &$history) {
            $history->expires_date = LRPHistoryHelper::getPointsExpiryDate($history->date_add, Configuration::get('lrp_points_expire_days'));
            $history->expires_days = LRPHistoryHelper::getDateDifferenceInDays(date('Y-m-d'), $history->expires_date);

            if ($history->type == LRPHistoryModel::TYPE_REWARDED) {
                if ($history->expires_days > 0) {
                    $points += $history->points;
                }
            } elseif ($history->type == LRPHistoryModel::TYPE_REDEEMED) {
                $points -= $history->points;
                IF ($points < 0) {
                    $points = 0;
                }
            }
        }
        $lrp_customer->points = (int)$points;
        $lrp_customer->save();
    }
}
