<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

class LRPDiscountHelper
{
    /**
     * Calculate how manny points to reward based on money
     * @param $money
     * @param $currency_iso
     * @param bool $round
     * @return float|int
     */
    public static function calculatePointsFromMoney($money, $currency_iso, $round = true)
    {
        $lrp_config = new LRPConfigModel($currency_iso);

        if ($lrp_config->getRatio() == 0) {
            return 0;
        }

        if ($round) {
            $points = floor($money / $lrp_config->getRatio());
        } else {
            $points = $money / $lrp_config->getRatio();
        }
        return $points;
    }

    /**
     * Applpy points impact based on Points Rules
     * @param $points
     * @param $id_product
     * @return float|int
     */
    public static function applyPointRules($points, $id_product)
    {
        $id_categories = Product::getProductCategories($id_product);
        $id_category_clause = '';

        if (!empty($id_categories)) {
            foreach ($id_categories as $id_category) {
                if ($id_category_clause != '') {
                    $id_category_clause .= ' OR ';
                }
                $id_category_clause .= 'FIND_IN_SET(' . (int)$id_category . ',id_categories)';
            }
        }

        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'lrp_rule
					WHERE (' . $id_category_clause . ')
					OR FIND_IN_SET(' . (int)$id_product . ', id_products)					
					AND ("' . pSQL(date('Y-m-d h:i:s')) . '" >= date_start AND "' . pSQL(date('Y-m-d h:i:s')) . '" <= date_end)
					ORDER BY date_end, date_start
					';

        $row = DB::getInstance()->getRow($sql);


        if (!empty($row)) {
            switch ($row['operator']) {
                case '+':
                    $points += $row['points'];
                    break;

                case '*':
                    $points = $points * $row['points'];
                    break;

                case '=':
                    $points = $row['points'];
                    break;
            }
        }
        return $points;
    }


    /**
     * Gett the value of points based on the products in the cart and the reward configuration
     * @param $obj_cart
     */
    public static function getCartPointsValue($obj_cart, $points_redeemed = 0)
    {
        $points = 0;
        $cart_total = $obj_cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, null, null, true);
        $cart_discount = $obj_cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, null, null, true);

        // do not apply rules when point have already been redeemed
        if ($points_redeemed > 0) {
            $cart_points = self::calculatePointsFromMoney($cart_total, Context::getContext()->currency->iso_code);
            return $cart_points;
        }

        //$cart_points = self::calculatePointsFromMoney($cart_total, Context::getContext()->currency->iso_code);
        $cart_products = $obj_cart->getProducts(true);
        foreach ($cart_products as $cart_product) {
            $product_points = self::calculatePointsFromMoney($cart_product['total_wt'], Context::getContext()->currency->iso_code, false);
            $product_points = self::applyPointRules($product_points, $cart_product['id_product']);
            $points += $product_points;
        }

        if ($points_redeemed > $points) {
            $points = 0;
        } else {
            $points = $points - $points_redeemed;
        }

        return $points;

        /*if ($cart_discount_total === null) {
            $cart_discount_total = $obj_cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, null, null, true);
        }

        $discount_points = self::calculatePointsFromMoney($cart_discount_total, Context::getContext()->currency->iso_code);  //we'll subtract these later in this function as we do not reward points on order total without discount

        $points = 0;
        $total = 0;
        $cart_products = $obj_cart->getProducts(true);
        foreach ($cart_products as $cart_product) {
            $product_points = 0;
            $product_points += self::calculatePointsFromMoney($cart_product['price_wt'], Context::getContext()->currency->iso_code, false);
			$product_points = self::applyPointRules($product_points, $cart_product['id_product']);
			$product_points *= $cart_product['quantity'];
            $points += $product_points;
        }
        $points -= floor($discount_points);
        return $points;*/
    }

    /**
     * Set the points redeemed for the current cart
     * @param $points
     * @throws Exception
     */
    public static function setPointsRedeem($points)
    {
        Context::getContext()->cookie->__set('lrp_points_redeemed', $points);

        if ((int)Context::getContext()->cart->id > 0 && (int)Context::getContext()->customer->id > 0) {
            $lrp_cart = new LRPCartModel();
            $lrp_cart->load(Context::getContext()->cart->id, Context::getContext()->customer->id);
            $lrp_cart->points_redeemed = $points;
            $lrp_cart->id_cart = (int)Context::getContext()->cart->id;
            $lrp_cart->id_customer = (int)Context::getContext()->customer->id;
            $lrp_cart->save();
        }
    }

    /**
     * Get the points redeemed for the current cart
     * @param bool $from_cookie
     * @return int
     */
    public static function getPointsRedeemed($from_cookie = true, $id_cart = null, $id_customer = null)
    {
        if ($from_cookie) {
            return (int)Context::getContext()->cookie->lrp_points_redeemed;
        } else {
            if (!is_null($id_cart) && !is_null($id_customer)) {
                $lrp_cart = new LRPCartModel();
                $lrp_cart->load($id_cart, $id_customer);

                if (!empty($lrp_cart->points_redeemed)) {
                    return (int)$lrp_cart->points_redeemed;
                } else {
                    return 0;
                }
            }
        }
    }

    /**
     * Get the monetary value of points
     */
    public static function getPointsMoneyValue($points, $point_value = null, $currency_iso = '')
    {
        if ($currency_iso == '') {
            $currency_iso = Context::getContext()->currency->iso_code;
        }
        $lrp_config = new LRPConfigModel($currency_iso);

        if ((float)$point_value > 0) {
            $discount = $points * $point_value;
        } else {
            $discount = $points * $lrp_config->getPointValue();
        }
        return $discount;
    }

    /**
     * Get the value of money in points
     * @param $money
     * @param $currency_iso
     * @return float|int
     */
    public static function getMoneyPointsValue($money, $currency_iso)
    {
        $lrp_config = new LRPConfigModel($currency_iso);

        if ($lrp_config->getPointValue() == 0) {
            return 0;
        }

        $points = floor($money / $lrp_config->getPointValue());
        return $points;
    }

    /**
     * @param int $idCustomer
     * @return int|mixed
     */
    public static function getPointsUsedInPreviousOrdersButNotRedeemed($idCustomer)
    {
        $sql = 'SELECT lc.points_redeemed FROM ' . _DB_PREFIX_ . 'lrp_cart lc
                INNER JOIN ' . _DB_PREFIX_ . 'orders o ON o.id_cart = lc.id_cart
					WHERE lc.id_customer = \''.$idCustomer.'\' AND o.current_state = 3 
					';
        $rows = DB::getInstance()->getRow($sql);

        $total = $rows['points_redeemed'];
        return floor($total);
    }

}
