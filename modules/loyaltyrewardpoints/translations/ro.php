<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{loyaltyrewardpoints}prestashop>lrpfrontcheckoutcontroller_0b0cdd6ead4e1c9d42d36b4c8ba3b8b9'] = 'Trebuie sa folosesti cel putin %s puncte';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_d9ddc8d356cb786f78642924b9097c72'] = 'Puncte de fidelitate';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_1d92ad0b2509542340c59fbd53aa2be7'] = 'Puncte';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_fef6efd4defb6a7b7ab134e9820a6ceb'] = 'trimite unui prieten';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_4b89a22eceecc5c77e1bc593394fe61f'] = 'Castiga [1]%d puncte de fidelitate[/1]';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_6f12cda041566222f267a834a8ce11c8'] = 'si prietenul tau va primi [1]%d puncte de fidelitate [/1] la prima comanda';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_f6492461f4a70e309b7a2e35b3da8be6'] = '%s pentru tine si %s pentru el!';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_129aac2de75ec1c2f740f6c3a16f7173'] = 'trimite linkul';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_6af62d24e1a225e2331054afc12a4087'] = 'Lista cu istoricul punctelor ';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_5d4710f9a8250b13164a82c94d5b00d1'] = 'Comanda';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_75dd5f1160a3f02b6fae89c54361a1b3'] = 'Puncte';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_5a8d4ab5205c5e4da758db20242668d8'] = 'Valoare puncte';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_004bf6c9a40003140292e97330236c53'] = 'Actiune';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_44749712dbec183e983dcd78a7736c41'] = 'Data';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_ad7a5dccbb87e6c4be1cfd32954711fc'] = 'Expira';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_b344d16193a2cd3a64dbbf73e05bc4bc'] = 'Recompensate';
$_MODULE['<{loyaltyrewardpoints}prestashop>account_d182bd3ccc300a64be9b3b816786c0b7'] = 'Rascumparate';
$_MODULE['<{loyaltyrewardpoints}prestashop>link_312506e680a023feb6cb62c5a465c455'] = 'Punctele mele de loialitate';
