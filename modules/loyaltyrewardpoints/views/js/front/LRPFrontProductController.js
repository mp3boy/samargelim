/*
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author Musaffar Patel <musaffar.patel@gmail.com>
 *  @copyright  2015-2018 Musaffar
 *  @version  Release: $Revision$
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Property of Musaffar Patel
 */

LRPFrontProductController = function(after_element, quickview) {
	let self = this;
	self.quickview = quickview;

	/**
	 * render the widget which displays points value for the product / selected attributes
 	 */
	self.renderWidget = function() {
		var query = $("#add-to-cart-or-refresh").serialize();

		var qty = 0;
		if ($("#quantity_wanted").length > 0) {
			qty = $("#quantity_wanted").val();
		}

		$.ajax({
			type: 'POST',
			url: baseDir + 'modules/loyaltyrewardpoints/ajax.php?route=lrpfrontproductcontroller&rand=' + new Date().getTime() + '&' + query,
			async: true,
			cache: false,
			data: {
				'id_product': $("input[name='id_product']").val(),
				'action' : 'renderwidget',
				'qty' : qty
			},
			//dataType: 'json',
			success: function (html_content) {
				$("#lrp-product-widget").remove();
				if (self.quickview) {
					$(html_content).insertAfter(".quickview.modal " + after_element);
				} else {
					$(html_content).insertAfter("section#main " + after_element);
				}
			}
		});
	};

	self.init = function() {
		self.renderWidget();
	};
	self.init();

	/**
	 * On Attributes changed
	 */
	prestashop.on('updatedProduct', function (event) {
		self.renderWidget();
	});

	if (self.quickview) {
		$("body").on("change", " #quantity_wanted", function () {
			self.renderWidget();
			return false;
		});
	}
};

var loyaltyPoints = function(){
    $.ajax({
		type: 'POST',
		headers: {"cache-control": "no-cache"},
		url: '/modules/loyaltyrewardpoints/ajax.php?route=lrpfrontproductcontroller&rand=' + new Date().getTime(),
		async: true,
		cache: false,
		data: 'action=getUserPoints&userEmail=' + prestashop.customer.email,
		success: function (result) {
			
				alert(result);
		
		},
		error: function(result){
		    console.log(result)
		}
	});
}





