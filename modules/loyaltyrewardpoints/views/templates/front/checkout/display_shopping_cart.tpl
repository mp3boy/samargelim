{*

* 2007-2017 Musaffar

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author Musaffar Patel <musaffar.patel@gmail.com>

*  @copyright  2017-2018 Musaffar Patel

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Property of Musaffar Patel

*}


<div id="lrp-points" class="card-block">
<div class="nuafisaincart"> 
    <hr>
    <h1 class="testimonialss" style="float:none;display:block">Factura</h1>
    <input type="checkbox" id="factura" onchange="if(this.checked == 1){ document.getElementById('hiddfields').style.display='block';}else{ document.getElementById('hiddfields').style.display='none'; }"/> <label for="factura">Doresc factura pe firma</label>
    
    <div id="hiddfields">
        <label class="inputLabel" for="camp1">Cumparator*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp1" id="camp1" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp2">Nr. Ord. Reg. Com.*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp2" id="camp2" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp3">AF / CUI*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp3" id="camp3" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp4">Sediul*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp4" id="camp4" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp5">Judet*</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp5" id="camp5" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp6">Cont</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp6" id="camp6" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
        <label class="inputLabel" for="camp7">Banca</label>
        <div class="input_wrap_b"></div><div class="input_wrap"><input type="text" name="camp7" id="camp7" size="33" class="accountInput" value=""></div><div class="input_wrap_e"></div>
        <br class="clearBoth">
    </div>
    
    <hr>
    <h1 class="testimonialss" style="float:none;display:block">Modalitate de plata</h1>
    <div class="bodyText2">Ramburs</div>
    
     <hr>
     <h1 class="testimonialss" style="float:none;display:block">Instructiuni speciale sau detalii comanda</h1>
     
     <div id="delivery">
        <textarea style="width:100%;height:100px;background: #f6e12f;resize:none;padding:10px;border:1px solid #000;" id="delivery_message" name="delivery_message">{$delivery_message}</textarea>
    </div>    
    <hr>


	{if $points_redeemed == 0}

		{if $points > 0}
        <h1 class="testimonialsse" style="float:none;display:block">Puncte de fidelitate</h1>

        <script src="/modules/loyaltyrewardpoints/views/js/front/LRPFrontCheckoutController.js"></script>

			<span id="lrp-summary" class="label">

				<i class="material-icons" style="margin-top: -2px;vertical-align: middle;">card_giftcard</i>

				{l s='Ai in cont %d puncte disponibile, valorand %d lei.' sprintf=[$points,$points*0.05] mod='loyaltyrewardpoints'}

				<a id="lrp-redeem-link" href="#lrp-redeem">{l s='' mod='loyaltyrewardpoints'}</a>

			</span>
           {assign var=numberofpoints value=$points/300|floor}
            {if $points >= 300}
			<div id="lrp-redeem-form">

				<!--<input name="points" type="hidden" value="0" size="6" maxlength="6"> -->

                <select name="points">
                    <option value="0">Alege numarul de puncte</option>
                    {for $xo=1 to $numberofpoints|floor}
                        <option value="{$xo*300}">{$xo*300}</option>
                    {/for}

                </select>

				<a href="#lrp-redeem" id="btn-lrp-redeem" class="btn btn-secondary">{l s='Foloseste' mod='loyaltyrewardpoints'}</a>

			</div>
            {/if}

		{/if}

	{else}
        <h1 class="testimonialsse" style="float:none;display:block">Puncte de fidelitate</h1>

        <script src="/modules/loyaltyrewardpoints/views/js/front/LRPFrontCheckoutController.js"></script>

		<strong>{$points_redeemed|escape:'htmlall':'UTF-8'} {l s='puncte folosite' mod='loyaltyrewardpoints'} ({$points_redeemed_value|escape:'htmlall':'UTF-8'})</strong>

		<a href="#" id="lrp-points-clear" class="material-icons" style="margin-top: -2px;vertical-align: middle;">clear</a>

	{/if}
</div>

</div>



<script>

	document.addEventListener("DOMContentLoaded", function (event) {

		$(function () {

			baseDir = "{$baseDir|escape:'html':'UTF-8'}";

			//let lrp_front_checkout_controller = new LRPFrontCheckoutController("#lrp-points");

		});

	});

	jQuery(document).ready(function(){
        jQuery.ajax({
            method:'POST',
            url: '/printareposta/cautareFactura.php',
            data: {
                emailClient: prestashop.customer.email
            },
            success: function(data){
                if (data) {
                    var obj = JSON.parse(data);
                    jQuery('#camp1').val(obj['buyer']);
                    jQuery('#camp2').val(obj['nrreg']);
                    jQuery('#camp3').val(obj['cui']);
                    jQuery('#camp4').val(obj['sediu']);
                    jQuery('#camp5').val(obj['judet']);
                    jQuery('#camp6').val(obj['cont']);
                    jQuery('#camp7').val(obj['banca']);
                }
            }
        });
    });

</script>

