{*

* 2007-2016 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author Musaffar Patel <musaffar.patel@gmail.com>

*  @copyright  2007-2018 Musaffar Patel

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Property of Musaffar Patel

*}



<div id="cart-points-summary">

	<div class="card" style="padding: 20px; margin-top: 20px;">

		<i class="material-icons" style="margin-top: -4px;vertical-align: middle;">card_giftcard</i>

		{l s="Vei castiga [1]%d de puncte [/1] finalizand aceasta comanda!" sprintf=[$points] tags=['<strong>'] mod='loyaltyrewardpoints'}<br>
		<a class="tableRow pseudolink2 m15px" style="font-weight:bold;color: #89a6cd;font-family: trebuchet ms;font-size: 11px;" href="/puncte-de-fidelitate-18">afla cum poti folosi punctele de fidelitate</a>
	</div>

</div>