{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2018 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<ul class="nav nav-tabs" id="myTab" role="tablist">
	<li class="nav-item active">
		<a class="nav-link" data-toggle="tab" href="#lrp-general-tab" role="tab">{l s='General' mod='loyaltyrewardpoints'}</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#lrp-rules-tab" role="tab">{l s='Point Rules' mod='loyaltyrewardpoints'}</a>
	</li>
</ul>

<div class="tab-content">
	{*<div class="lrp-breadcrumb"></div>*}
	<div class="tab-pane active pap-tab-pane" id="lrp-general-tab" role="tabpanel">
		<div class="canvas panel"></div>
	</div>
	<div class="tab-pane pap-tab-pane" id="lrp-rules-tab" role="tabpanel">
		<div class="canvas panel"></div>
	</div>
</div>

<script>
	$(document).ready(function () {
		breadcrumb = [];
		module_config_url = '{$module_config_url|escape:'quotes':'UTF-8'}';
		module_ajax_url_lrp = '{$module_ajax_url|escape:'quotes':'UTF-8'}';
		lrp_general_controller = new LRPAdminConfigGeneralController('#lrp-general-tab .canvas');
		lrp_rules_controller = new LRPAdminConfigRulesController('#lrp-rules-tab .canvas');
	});
</script>