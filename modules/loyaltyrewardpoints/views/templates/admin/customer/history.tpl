{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2018 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}
<script src="../modules/loyaltyrewardpoints/views/js/lib/popup.js"></script>
<script src="../modules/loyaltyrewardpoints/views/js/lib/tools.js"></script>
<script src="../modules/loyaltyrewardpoints/views/js/admin/customer/LRPAdminCustomerController.js"></script>
<div class="col-lg-6">
	<div id="lrp-customer-history" class="panel">
		<div class="panel-heading">
			<i class="icon-eye"></i>
			{l s='Loyalty Reward Points' mod='loyaltyrewardpoints'}
		</div>

		<div>
			<strong>{l s='Customer Points' mod='loyaltyrewardpoints'} : {$points|escape:'htmlall':'UTF-8'}</strong>
			<form id="lrp-customer-points-form">
				<select id="type" name="type" style="width: 100px; float: left;">
					<option value="add">{l s='Add' mod='loyaltyrewardpoints'}</option>
					<option value="subtract">{l s='Subtract' mod='loyaltyrewardpoints'}</option>
				</select>

				<select id="currency_iso_code" name="currency_iso_code" style="width: 100px; float: left;">
					{foreach from=$currencies item=currency}
						<option value="{$currency.iso_code|escape:'htmlall':'UTF-8'}">{$currency.iso_code|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>

				<div class="input-group" style="width: 280px; float: left; margin-left: 10px;">
					<input type="text" id="points" name="points" value="0" size="20">
					<span class="input-group-addon">{l s='points' mod='loyaltyrewardpoints'}</span>
					<span id="btn-lrp-update-smrg" class="btn btn-default pull-right" style="margin-left: 10px;">
						<i class="icon-save"></i>
						{l s='Update' mod='loyaltyrewardpoints'}
					</span>
				</div>

			</form>
		</div>
		<div style="clear:both"></div>

		<div id="lrp-history-list">

		</div>

	</div>
</div>

<script>
	id_customer = {$id_customer|escape:'htmlall':'UTF-8'};
	module_config_url = '{$module_config_url|escape:'quotes':'UTF-8'}';
</script>
