<?php

class Cart extends CartCore
{
    /**
     * @param bool $with_taxes
     * @param int $type
     * @param null $products
     * @param null $id_carrier
     * @param bool $use_cache
     * @return float|int
     */
    public function getOrderTotal($with_taxes = true, $type = Cart::BOTH, $products = null, $id_carrier = null, $use_cache = true)
    {
        $order_total = parent::getOrderTotal($with_taxes, $type, $products, $id_carrier, $use_cache);

        if (!Module::isEnabled('LoyaltyRewardPoints')) {
            return $order_total;
        }

        include_once(_PS_MODULE_DIR_ . '/loyaltyrewardpoints/lib/bootstrap.php');

        // auto redeem points for referred customers
        $id_referer = LRPReferralHelper::getReferrerIdFromCookie();
        if ($id_referer > 0 && $type == Cart::BOTH_WITHOUT_SHIPPING && $with_taxes) {
            if ((int)Context::getContext()->customer->id == 0) {
                $lrp_config = new LRPConfigModel(Context::getContext()->currency->iso_code);
                $cart_points_value = LRPDiscountHelper::getMoneyPointsValue($order_total, Context::getContext()->currency->iso_code);

                if ($lrp_config->getReferralFriendPoints() > $cart_points_value) {
                    LRPDiscountHelper::setPointsRedeem($cart_points_value);
                } else {
                    LRPDiscountHelper::setPointsRedeem($lrp_config->getReferralFriendPoints());
                }
            } else {
                if (LRPReferralHelper::getPaidOrderCount(Context::getContext()->customer->id) > 0 && !LRPReferralHelper::isGenuineNewCustomer(Context::getContext()->customer->id)) {
                    LRPDiscountHelper::setPointsRedeem(0);
                }
            }
        }

        $discount = LRPDiscountHelper::getPointsMoneyValue(LRPDiscountHelper::getPointsRedeemed());

        // get discount without tax if we need to
        if (!$with_taxes && !empty($this->_products[0])) {
            $tax_rate = $this->_products[0]['rate'];
            if ($tax_rate > 0) {
                $discount = $discount / (1 + ($tax_rate / 100));
            }
        }

        if ($type == Cart::ONLY_PRODUCTS) {
            return $order_total;
        }

        if ($type == Cart::BOTH) {
            // points discount should not be greater than the products total
            if ($discount > $order_total) {
                LRPDiscountHelper::setPointsRedeem(0);
                $discount = 0;
            }
            return Tools::math_round($order_total - $discount, 6);
        }

        if ($type == Cart::ONLY_DISCOUNTS) {
            $total_discount = $discount + $order_total;
            return Tools::math_round($total_discount, 6);
            //return Tools::math_round($discount, 6);
        }
        return $order_total;
    }
}