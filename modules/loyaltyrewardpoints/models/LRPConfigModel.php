<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class LRPConfigModel
{
    /** @var float Ratio */
    private $ratio;

    /** @var float Point Value */
    private $point_value;

    /** @var integer Order State for Validation */
    private $id_order_state_validation;

    /** @var integer Order State for Cancelled */
    private $id_order_state_cancel;

    /** @var integer boolean referral enabled */
    private $referral_enabled;

    /** @var float Points for referring a friend */
    private $referral_points;

    /** @var float points rewarded to referred friend */
    private $referral_friend_points;

    /** @var float Minimum Cart Value Required for redemption */
    private $min_cart_value;

    /** @var float Minimum Cart Value Required for redemption */
    private $min_points_redemption;

    /** @var Currency to load configuration for */
    private $currency_iso;

    /**
     * LRPConfigModel constructor.
     */
    public function __construct($currency_iso = '')
    {
        if ($currency_iso == '') {
            $currency_iso = Context::getContext()->currency->iso_code;
        }

        $this->currency_iso = $currency_iso;
        $this->setRatio(Configuration::get('lrp_ratio_' . $currency_iso));
        $this->setPointValue(Configuration::get('lrp_point_value_' . $currency_iso));
        $this->setIdOrderStateValidation(Configuration::get('lrp_id_order_state_validation'));
        $this->setIdOrderStateCancel(Configuration::get('lrp_id_order_state_cancel'));
        $this->setReferralEnabled(Configuration::get('lrp_referral_enabled'));
        $this->setReferralPoints(Configuration::get('lrp_referral_points_' . $currency_iso));
        $this->setReferralFriendPoints(Configuration::get('lrp_referral_friend_points_' . $currency_iso));
        $this->setMinCartValue(Configuration::get('lrp_min_cart_value_' . $currency_iso));
        $this->setMinPointsRedemption(Configuration::get('lrp_min_points_redemption_' . $currency_iso));
    }

    /**
     * @return int
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * @param int $ratio
     * @return LRPConfigModel
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
        return $this;
    }

    /**
     * @return int
     */
    public function getPointValue()
    {
        return $this->point_value;
    }

    /**
     * @param int $point_value
     * @return LRPConfigModel
     */
    public function setPointValue($point_value)
    {
        $this->point_value = $point_value;
        return $this;
    }

    /**
     * @return float
     */
    public function getIdOrderStateValidation()
    {
        return $this->id_order_state_validation;
    }

    /**
     * @param float $id_order_state_validation
     * @return LRPConfigModel
     */
    public function setIdOrderStateValidation($id_order_state_validation)
    {
        $this->id_order_state_validation = $id_order_state_validation;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdOrderStateCancel()
    {
        return $this->id_order_state_cancel;
    }

    /**
     * @param string $id_order_state_cancel
     * @return LRPConfigModel
     */
    public function setIdOrderStateCancel($id_order_state_cancel)
    {
        $this->id_order_state_cancel = $id_order_state_cancel;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferralEnabled()
    {
        return $this->referral_enabled;
    }

    /**
     * @param string $referral_enabled
     * @return LRPConfigModel
     */
    public function setReferralEnabled($referral_enabled)
    {
        $this->referral_enabled = $referral_enabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferralPoints()
    {
        return $this->referral_points;
    }

    /**
     * @param string $referral_points
     * @return LRPConfigModel
     */
    public function setReferralPoints($referral_points)
    {
        $this->referral_points = $referral_points;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferralFriendPoints()
    {
        return $this->referral_friend_points;
    }

    /**
     * @param string $referral_friend_points
     * @return LRPConfigModel
     */
    public function setReferralFriendPoints($referral_friend_points)
    {
        $this->referral_friend_points = $referral_friend_points;
        return $this;
    }

    /**
     * @return string
     */
    public function getMinCartValue()
    {
        return $this->min_cart_value;
    }

    /**
     * @return float
     */
    public function getMinPointsRedemption()
    {
        return $this->min_points_redemption;
    }

    /**
     * @param string $min_cart_value
     * @return LRPConfigModel
     */
    public function setMinCartValue($min_cart_value)
    {
        $this->min_cart_value = $min_cart_value;
        return $this;
    }

    /**
     * @param $min_points_redemption
     * @return $this
     */
    public function setMinPointsRedemption($min_points_redemption)
    {
        $this->min_points_redemption = $min_points_redemption;
        return $this;
    }
}
