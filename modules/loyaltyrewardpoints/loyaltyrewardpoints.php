<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_ . '/loyaltyrewardpoints/lib/bootstrap.php');

class LoyaltyRewardPoints extends Module
{
    const __MA_MAIL_DELIMITOR__ = ',';

    public $module_folder = 'loyaltyrewardpoints';
    public $module_file = __FILE__;
    public $base_url = '';

    public function __construct()
    {
        $this->name = 'loyaltyrewardpoints';
        $this->tab = 'others';
        $this->version = '2.0.8';
        $this->author = 'Musaffar Patel';
        parent::__construct();
        $this->displayName = $this->l('Loyalty Reward Points');
        $this->description = $this->l('Loyalty Reward Points');
        $this->module_key = 'ae5ac4d263462fae8497791a3626c91a';
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->file = __FILE__;
        $this->bootstrap = true;

        $this->base_url = Tools::getShopProtocol() . Tools::getShopDomain() . __PS_BASE_URI__;
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('backOfficeHeader')
            || !$this->registerHook('backOfficeFooter')
			|| !$this->registerHook('displayHeader')
			|| !$this->registerHook('displayShoppingCart')			
            || !$this->registerHook('displayShoppingCartFooter')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('displayAdminOrderLeft')
			|| !$this->registerHook('displayCustomerAccount')
			|| !$this->registerHook('displayAdminCustomers')
            || !$this->registerHook('actionDispatcher')
            || !$this->registerHook('actionCustomerAccountAdd')
            || !$this->registerHook('displayFooterProduct')
            || !$this->registerHook('displayProductAdditionalInfo')
            || !$this->registerHook('actionAuthentication')
            || !$this->registerHook('displayAfterCarrier')
            || !$this->installModule()
        ) {
            return false;
        }
        return true;
    }

    private function installModule()
    {
        return LRPInstall::installDB();
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    /* Call set media for all the various controllers in this module.  Each controller will decide if the time is appropriate for queuing it's css and js */
    public function setMedia()
    {
        (new LRPAdminConfigMainController($this))->setMedia();
        (new LRPAdminOrderController($this))->setMedia();
        (new LRPAdminCustomerController($this))->setMedia();
        (new LRPFrontCheckoutController($this))->setMedia();
        (new LRPFrontProductController($this))->setMedia();
        (new LRPFrontCustomerController($this))->setMedia();
    }

    public function getContent()
    {
        return $this->route();
    }

    public function hookHeader($params)
    {
        $this->setMedia();
    }

    public function hookBackOfficeHeader($params)
    {
        $this->setMedia();
    }

    public function hookDisplayShoppingCart($params)
    {
        return (new LRPFrontCheckoutController($this))->hookDisplayShoppingCart($params);
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        (new LRPFrontCheckoutController($this))->hookActionOrderStatusPostUpdate($params);
    }

    public function hookDisplayAdminOrderLeft($params)
    {
        return (new LRPAdminOrderController($this))->render();
    }

    /**
     * Customer account Page
     * @param $params
     */
    public function hookDisplayCustomerAccount($params)
    {
        return LoyaltyRewardPointsCustomerAccountModuleFrontController::renderIndexBlock($this);
    }

    public function hookDisplayAdminCustomers($params)
    {
        return (new LRPAdminCustomerController($this))->render();
    }

    /**
     * Set referral link when landing one site if qwe need to
     * @param $params
     * @throws Exception
     */
    public function hookActionDispatcher($params)
    {
        if ((int)Tools::getValue('rid') > 0) {
            LRPReferralHelper::setRefererCookie(Tools::getValue('rid'));
        }

        if (!empty(Context::getContext()->customer->id)) {
            if (Context::getContext()->customer->id > 0) {
                LRPCustomerHelper::calculateAndUpdatePointsTotal(Context::getContext()->customer->id);
            }
        }
    }

    /**
     * @param $params
     */
    public function hookActionCustomerAccountAdd($params)
    {
        (new LRPFrontCustomerController($this))->hookActionCustomerAccountAdd($params);
    }

    /**
     * @param $params
     */
    public function hookActionAuthentication($params) {
        (new LRPFrontCustomerController($this))->hookActionAuthentication($params);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayShoppingCartFooter($params)
    {
        return (new LRPFrontCheckoutController($this))->hookDisplayShoppingCartFooter($params);
    }

    public function hookDisplayAfterCarrier($params){
         return (new LRPFrontCheckoutController($this))->hookDisplayAfterCarrier($params);
    }
    
    /**
     * @param $params
     */
    public function hookDisplayFooterProduct($params)
    {
        return (new LRPFrontProductController($this))->hookDisplayFooterProduct($params);
    }

    /**
     * Product Quick View
     * @param $params
     * @return string
     */
    public function hookDisplayProductAdditionalInfo($params)
    {
        if (Tools::getValue('action') == 'quickview') {
            $params['quickview'] = 1;
            return (new LRPFrontProductController($this))->hookDisplayFooterProduct($params);
        }
    }

    public function route()
    {
        switch (Tools::getValue('route')) {
            case 'lrpadminconfiggeneralcontroller':
                return (new LRPAdminConfigGeneralController($this))->route();

            case 'lrpadminconfigrulescontroller':
                return (new LRPAdminConfigRulesController($this))->route();

            case 'lrpadmincustomercontoller':
                return (new LRPAdminCustomerController($this))->route();
            default:
                return (new LRPAdminConfigMainController($this))->route();
        }
    }
}
