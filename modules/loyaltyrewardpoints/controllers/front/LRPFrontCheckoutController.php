<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

class LRPFrontCheckoutController extends LRPControllerCore
{
    protected $sibling;

    private $route = 'lrpfrontcheckoutcontroller';

    public function __construct(&$sibling = null)
    {
        parent::__construct($sibling);
        if ($sibling !== null) {
            $this->sibling = &$sibling;
        }
    }

    public function setMedia()
    {
        if (Tools::getValue('controller') == 'cart') {
            Context::getContext()->controller->addJS($this->sibling->_path . 'views/js/front/LRPFrontCheckoutController.js');
            Context::getContext()->controller->addCSS($this->sibling->_path . 'views/css/front/front.css');
        }
    }

    /**
     * @param $params
     */
    public function hookActionOrderStatusPostUpdate($params)
    {
        if (!isset($params['id_order']) || !isset($params['newOrderStatus']->id)) {
            return false;
        }

        // store the referral cookie into database, as the order may be marked as paid by admin on a different device
        $id_referer = LRPReferralHelper::getReferrerIdFromCookie();
        if ($id_referer > 0) {
            $lrp_referral_cookie = new LRPReferralCookieModel();
            $lrp_referral_cookie->id_customer = Context::getContext()->customer->id;
            $lrp_referral_cookie->id_referrer = (int)$id_referer;
            $lrp_referral_cookie->id_cart = (int)Context::getContext()->cart->id;
            $lrp_referral_cookie->save();
        }

        if ($params['newOrderStatus']->paid == 0 && $params['newOrderStatus']->id != Configuration::get('lrp_id_order_state_cancel')) {
            return false;
        }

        if (Configuration::get('lrp_id_order_state_validation') != -1 && $params['newOrderStatus']->id != Configuration::get('lrp_id_order_state_validation') && $params['newOrderStatus']->id != Configuration::get('lrp_id_order_state_cancel')) {
            return false;
		}

        $points_redeemed = 0;
        $order = new Order($params['id_order']);

        if (empty(Context::getContext()->cart->id) && !empty($params['id_order'])) {
            $cart = new Cart($order->id_cart);
            $points_redeemed = LRPDiscountHelper::getPointsRedeemed(false, $cart->id, $cart->id_customer);
            $cart_points_to_reward = LRPDiscountHelper::getCartPointsValue($cart, $points_redeemed);
        } else {
            $cart = Context::getContext()->cart;
            $points_redeemed = LRPDiscountHelper::getPointsRedeemed(false, $cart->id, $cart->id_customer);
            $cart_points_to_reward = LRPDiscountHelper::getCartPointsValue($cart, $points_redeemed);
        }

        // Order is being cancelled
        if ($params['newOrderStatus']->id == Configuration::get('lrp_id_order_state_cancel')) {
            // How many points were rewarded for this order before?
            $lrp_history = new LRPHistoryModel();
            $lrp_history->loadByOrder($params['id_order'], LRPHistoryModel::TYPE_REWARDED);
            if (empty($lrp_history->id_order)) {
                return false;
            }

            if ($lrp_history->points > 0) {
                LRPHistoryHelper::redeemPoints($params['id_order'], $cart->id_customer, $lrp_history->points, '', $cart->id_currency);
            }
            return true;
        }

        // do not reward same order twice
        $lrp_history = new LRPHistoryModel();
        $lrp_history->loadByOrder($params['id_order'], LRPHistoryModel::TYPE_REWARDED);        
        if (!empty($lrp_history->id_order)) {
            return false;
        }

        $lrp_config = new LRPConfigModel(Context::getContext()->currency->iso_code);

        // Reward points for purchase
        if ($cart_points_to_reward > 0) {
            LRPHistoryHelper::rewardPoints($params['id_order'], $cart->id_customer, $cart_points_to_reward, '', Context::getContext()->currency->iso_code);
        }

        // Redeem points from customer records
        if ($points_redeemed > 0) {
            LRPHistoryHelper::redeemPoints($params['id_order'], $cart->id_customer, $points_redeemed, '', Context::getContext()->currency->iso_code);
        }

        //Was this customer referred?  If so, and, customer does not have previously fulfilled orders then reward the referer
        $id_referrer = LRPReferralHelper::getReferrerIdFromStorage($cart->id_customer, $cart->id);
        if ($id_referrer > 0) {
            $customer = new Customer($cart->id_customer);
            $referrer_customer = new Customer($id_referrer);

            if ($referrer_customer->email != $customer->email) {
                if (LRPReferralHelper::getPaidOrderCount($cart->id_customer) == 1) {
                    LRPHistoryHelper::rewardPoints(0, $id_referrer, $lrp_config->getReferralPoints(), 'referral_completed', Context::getContext()->currency->iso_code);
                    LRPReferralCookieModel::deleteByCustomer($cart->id_customer);
                }
            }
        }
    }

    /**
     * Redeem points in the cart
     */
    public function processRedeemPoints()
    {
        $json_return = array(
            'message' => ''
        );

        $lrp_customer = new LRPCustomerModel();
        $redeem_points = (int)Tools::getValue('points');

        if (empty(Context::getContext()->cart->id)) {
            return false;
        }

        $id_cart = Context::getContext()->cart->id;
        $id_cuastomer = Context::getContext()->cart->id_customer;
        $lrp_customer->loadByCustomerID($id_cuastomer);

        if ($redeem_points <= 0) {
            $redeem_points = 0;
        }

        if ($redeem_points > $lrp_customer->points) {
            $redeem_points = $lrp_customer->points;
        }

        $cart_total = Context::getContext()->cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);

        //@todo: min cart value check

        $min_points_redemption = Configuration::get('lrp_min_points_redemption_' . Context::getContext()->currency->iso_code);

        if ($min_points_redemption > 0 && $redeem_points < $min_points_redemption) {
            $json_return = array(
                'message' => sprintf($this->l('Trebuie sa ai cel putin %s de puncte pentru a aplica reducerea'), $min_points_redemption)
            );
            die (json_encode($json_return));
        }

        //$cart_points_value = LRPDiscountHelper::getCartPointsValue(Context::getContext()->cart);
        $redeem_points_value = LRPDiscountHelper::getPointsMoneyValue($redeem_points, Context::getContext()->currency->iso_code);

        // do not allow customer to redeem more points than the cart is worth
        if ($redeem_points_value > $cart_total) {
            $redeem_points = LRPDiscountHelper::getMoneyPointsValue($cart_total, Context::getContext()->currency->iso_code);
        }

        if ($redeem_points > 0) {
            LRPDiscountHelper::setPointsRedeem($redeem_points);
        }
        die (json_encode($json_return));
    }

    /**
     * Display the points redemption form
     * @param $params
     * @return mixed
     */
    public function hookDisplayShoppingCart($params)
    {
        $lrp_customer = new LRPCustomerModel();
        $lrp_customer->loadByCustomerID(Context::getContext()->customer->id);

        if (!empty($lrp_customer->id_customer)) {
            $points = $lrp_customer->points;
        } else {
            $points = 0;
        }

        $points_redeemed = LRPDiscountHelper::getPointsRedeemed();

        Context::getContext()->smarty->assign(array(
            'points' => $points,
            'points_redeemed' => $points_redeemed,
            'points_redeemed_value' => Tools::displayPrice(LRPDiscountHelper::getPointsMoneyValue($points_redeemed)),
            'module_config_url' => $this->module_config_url,
            'baseDir' => __PS_BASE_URI__
        ));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/front/checkout/display_shopping_cart.tpl');
    }
    
     public function hookDisplayAfterCarrier($params)
    {
            
        $lrp_customer = new LRPCustomerModel();
        $lrp_customer->loadByCustomerID(Context::getContext()->customer->id);

        if (!empty($lrp_customer->id_customer)) {

            $finalPoints = LRPDiscountHelper::getPointsUsedInPreviousOrdersButNotRedeemed($lrp_customer->id_customer);

            if ($finalPoints > 0) {
                $points = $lrp_customer->points - $finalPoints;
            } else {
                $points = $lrp_customer->points;
            }

        } else {
            $points = 0;
        }

        $points_redeemed = LRPDiscountHelper::getPointsRedeemed();

        Context::getContext()->smarty->assign(array(
            'points' => $points,
            'points_redeemed' => $points_redeemed,
            'points_redeemed_value' => Tools::displayPrice(LRPDiscountHelper::getPointsMoneyValue($points_redeemed)),
            'module_config_url' => $this->module_config_url,
            'baseDir' => __PS_BASE_URI__
        ));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/front/checkout/display_shopping_cart.tpl');
    }

    /**
     * Display information about how many points the current cart is worth
     */
    public function hookDisplayShoppingCartFooter($params)
    {
        Context::getContext()->smarty->assign(array(
            'points' => LRPDiscountHelper::getCartPointsValue(Context::getContext()->cart, LRPDiscountHelper::getPointsRedeemed()),
        ));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/front/checkout/display_shopping_cart_footer.tpl');
    }

    

    /**
     * Clear any points redeemed ion the cart and reset to 0
     */
    public function processClearPoints()
    {
        LRPDiscountHelper::setPointsRedeem(0);
    }

    public function renderPointsSummary()
    {
        return $this->hookDisplayShoppingCartFooter(array());
    }

    public function route()
    {
        switch (Tools::getValue('action')) {
            case 'processredeempoints':
                die($this->processRedeemPoints());

            case 'processclearpoints':
                die($this->processClearPoints());

            case 'renderpointssummary':
                die($this->renderPointsSummary());
                die();
        }
    }
}
