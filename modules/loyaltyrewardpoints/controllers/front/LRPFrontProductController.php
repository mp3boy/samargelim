<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

class LRPFrontProductController extends LRPControllerCore
{
    protected $sibling;

    private $route = 'lrpfrontproductcontroller';

    public function __construct(&$sibling = null)
    {
        parent::__construct($sibling);
        if ($sibling !== null) {
            $this->sibling = &$sibling;
        }
    }

    public function setMedia()
    {
        $allowed = array('index', 'product', 'category');
        $controller = Tools::getValue('controller');

        if (in_array($controller, $allowed)) {
            Context::getContext()->controller->addJS($this->sibling->_path . 'views/js/front/LRPFrontProductController.js');
            Context::getContext()->controller->addCSS($this->sibling->_path . 'views/css/front/front.css');
        }
    }

    /**
     * Display the amount of points this product is worth
     * @param $params
     * @return string
     */
    public function hookDisplayFooterProduct($params)
    {
        $action = '';

        if (Context::getContext()->controller->php_self != 'product') {
            return false;
        }

        if (!empty($params['quickview'])) {
            $action = 'quickview';
        }

        Context::getContext()->smarty->assign(array(
            'action' => $action,
            'module_config_url' => $this->module_config_url,
            'baseDir' => __PS_BASE_URI__
        ));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/front/product/footer.tpl');
    }

    /**
     * Render widget
     * @return string
     */
    public function renderWidget()
    {
        $product_info = LRPProductHelper::getProductInfo(Tools::getValue('id_product'), Tools::getValue('group'));

        if ((float)$product_info['price'] == 0) {
            return false;
        }

        $points = LRPDiscountHelper::calculatePointsFromMoney($product_info['price'] * (int)Tools::getValue('qty'), Context::getContext()->currency->iso_code);
        $points = LRPDiscountHelper::applyPointRules($points, $product_info['id_product']);
        $points_money_value = LRPDiscountHelper::getPointsMoneyValue($points);
        Context::getContext()->smarty->assign(array(
            'points' => $points,
            'points_money_value' => Tools::displayPrice($points_money_value * (int)Tools::getValue('qty')),
        ));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/front/product/widget.tpl');
    }

    public function route()
    {
        switch (Tools::getValue('action')) {
            case 'renderwidget':
                die($this->renderWidget());
                break;
            case 'getUserPoints':
                $this->getUserPoints(Tools::getValue('userEmail'));
                break;
            case 'getCategories':
                $this->getSamargelimCategories();
                break;    
            case 'getBrosure':
                $this->getBrosure();
                break;
            case 'updateBrosure':
                $this->updateBrosure();
                break;
        }
    }
    
    private function getUserPoints($email){
        $cart = Context::getContext()->cart;
        $points_redeemed = LRPDiscountHelper::getPointsRedeemed(false, $cart->id, $cart->id_customer);
        $cart_points_to_reward = LRPDiscountHelper::getCartPointsValue($cart, $points_redeemed);
        echo floor($cart_points_to_reward);
    }
    
    private function getSamargelimCategories(){
        $cats = Category::getCategories( 1, true, false);
        $categoriiPrincipale = [];
        $categoriiSecundare  = [];
        $categoryMenu = '';
        
       
       
        foreach($cats as $cat){
            if ($cat['id_parent'] === "10000"){
                 $categoriiPrincipale[] = ['id_category' => $cat['id_category'], 'link_category'=> $cat['id_category'].'-'.$cat['link_rewrite'], 'name_category' => $cat['name']];
            }
        }
        
        foreach($categoriiPrincipale as $cat){
            $x=0;
            $megamenu = '';
            $carret = '';
            $hasmega = '';
            $subcat  = '';
            foreach($cats as $cat1){
                if ($cat1['id_parent'] === $cat['id_category']){
                    $x++;
                    $subcat .= '<td style=\'display: inline-block;width: 25%;\'><a href="'.$cat1['id_category'].'-'.$cat1['link_rewrite'].'">'.$cat1['name'].'</a></td>';
                   
                }
            }
            
            if ($x !== 0){
                $hasmega = 'mega';
                $carret = '<em class="caret"></em>';
                $megamenu .= '<div class="nav-child dropdown-menu mega-dropdown-menu"><div class="mega-dropdown-inner"><div class="row"><div class="mega-col-nav col-sm-12" data-width="12"><div class="mega-inner"><ul class="mega-nav"><li class="menu-item" data-id="281" data-level="1" data-title="1">';
                $megamenu .= '<a href="#" target="_self">'.$cat['name_category'].'</a><div class="mod-content"><table><tbody><tr>';
                $megamenu .= $subcat;
                $megamenu .= '</tr></tbody></table><br><img src="http://samargelim1.frincu.info/img/cms/RMJLR1017_natural_blue_(1_of_1)__43949-1326986552.png" alt="" width="200" height="133"></div></li></ul></div></div></div></div></div> ';
            }
                
    


           
            
            $categoryMenu .='<li class="menu-item '.$hasmega.'" data-id="'.$cat['id_category'].'" data-level="0" data-title="1"><a href="http://samargelim.ro/'.$cat['link_category'].'" target="_self">'.$cat['name_category'].' '.$carret.'</a>'.$megamenu.'</li>';
        }
        

        echo $categoryMenu;
    }

    private function getBrosure()
    {
        $sql = "SELECT value FROM ps_configuration WHERE name='SAMARGELIM_BROSURA'";
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        echo $result;
    }

    private function updateBrosure()
    {
        $sql = "UPDATE ps_configuration SET value = value - 1 WHERE name='SAMARGELIM_BROSURA'";
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
}
