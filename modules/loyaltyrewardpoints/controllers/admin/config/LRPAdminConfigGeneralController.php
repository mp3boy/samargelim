<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2018 Musaffar Patel
 * @license   LICENSE.txt
 */

class LRPAdminConfigGeneralController extends LRPControllerCore
{
    protected $sibling;

    private $route = 'lrpadminconfiggeneralcontroller';

    public function __construct(&$sibling = null)
    {
        parent::__construct($sibling);
        if ($sibling !== null) {
            $this->sibling = &$sibling;
        }
    }

    public function setMedia()
    {
    }

    /**
     * Generate group of fields for specific currency for the reward configuration
     * @param $currency
     */
    private function getCurrencyRewardFieldsGroup($currency)
    {
        return array(
            array(
                'label' => $this->sibling->l('Ratio', $this->route),
                'type' => 'text',
                'name' => 'ratio_' . $currency['iso_code'],
                'prefix' => $currency['sign'],
                'suffix' => $this->sibling->l('= 1 reward point.', $this->route),
                'size' => 10,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->sibling->l('1 point =', $this->route),
                'name' => 'point_value_' . $currency['iso_code'],
                'prefix' => $currency['sign'],
                'suffix' => $this->sibling->l('for the discount.', $this->route),
            ),
            array(
                'type' => 'text',
                'label' => $this->sibling->l('Points for referring customer', $this->route),
                'name' => 'referral_points_' . $currency['iso_code']
            ),
            array(
                'type' => 'text',
                'label' => $this->sibling->l('Points for friend', $this->route),
                'name' => 'referral_friend_points_' . $currency['iso_code']
            ),
            array(
                'type' => 'text',
                'label' => $this->sibling->l('Minimum cart value', $this->route),
                'name' => 'min_cart_value_' . $currency['iso_code'],
                'prefix' => $currency['sign'],
                'suffix' => $this->sibling->l('inc. Tax')
            ),
            array(
                'type' => 'text',
                'label' => $this->sibling->l('Minimum Points required for redemption', $this->route),
                'name' => 'min_points_redemption_' . $currency['iso_code']
            )
        );
    }

    public function render()
    {
        $order_states = OrderState::getOrderStates($this->context->language->id);

        $order_states[] = array(
            'name' => "Any Order State flagged as 'Paid'",
            'id_order_state' => -1
        );

        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->sibling->l('General', $this->route),
                'icon' => 'icon-question'
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->sibling->l('Points are awarded when the order is', $this->route),
                    'name' => 'id_order_state_validation',
                    'options' => array(
                        'query' => $order_states,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->sibling->l('Points are cancelled when the order is', $this->route),
                    'name' => 'id_order_state_cancel',
                    'options' => array(
                        'query' => $order_states,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->sibling->l('Customer Referral Enabled?', $this->route),
                    'name' => 'referral_enabled',
                    'options' => array(
                        'query' => array(
                            array(
                                'referral_enabled' => 0,
                                'name' => 'No'
                            ),
                            array(
                                'referral_enabled' => 1,
                                'name' => 'Yes'
                            )
                        ),
                        'id' => 'referral_enabled',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->sibling->l('Points expire after', $this->route),
                    'name' => 'points_expire_days',
                    'prefix' => $this->sibling->l('days', $this->route),
                    'suffix' => $this->sibling->l('enter 0 for no expiration', $this->route),
                    'class' => 'col-md-4',
                )
            )
        );

        $currencies = Currency::getCurrencies();
        $i = 1;
        foreach ($currencies as $currency) {
            $fields_form[$i]['form'] = array(
                'legend' => array(
                    'title' => $this->sibling->l('Reward Structure for ', $this->route).$currency['iso_code'],
                    'icon' => 'icon-question'
                ),
                'input' => $this->getCurrencyRewardFieldsGroup($currency)
            );
            $i++;
        }

        $fields_form[$i]['form'] = array(
            'submit' => array(
                'title' => $this->sibling->l('Save', $this->route),
                'class' => 'btn btn-default pull-right')
        );

        $helper = new HelperForm();
        $this->setupHelperConfigForm($helper, $this->route, 'process');

        $helper->fields_value['id_order_state_validation'] = (Tools::getValue('id_order_state_validation') != '' ? Tools::getValue('id_order_state_validation') : Configuration::get('lrp_id_order_state_validation'));
        $helper->fields_value['id_order_state_cancel'] = (Tools::getValue('id_order_state_cancel') != '' ? Tools::getValue('id_order_state_cancel') : Configuration::get('lrp_id_order_state_cancel'));
        $helper->fields_value['referral_enabled'] = (Tools::getValue('referral_enabled') != '' ? Tools::getValue('referral_enabled') : Configuration::get('lrp_referral_enabled'));
        $helper->fields_value['points_expire_days'] = (Tools::getValue('points_expire_days') != '' ? Tools::getValue('points_expire_days') : Configuration::get('lrp_points_expire_days'));

        foreach ($currencies as $currency) {
            $helper->fields_value['ratio_' . $currency['iso_code']] = (Tools::getValue('ratio_' . $currency['iso_code']) != '' ? Tools::getValue('ratio_' . $currency['iso_code']) : Configuration::get('lrp_ratio_' . $currency['iso_code']));
            $helper->fields_value['point_value_' . $currency['iso_code']] = (Tools::getValue('point_value_' . $currency['iso_code']) != '' ? Tools::getValue('point_value_' . $currency['iso_code']) : Configuration::get('lrp_point_value_' . $currency['iso_code']));
            $helper->fields_value['referral_points_' . $currency['iso_code']] = (Tools::getValue('referral_points_' . $currency['iso_code']) != '' ? Tools::getValue('referral_points_' . $currency['iso_code']) : Configuration::get('lrp_referral_points_' . $currency['iso_code']));
            $helper->fields_value['referral_friend_points_' . $currency['iso_code']] = (Tools::getValue('referral_friend_points_' . $currency['iso_code']) != '' ? Tools::getValue('referral_friend_points_' . $currency['iso_code']) : Configuration::get('lrp_referral_friend_points_' . $currency['iso_code']));
            $helper->fields_value['min_cart_value_' . $currency['iso_code']] = (Tools::getValue('min_cart_value_' . $currency['iso_code']) != '' ? Tools::getValue('min_cart_value_' . $currency['iso_code']) : Configuration::get('lrp_min_cart_value_' . $currency['iso_code']));
            $helper->fields_value['min_points_redemption_' . $currency['iso_code']] = (Tools::getValue('min_points_redemption_' . $currency['iso_code']) != '' ? Tools::getValue('min_points_redemption_' . $currency['iso_code']) : Configuration::get('lrp_min_points_redemption_' . $currency['iso_code']));
        }
        return $helper->generateForm($fields_form);
    }

    public function process()
    {
        $output = null;
        $id_shop = Context::getContext()->shop->id;

        if (Tools::isSubmit('submit' . $this->sibling->name)) {
            $currencies = Currency::getCurrencies();

            if (!Validate::isInt(Tools::getValue('points_expire_days'))) {
                $output .= $this->displayError($this->sibling->l('Invalid value for Points Expiry Days', $this->route));
            }

            foreach ($currencies as $currency) {
                if (!Validate::isFloat(Tools::getValue('ratio_' . $currency['iso_code']))) {
                    $output .= $this->displayError($this->sibling->l('Invalid value for Ratio (' . $currency['iso_code'] . ')'));
                }

                if (!Validate::isFloat(Tools::getValue('point_value_' . $currency['iso_code']))) {
                    $output .= $this->displayError($this->sibling->l('Invalid value for Point Value (' . $currency['iso_code'] . ')'));
                }
            }

            if ($output == '') {
                Configuration::updateValue('lrp_id_order_state_validation', (int)Tools::getValue('id_order_state_validation'), false, null, (int)$id_shop);
                Configuration::updateValue('lrp_id_order_state_cancel', (int)Tools::getValue('id_order_state_cancel'), false, null, (int)$id_shop);
                Configuration::updateValue('lrp_referral_enabled', (int)Tools::getValue('referral_enabled'), false, null, (int)$id_shop);
                Configuration::updateValue('lrp_points_expire_days', (int)Tools::getValue('points_expire_days'), false, null, (int)$id_shop);

                foreach ($currencies as $currency) {
                    Configuration::updateValue('lrp_ratio_' . $currency['iso_code'], (float)Tools::getValue('ratio_' . $currency['iso_code']), false, null, (int)$id_shop);
                    Configuration::updateValue('lrp_point_value_' . $currency['iso_code'], (float)Tools::getValue('point_value_' . $currency['iso_code']), false, null, (int)$id_shop);
                    Configuration::updateValue('lrp_referral_points_' . $currency['iso_code'], (int)Tools::getValue('referral_points_' . $currency['iso_code']), false, null, (int)$id_shop);
                    Configuration::updateValue('lrp_referral_friend_points_' . $currency['iso_code'], (int)Tools::getValue('referral_friend_points_' . $currency['iso_code']), false, null, (int)$id_shop);
                    Configuration::updateValue('lrp_min_cart_value_' . $currency['iso_code'], (int)Tools::getValue('min_cart_value_' . $currency['iso_code']), false, null, (int)$id_shop);
                    Configuration::updateValue('lrp_min_points_redemption_' . $currency['iso_code'], (int)Tools::getValue('min_points_redemption_' . $currency['iso_code']), false, null, (int)$id_shop);
                }
            }
        }
        return $output . $this->render();
    }

    public function route()
    {
        switch (Tools::getValue('action')) {
            case 'process':
                die($this->process());
            default:
                die($this->render());
        }
    }
}
