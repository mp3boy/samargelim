<?php
/**
* 2007-2017 PrestaShop
*
* Jms Adv Search
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class JsmAdvSearchSamargelim extends JmsAdvSearch
{
    public function getProductByName($query1){

        $queryProd = explode(" ", $query1);

        $sql = new DbQuery();
        $sql->select('pl.`name`, p.`active`');
        $sql->from('product', 'p');
        $sql->leftJoin('product_lang', 'pl', 'p.`id_product` = pl.`id_product` ');

        $i=0;
        $where = '';
        foreach($queryProd as $query) {
            if (strlen($query) > 2) {
                $i++;
                if ($i > 1) {
                    $where .= ' OR ';
                }
                $where .= '(pl.`name` LIKE \'%' . pSQL($query) . '%\' OR p.`supplier_reference` LIKE \'%' . pSQL($query) . '%\')';
            }
        }

        $sql->orderBy('p.`active` DESC');
        $sql->limit(3);

        $sql->where($where);

        $result = Db::getInstance()->executeS($sql);

        if (!$result) {
            return false;
        }
//        return $sql->build();
        return $result;
    }

}