<?php
/**
* 2007-2014 PrestaShop
*
* Jms Adv Search
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

require_once('../../config/config.inc.php');
require_once('../../init.php');
require_once(dirname(__FILE__).'/jmsadvsearch.php');
require_once(dirname(__FILE__).'/jmsadvsearchsamargelim.php');


$search_key = Tools::getValue('search_key');

//cautare in categorii
$categories = Category::getCategories( 1, true, false);
$catS = [];
foreach($categories as $category){
    $words = explode(" ", strtolower($search_key));
    foreach($words as $word){
        $pos = strpos(strtolower($category['name']), strtolower($word));
        if ($pos !== false) {
            $catS[$category['name']] = $category['id_category'].'-'.$category['link_rewrite'];
        }
    }
}

$products = new JsmAdvSearchSamargelim();
$getProduct = $products->getProductByName(strtolower($search_key));

    $searchFinal = '<div class="search_suggestion">';
        $searchFinal .='<span class="search_suggestion_header">Cautari populare pe SaMargelim</span>';

        $searchFinal .='<a class="top_search search_res_autocomplete" href="/cauta?s='.urlencode($search_key).'">'.$search_key.'</a>';

        if (count($catS)>0) {
            $i = 0;
            foreach($catS as $name => $cat){
                if ($i<3) {
                    $searchFinal .= '<a class="top_search search_res_autocomplete" href="/cauta?s='.urlencode($search_key).'&cat='.urlencode($name).'">'.$search_key.' <span class="inthecat"> in categoria '.$name.'</span></a>';
                }
                $i++;
            }
        }

        if (count($getProduct)>0) {



            foreach ($getProduct as $prod){
                $prodArray = [];
                $search_key_broked = explode(" ", $search_key);

                foreach($search_key_broked as $skb){

                   if (strpos(strtolower($prod['name']), strtolower($skb)) !== false){

                          $produs = explode ($skb, strtolower($prod['name']));
                          $space = substr($skb, -1) === " " ? " " : "";
                          $produsSuggestion = explode(" ",$produs[1]);


                        if (count($produsSuggestion) > 1){

                            if (in_array(strtolower($produsSuggestion[0]." ".$produsSuggestion[1]), $prodArray)){
                                $prodSug = $produsSuggestion[0]." ".$produsSuggestion[1]." ".$produsSuggestion[2];
                            } else {
                                $prodSug = $produsSuggestion[0]." ".$produsSuggestion[1];
                            }

                        } else {
                            $prodSug = $produsSuggestion[0];
                        }

                    }

                }

                if (!in_array(in_array(strtolower($prodSug), $prodArray))) {

                    array_push($prodArray, strtolower($prodSug));
                    $searchFinal .= '<a class="top_search search_res_autocomplete" href="/cauta?s='.urlencode($search_key . $space .$prodSug).'">' . $search_key . $space . '<span class="inthecat">' . $prodSug . '</span></a>';

                }
            }


        }

        if (count($catS)>0) {
            $searchFinal .= '<span class="search_suggestion_header">Mergi direct la categoria</span>';
            $i = 0;
            foreach($catS as $name => $cat){
                if ($i<3) {
                    $searchFinal .= '<a class="top_search search_res_autocomplete" href="'.$cat.'">'.$name.'</a>';
                }
                $i++;
            }

        }
    $searchFinal .= '</div>';

    echo $searchFinal;
