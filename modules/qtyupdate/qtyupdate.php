<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
class Qtyupdate extends Module
{
	private $_html = '';
	public function __construct()
	{
		$this->name = 'qtyupdate';
		$this->tab = 'content_management';
		$this->version = '1.0.1';
		$this->author = 'Prestashopaddon.com';
		$this->need_instance = 0;
		$this->bootstrap = true;

		parent::__construct();

		$this->secure_key = Tools::encrypt($this->name);

		$this->displayName = $this->l('Product status update');
		$this->description = $this->l('Hide out of stock products automatically');
	}

	public function install($keep = true)
	{
		if (parent::install() == false || 
			!$this->registerHook('displayHeader'))
			return false;
		return true;
	}
	public function uninstall($keep = true)
	{
		if (!parent::uninstall() || 
			!$this->unregisterHook('displayHeader'))
			return false;
		return true;
	}
    public function hookdisplayHeader($params)
    {
        $sql = "UPDATE ps_product SET active = 0 WHERE id_product IN (SELECT id_product FROM ps_stock_available WHERE quantity<1)";
        Db::getInstance()->execute($sql);
        $sql1 = "UPDATE ps_product_shop SET active = 0 WHERE id_product IN (SELECT id_product FROM ps_stock_available WHERE quantity<1)";
        Db::getInstance()->execute($sql1);   
        
    }

}
