{extends file='page.tpl'}
{block name='page_content'}
<div style="background:transparent url('/themes/jms_freshshop/assets/img/search.jpg') no-repeat scroll center center / cover; height: 229px" class="content_scene_cat_bg">
	<div class="category_naming">{$count} rezultate pentru: "{$search}"</div>
</div>

{if $count > 0}

<div id="js-product-list">
  <div class="products row">

{foreach $products as $product}

<div class="item">
        
 
 <div class="product-miniature js-product-miniature product-preview " data-id-product="{$product[0]}" data-id-product-attribute="{$product.id_product_attribute}">	
	<div class="product-preview-box">
			   <div class="preview">
				   {if $product[4]}
					   {assign var=imagineProdus value=$product[4]}
				   {else}
					   {assign var=imagineProdus value='/themes/jms_freshshop/assets/img/placeholder-samargelim.png'}
				   {/if}
					  <a href="{$product[3]}" class="product-image">
						<img class="img-responsive product-img1"
						  src = "{$imagineProdus}"
						  alt = "{$product[1]|truncate:200:'':'UTF-8'}"
						  data-full-size-image-url = "{$product.cover.large.url}"
						/>

					  </a>
				</div>
						
		<div class="product-info">
		     
					<div class="product-name">
						<a href="{$product[3]|escape:'html'}" class="product-link">{$product[1]|truncate:200:'':'UTF-8'}</a>
						<!--  -->
					</div>
					

			    <div class="category-name">
					<a href="{url entity='category' id=$product.id_category_default}">
					{$product.category|escape:'html':'UTF-8'}</a>
				</div>
				  
					{block name='product_price_and_shipping'}
						  <div class="content_price">
							{hook h='displayProductPriceBlock' product=$product type="before_price"}
							<span class="price new">{$product[2]}</span>
							
							{if $product.has_discount}
							  {hook h='displayProductPriceBlock' product=$product type="old_price"}
							  <span class="old price">{$product.regular_price}</span>
							{/if}

							{hook h='displayProductPriceBlock' product=$product type='unit_price'}

							{hook h='displayProductPriceBlock' product=$product type='weight'}
						  </div>
					{/block}
					<div class="product-description">
						{$product.description_short|truncate:170:'...' nofilter}
					 </div>
					<a href="{$product[3]|escape:'html'}" class="product-link-button">Detalii produs</a>
					
                    <div class="product_button">
						<a class="ajax-add-to-cart product-btn cart-button btn-effect btn-default" data-id-product="{$product[0]}" data-minimal-quantity="1" data-token="{if isset($static_token) && $static_token}{$static_token}{/if}" title="{l s='Add to Cart'}">
							    <span class="text-addcart">{l s='Add to cart' d='Shop.Theme.Actions'}	</span>		
							    <span class="text-outofstock">{l s='Out of stock' d='Shop.Theme.Actions'}</span>
                                <span class="fa fa-spin fa-spinner"></span>
								<span class="pe pe-7s-check"></span>								   
						</a>
					</div>
					
                    <div class="product_action">						
						<a class="addToWishlist product-btn btn-effect btn-default"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product[0]}', false, 1); return false;" data-id-product="{$product[0]}" title="{l s='Add to Wishlist'}">
						  <span class="la la-heart"></span>
						  <span class="text">Wishlist</span>
						</a>

						<a  data-link-action="quickview" class="quick-view-samargelim product-btn hidden-xs" title="{l s='Quick View'}">
							<span class="fa fa-eye">
								<div class="preview_big_image">
									<img src="{$imagineProdus}" />
								</div>
							</span>
							<span class="text">quick view</span>
						</a>
					</div>
					
			</div>
		</div>
</div>


</div>


 {/foreach}


</div>

  

  

</div>

{else}
	<div class="nofound">
		<h3 class="head">Pentru a gasi produsul dorit, incearca urmatoarele:</h3>
		- verifica daca ai scris corect termenii<br>
		- incearca sa folosesti sinonime<br>
		- incearca din nou, folosind o cautare mai generala<br>

		<div class="frequent-search">
			<span>Cautari frecvente</span><br>
			<a class="top_search search_res" href="/cauta?s=cristale+sticla">cristale sticla</a>
			<a class="top_search search_res" href="/cauta?s=ustensile+fimo">ustensile fimo</a>
			<a class="top_search search_res" href="/cauta?s=cercei+sticla">cercei sticla</a>
			<a class="top_search search_res" href="/cauta?s=baza+inel">baza inel</a>
			<a class="top_search search_res" href="/cauta?s=inel+argintiu">inel argintiu</a>
			<a class="top_search search_res" href="/cauta?s=lant+cupru">lant cupru</a>
			<a class="top_search search_res" href="/cauta?s=cristale+opal">cristale opal</a>
			<a class="top_search search_res" href="/cauta?s=pandantiv+stea">pandantiv stea</a>
			<a class="top_search search_res" href="/cauta?s=margele+murano">margele murano</a>
			<a class="top_search search_res" href="/cauta?s=margele+lemn">margele lemn</a>
		</div>


	</div>
{/if}

{/block}
