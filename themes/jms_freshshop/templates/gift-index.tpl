{extends file='page.tpl'}
{block name='page_content'}
<div style="background:transparent url('/img/cms/samargelim-handmade.png') no-repeat scroll center center / cover; height: 229px" class="content_scene_cat_bg">
	<div class="category_naming" style="text-align: center;color: #fff;text-shadow: 1px 1px 8px black;">De ce am nevoie ca sa creez {$categorie}</div> 					
</div>
<div class="cat_desc">
	<div class="flex-box">
	 	 <div id="category-description" class="text-muted">
	 	 	{if $categorie=="cercei handmade"}
	 	 		Alege materialele necesare pentru confectionarea unei perechi de cercei handmade, diverse modele: cercei lungi, cercei cu pietre, cercei clipsuri, cercei candelabru
			{else if $categorie=="o bratara handmade"}
				Alege materialele necesare pentru confectionarea unei bratari handmade, modele diferite: bratara din ata, bratara cu snur, bratara din piele, bratara din margele, bratara din pietre semipretioase.
			{else if $categorie=="o brosa handmade"}
				Alege materialele necesare pentru confectionarea unei brose, diverse modele, baze patinate, baze argintii, baze rotunde sau baze cu clama
			{else if $categorie=="un inel handmade"}
				Alege materialele necesare pentru confectionarea unui inel handmade, in functie de modelul dorit. Metode usoare de a realiza un inel handmade ieftin sau sofisticat.
			{else if $categorie=="un colier handmade"}
				Alege materialele necesare pentru confectionarea unui colier handmade: crimpuri, inchizatori, cupe pentru sirag, capete de lant si alte elemente pentru crearea de coliere.
			{else if $categorie=="un pandantiv handmade"}
				Alege materialele necesare pentru confectionarea unui pandativ handmade: accesorii, margele, contraverigi, baze, fire siliconate si alte furnituri pentru handmade
	 	 	{/if}
	 	 </div>
	</div>
</div>



<div id="js-product-list">
  <div class="products row product2gift">

{foreach $products as $product}

<div class="item col-sm-6 col-md-3 col-xs-12">
        
 
 <div class="product-miniature js-product-miniature product-preview " data-id-product="{$product[0]}" data-id-product-attribute="{$product.id_product_attribute}">	
	<div class="product-preview-box">
			   <div class="preview">
				   {if $product[4]}
					   {assign var=imagineProdus value=$product[4]}
				   {else}
					   {assign var=imagineProdus value='/themes/jms_freshshop/assets/img/placeholder-samargelim.png'}
				   {/if}
					  <a href="{$product[3]}" class="product-image">
						<img class="img-responsive product-img1"
						  src = "{$imagineProdus}"
						  alt = "{$product[1]|truncate:200:'':'UTF-8'}"
						  data-full-size-image-url = "{$product.cover.large.url}"
						/>
						
							
					  </a>
				</div>
						
		<div class="product-info">
		     
					<div class="product-name">
						<a href="{$product[3]|escape:'html'}" class="product-link">{$product[1]|truncate:200:'':'UTF-8'}</a>
						<!--  -->
					</div>
					

			    <div class="category-name">
					<a href="{url entity='category' id=$product.id_category_default}">
					{$product.category|escape:'html':'UTF-8'}</a>
				</div>
				  
				  
					<div class="content_price">
						<span class="price new">{$product[2]|number_format:2:",":"."}&nbsp;RON</span>
					</div>
					
					<div class="product-description">
						{$product.description_short|truncate:170:'...' nofilter}
					 </div>
					 <a href="{$product.link|escape:'html'}" class="product-link-button">DETALII PRODUS</a>

                    <div class="product_button">
						<a class="ajax-add-to-cart product-btn cart-button btn-effect btn-default" data-id-product="{$product[0]}" data-minimal-quantity="1" data-token="{if isset($static_token) && $static_token}{$static_token}{/if}" title="{l s='Add to Cart'}">
							    <span class="text-addcart">{l s='Add to cart' d='Shop.Theme.Actions'}	</span>		
							    <span class="text-outofstock">{l s='Out of stock' d='Shop.Theme.Actions'}</span>
                                <span class="fa fa-spin fa-spinner"></span>
								<span class="pe pe-7s-check"></span>								   
						</a>
					</div>
					
                    <div class="product_action">						
						<a class="addToWishlist product-btn btn-effect btn-default"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product[0]}', false, 1); return false;" data-id-product="{$product[0]}" title="{l s='Add to Wishlist'}">
						  <span class="la la-heart"></span>
						  <span class="text">Wishlist</span>
						</a>

						<a  data-link-action="quickview" class="quick-view-samargelim product-btn hidden-xs" title="{l s='Quick View'}">
							<span class="fa fa-eye">
								<div class="preview_big_image">
									<img src="{$imagineProdus}" />
								</div>
							</span>
							<span class="text">quick view</span>
						</a>
					</div>
					
			</div>
		</div>
</div>


</div>

 {/foreach}


</div>
</div>





<div id="gift_extra" style="display:none;">
<p class="gift_announcement">Pentru a creea un pandantiv handmade mai poti folosi si alte materiale:</p>
  <div class="extra_item first_item">
      <div class="extra_item_border">
        <a class="title_item" href="http://www.samargelim.ro/cabochone-c-119.html">Cabochon</a>
        <div class="clear"></div>
        <a class="item_pic" href="http://www.samargelim.ro/cabochone-c-119.html"><img src="http://www.samargelim.ro/images/gifts/pandantiv_cabochon.jpg"></a>
    </div>
  </div>
  <div class="extra_item ">
      <div class="extra_item_border">
        <a class="title_item" href="http://www.samargelim.ro/snur-soutache-c-196.html">Snur Soutache</a>
        <div class="clear"></div>
        <a class="item_pic" href="http://www.samargelim.ro/snur-soutache-c-196.html"><img src="http://www.samargelim.ro/images/gifts/pandantiv_soutache.jpg"></a>
    </div>
  </div>
  <div class="extra_item ">
      <div class="extra_item_border">
        <a class="title_item" href="http://www.samargelim.ro/fimo-c-170.html">FIMO</a>
        <div class="clear"></div>
        <a class="item_pic" href="http://www.samargelim.ro/fimo-c-170.html"><img src="http://www.samargelim.ro/images/gifts/pandantiv_fimo.jpg"></a>
    </div>
  </div>
  <div class="extra_item last_item">
      <div class="extra_item_border">
        <a class="title_item" href="http://www.samargelim.ro/advanced_search_result.html?main_page=advanced_search_result&amp;search_in_description=1&amp;keyword=Floare%20din%20polimer">Polimer</a>
        <div class="clear"></div>
        <a class="item_pic" href="http://www.samargelim.ro/advanced_search_result.html?main_page=advanced_search_result&amp;search_in_description=1&amp;keyword=Floare%20din%20polimer"><img src="http://www.samargelim.ro/images/gifts/pandantiv_polimer.jpg"></a>
    </div>
  </div>

<div class="clear"></div>

</div>



{/block}
