{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 **}

{if $cart.products}

<div class="cart-detailed-totals" style="width:50%;float:right;margin-top:-125px;">
<style>
{literal}
    .estegol{display:none;}
{/literal}
</style>
  <div class="card-block">
    {foreach from=$cart.subtotals item="subtotal"}
      {if $subtotal.value && $subtotal.type !== 'tax'}
        <div class="cart-summary-line" id="cart-subtotal-{$subtotal.type}">
          <span class="label{if 'products' === $subtotal.type} js-subtotal{/if}">
            {if 'products' == $subtotal.type}
              {$cart.summary_string}
            {else}
              {$subtotal.label}
            {/if}
          </span>
          <span class="value">{$subtotal.value}</span>
          {if $subtotal.type === 'shipping'}
              <div><small class="value">{hook h='displayCheckoutSubtotalDetails' subtotal=$subtotal}</small></div>
          {/if}
        </div>
      {/if}
    {/foreach}
  </div>

  {*
    {block name='cart_voucher'}
    {include file='checkout/_partials/cart-voucher.tpl'}
  {/block} 
  *}

 

  <div class="card-block">
    <div class="cart-summary-line cart-total" style="text-align:right;">
      <span class="label">{$cart.totals.total.label}</span>
      <span class="value" style="color:#636363">{$cart.totals.total.value}</span>
    </div>

    <div class="cart-summary-line">
      <small class="label">{$cart.subtotals.tax.label}</small>
      <small class="value">{$cart.subtotals.tax.value}</small>
    </div>
  </div>

 
</div>



<div class="ppppp card-block cart-summary-line">
  <div style="clear:both;"></div>
<div class="order-f">
  
  <div class="m15px buttonRow back" style="float:left;">
    <a href="/"><font color="#3f608a"><b>&lt;&lt; Inapoi la lista</b></font></a>
  </div>
  <div class="m15px buttonRow forward" style="color:#3f608a;font-size:12px;float: right;font-family: trebuchet ms;font-weight: bold;">
  {if (60>$cart.totals.total.value)}
  Comanda de inca {(60|intval-$cart.totals.total.value|intval)} RON si ai livrarea gratuita!
  {else}
    
  {/if}
  </div>
  <div style="clear:both;margin-top:10px;"></div>
  <div class="buttonRow forward" style="float: right;">
    <input type="button" value="Mergi la casa" class="comanda-button" />
  </div>

  <div style="color:#F26039;font-size:15px;"><a href="/discounturi-24" style="color:#ff0000;font-size:15px;" <em="">» <em>Cauti un produs anume si nu l-ai gasit pe site?  Suna-ne!</em></a></div>

</div>


<div style="padding:10px;font-size:12px;line-height:24px;font-family: Trebuchet ms;font-weight: bold;margin-top:50px;" >

<table>

  <tbody><tr>

    <td width="85%">

      <span style="color:#f7c61d;font-size:18px; font-weight:bold;font-family:trebuchet ms;">Comanda cu incredere de pe SaMargelim.ro!</span><br><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Comanda in siguranta!</span> Comenzile tale pe SaMargelim.ro sunt sigure.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Rapiditate.</span> Coletul pleaca spre tine in ziua comenzii*. Te sunam inainte!</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#garantie" style="color:#636363;"><span style="color: #8dca07;">Garantie.</span> Vei fi complet satisfacuta de alegerea facuta.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#returnare" style="color:#636363;"><span style="color: #8dca07;">Politica de returnare.</span> Daca nu esti multumita, poti returna produsele.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/politica-de-confidentialitate-26" style="color:#636363;"><span style="color: #8dca07;">Confidentialitate.</span> Respectam confidentialitatea datelor tale personale.</a>

    </td>

    <td><br><br><img src="/themes/jms_freshshop/assets/img/garantat.jpg"></td>

  </tr>

</tbody></table>

</div>



</div>

{else}
<style>
{literal}
    .order-f,#cart-points-summary{ display:none; }
    .showit{display:block !important;}
{/literal}
</style>
<a href="/" style="color:#fff;background: #FFEB3B;padding: 10px;" class="noshow">Intoarce-te in magazin</a>
<div style="padding:10px;font-size:12px;line-height:24px;font-family: Trebuchet ms;font-weight: bold;margin-top:50px;" class="showit">

<table>

  <tbody><tr>

    <td width="85%">

      <span style="color:#f7c61d;font-size:18px; font-weight:bold;font-family:trebuchet ms;">Comanda cu incredere de pe SaMargelim.ro!</span><br><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Comanda in siguranta!</span> Comenzile tale pe SaMargelim.ro sunt sigure.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Rapiditate.</span> Coletul pleaca spre tine in ziua comenzii*. Te sunam inainte!</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#garantie" style="color:#636363;"><span style="color: #8dca07;">Garantie.</span> Vei fi complet satisfacuta de alegerea facuta.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#returnare" style="color:#636363;"><span style="color: #8dca07;">Politica de returnare.</span> Daca nu esti multumita, poti returna produsele.</a><br>

      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/politica-de-confidentialitate-26" style="color:#636363;"><span style="color: #8dca07;">Confidentialitate.</span> Respectam confidentialitatea datelor tale personale.</a>

    </td>

    <td><br><br><img src="/themes/jms_freshshop/assets/img/garantat.jpg"></td>

  </tr>

</tbody></table>

</div>
{/if} 
