{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{foreach $addresses as $address}
  <article
    class="address-item{if $address.id == $selected} selected{/if}"
    id="{$name|classname}-address-{$address.id}"
  >
    <label class="row samargelim-address-row" style="width:100%">
      <div class="col-md-1">
           <span class="custom-radio">
          <input
                  type="radio"
                  name="{$name}"
                  value="{$address.id}"
                  {if $address.id == $selected}checked{/if}
          >
          <span></span>
        </span>
      </div>
      <div class="col-md-9">
        <div class="address">
          <span class="address-name-phone">
            {$address.firstname} {$address.lastname} - {$address.phone}
          </span>
          <span class="address-other-details">
            <b>Adresa:</b> {$address.address1}, {$address.city}, {$address.state}
          </span>
        </div>
      </div>
      <div class="col-md-2 address-fields">
        <input type="hidden" data-id="address_firstname" value="{$address.firstname}" />
        <input type="hidden" data-id="address_lastname" value="{$address.lastname}" />
        <input type="hidden" data-id="address_phone" value="{$address.phone}" />
        <input type="hidden" data-id="address_city" value="{$address.city}" />
        <input type="hidden" data-id="address_state" value="{$address.state}" />
        <input type="hidden" data-id="address_full" value="{$address.address1}" />
        <input type="button" class="edit_address" value="Editeaza Adresa" idAddress="{$address.id}"/>
        <input type="button" class="delete_address" value="Sterge Adresa" idAddress="{$address.id}" />
        &nbsp;
      </div>
    </label>

  </article>
{/foreach}
{if $interactive}
  <p>
    <button class="ps-hidden-by-js form-control-submit center-block" type="submit">{l s='Save' d='Shop.Theme.Actions'}</button>
  </p>
{/if}
