{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='checkout/_partials/steps/checkout-step.tpl'}

{block name='step_content'}
  <div class="js-address-form">
    <form
      method="POST"
      action="{$urls.pages.order}"
      data-refresh-url="{url entity='order' params=['ajax' => 1, 'action' => 'addressForm']}"
    >
    <h1 class="testimonialss" style="float:none;">Adresa de livrare</h1>



        <div id="delivery-addresses" class="address-selector js-address-selector samargelim-address">
          {include  file        = 'checkout/_partials/address-selector-block.tpl'
                    addresses   = $customer.addresses
                    name        = "id_address_delivery"
                    selected    = $id_address_delivery
                    type        = "delivery"
                    interactive = !$show_delivery_address_form and !$show_invoice_address_form
          }
        </div>
        <input type="button" class="add_anew_address" value="Adauga adresa noua" />


      {if !$form_has_continue_button}
        <div class="clearfix">
          <button type="submit" class="btn-default continue pull-xs-right button-small" name="confirm-addresses" value="1">
              {l s='Continue' d='Shop.Theme.Actions'}
          </button>
        </div>
      {/if}

    </form>
  </div>



    <div id="smrg_modal_address" class="smrg_modal">
        <div class="modal-content">
            <span class="close">×</span>
            <div class="themodalloader"></div>
            <div class="themodalcontent">
                <h3 class="add_address_text">Adauga o noua adresa de livrare</h3>
                <div class="row" style="margin:0px;padding:0px;">
                    <div class="col-md-12" style="margin-top:20px"><b>Persoana de contact</b></div>
                    <div class="col-md-6">Nume si prenume<br> <input type="text" id="numeprenume_popup" placeholder="ex. Popescu Ioana" /></div>
                    <div class="col-md-6">Telefon <br> <input type="text" id="telefon_popup" placeholder="07xxxxxxxx" /></div><br><br>
                    <div class="col-md-12" style="margin-top:20px"><b>Adresa de livrare</b></div>
                    <div class="col-md-6">Judet<br> <select id="judet_popup" /></select></div>
                    <div class="col-md-6">Localitate <br> <input type="text" id="localitate_popup" /></div>
                    <div class="col-md-12">Adresa <br> <input type="text" id="adresa_popup" placeholder="ex. Strada, numar, bloc, scara, apartament"/></div>
                    <div class="col-md-12 address-modal-errors"></div>
                    <div class="col-md-12" style="margin-top:30px;">
                        <input type="button" value="Salveaza" class="save_address_button"/>
                        <input type="button" value="Anuleaza" class="cancel_address_button"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script>
        function getStates(idState=''){
            var states = [];
            $.ajax({
                url: '/modules/stateList/ajax.php',
                method: 'POST',
                data: {
                    idState : idState
                },
                async: false,
                success: function(data){
                    states = data;
                }
            });
            return states;
        }
        $(document).ready(function(){

            $('[name="id_address_delivery"]').change(function(){
                $(this).parents('form').submit();
            });


            $('.add_anew_address').click(function(){
               // alert(prestashop.customer.firstname)
                if ($('#edit_address_id').length){
                    $('#edit_address_id').remove();
                }
                $('#smrg_modal_address .themodalloader').show();
                $('#smrg_modal_address').show();
                $('.add_address_text').html('Adauga o noua adresa de livrare');

                $('#numeprenume_popup').val(prestashop.customer.firstname+ ' '+prestashop.customer.lastname);
                //$('#numeprenume_popup').val('');

                $('#telefon_popup').val('');
                $('#judet_popup').html(getStates());
                $('#localitate_popup').val('');
                $('#adresa_popup').val('');

                $('#smrg_modal_address .themodalloader').hide();
            });

            $('.save_address_button').click(function(){
                var popupError = '';
                $('.address-modal-errors').html('');
                if ($('#numeprenume_popup').val() === ''){
                    popupError = 'Numele este obligatoriu!'
                } else if ($('#telefon_popup').val() === ''){
                    popupError = 'Telefonul este obligatoriu!'
                } else if ($('#judet_popup').val() === ''){
                    popupError = 'Judetul este obligatoriu!'
                } else if ($('#localitate_popup').val() === ''){
                    popupError = 'Localitatea este obligatorie!'
                } else if ($('#adresa_popup').val() === ''){
                    popupError = 'Adresa este obligatorie!'
                }

                if (popupError !== ''){
                    $('.address-modal-errors').html(popupError);
                } else {
                    $('#smrg_modal_address .themodalloader').show();
                    var theName = $('#numeprenume_popup').val().split(' ');

                    var theUrl = '/adresa';
                    if ($('#edit_address_id').length){
                        theUrl +='?id_address=' + $('#edit_address_id').val();
                    }
                    $.ajax({
                        url: theUrl,
                        method: 'POST',
                        data: {
                            firstname : theName[0],
                            lastname: theName[1],
                            phone: $('#telefon_popup').val(),
                            id_state: $('#judet_popup').val(),
                            city: $('#localitate_popup').val(),
                            address1: $('#adresa_popup').val(),
                            submitAddress: 1,
                            postcode: '111111',
                            token: prestashop.token
                        },
                        success: function(){
                            $('#smrg_modal_address .themodalloader').hide();
                            $('#smrg_modal_address').hide();
                            window.location.reload();
                        },
                        error: function(){
                            $('#smrg_modal_address .themodalloader').hide();
                            $('#smrg_modal_address').hide();
                            window.location.reload();
                        }
                    });
                }

            });

            $('.edit_address').click(function(){
                if ($('#edit_address_id').length){
                    $('#edit_address_id').remove();
                }
                $('#smrg_modal_address .themodalloader').show();
                $('#smrg_modal_address').show();
                $('.add_address_text').html('Editeaza adresa');
                $('#judet_popup').html(getStates($(this).siblings('[data-id="address_state"]').val()));
                $('#numeprenume_popup').val($(this).siblings('[data-id="address_firstname"]').val()+' '+$(this).siblings('[data-id="address_lastname"]').val());
                $('#telefon_popup').val($(this).siblings('[data-id="address_phone"]').val());
                $('#localitate_popup').val($(this).siblings('[data-id="address_city"]').val());
                $('#adresa_popup').val($(this).siblings('[data-id="address_full"]').val());
                $('#smrg_modal_address .themodalcontent').append('<input type="hidden" id="edit_address_id" value="'+$(this).attr('idaddress')+'"/>');
                $('#smrg_modal_address .themodalloader').hide();
            });

            $('.delete_address').click(function(){
                if (confirm('Esti sigur ca vrei sa stergi adresa aceasta?')){
                    var idAddress= $(this).attr('idAddress');
                    $.ajax({
                        url: '/adresa?id_address='+idAddress+'&delete=1&token='+prestashop.token,
                        method:'GET',
                        success: function(){
                            $('#smrg_modal_address .themodalloader').hide();
                            $('#smrg_modal_address').hide();
                            window.location.reload();
                        }
                    });
                }
            });

            $('#smrg_modal_address .close, #smrg_modal_address .cancel_address_button').click(function(){
                $('#smrg_modal_address').hide();
            });
        });
    </script>
{/block}


