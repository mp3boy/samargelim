{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='checkout/_partials/steps/checkout-step.tpl'}






{block name='step_content'}
  <div id="hook-display-before-carrier">
    {$hookDisplayBeforeCarrier nofilter}
  </div>

  <div class="delivery-options-list">
      
      <input type="hidden" value="{$cart}" id="currentcartid">
    {if $delivery_options|count}
      <form
        class="clearfix"
        id="js-delivery"
        data-url-update="{url entity='order' params=['ajax' => 1, 'action' => 'selectDeliveryOption']}"
        method="post"
      >
        <hr>
        <h1 class="testimonialss" style="float:none;display:block">Modalitati de livrare</h1>   
        <div class="form-fields">
          {block name='delivery_options'}
          {foreach $customer.addresses as $address}
            {* {if $address.id == $cart.id_address_delivery} *}
                {if $address.state|lower=="bucuresti" || $address.state|lower=="bucurești" || $address.state|lower=="ilfov"}
                  {assign var=thetransport value="transport_bucuresti"} 
                {/if}
                {if $address.state|lower!="bucuresti" && $address.state|lower!="bucurești" && $address.state|lower!="ilfov"}
                  {assign var=thetransport value="transport_tara"} 
                {/if}
            {* {/if} *}
          {/foreach}
            <div class="delivery-options {$thetransport}">
              {foreach from=$delivery_options item=carrier key=carrier_id}
                {if $carrier.id==23 && $thetransport=="transport_tara"}
                {elseif $carrier.id==25 && $thetransport=="transport_bucuresti"}
                {else}
                  <div class="delivery-option " >
                  
                    <label for="delivery_option_{$carrier.id}" class="col-sm-12 delivery-option-2" style="margin-left:20px;">
                      <div class="row">
                        <div class="col-sm-2 col-xs-12">
                          <div class="row">
                           
                            <div class="col-xs-12">
                                <span class="custom-radio pull-xs-left">
                                    <input type="radio" name="delivery_option[{$id_address}]" id="delivery_option_{$carrier.id}" value="{$carrier_id}"{if $delivery_option == $carrier_id} checked{/if}>
                                    
                                  </span>
                              <span class="h6 carrier-name bodyText2">{$carrier.name}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                          <span class="carrier-delay labelsl">{$carrier.delay}</span>
                        </div>
                        <div class="col-sm-1 col-xs-12">
                          <span class="carrier-price smgg2">{$carrier.price}</span>
                        </div>
                      </div>
                    </label>
                    <div class="col-md-12 carrier-extra-content"{if $delivery_option != $carrier_id} style="display:none;"{/if}>
                        {$carrier.extraContent nofilter}
                    </div>
                    <div class="clearfix"></div>
                  </div>
                {/if}

              {/foreach}
            </div>
          {/block}
          <div class="order-options">
            {if $recyclablePackAllowed}
              <label>
                <input type="checkbox" name="recyclable" value="1" {if $recyclable} checked {/if}>
                <span>{l s='I would like to receive my order in recycled packaging.' d='Shop.Theme.Checkout'}</span>
              </label>
            {/if}
            {if $gift.allowed}
              <span class="custom-checkbox">
                <input
                  class="js-gift-checkbox"
                  name="gift"
                  type="checkbox"
                  value="1"
                  {if $gift.isGift}checked="checked"{/if}
                >
                <span><i class="material-icons checkbox-checked">&#xE5CA;</i></span>
                <label>{$gift.label}</label >
              </span>

              <div id="gift" class="collapse{if $gift.isGift} in{/if}">
                <label for="gift_message">{l s='If you\'d like, you can add a note to the gift:' d='Shop.Theme.Checkout'}</label>
                <textarea rows="2" cols="120" id="gift_message" name="gift_message">{$gift.message}</textarea>
              </div>

            {/if}
          </div>
        </div>

       
      
        

        <button type="submit" class="continue btn btn-primary button-small pull-xs-right finalizeaza_comanda" name="confirmDeliveryOption" value="1">
          {l s='Continue' d='Shop.Theme.Actions'}
        </button>
      </form>
    {else}
      <p class="alert alert-danger">{l s='Unfortunately, there are no carriers available for your delivery address.' d='Shop.Theme.Checkout'}</p>
    {/if}
  </div>

  <div id="hook-display-after-carrier">
    {$hookDisplayAfterCarrier nofilter}
  </div>

  <div id="extra_carrier"></div>

{/block}


