{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}


{extends file='checkout/cart.tpl'}


 

{block name='content' append}
  {hook h='displayCrossSellingShoppingCart'}
{/block}

{block name='continue_shopping' append}

<a href="/" style="color:#fff;background: #FFEB3B;padding: 10px;">Intoarce-te in magazin</a>

<div style="padding:10px;font-size:12px;line-height:24px;font-family: Trebuchet ms;font-weight: bold;margin-top:50px;">
<table>
  <tbody><tr>
    <td width="85%">
      <span style="color:#f7c61d;font-size:18px; font-weight:bold;font-family:trebuchet ms;">Comanda cu incredere de pe SaMargelim.ro!</span><br><br>
      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Comanda in siguranta!</span> Comenzile tale pe SaMargelim.ro sunt sigure.</a><br>
      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/kit-cadou-4" style="color:#636363;"><span style="color: #8dca07;">Rapiditate.</span> Coletul pleaca spre tine in ziua comenzii*. Te sunam inainte!</a><br>
      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#garantie" style="color:#636363;"><span style="color: #8dca07;">Garantie.</span> Vei fi complet satisfacuta de alegerea facuta.</a><br>
      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/conditii-de-utilizare-25#returnare" style="color:#636363;"><span style="color: #8dca07;">Politica de returnare.</span> Daca nu esti multumita, poti returna produsele.</a><br>
      <img src="/themes/jms_freshshop/assets/img/smrglim.jpg"> <a href="/politica-de-confidentialitate-26" style="color:#636363;"><span style="color: #8dca07;">Confidentialitate.</span> Respectam confidentialitatea datelor tale personale.</a>
    </td>
    <td><br><br><img src="/themes/jms_freshshop/assets/img/garantat.jpg"></td>
  </tr>
</tbody></table>
</div>

<style> 
{literal}
    body .noshow,body .showit{display:none !important;}
{/literal}
</style>
{/block}


{block name='cart_actions'}
  <div class="checkout text-xs-center card-block">
    <button type="button" class="btn disabled button-small btn-effect btn-default" disabled>{l s='Checkout' d='Shop.Theme.Actions'}</button>
  </div>
{/block}

{block name='continue_shopping'}{/block}
{block name='cart_voucher'}{/block}
{block name='display_reassurance'}{/block}

