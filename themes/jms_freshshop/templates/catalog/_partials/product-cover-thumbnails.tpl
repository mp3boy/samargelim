{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<script type="text/javascript">
  var img0 = new Image(  );
  img0.src = "/themes/jms_freshshop/assets/img/scrollhand/001.png";
  var img1 = new Image( );
  img1.src = "/themes/jms_freshshop/assets/img/scrollhand/002.png";
  var img2 = new Image( );
  img2.src = "/themes/jms_freshshop/assets/img/scrollhand/003.png";
  var img3 = new Image( );
  img3.src = "/themes/jms_freshshop/assets/img/scrollhand/004.png";
  var img4 = new Image( );
  img4.src = "/themes/jms_freshshop/assets/img/scrollhand/005.png";
  var img5 = new Image();
  img5.src = "/themes/jms_freshshop/assets/img/scrollhand/006.png";
  var i = 0;
  var animation = true;
  var nbImg = 6; // change to the number of different images you have
  function animate() {
    if (animation) {
      document.getElementById("mana").src = eval("img" + i ).src;
    }
    i++;
    // if (i == nbImg) i=0;
    junk = setTimeout("animate();", 120); // in milliseconds
  }

  function loadHand(){
    $('#arm').css('top','0');
    $("#arm").show();
    i=0;

    animate();

    setTimeout(function() {
      $('html, body').animate({
        scrollTop: jQuery("#product_description_samargelim_new").offset().top
      }, 1600);
    }, 1000);

    setTimeout(function() {
      $('#arm').animate({
        top: '-=1000'
      }, 1400);
    }, 1000);

    setTimeout(function() { $("#arm").hide();}, 3000);
  }
</script>

<div class="images-container images-product-detail">
  {block name='product_cover'}

  {block name='product_images'}
    {assign var=val value=1}
    {if $product.images}
      {foreach from=$product.images item=image}
        <a href="{$image.bySize.large_default.url}" class="product-coverescu" rel="samargelim_group" title="{$product.name}" {if $val>1}style="display: none;"{/if}>
          <img class="js-qv-product-cover" src="{$image.bySize.large_default.url}" alt="{$product.title}"  style="width:100%;" itemprop="image">
        </a>
        {assign var=val value=$val+1}
      {/foreach}
    {else}
      <img class="js-qv-product-cover" src="/themes/jms_freshshop/assets/img/placeholder-samargelim.png" alt="{$product.title}"  style="width:100%;" itemprop="image">
    {/if}
  {/block}

  {/block}

  {block name='product_images'}
    <div class="js-qv-mask mask">
      <ul class="product-images js-qv-product-images">
        {foreach from=$product.images item=image}
          <li class="thumb-container">
            <img
              class="thumb jsa-thumb {if $image.id_image == $product.cover.id_image} selected {/if}"
              data-image-medium-src="{$image.bySize.medium_default.url}"
              data-image-large-src="{$image.bySize.large_default.url}"
              src="{$image.bySize.home_default.url}"
              alt="{$image.legend}"
              title="{$image.legend}"
              width="100"
              itemprop="image"
            >
          </li>
        {/foreach}
      </ul>

      {block name='product_description'}
        {if $product.description || $product.images|count > 1}
        <div style="width:60px;float:left;text-align:center;margin-left:10px;margin-top:30px;display:inline-block" onclick="loadHand();" class="productBigAdditionalImages">
          <img src="/themes/jms_freshshop/assets/img/scrollhand/maneta.jpg"
               onmouseover="this.src='/themes/jms_freshshop/assets/img/scrollhand/maneta_hover.jpg';this.style.cursor='pointer';"
               onmouseout="this.src='/themes/jms_freshshop/assets/img/scrollhand/maneta.jpg'"
               onclick="this.src='/themes/jms_freshshop/assets/img/scrollhand/maneta_pull.jpg'"
               height="35"/><br />
          <span style="color:#475e80;font-size:13px;font-weight:bold;" onmouseover="this.style.cursor='pointer';this.style.color='#002458';" onmouseout="this.style.cursor='pointer';this.style.color='#475e80';">Nu apasa!</span>
          <div id='arm' style='position:fixed; top:0px;z-index:99999999;display:none; '>
            <img id="mana" src=""/>
          </div>
        </div>
        {/if}
      {/block}

    </div>
  {/block}
</div>
<script src="/themes/jms_freshshop/assets/js/jquery.js"></script>
<script src="/themes/jms_freshshop/assets/js/customwheel.js"></script>
<script src="/themes/jms_freshshop/assets/js/fancybox.js"></script>

<link rel="stylesheet" type="text/css" href="/themes/jms_freshshop/assets/css/fancybox.css" media="screen" />
<script type="text/javascript">
  $(document).ready(function() {
    $("a[rel=\"samargelim_group\"]").fancybox({
      'transitionIn'		: 'elastic',
      'transitionOut'		: 'elastic',
      'titlePosition' 	: 'inside',
    });

    $('.jsa-thumb').click(function(){
      var largeImg = $(this).attr('data-image-large-src');
      $('.product-coverescu').hide();
      $('.product-coverescu[href="'+largeImg+'"]').show();
      $('.selected').removeClass('selected');
      $(this).addClass('selected');

    });
  });
</script>
{hook h='displayAfterProductThumbs'}
