{*
 * This file allows you to customize your search page.
 * You can safely remove it if you want it to appear exactly like all other product listing pages
 *}

{extends file=$layout}
{block name='content'}
  <section id="main">
    <section id="products">
     
       
          <div style="background:transparent url('/c/6-category_default/snur-si-ata.jpg')
            no-repeat scroll center center / cover; height: 229px" class="content_scene_cat_bg">
             <div class="category_naming">Ai cautat produse dupa: {$smarty.get.search_query}</div>           
          </div>
          <div class="cat_desc">
            <div class="flex-box">
               <div id="category-description" class="text-muted">
               	
               </div>
               
            </div>
          </div>  
      
      
      {if $listing.products|count}
        <div id="">
          {block name='product_list_top'}
            {include file='catalog/_partials/products-top.tpl' listing=$listing}
          {/block}
        </div>

        {block name='product_list_active_filters'}
          <div id="" class="hidden-sm-down">
            {$listing.rendered_active_filters nofilter}
          </div>
        {/block}

        <div id="product_list" class="product_list {if $jpb_grid == 1}products-list-in-column{else}products-list-in-row{/if}  products-list-{$jpb_productperrow}">
          {block name='product_list'}
            {include file='catalog/_partials/products.tpl' listing=$listing}
          {/block}
        </div>

        <div id="js-product-list-bottom">
          {block name='product_list_bottom'}
            {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
          {/block}
        </div>

      {else}
        {*
        {include file='errors/not-found.tpl'}
        *}
        Nu exista produse care sa corespunda criteriilor de cautare. Incearca din nou.<br><br>
        <div class="search-box">
    	 {block name='search'}
          {hook h='displaySearch'}
        {/block}
    
        {block name='hook_not_found'}
          {hook h='displayNotFound'}
        {/block}
    	</div>    
        <br><br><br>
        <h1 class="testimonialss" style="width:100%;float:none;">Pentru a gasi produsul dorit, incearca urmatoarele:</h1><br>
        - Verifica daca ai scris corect termenii<br>
        - Incearca sa folosesti sinonime<br>
        - Incearca din nou, folosind o cautare mai generala
        
      {/if}
    </section>

  </section>
{/block}
