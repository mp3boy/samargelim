{*
 * This file allows you to customize your price-drop page.
 * You can safely remove it if you want it to appear exactly like all other product listing pages
 *}

{extends file=$layout}
{block name='content'}
  <section id="main">
    <section id="products">
     
       
          <div style="background:transparent url('/c/6-category_default/snur-si-ata.jpg')
            no-repeat scroll center center / cover; height: 229px" class="content_scene_cat_bg">
             <div class="category_naming">Reduceri de pret</div>           
          </div>
          <div class="cat_desc">
            <div class="flex-box">
               <div id="category-description" class="text-muted">
                  Te invitam sa descoperi noutatile noastre si sa alegi materialele potrivite pentru viitoarele tale creatii handmade. Spor la margelit!
                </div>
               
            </div>
          </div>  
      
      
      {if $listing.products|count}
        <div id="">
          {block name='product_list_top'}
            {include file='catalog/_partials/products-top.tpl' listing=$listing}
          {/block}
        </div>

        {block name='product_list_active_filters'}
          <div id="" class="hidden-sm-down">
            {$listing.rendered_active_filters nofilter}
          </div>
        {/block}

        <div id="product_list" class="product_list {if $jpb_grid == 1}products-list-in-column{else}products-list-in-row{/if}  products-list-{$jpb_productperrow}">
          {block name='product_list'}
            {include file='catalog/_partials/products.tpl' listing=$listing}
          {/block}
        </div>

        <div id="js-product-list-bottom">
          {block name='product_list_bottom'}
            {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
          {/block}
        </div>

      {else}

        {include file='errors/not-found.tpl'}

      {/if}
    </section>

  </section>
{/block}


