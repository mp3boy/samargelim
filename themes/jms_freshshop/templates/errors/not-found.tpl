{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<section id="content" class="page-content page-not-found" style="text-align:left"> 
    <h1 class="testimonialss" style="display:block;float:none;">Pagina pe care o cauti e posibil sa se fi mutat.</h1>

    Foloseste optiunea de cautare pentru a gasi bijuteria sau informatia dorita.<br><br>
    Sau contacteaza-ne: 0723737752, 0213200011, <a href="mailto:office@edora.ro">office@edora.ro</a>. <br>
    Sau mergi la <a href="/">pagina principala</a>.
	{*
	<div class="search-box">
	 {block name='search'}
      {hook h='displaySearch'}
    {/block}

    {block name='hook_not_found'}
      {hook h='displayNotFound'}
    {/block}
	</div>
	*}
<br><br><br>
<span style="font-size:16px;">Vezi noutatile</span>  	
<br><br>
<div id="js-product-list">
  <div class="products row">

{foreach $products as $product}

<div class="item">
        
 
 <div class="product-miniature js-product-miniature product-preview " data-id-product="{$product[0]}" data-id-product-attribute="{$product.id_product_attribute}">	
	<div class="product-preview-box">
			   <div class="preview">

				   {if $product[4]}
					   {assign var=imagineProdus value=$product[4]}
				   {else}
					   {assign var=imagineProdus value='/themes/jms_freshshop/assets/img/placeholder-samargelim.png'}
				   {/if}

					  <a href="{$product[3]}" class="product-image">
						<img class="img-responsive product-img1"
						  src = "{$imagineProdus}"
						  alt = "{$product[1]|truncate:200:'':'UTF-8'}"
						  data-full-size-image-url = "{$product.cover.large.url}"
						/>
						
							
					  </a>
				</div>
						
		<div class="product-info">
		     
					<div class="product-name">
						<a href="{$product[3]|escape:'html'}" class="product-link">{$product[1]|truncate:200:'':'UTF-8'}</a>
						<!--  -->
					</div>
					

			    <div class="category-name">
					<a href="{url entity='category' id=$product.id_category_default}">
					{$product.category|escape:'html':'UTF-8'}</a>
				</div>
				  
					{block name='product_price_and_shipping'}
						{if $product.show_price}
						  <div class="content_price">
							{hook h='displayProductPriceBlock' product=$product type="before_price"}
							<span class="price new">{$product.price}</span>
							
							{if $product.has_discount}
							  {hook h='displayProductPriceBlock' product=$product type="old_price"}
							  <span class="old price">{$product.regular_price}</span>
							{/if}

							{hook h='displayProductPriceBlock' product=$product type='unit_price'}

							{hook h='displayProductPriceBlock' product=$product type='weight'}
						  </div>
						{/if}
					{/block}
					<div class="product-description">
						{$product.description_short|truncate:170:'...' nofilter}
					 </div>
					<a href="{$product[3]|escape:'html'}" class="product-link-button">Detalii produs</a>
					
                    <div class="product_button">
						<a class="ajax-add-to-cart product-btn cart-button btn-effect btn-default" data-id-product="{$product[0]}" data-minimal-quantity="1" data-token="{if isset($static_token) && $static_token}{$static_token}{/if}" title="{l s='Add to Cart'}">
							    <span class="text-addcart">{l s='Add to cart' d='Shop.Theme.Actions'}	</span>		
							    <span class="text-outofstock">{l s='Out of stock' d='Shop.Theme.Actions'}</span>
                                <span class="fa fa-spin fa-spinner"></span>
								<span class="pe pe-7s-check"></span>								   
						</a>
					</div>
					
                    <div class="product_action">						
						<a class="addToWishlist product-btn btn-effect btn-default"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product[0]}', false, 1); return false;" data-id-product="{$product[0]}" title="{l s='Add to Wishlist'}">
						  <span class="la la-heart"></span>
						  <span class="text">Wishlist</span>
						</a>

						<a  data-link-action="quickview" class="quick-view-samargelim product-btn hidden-xs" title="{l s='Quick View'}">
							<span class="fa fa-eye">
								<div class="preview_big_image">
									<img src="{$imagineProdus}" />
								</div>
							</span>
							<span class="text">quick view</span>
						</a>
					</div>
					
			</div>
		</div>
</div>


</div>

 {/foreach}


</div>

  

  

</div>	
	
	
</section>
