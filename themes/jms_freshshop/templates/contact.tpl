{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_header_container'}{/block}

{block name='left_column'}
  <div id="left-column" class="col-xs-12 col-sm-3">
    {widget name="ps_contactinfo" hook='displayLeftColumn'}
  </div>
{/block}

{block name='page_content'}
	<h1>Contact</h1>
<div class="content_samargelim">
<div class="col-sm-6">
	Poti ridica comanda sau CUMPARA DIRECT de la sediul din Bucuresti: Str. Ernest Juvara, nr.24 - langa Gradina Botanica. <br /><em><b>In incinta Cafenelei Kuber, cladirea din spate.</b></em> <br /><br /><b>Program:</b><br /><em>Luni - Vineri:</em> 10:30 - 19:00 | <em>Sambata:</em> 10:00 - 14:00 | <em>Duminica:</em> Inchis<br /><br /><b>Cum ajungi?</b> <br /> Cu troleibuzele 62, 93 si 96, autobuzul 601. De la oricare din statiile de metrou Eroilor sau Grozavesti (6 minute de mers)<br /><br /><b>Contacteaza-ne:</b>
	<br /><br />
	<div class="col-sm-10"> 
	<!--{widget name="contactform"}-->
	<form action="/themes/jms_freshshop/assets/img/search.jpg/contact" method="post" enctype="multipart/form-data" id="cfcontact">
		<input type="hidden" name="id_contact" value="2" />

		<div>
			<label>Nume:</label>
			<input type="text" name="nume" required/>
		</div>
		<div>
			<label>Email:</label>
			<input type="email" name="from" required/>
		</div>
		<div>
			<label>Telefon:</label>
			<input type="text" name="telefon" required/>
		</div>
		<div>
			<label>Mesaj:</label>
			<textarea name="message" required></textarea>
		</div>
		<div class="g-recaptcha" data-sitekey="6LePZ10UAAAAAClZ6Tq5rqTeRo8p_XwCDncpC0r9"></div>
		{if $error!=""}
		<div class="cferror">{$error}</div>
		{/if}
		<div>
			<input type="submit" class="trimite_cf" name="submitMessage" />
		</div>
	</form>
	</div>
	<div style="clear:both;"></div>
<p><span class="edInfo_tel"><img src="/themes/jms_freshshop/assets/img/search.jpg/img/cms/phone1.png" alt="" width="20" height="20" />  0720 241 480 / 0749 394 968</span><br class="clearBoth" /><span class="edInfo_adr"><img src="/themes/jms_freshshop/assets/img/search.jpg/img/cms/email_contact.jpg" alt="" width="25" height="20" /> EDORA Corporation SRL<br />        RO18577681<br />        J40/6125/2006<br /><img src="/themes/jms_freshshop/assets/img/search.jpg/img/cms/edora_email.png" alt="" width="119" height="27" /><br /><br />Bucuresti, Str. Ernest Juvara nr 24.</span> <span class="edInfo_ema"></span>
</div>
<div class="col-sm-6">
<a class="llcolor" target="_blank" href="/themes/jms_freshshop/assets/img/search.jpg/includes/templates/edora/images/templ2012/harta.jpg"> Harta sediu<br /></a><br /><a href="/themes/jms_freshshop/assets/img/search.jpg/img/cms/harta.jpg" target="_blank"><img src="/themes/jms_freshshop/assets/img/search.jpg/img/cms/harta.jpg" alt="" width="285" height="158" /></a><br /><br /><iframe width="285" height="285" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ro/maps?f=d&source=s_d&saddr=Strada+Ernest+Juvara&daddr=Strada+Ernest+Juvara&hl=ro&geocode=FdQUpgIdwb6NAQ%3BFdUUpgIdvr6NAQ&sll=44.438838,26.06696&sspn=0.000965,0.002411&mra=dme&mrsp=1&sz=19&ie=UTF8&t=m&ll=44.438836,26.066995&spn=0.010725,0.018239&z=15&output=embed"></iframe><br /><small><a href="https://maps.google.ro/maps?f=d&source=embed&saddr=Strada+Ernest+Juvara&daddr=Strada+Ernest+Juvara&hl=ro&geocode=FdQUpgIdwb6NAQ%3BFdUUpgIdvr6NAQ&sll=44.438838,26.06696&sspn=0.000965,0.002411&mra=dme&mrsp=1&sz=19&ie=UTF8&t=m&ll=44.438836,26.066995&spn=0.010725,0.018239&z=15" style="color: #0000ff; text-align: left;">Vizualizare harta marita</a></small></p>
</div>
</div>
 

{/block}
