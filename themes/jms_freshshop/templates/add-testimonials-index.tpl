{extends file='page.tpl'}
{block name='page_content'}

<h1>Spune-ne parerea ta despre produsele primite si modul de livrare</h1>
<p style="color:#636363; font-style:trebuchet ms; font-size:14px; font-weight:bold;">Orice critica sau sugestie sunt binevenite :) </p>

<form action="" id="testimonials_form" method="POST">
	<div>
		<label>Nume:</label>
		<input type="text" name="nume" required/>
	</div>
	<div>
		<label>Email:</label>
		<input type="email" name="email" required/>
	</div>
	<div>
		<label>Oras:</label>
		<input type="text" name="oras" required/>
	</div>
	<hr>
	<div>
		<label>Marturie:</label>
		<textarea required name="marturie"></textarea>
	</div>
	<div class="g-recaptcha" data-sitekey="6LePZ10UAAAAAClZ6Tq5rqTeRo8p_XwCDncpC0r9"></div>
	{if $error!=""}
	<div class="cferror">{$error}</div>
	{/if}
	<div>
		<input type="submit" class="trimite_marturie" name="trimite_marturie" />
	</div>

</form>

{/block}