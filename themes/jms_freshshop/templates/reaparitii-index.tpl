{extends file='page.tpl'}
{block name='page_content'}
<div style="background:transparent url('/c/6-category_default/snur-si-ata.jpg') no-repeat scroll center center / cover; height: 229px" class="content_scene_cat_bg">
	<div class="category_naming">Reaparitii</div> 					
</div>
<div class="cat_desc">
	<div class="flex-box">
	 	 <div id="category-description" class="text-muted">Te invitam sa descoperi noutatile noastre si sa alegi materialele potrivite pentru viitoarele tale creatii handmade. Spor la margelit!</div>
	</div>
</div>



<div id="js-product-list">
  <div class="products row">

{foreach $products as $product}

<div class="item">
        
 
 <div class="product-miniature js-product-miniature product-preview " data-id-product="{$product[0]}" data-id-product-attribute="{$product.id_product_attribute}">	
	<div class="product-preview-box">
			   <div class="preview">
				   {if $product[4]}
					   {assign var=imagineProdus value=$product[4]}
				   {else}
					   {assign var=imagineProdus value='/themes/jms_freshshop/assets/img/placeholder-samargelim.png'}
				   {/if}
					  <a href="{$product[3]}" class="product-image">
						<img class="img-responsive product-img1"
						  src = "{$imagineProdus}"
						  alt = "{$product[1]|truncate:200:'':'UTF-8'}"
						  data-full-size-image-url = "{$product.cover.large.url}"
						/>
						
							
					  </a>
				</div>
						
		<div class="product-info">
		     
					<div class="product-name">
						<a href="{$product[3]|escape:'html'}" class="product-link">{$product[1]|truncate:200:'':'UTF-8'}</a>
						<!--  -->
					</div>
					

			    <div class="category-name">
					<a href="{url entity='category' id=$product.id_category_default}">
					{$product.category|escape:'html':'UTF-8'}</a>
				</div>
				  
					{block name='product_price_and_shipping'}

						  <div class="content_price">
							{hook h='displayProductPriceBlock' product=$product type="before_price"}
							<span class="price new">{$product[2]}</span>
							
							{if $product.has_discount}
							  {hook h='displayProductPriceBlock' product=$product type="old_price"}
							  <span class="old price">{$product.regular_price}</span>
							{/if}

							{hook h='displayProductPriceBlock' product=$product type='unit_price'}

							{hook h='displayProductPriceBlock' product=$product type='weight'}
						  </div>
					{/block}
					<div class="product-description">
						{$product.description_short|truncate:170:'...' nofilter}
					 </div>
					<a href="{$product[3]|escape:'html'}" class="product-link-button">Detalii produs</a>
					
                    <div class="product_button {if $product.cart_quantity>0} productInCart {/if}">
						<a class="ajax-add-to-cart product-btn cart-button btn-effect btn-default" data-id-product="{$product[0]}" data-minimal-quantity="1" data-token="{if isset($static_token) && $static_token}{$static_token}{/if}" title="{l s='Add to Cart'}">
							    <span class="text-addcart">{l s='Add to cart' d='Shop.Theme.Actions'}	</span>		
							    <span class="text-outofstock">{l s='Out of stock' d='Shop.Theme.Actions'}</span>
								<span class="text-added-cart">Ai deja in cos (<counter>{if $product.cart_quantity>0}{$product.cart_quantity}{else}0{/if}</counter>)</span>
                                <span class="fa fa-spin fa-spinner"></span>
								<span class="pe pe-7s-check"></span>
							    <input type="hidden" class="product-quantity-total" value="{$product[0].quantity+$product.cart_quantity}" />
						</a>
					</div>
					
                    <div class="product_action">						
						<a class="addToWishlist product-btn btn-effect btn-default"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product[0]}', false, 1); return false;" data-id-product="{$product[0]}" title="{l s='Add to Wishlist'}">
						  <span class="la la-heart"></span>
						  <span class="text">Wishlist</span>
						</a>

						<a  data-link-action="quickview" class="quick-view-samargelim product-btn hidden-xs" title="{l s='Quick View'}">
							<span class="fa fa-eye">
								<div class="preview_big_image">
									<img src="{$imagineProdus}" />
								</div>
							</span>
							<span class="text">quick view</span>
						</a>
					</div>
					
			</div>
		</div>
</div>


</div>

 {/foreach}


</div>

  

  

</div>

{/block}
