{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  {l s='Reset your password' d='Shop.Theme.CustomerAccount'}
{/block}

{block name='page_content'}
    <form action="{$urls.pages.password}" method="post" data-action="change_password" style="width:70%">

      <div class="form-fields row">
        <div class="col-md-12">
          <label>
            <span>{l s='Schimba parola pentru: %email%' d='Shop.Theme.CustomerAccount' sprintf=['%email%' => $customer_email|stripslashes]}</span>
          </label>
        </div>

        <div class="col-md-3">
          <span>{l s='New password' d='Shop.Forms.Labels'}</span>
        </div>
        <div class="col-md-9">
          <input type="password" data-validate="isPasswd" name="passwd" value="">
        </div>


        <div class="col-md-3">
          <span>{l s='Confirmation' d='Shop.Forms.Labels'}</span>
        </div>
        <div class="col-md-9">
          <input type="password" data-validate="isPasswd" name="confirmation" value="">
        </div>

        <div class="col-md-12 text-right">
          <input type="hidden" name="token" id="token" value="{$customer_token}">
          <input type="hidden" name="id_customer" id="id_customer" value="{$id_customer}">
          <input type="hidden" name="reset_token" id="reset_token" value="{$reset_token}">
          <button type="submit" name="submit" class="change-the-pass">
            {l s='Change Password' d='Shop.Theme.Actions'}
          </button>
        </div>
      </div>


    </form>

  {if $theMessages}
    <div class="theMessages">{$theMessages|unescape: "html" nofilter}</div>
  {/if}
{/block}

{block name='page_footer'}
  <ul>
    <li><a href="{$urls.pages.authentication}">{l s='Back to Login' d='Shop.Theme.Actions'}</a></li>
  </ul>
{/block}
