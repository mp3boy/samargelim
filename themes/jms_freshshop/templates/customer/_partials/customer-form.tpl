{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{include file='_partials/form-errors.tpl' errors=$errors['']}

<form action="{$action}" id="customer-form" class="js-customer-form" method="post" style="display:none;">
  <section>
    {block "form_fields"}
      {foreach from=$formFields item="field"}
        {block "form_field"}
          {form_field field=$field}
        {/block}
      {/foreach}
    {/block}
  </section>

  <footer class="form-footer clearfix">
    <input type="hidden" name="submitCreate" value="1">
    {block "form_buttons"}
      <button class="btn btn-primary form-control-submit pull-xs-right" data-link-action="save-customer" type="submit">
        {l s='Save' d='Shop.Theme.Actions'}
      </button>
    {/block}
  </footer>

</form>

{* FORM IS HIDDEN SO WE ARE JUST SAVING INFO*}
<script src="/themes/jms_freshshop/assets/js/jquery.js"></script>
{if $smarty.get['pass'] == 'change_pass'}
  <h3>Schimba parola</h3>
  <div class="samargelim-change-pass">
    <div class="form-group row">
      <div class="col-md-3">Parola veche</div>
      <div class="col-md-9"><input type="password" id="samargelim_old_pass" name="oldpass" class="form-control"></div>
    </div>

    <div class="form-group row">
      <div class="col-md-3">Parola noua</div>
      <div class="col-md-9"><input type="password" id="samargelim_pass" name="firstpass" class="form-control"></div>
    </div>

    <div class="form-group row">
      <div class="col-md-3">Repeta parola noua</div>
      <div class="col-md-9"><input type="password" id="samargelim_repeat_pass" name="repeatpass" class="form-control"></div>
    </div>

    <div class="form-group row" style="margin-top:10px;">
      <div class="col-md-12 text-right"><input type="button" id="change_pass_samargelim" class="btn btn-default btn-effect" value="Schimba parola"></div>
  </div>
  </div>

  <script>

    function validatePassword(password){
      return password.length > 4;
    }
    jQuery(document).ready(function(){

      $('#change_pass_samargelim').click(function(){
        var text_='';
        if ($('#samargelim_old_pass').val() === ''){
          text_ = 'Parola veche este obligatorie!';
        } else if ($('#samargelim_pass').val() === ''){
          text_ = 'Parola noua este obligatorie!';
        } else if ($('#samargelim_repeat_pass').val() === '') {
          text_ = 'Repeta Parola este obligatorie!';
        } else if (!validatePassword($('#samargelim_pass').val())){
          text_ = 'Parola trebuie sa contina cel putin 5 caractere';
        } else if ($('#samargelim_pass').val() !== $('#samargelim_repeat_pass').val()){
          text_ = 'Parolele trebuie sa fie identice!';
        }

        if (text_ === ''){
          $('input[name="password"]').val($('#samargelim_old_pass').val());
          $('input[name="new_password"]').val($('#samargelim_pass').val());
          $('input[name="psgdpr"]').prop('checked', true);
          $('#customer-form').submit();
        } else {
          alert(text_);
        }

      });



    });
  </script>

{else}
  <h3>Datele contului tau</h3>
<div class="samargelim-user-details">
  <div class="form-group row">
    <div class="col-md-3">Prenume</div>
    <div class="col-md-9"><input type="text" id="samargelim_prenume" name="samargelim_prenume" class="form-control"></div>
  </div>

  <div class="form-group row">
    <div class="col-md-3">Nume</div>
    <div class="col-md-9"><input type="text" id="samargelim_nume" name="samargelim_nume" class="form-control"></div>
  </div>

  <div class="form-group row">
    <div class="col-md-3">Email</div>
    <div class="col-md-9"><input type="text" id="samargelim_email" name="samargelim_email" class="form-control"></div>
  </div>

  <div class="form-group row">
    <div class="col-md-3">Parola contului</div>
    <div class="col-md-9"><input type="password" id="samargelim_old_pass" name="oldpass" class="form-control"></div>
  </div>


  <div class="form-group row" style="margin-top:10px;">
    <div class="col-md-6"><input name="thenewsletter" id="thenewsletter" type="checkbox" value="1"> <label for="thenewsletter">Ma abonez la newsletter</label></div>
    <div class="col-md-6 text-right"><input type="button" id="user_details_samargelim" class="btn btn-default btn-effect" value="Salveaza informatiile"></div>
  </div>
</div>

  <script>
    function validateEmail(email)
    {
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
    }

    function validateText(text){
      var re = /^[a-zA-Z]+$/;
      return re.test(text);
    }

    jQuery(document).ready(function(){

       $('#samargelim_prenume').val($('input[name="firstname"]').val());
       $('#samargelim_nume').val($('input[name="lastname"]').val());
       $('#samargelim_email').val($('input[name="email"]').val());
       if ($('input[name="newsletter"]').is(':checked')){
        $('#thenewsletter').prop('checked', true);
       }

      $('#user_details_samargelim').click(function(){
        var text_='';
        if ($('#samargelim_firstname').val() === ''){
          text_ = 'Prenumele este obligatoriu!';
        } else if (!validateText($('#samargelim_firstname').val())){
          text_ = 'Prenumele trebuie sa contina doar litere!';
        } else if ($('#samargelim_email').val() === ''){
          text_ = 'Adresa de email este obligatorie!';
        } else if (!validateEmail($('#samargelim_email').val())) {
          text_ = 'Adresa de email nu este valida!';
        } else if ($('#samargelim_old_pass').val() === ''){
          text_ = 'Parola veche este obligatorie!';
        }

        if (text_ === ''){
          $('input[name="firstname"]').val($('#samargelim_prenume').val());
          $('input[name="lastname"]').val($('#samargelim_nume').val());
          $('input[name="email"]').val($('#samargelim_email').val());
          $('input[name="psgdpr"]').prop('checked', true);
          if ($('#thenewsletter').is(':checked')){
            $('input[name="newsletter"]').prop('checked', true);
          }
          $('input[name="password"]').val($('#samargelim_old_pass').val());
          $('#customer-form').submit();
        } else {
          alert(text_);
        }

      });



    });
    </script>
{/if}
