{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  {l s='Log in to your account' d='Shop.Theme.CustomerAccount'}
{/block}


{block name='page_content'}

	<div class="left_login">
		<h2 style="color:#646464;"><i class="fa fa-user-plus" aria-hidden="true"></i> Client Nou</h2>
		<div class="customerBox account-creation">
			<form action="/autentificare?create_account=1" id="form-create-account" method="POST">
				<div class="form-group row">
					<div class="col-md-3">Nume</div>
					<div class="col-md-9"><input type="text" id="samargelim_lastname" name="lastname" class="form-control"></div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">Prenume</div>
					<div class="col-md-9"><input type="text" id="samargelim_firstname" name="firstname" class="form-control"></div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">E-mail</div>
					<div class="col-md-9"><input type="email" id="samargelim_email" name="email" class="form-control"></div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">Parola </div>
					<div class="col-md-9"><input type="password" id="samargelim_password" name="password" class="form-control"></div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">Repeta Parola</div>
				<div class="col-md-9"><input type="password" id="samargelim_repeat_password" name="repeatpassword" class="form-control"></div>
				</div>

				<div class="row" id="ca_error" >
					<div class="col-md-3">&nbsp;</div>
					<div class="col-md-9" style="color:red"></div>
				</div>

				<div class="form-group row" style="margin-top:10px;">
					<div class="col-md-6"><input name="newsletter" id="newsletter" type="checkbox" value="1"> <label for="newsletter">Ma abonez la newsletter</label></div>
					<div class="col-md-6 text-right"><input type="button" id="createtheaccount" class="btn btn-default btn-effect" value="Creaza cont nou" /></div>
				</div>
				<input type="hidden" name="psgdpr" value="1" />
				<input type="hidden" name="id_customer" value="">
				<input type="hidden" name="submitCreate" value="1">
			</form>
		</div>
	</div>
	<script
			src="https://code.jquery.com/jquery-3.5.1.min.js"
			integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			crossorigin="anonymous"></script>
	<script>
		function validateEmail(email)
		{
			var re = /\S+@\S+\.\S+/;
			return re.test(email);
		}

		function validateText(text){
			var re = /^[a-zA-Z]+$/;
			return re.test(text);
		}

		function validatePassword(password){
			return password.length > 4;
		}

		function isExistingUser(email)
		{
			var dataAccess = 0;
			$.ajax({
				url:"/printareposta/check_user_exists.php",
				method:"POST",
				async: false,
				data:'useremail='+email,
				success:function(data){
					dataAccess = data;
				}

			});

			return dataAccess;
		}

		$(document).ready(function($){
			$('#createtheaccount').click(function(){
				var text_ = '';
				if ($('#samargelim_lastname').val() === ''){
					text_ = 'Numele este obligatoriu!';
				} else if (!validateText($('#samargelim_lastname').val())){
					text_ = 'Numele trebuie sa contina doar litere!';
				} else if ($('#samargelim_firstname').val() === ''){
					text_ = 'Prenumele este obligatoriu!';
				} else if (!validateText($('#samargelim_firstname').val())){
					text_ = 'Prenumele trebuie sa contina doar litere!';
				} else if ($('#samargelim_email').val() === ''){
					text_ = 'Adresa de email este obligatorie!';
				} else if (!validateEmail($('#samargelim_email').val())){
					text_ = 'Adresa de email nu este valida!';
				} else if (isExistingUser($('#samargelim_email').val()) === "1"){
					text_ = 'Userul exista deja! Daca ai uitat parola apasa <a href="/recuperare-parola">AICI</a>';
				} else if ($('#samargelim_password').val() === ''){
					text_ = 'Parola este obligatorie!';
				} else if ($('#samargelim_repeat_password').val() === '') {
					text_ = 'Repeta Parola este obligatorie!';
				} else if (!validatePassword($('#samargelim_password').val())){
					text_ = 'Parola trebuie sa contina cel putin 5 caractere';
				} else if ($('#samargelim_repeat_password').val() !== $('#samargelim_password').val()){
					text_ = 'Parolele trebuie sa fie identice!';
				} else {
					$('#form-create-account').submit();
				}




				if (text_ !== '') {
					$('#ca_error .col-md-9').html(text_);
				} else {
					$('#ca_error .col-md-9').html('');
				}

			});

		});
	</script>

	<div class="right_login">
		<h2 style="color:#646464;"><i class="fa fa-user" aria-hidden="true"></i> Client Existent</h2>
	    {block name='login_form_container'}
	      <section class="login-form">
	        {render file='customer/_partials/login-form.tpl' ui=$login_form}
			  <div id="social-login-samargelim"></div>
	      </section>

	      {block name='display_after_login_form'}
	        {hook h='displayCustomerLoginFormAfter'}
	        {hook h='displayMobileHeader'}
	      {/block}
	      
	    {/block}
	</div>

{/block}


