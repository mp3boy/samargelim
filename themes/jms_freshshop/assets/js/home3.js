jQuery(function ($) {
	// click background dropdown-menu no hide
	$('.myaccount_box .account-list').click(function(e) {
        e.stopPropagation();
    });

	// show create-account-group
	$('.myaccount_box .login-group .btn-create-account').click(function(){
		$('.myaccount_box .login-group').removeClass('active');
		$('.myaccount_box .create-account-group').addClass('active');
	});

	// show login-group
	$('.myaccount_box .create-account-group .btn-login').click(function(){
		$('.myaccount_box .create-account-group').removeClass('active');
		$('.myaccount_box .login-group').addClass('active');
	});

	/*fixed menu*/
	$(document).ready(function() {
		var wd = $(window).width();

	    $(window).scroll(function() {
		    var scroll = $(window).scrollTop();

	        if (scroll > 160 && $(window).width() > 991) {
	            $('.header_bar').addClass('navbar-fixed-top');
	        } else {
	            $('.header_bar').removeClass('navbar-fixed-top');
	        }
	    });
	});

	// product add class addon-box
	var hsaddb = $('.main .product-row').parent().hasClass('addon-box');
	var hsaddb1 = $('.main .feature-box').parent().hasClass('addon-box');
	var hsaddb2 = $('.main .product_sidebar').parent().hasClass('addon-box');
	if (hsaddb) {
		$('.main .product-row').parent('.addon-box').addClass('left-box');
	}
	if (hsaddb1) {
		$('.main .feature-box').parent('.addon-box').addClass('right-box');
	}
	if (hsaddb2) {
		$('.main .product_sidebar').parent('.addon-box').addClass('right-box');
	}
});

$(window).load(function(){
		if($('.slider').length > 0)
		$('.slider').fractionSlider({	
			'slideTransition' : jmsslider_trans,
			'slideEndAnimation' : jmsslider_end_animate,
			'transitionIn' : jmsslider_trans_in,
			'transitionOut' : jmsslider_trans_out,
			'fullWidth' : jmsslider_full_width,
			'delay' : jmsslider_delay,
			'timeout' : jmsslider_duration,
			'speedIn' : jmsslider_speed_in,
			'speedOut' : jmsslider_speed_out,
			'easeIn' : jmsslider_ease_in,
			'easeOut' : jmsslider_ease_out,
			'controls' : jmsslider_navigation,
			'pager' : jmsslider_pagination,
			'autoChange' : jmsslider_autoplay,
			'pauseOnHover' : jmsslider_pausehover,
			'backgroundAnimation' : jmsslider_bg_animate,
			'backgroundEase' : jmsslider_bg_ease,
			'responsive' : jmsslider_responsive,
			'dimensions' : jmsslider_dimensions,
			'fullscreen' : true
		});
});

jQuery(function ($) {
    "use strict";
	if($(".megatab-carousel-1").length) {
		var megatab1Carousel = $(".megatab-carousel-1");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		megatab1Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:mag_itemsDesktop
				},
				991:{
					items:mag_itemsDesktopSmall
				},
				768:{
					items:mag_itemsTablet
				},
				481:{
					items:mag_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_mag,
		        dots: p_pag_mag,
				autoplay: auto_play_mag,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}
 	
	if($(".megatab-carousel-2").length) {
		var megatab2Carousel = $(".megatab-carousel-2");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		megatab2Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:mag_2_itemsDesktop
				},
				991:{
					items:mag_2_itemsDesktopSmall
				},
				768:{
					items:mag_2_itemsTablet
				},
				481:{
					items:mag_2_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_mag_2,
		        dots: p_pag_mag_2,
				autoplay: auto_play_mag_2,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel").length) {
		var productCarousel = $(".product-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		productCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:p_itemsDesktop
				},
				991:{
					items:p_itemsDesktopSmall
				},
				768:{
					items:p_itemsTablet
				},
				481:{
					items:p_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_p,
		        dots: p_pag_p,
				autoplay: auto_play_p,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel-1").length) {
		var product1Carousel = $(".product-carousel-1");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		product1Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:p_1_itemsDesktop
				},
				991:{
					items:p_1_itemsDesktopSmall
				},
				768:{
					items:p_1_itemsTablet
				},
				481:{
					items:p_1_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_p_1,
		        dots: p_pag_p_1,
				autoplay: auto_play_p_1,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel-sidebar").length) {
		var productsdCarousel = $(".product-carousel-sidebar");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		productsdCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:ps_itemsDesktop
				},
				991:{
					items:ps_itemsDesktopSmall
				},
				768:{
					items:ps_itemsTablet
				},
				481:{
					items:ps_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_ps,
		        dots: p_pag_ps,
				autoplay: auto_play_ps,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel-sidebar-2").length) {
		var productsd2Carousel = $(".product-carousel-sidebar-2");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		productsd2Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:ps2_itemsDesktop
				},
				991:{
					items:ps2_itemsDesktopSmall
				},
				768:{
					items:ps2_itemsTablet
				},
				481:{
					items:ps2_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_ps2,
		        dots: p_pag_ps2,
				autoplay: auto_play_ps2,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel-sidebar-3").length) {
		var productsd3Carousel = $(".product-carousel-sidebar-3");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		productsd3Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:ps3_itemsDesktop
				},
				991:{
					items:ps3_itemsDesktopSmall
				},
				768:{
					items:ps3_itemsTablet
				},
				481:{
					items:ps3_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_ps3,
		        dots: p_pag_ps3,
				autoplay: auto_play_ps3,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}
	
 	if($(".brand-carousel").length) {
		var brandCarousel = $(".brand-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		brandCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:brand_itemsDesktop
				},
				991:{
					items:brand_itemsDesktopSmall
				},
				768:{
					items:brand_itemsTablet
				},
				481:{
					items:brand_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 32,
			    nav: p_nav_brand,
		        dots: p_pag_brand,
				autoplay: auto_play_brand,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".product-carousel-1").length) {
		var product1Carousel = $(".product-carousel-1");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		product1Carousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:p_1_itemsDesktop
				},
				991:{
					items:p_1_itemsDesktopSmall
				},
				768:{
					items:p_1_itemsTablet
				},
				481:{
					items:p_1_itemsMobile
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 32,
			    nav: p_nav_p_1,
		        dots: p_pag_p_1,
				autoplay: auto_play_p_1,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}
});