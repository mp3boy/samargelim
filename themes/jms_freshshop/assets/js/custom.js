/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
 /*fixed menu*/
 jQuery(document).ready(function($) {
	$('.jms-megamenu').jmsMegaMenu({    			
		event: jmmm_event,
		duration: jmmm_duration
	});	
});	

 jQuery(document).ready(function($) {
 	if($(".jms-vermegamenu").length) {
 		$('.jms-vermegamenu').jmsVerMegaMenu({    			
			event: jmvmm_event,
			duration: jmvmm_duration
		});	
 	}
 	
 	

 	function loyaltyPoints(){
        jQuery.ajax({
    		type: 'POST',
    		headers: {"cache-control": "no-cache"},
    		url: '/modules/loyaltyrewardpoints/ajax.php?route=lrpfrontproductcontroller&rand=' + new Date().getTime(),
    		async: true,
    		cache: false,
    		data: 'action=getUserPoints',
    		success: function (result) {
    		    if (result <= 0){
    		        var numberOfLoyaltyPoints = 'Nu sunt puncte de fidelitate in comanda curenta.';
    		    } else if (result == 1){
    		        var numberOfLoyaltyPoints = 'Ai un punct de fidelitate in comanda curenta.';
    		    } else {
    		        var numberOfLoyaltyPoints = 'Ai '+result+' puncte de fidelitate in comanda curenta.';
    		    }
    			
    			jQuery('#rewardpointsContent .cartBoxEmpty').html(numberOfLoyaltyPoints);
    		
    		},
    		error: function(result){
    		    console.log(result)
    		}
    	});
    }
    loyaltyPoints();
    prestashop.on('updateCart', function (event) {
		loyaltyPoints();
	});
	
	
 	
});	

$('body').on('click', '.ajax-add-to-cart', function (event) {

	if ( $('.cart_product_infos[data-product-id="' + $(this).attr('data-id-product') + '"]').length) {
		var quantityTotal = $(this).children('.product-quantity-total').val();
		var quantityOnCart = $('.cart_product_infos[data-product-id="' + $(this).attr('data-id-product') + '"]').attr('data-quantity');
		console.log(quantityTotal+ ' ... '+quantityOnCart+' ... '+$(this).attr('data-id-product'));
		if (parseInt(quantityOnCart) >= parseInt(quantityTotal)){
			// var textModalNoQty= '<div id="smrg_modal" class="smrg_modal"><div class="modal-content"><span class="close">&times;</span><p>Cantitatea maxima pentru acest produs a fost atinsa.</p></div></div>';
			// jQuery('body').append(textModalNoQty);
			// $('.smrg_modal .close').click(function(){
			// 	$('.smrg_modal').remove();
			// });
			alert('Cantitatea maxima pentru acest produs a fost atinsa!');
			return false;
		}
	}

	event.preventDefault();
	var productInCart = 1;
	var productInCart1 = 0;
	var productRemaining = 0;
	var thisObject = $(this);

	if ($('body#product').length<=0) {

		//update buttons
		productInCart = productInCart + parseInt($(this).children('.text-added-cart').children('counter').html());
		$(this).children('.text-added-cart').children('counter').html(productInCart);
		$(this).parent().addClass('productInCart');
		var query = 'id_product=' + $(this).attr('data-id-product') + '&qty='+ $(this).attr('data-minimal-quantity') + '&token=' + $(this).attr('data-token') + '&add=1&action=update';
	} else {
		if ($(this).attr('data-id-product')){
			productInCart = productInCart + parseInt($(this).children('.text-added-cart').children('counter').html());
			$(this).children('.text-added-cart').children('counter').html(productInCart);
			$(this).parent().addClass('productInCart');
			var query = 'id_product=' + $(this).attr('data-id-product') + '&qty='+ $(this).attr('data-minimal-quantity') + '&token=' + $(this).attr('data-token') + '&add=1&action=update';

		} else {
			$('productqty').html(parseInt($('#quantity_wanted').val()) + parseInt($('productqty').html()));
			var query = 'id_product=' + $('#product_page_product_id').val() + '&qty=' + $('#quantity_wanted').val() + '&token=' + $('input[name="token"]').val() + '&add=1&action=update';

			productRemaining = parseInt($('#quantity_wanted option').length) - $('#quantity_wanted').val();
		}
	}







	//if cart has same value as product quantity append modal

	var actionURL = prestashop['urls']['base_url'] +  'index.php?controller=cart';


	$('.ajax-add-to-cart').removeClass('addtocart-selected');
	$(this).addClass('addtocart-selected');
	$(this).removeClass('checked');
	$(this).addClass('checking');


	$.post(actionURL, query, null, 'json')
		.then(function (resp) {

		if (resp.hasError){
			alert('Cantitatea maxima pentru acest produs a fost atinsa!');
		} else {
			console.log(resp);
			prestashop.emit('updateCart', {
				reason: {
					idProduct: resp.id_product,
					idProductAttribute: resp.id_product_attribute,
					linkAction: 'add-to-cart'
				}
			});

			/////////////////////////////////////////////////
			 var cart = $('#cart_block');
			//
			if (thisObject.parents('.product-miniature').find("img").eq(0).length) {
				var imgtodrag = thisObject.parents('.product-miniature').find("img").eq(0);
			} else {
				var imgtodrag = thisObject.parents('.product-detail').find('.images-product-detail img').eq(0);
			}
			//
			console.log(imgtodrag.attr('src'));
			var imgclone = imgtodrag.clone()
					.offset({
						top: imgtodrag.offset().top,
						left: imgtodrag.offset().left
					})
					.css({
						'opacity': '0.5',
						'position': 'absolute',
						'height': '150px',
						'width': '150px',
						'z-index': '999999999'
					})
					.appendTo($('body'))
					.animate({
						'top': cart.offset().top + 10,
						'left': cart.offset().left + 10,
						'width': 75,
						'height': 75
					}, 1000, 'easeInOutExpo');

				setTimeout(function () {
					cart.effect("shake", {
						times: 2
					}, 200);
				}, 1500);

				imgclone.animate({
					'width': 0,
					'height': 0
				}, function () {
					//thisObject.detach()
				});

				//if product page update quntity
				if ($('body#product').length>0) {
					var theNewQuantity = '';
					if (productRemaining>0){
						for(var x=1;x<=productRemaining;x++){
							theNewQuantity += '<option value="'+x+'">'+x+'</option>';
						}
						$('#quantity_wanted').html(theNewQuantity);
					} else {
						$('.product-add-to-cart').append('<span style="color:red">Produsul nu mai este pe stoc!</span>');
					}
				}

		}

		$(thisObject).removeClass('checking');
		$(thisObject).addClass('checked');
		window.setTimeout(function () {
			$(thisObject).removeClass('checked');
		}, 100);


	}).fail(function (resp) {
	    prestashop.emit('handleError', { eventType: 'addProductToCart', resp: resp });
	});


});




function view_as() { 
    var viewGrid = $(".view-grid"),
        viewList = $(".view-list"),
        viewListsmall = $(".view-list-small"),
        productList = $(".product_list");

	viewGrid.click(function (e) {       
        productList.removeClass("products-list-in-row");
        productList.removeClass("products-list-in-row-small");
        productList.addClass("products-list-in-column");
        $(this).addClass('active');
        viewList.removeClass("active");
        e.preventDefault()
    });

    viewList.click(function (e) {       
        productList.removeClass("products-list-in-column");
        productList.removeClass("products-list-in-row-small");
        productList.addClass("products-list-in-row");
        viewGrid.removeClass("active");
        $(this).addClass('active');        
        e.preventDefault()
    });
    
    viewListsmall.click(function (e) {       
    	var hscl = productList.hasClass("products-list-in-row");
    	if (!hscl) {
    		productList.addClass("products-list-in-row");
    	}
        productList.removeClass("products-list-in-column");
        // productList.removeClass("products-list-in-row");  
        productList.addClass("products-list-in-row-small");
        viewGrid.removeClass("active");
        $(this).addClass('active');        
        e.preventDefault()
    });
}
jQuery(function ($) {
    "use strict";
    $(".view-grid").addClass('active');
    view_as();
});

jQuery(function ($) {
	"use strict";
	if($(".featured-carousel").length) {
		  var featuredCarouselProduct = $(".featured-carousel");
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;
		featuredCarouselProduct.owlCarousel({
			responsiveClass:true,
			responsive:{            
				1199:{
                  items:4
				},
				991:{
					items:3
				},
				768:{
					items:2
				},
				481:{
					items:2
				},
				361:{
					items:1
				},
				0:{
					items:1
				}
			},
			rtl: rtl,
			margin: 0,
			nav: true,
			dots: false,
			autoplay: true,
			slideSpeed: 200,
			loop:true,
		});
	}

	if($(".accessories-carousel").length) {
		  var accessoriesCarouselProduct = $(".accessories-carousel");
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;
		accessoriesCarouselProduct.owlCarousel({
			responsiveClass:true,
			responsive:{            
				1199:{
                  items:4
				},
				991:{
					items:3
				},
				768:{
					items:2
				},
				481:{
					items:2
				},
				361:{
					items:1
				},
				0:{
					items:1
				}
			},
			rtl: rtl,
			margin: 0,
			nav: true,
			dots: false,
			autoplay: true,
			slideSpeed: 200,
			loop:true,
		});
	}

	if($(".featured-carousel-products").length) {
		  var featuredCarouselProduct = $(".featured-carousel-products");
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;
		featuredCarouselProduct.owlCarousel({
			responsiveClass:true,
			responsive:{            
				1199:{
                  items:4
				},
				991:{
					items:3
				},
				768:{
					items:2
				},
				481:{
					items:2
				},
				361:{
					items:1
				},
				0:{
					items:1
				}
			},
			rtl: rtl,
			margin: 0,
			nav: true,
			dots: false,
			autoplay: true,
			slideSpeed: 200,
			loop:true,
		});
	}
});
function back_to_top() {   
    $('#back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 500);
        return false;
    })
}
jQuery(function ($) {
    "use strict";
    $(window).scroll(function () {
     if ($(window).scrollTop() >= 30) {
      $("#back-to-top").stop().fadeIn(300);
     } else if ($(window).scrollTop() < $('header').outerHeight()) {
      $("#back-to-top").stop().fadeOut(300);
     }
    });
});

$(window).load(function () {     
    back_to_top(); 
});

var initialLoad = true;
$(document).ready(function() {	
	if(initialLoad){
		setTimeout(function() {
			jQuery('.preloader').fadeOut();
		}, 3000);		
		initialLoad = false;
	}
});

jQuery(function ($) {
    "use strict";
	var bodyEl = $("body"),
		content = $('.main-site'),
		openbtn = $('.top_menu p'),
		closebtn = $('.main-site' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.click(function(e) {		
			toggleMenu();
			e.stopPropagation();
		});
		if( closebtn ) {
			closebtn.click(function() {		
				toggleMenu();
			});
		}
		content.click(function(e) {		
			var target = e.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		});		
	}
	function toggleMenu() {		
		if( isOpen ) {
			bodyEl.removeClass('open');
		}
		else {
			bodyEl.addClass('open');
		}
		isOpen = !isOpen;
	}
	init();

});
jQuery(function ($) {
    "use strict";
$('.team-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:1
        }
    }
})
});

function initMap() {
		var coords = [
        {lat: 37.42006360000001, lng: -122.08215130000002, zoom: 12, maptype: 'roadmap', mapstyle:''},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 12 , maptype: 'terrain', mapstyle: ''},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 10 , maptype: 'roadmap', mapstyle:[{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]},
        {lat: -33.8688197, lng: 151.20929550000005, zoom: 15, maptype: 'roadmap', mapstyle:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 13, maptype: 'hybrid', mapstyle:''},
        {lat: 29.589869, lng: 102.04752600000006, zoom: 12 , maptype: 'satellite', mapstyle: ''},
		];
		var markers = [];
		var maps = [];
		for(var i = 0, length = coords.length; i < length; i++)
		{
			var point = coords[i];
			var latlng = new google.maps.LatLng(point.lat, point.lng);

			maps[i] = new google.maps.Map(document.getElementById('map_' + (i + 1)), {
				zoom: point.zoom,
				center: latlng,
				mapTypeId: point.maptype,
				styles: point.mapstyle  
			});

			markers[i] = new google.maps.Marker({
				position: latlng,
				map: maps[i]
			});
		}
	
	
}


jQuery(function ($) {
    "use strict";	
 	if($(".flashsales-carousel").length) {
		var flashsalesCarousel = $(".flashsales-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		flashsalesCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:1
				},
				991:{
					items:1
				},
				768:{
					items:1
				},
				481:{
					items:1
				},
				361:{
					items:1
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: false,
		        dots: false,
				autoplay: false,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".hotdeal-carousel").length) {
		var hotdealCarousel = $(".hotdeal-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		hotdealCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:h_itemsDesktop
				},
				991:{
					items:h_itemsDesktopSmall
				},
				768:{
					items:h_itemsTablet
				},
				640:{
					items:h_itemsMobile
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: false,
		        dots: false,
				autoplay: false,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".hotdeal-carousel").length) {
		var hotdealCarousel = $(".hotdeal-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		hotdealCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:h_itemsDesktop
				},
				991:{
					items:h_itemsDesktopSmall
				},
				768:{
					items:h_itemsTablet
				},
				640:{
					items:h_itemsMobile
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: false,
		        dots: false,
				autoplay: false,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".testimonial-carousel").length) {
		var testimonialCarousel = $(".testimonial-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		testimonialCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:testi_itemsDesktop
				},
				991:{
					items:testi_itemsDesktopSmall
				},
				768:{
					items:testi_itemsTablet
				},
				640:{
					items:testi_itemsMobile
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_testi,
		        dots: p_pag_testi,
				autoplay: auto_play_testi,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".blog-carousel").length) {
		var blogCarousel = $(".blog-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		blogCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:blog_itemsDesktop
				},
				991:{
					items:blog_itemsDesktopSmall
				},
				768:{
					items:blog_itemsTablet
				},
				640:{
					items:blog_itemsMobile
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: p_nav_blog,
		        dots: p_pag_blog,
				autoplay: auto_play_blog,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}

 	if($(".categories-carousel").length) {
		var categoriesCarousel = $(".categories-carousel");			
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		categoriesCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:ca_itemsDesktop
				},
				991:{
					items:ca_itemsDesktopSmall
				},
				768:{
					items:ca_itemsTablet
				},
				640:{
					items:ca_itemsMobile
				},
				0: {
					items:1
				}
			},
			rtl: rtl,
				margin: 0,
			    nav: false,
		        dots: true,
				autoplay: false,
				loop:true,
			    navigationText: ["", ""],
			    slideSpeed: 200
		});
	}	
});

jQuery(function ($) {
	// product, product-tab, mega tab product
	$('.mega-tab .nav-tabs .button').click(function(){
		var hsac = $(this).hasClass('active');
		if (!hsac) {
			$('.mega-tab .nav-tabs .button').removeClass('active');
		}
	});

	// show continue vermegamenu
	$('.vermegamenu_box .nav > li.btn-continue').click(function(){
		$(this).parent().toggleClass('show');
	});
});


$('.newsletter_title span').html('Abonează-te la newsletter ca să afli prima noutățile');

//Click on load more + change order of the second baner
jQuery(document).ready(function($) {
	$('.jms-megamenu-wrap').after('<i class="fa fa-plus-square mobile-category" aria-hidden="true">Categorii</i>');

	$('#instant-products .item').show();
	$('.instant-products .addon-box .instant-tabs .nav-tabs').hide();


	if ($(document).width()>700) {
		if ($('body#checkout section#content').length) {
			if ($('.the_left_side').height() > $('body#checkout section#content').height()) {
				$('body#checkout section#content').css('min-height', $('.the_left_side').height());
			}
		} else {
			if ($('.the_left_side').height() > $('body:not(#index) #content-wrapper').height()) {
				$('body:not(#index) #content-wrapper').css('min-height', $('.the_left_side').height());
			}
		}
	}

	$('.cometoshop').click(function(){
		$('.dialog').show();
	});	
	$('#dialog2 .close').click(function(){
		$('.dialog').hide();
	});
	$('.certificate,.certificate1').click(function(){
		$('.dialog1').show();
	});	
	$('#dialog .close').click(function(){
		$('.dialog1').hide();
	});
	if ($('#block-reassurance').length){
		$('#block-reassurance + div.card-block').hide();
		//$('.productRewardPoints x').html($('#block-reassurance + div.card-block p').html().match(/\d+/)[0]);
	}

	// var ocho=$('.the_left_side .addon-box:last-child p').html().match(/\d+/)[0];
	// if (ocho==0){
	// 	var kik='Nu sunt puncte de fidelitate in comanda curenta.';
	// }else{
	// 	var kik=ocho+' puncte de fidelitate';
	// }
	// $('#rewardpointsContent .cartBoxEmpty:first-child').html(kik);

	if ($('.tyuiop').length){
		$('.tyuiop').html(kik);
	}

	
	if ($('#_desktop_user_info .user-info .dropdown-toggle').length){
		$('#_desktop_user_info .user-info .dropdown-toggle').attr({'data-toggle':'','target':'_self'});
	}
	$('#searchbox').attr('autocomplete','off');

$(document).on('click','.quick-view-samargelim',function(e){

	var theRedirect = $(this).parents('.product-info').children('.product-link-button').attr('href');
	window.location = theRedirect;

	e.stopImmediatePropagation();
	e.preventDefault();
});

$(document).on('mouseenter','.product_action .quick-view-samargelim .fa-eye',function(){
	 
  var $this = $(this);
  var tooltip = $(this).children('.preview_big_image');

  var offset1=$(this).offset().top+tooltip.height();
  var offset2=$(window).scrollTop()+$(window).height();


  if (offset2<offset1){
  	tooltip.addClass('class111');
  }else{
  	tooltip.addClass('class222');
  }

})

$(document).on('mouseleave','.product_action .quick-view-samargelim .fa-eye',function(){
	var tooltip = $(this).find('.preview_big_image');
  	tooltip.removeClass('class111');
  	tooltip.removeClass('class222');
})



jQuery(document).on('click','.mobile-category',function(){

	if (jQuery('.the_left_side').hasClass('mobile-view-category')){
		jQuery('.the_left_side').removeClass('mobile-view-category');
	} else {
		$('html, body').animate({scrollTop: 200}, 500);
		jQuery('.the_left_side').addClass('mobile-view-category');
	}
});

jQuery(document).on('click','.finalizeaza_comanda',function(){
	//save extra info to cart
	if(!jQuery('[name="confirm-addresses"]').length) {
		alert('Adauga o adresa de livrare!');
		return;
	}
	if(jQuery('#factura').is(':checked')){ var factura=1;}else{factura=0;}
	if (factura==1 && (jQuery('#camp1').val()=="" || jQuery('#camp2').val()=="" || jQuery('#camp3').val()=="" || jQuery('#camp4').val()=="" || jQuery('#camp5').val()=="")){
	    alert('Completati datele obligatorii de facturare!');
	    return;
	}
	jQuery.ajax({
        url:'/printareposta/addtocart.php',
        method:"POST",
        data:{
            cart:jQuery('#currentcartid').val(),
            message:jQuery('#delivery_message').val(),
            factura:factura,
            buyer:jQuery('#camp1').val(),
            nrreg:jQuery('#camp2').val(),
            cui:jQuery('#camp3').val(),
            sediu:jQuery('#camp4').val(),
            judet:jQuery('#camp5').val(),
            cont:jQuery('#camp6').val(),
            banca:jQuery('#camp7').val()
        },
        success:function(data){
         //save to cart
         if(data==1){
         	//make update for brochure
			 jQuery.ajax({
				 type: 'POST',
				 headers: {"cache-control": "no-cache"},
				 url: '/modules/loyaltyrewardpoints/ajax.php?route=lrpfrontproductcontroller&rand=' + new Date().getTime(),
				 async: true,
				 cache: false,
				 data: 'action=updateBrosure',
				 success: function (result) {

				 },
				 error: function(result){
					 console.log(result)
				 }
			 });

            jQuery('.submit_order_final').click(); 
         } else {
             alert('A fost o eroare la comanda, verificati inca o data')
         }
        }
      });

	
})

/*

var count_poze=jQuery('.theproductslider .container .row .layout-column .addon-box').length;

var rand_pic=Math.floor(Math.random() * count_poze);

var theproductslider=jQuery('.theproductslider .container .row .layout-column .addon-box').eq(rand_pic).find('img');
var slide_img=theproductslider.attr('src');
var location_banner = jQuery('.theproductslider .brosura_gratuita a').attr('href');
jQuery.ajax({
	type: 'POST',
	headers: {"cache-control": "no-cache"},
	url: '/modules/loyaltyrewardpoints/ajax.php?route=lrpfrontproductcontroller&rand=' + new Date().getTime(),
	async: true,
	cache: false,
	data: 'action=getBrosure',
	success: function (result) {
		var ttimer='<span class="banner_timer"><span class="teal_text fl">Au mai ramas</span><span class="fl brosures_remaining">'+result+'</span><span class="teal_text fl">brosuri pe stoc</span></span>';

    	$('.theproductslider').remove();

		if (jQuery('#instant-products').length){

			jQuery('#instant-products .item:nth-child(4)').after('<div onclick="location.href=\''+location_banner+'\'" class="theproductslider1" style="background:url('+slide_img+') 0 0 no-repeat;">'+ttimer+'</div>');
		}
	},
	error: function(result){
		console.log(result)
	}
});

 */

jQuery('.intraincont').click(function(e){
    e.preventDefault();
    window.location = '/autentificare?back='+encodeURI(window.location);
});

jQuery('.inregistreazate').click(function(e){
    e.preventDefault();
    var newaccountlocation = jQuery(this).attr('href');
    if (jQuery('.ajax_cart_quantity').html()>0){
        newaccountlocation +='&back=order.php';
    } 
    window.location = newaccountlocation;
});


//make some fixes
jQuery('.form-control[name=vat_number]').parents('.form-group.row').hide();
jQuery('.form-control[name=company]').parents('.form-group.row').hide();
jQuery('.form-control[name=address2]').parents('.form-group.row').hide();
jQuery('.form-control[name=postcode]').val('111111')
jQuery('.form-control[name=postcode]').parents('.form-group.row').hide();
jQuery('.form-control[name=id_country]').parents('.form-group.row').hide();
jQuery('.form-control[name=address1]').parents('.form-group.row').attr('samargelim','address');
//jQuery('.form-control[name=address1]').remove();
//jQuery('[samargelim="address"]').children('.col-md-6').html('<textarea class="form-control" name="address1" rows="2" width="100%" required></textarea>');
jQuery('[name="use_same_address"], [name="use_same_address"] + label').hide();

if (jQuery('#lrp-redeem-form').length){ } else { jQuery( ".testimonialss:contains('Puncte de fidelitate')" ).hide(); }

var isTriggered = false;
$('#submit_the_checkout').on('click', function(e) {
    if (jQuery('[name="id_address_delivery"]').length){} else{   
        if(jQuery('[name="confirm-addresses"]').length && isTriggered==false){
            saveFirstAddress();
            e.preventDefault();
            e.stopPropagation();
        }
    }
});

function saveFirstAddress(){
    if (jQuery('.form-control[name=firstname]').val()==""){ jQuery('.form-control[name=firstname]').focus(); alert('Prenumele este obligatoriu')}   
else if (jQuery('.form-control[name=lastname]').val()==""){ jQuery('.form-control[name=lastname]').focus(); alert('Numele este obligatoriu')} 
else if (jQuery('.form-control[name=address1]').val()==""){ jQuery('.form-control[name=address1]').focus(); alert('Adresa este obligatorie')} 
else if (jQuery('.form-control[name=city]').val()==""){ jQuery('.form-control[name=city]').focus(); alert('Localitatea este obligatorie')} 
else if (jQuery('.form-control[name=id_state]').val()=="" || jQuery('.form-control[name=id_state]').val()==null){  alert('Alege judetul')} 
else if (jQuery('.form-control[name=phone]').val()==""){ jQuery('.form-control[name=phone]').focus(); alert('Telefonul este obligatoriu')} 
else {
    
    jQuery.ajax({
       	type: 'POST',
    		headers: {"cache-control": "no-cache"},
    		url: '/adresa?id_address=0',
    		async: true,
    		cache: false,
    		data: {
    		        firstname:jQuery('.form-control[name=firstname]').val(),
    		        lastname:jQuery('.form-control[name=lastname]').val(),
    		        address1:jQuery('.form-control[name=address1]').val(),
    		        city:jQuery('.form-control[name=city]').val(),
    		        id_state:jQuery('.form-control[name=id_state]').val(),
    		        phone:jQuery('.form-control[name=phone]').val(),
    		        address2:jQuery('.form-control[name=address2]').val(),
    		        postcode:jQuery('.form-control[name=postcode]').val(),
    		        token:jQuery('input[name=token]').val(),
    		        submitAddress:jQuery('input[name=submitAddress]').val()
    		      },
    		success: function (result) {
    		    isTriggered=true;
    		    $('#submit_the_checkout').click();
    		},
    		error: function(result){
    		    console.log(result)
    		} 
    });
     
}

}


jQuery('.form-control[name=city]').on('change',function(){
    jQuery('.delivery-options .delivery-option').show();
    if (jQuery(this).val()=="Bucuresti") {
        jQuery('.delivery-options .delivery-option:nth-child(4)').hide();
    }else{
        jQuery('.delivery-options .delivery-option:nth-child(3)').hide();
    }
})

// jQuery('.form-control[name=id_state]').on('change',function(){
//     jQuery('.delivery-options .delivery-option').show();
//     if (jQuery(this).val()=="Bucuresti") {
//         jQuery('.delivery-options .delivery-option:nth-child(4)').hide();
//     }else{
//         jQuery('.delivery-options .delivery-option:nth-child(3)').hide();
//     }
// })




});

jQuery('body:not(#index) .top_slider_homepage').remove();

jQuery(document).on('click','.comanda-button', function(){
	//if (!jQuery('.alert.alert-warning').length){
		window.location.href = '/comanda';
	// } else {
	// 	var textModalNoQty1= '<div id="smrg_modal" class="smrg_modal"><div class="modal-content"><span class="close">&times;</span><p>Verificati cantitatea produselor din cos!</p></div></div>';
	// 	jQuery('body').append(textModalNoQty1);
	// 	jQuery('.smrg_modal .close').click(function(){
	// 		jQuery('.smrg_modal').remove();
	// 	});
	// }
});

/*SEARCH BOX WITH SUGGESTIONS*/
jQuery(document).ready(function(){
	jQuery('#ajax_advsearch').click(function(){
		jQuery('.jms-advsearch').addClass('activate-samargelim-search');
		jQuery('#advsearch_result').show();
		jQuery('.input-reset-search').show();
		if (jQuery("#ajax_advsearch").val().length === 0){
			jQuery('.search_suggestion_default').show();
		}
	});

	jQuery('.search-background, .input-reset-search').click(function(){
		jQuery('.jms-advsearch').removeClass('activate-samargelim-search');
		jQuery('#advsearch_result').hide();
		jQuery('.input-reset-search').hide();
	});

	jQuery('.input-reset-search').click(function(){
		jQuery('#ajax_advsearch').val('');
		jQuery('.search_suggestion').html('');

	})



	var timer;
	jQuery("#ajax_advsearch").keyup(function(e) {
        if (timer != null) {
            clearTimeout(timer);
        }
		var search_key = jQuery(this).val();

        if (search_key.length === 0){

        	jQuery(".search_suggestion_default").show();


		} else if (search_key.length >= 3){

        	if (e.which === 13){
        		location.href = '/cauta?s='+encodeURIComponent(search_key);
			}

			jQuery('.search_suggestion_default').hide();
			timer = setTimeout(function() {
                timer = null;
				jQuery.ajax({
					type: 'GET',
					url: prestashop.urls.base_url + 'modules/jmsadvsearch/ajax_search_samargelim.php',
					data: 'search_key=' + search_key,
					beforeSend: function(){
						jQuery('.samargelim-search-preloader').show();
					},
					success: function(data)
					{
						jQuery('.search_suggestion').html(data);
						jQuery('.samargelim-search-preloader').hide();
					}
				});

			}, 100);
		} else if (search_key.length >= 0 && search_key.length <= 2) {
        	jQuery('.search_suggestion_default').hide();
        	jQuery('.search_suggestion').html('');
		}

	})

	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	if ($('.basket_quantity_samargelim').length) {
		var cantitateEroare = false;

		$('.basket_quantity_samargelim').each(function () {
			var valoareCurenta = $(this).attr('value');
			var valoareMaxima = $(this).attr('max');

			//console.log(valoareCurenta, valoareMaxima)

			if (valoareMaxima < 1) {
				console.log(valoareCurenta, valoareMaxima)
				cantitateEroare = true;
				$(this).parents('.product-line-actions').children('.row').children('.text-xs-right').children('.cart-line-product-actions').children('.remove-from-cart').click();

			} else if (valoareCurenta > valoareMaxima) {

				console.log(valoareCurenta, valoareMaxima)

				$(this).val(valoareMaxima);
				$(this).focus();
				$(this).blur();
				cantitateEroare = true;

			}

		});

		if (cantitateEroare) {
			if (!$('.samargelim_alert').length) {
				$('.cart-block h1').after(
					'<div class="alert alert-warning samargelim_alert" role="alert">Unul dintre produse are in stoc mai putin! Lista de produse a fost reactualizata!</div>'
				);
			}
		}

		if (parseInt(getCookie('samargelim_basket_updated')) === 1)
		{
			$('.cart-block h1').after(
				'<div class="alert alert-warning samargelim_alert" role="alert">Lista ta de produse a fost reactualizata!</div>'
			);
			document.cookie = "samargelim_basket_updated=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		}

			$('.basket_quantity_samargelim').on('change', function(){
				console.log(this);
				$(this).blur();
			});

	}

	$('.left_login h2').click(function(){
		//$('.right_login .login-form').hide();
		$('.left_login .customerBox.account-creation').toggle();
		$('html, body').animate({scrollTop: 210}, 500);
	});

	$('.right_login h2').click(function(){
		$('.left_login .customerBox.account-creation').hide();
		$('.right_login .login-form').toggle();
		$('html, body').animate({scrollTop: 300}, 500);
	});
});
