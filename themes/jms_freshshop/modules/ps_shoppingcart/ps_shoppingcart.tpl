<div class="btn-group compact-hidden blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if} dropdown js-dropdown {if isset($jpb_addtocart) && $jpb_addtocart == 'ajax_cartbottom'}shoppingcart-bottom{/if}" id="cart_block" data-refresh-url="{$refresh_url}">
	<a href="{$cart_url}" onclick="location.href='{$cart_url}'" class="dropdown-toggle cart-title" data-toggle="dropdown">
        <span class="fa fa-shopping-cart shopping-cart-icon"></span>

		{if {$cart.products_count} == 0 }
			<span class="tab-title">
				<span class="box-cart ajax_cart_quantity" style="text-transform: none;">Ai 0 produse in cos</span>
			</span>
		{else}
			<span class="tab-title">
				<span class="box-cart ajax_cart_quantity">{$cart.products_count}</span>
				{if $cart.products_count == 1} produs {else} produse {/if}
			</span>

			<span class="subtotal_title">
				{foreach from=$cart.subtotals item="subtotal"}
					{if {$subtotal.type} == "products"}
						{$subtotal.value}
					{/if}
				{/foreach}
			</span>
		{/if}
	</a>
	{if {$cart.products_count} != 0 }
	<div class="dropdown-menu shoppingcart-box">
	   <div class="shoppingcart-content">
        	<span class="ajax_cart_no_product" {if $cart.products_count != 0}style="display:none"{/if}>{l s='There is no product' d='Shop.Theme.Actions'}</span>
			<ul class="list products cart_block_list">
				{foreach from=$cart.products item=product}
					<li>{include 'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}</li>
				{/foreach}
			</ul>
		</div>

		<div class="cart-prices">
			<div class="checkout-info">

				<div class="cart-prices-line"  >
					<span class="label">Total - {$cart.products_count} {if $cart.products_count == 1} produs {else} produse {/if}</span>
					<span class="value pull-right">
						{foreach from=$cart.subtotals item="subtotal"}
							{if {$subtotal.type} == "products"}
								{$subtotal.value}
							{/if}
						{/foreach}
					</span>
				</div>
				{if {$cart.products_count} != 0}
				<div class="cart-button">
					<a id="button_order_cart" style="margin-top:5px;margin-bottom:5px;font-size:18px;" class="btn-default btn-effect" href="{$cart_url}" title="{l s='Check out' d='Shop.Theme'}" rel="nofollow">
						>> Vezi detalii cos
					</a> 
				</div>
				{/if}
			</div>
		</div>
	</div>
	{/if}
</div>
