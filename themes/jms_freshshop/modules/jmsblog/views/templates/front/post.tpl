{*

* 2007-2017 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2017 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}

{block name='head_seo' prepend}
  <meta property="og:title" content="{$post.fulltext nofilter}">
  <meta property="og:site_name" content="{$post.fulltext nofilter}">	
  <title>{$post.fulltext nofilter}</title>
{/block}


{extends file='page.tpl'}




{block name="page_content"}

{capture name=path}{$post.title nofilter}{/capture}

<h3 class="title-blog">{$post.title nofilter}</h3>

<div class="blog-post">

	{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}	

	<ul class="post-meta">

		{if $jmsblog_setting.JMSBLOG_SHOW_CATEGORY}

		<li class="post-category"><span>{l s='Category' d='Modules.JmsBlog'} :</span> <a href="{jmsblog::getPageLink('jmsblog-category', $catparams) nofilter}">{$post.category_name nofilter}</a></li>

		{/if}

		<li class="post-created"><span>{l s='Created' d='Modules.JmsBlog'} :</span> {$post.created|escape:'htmlall':'UTF-8'|date_format:"%B %e, %Y"}</li>

		{if $jmsblog_setting.JMSBLOG_SHOW_VIEWS}

		<li class="post-views"><span>{l s='Views' d='Modules.JmsBlog'} :</span> {$post.views nofilter}</li>		

		{/if}

		{if $jmsblog_setting.JMSBLOG_SHOW_COMMENTS}

		<li class="post-comment-count">{l s='Comments' d='Modules.JmsBlog'} :</span> <a title="Comment on {$post.title nofilter}" href="#comments"> {$comments|@count nofilter}</a></li>

		{/if}

	</ul>		

	<div class="post-fulltext">

		{$post.fulltext nofilter}	

	</div>

	{if $post.link_video && $jmsblog_setting.JMSBLOG_SHOW_MEDIA}

		<div class="post-video">
			<iframe class="blog_video" src="{$post.link_video nofilter}" width="800" height="400"></iframe>
		</div>

	

	{/if}

	

</div>
<p style="color:#fcb225; font-size:20px;">Ti-a placut acest tutorial?</p>
{if $jmsblog_setting.JMSBLOG_SHOW_SOCIAL_SHARING}

<div class="social-sharing">

{literal}

<script type="text/javascript">var switchTo5x=true;</script>

{/literal}

{if $use_https}

	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>

{else}

	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

{/if}

{literal}

<script type="text/javascript">stLight.options({publisher: "a6f949b3-864b-44c5-b0ec-4140186ad958", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

{/literal}

<span class='st_sharethis_large' displayText='ShareThis'></span>

{if $jmsblog_setting.JMSBLOG_SHOW_FACEBOOK}

<span class='st_facebook_large' displayText='Facebook'></span>

{/if}

{if $jmsblog_setting.JMSBLOG_SHOW_TWITTER}

<span class='st_twitter_large' displayText='Tweet'></span>

{/if}

{if $jmsblog_setting.JMSBLOG_SHOW_GOOGLEPLUS}

<span class='st_googleplus_large' displayText='Google +'></span>

{/if}

{if $jmsblog_setting.JMSBLOG_SHOW_LINKEDIN}

<span class='st_linkedin_large' displayText='LinkedIn'></span>

{/if}

{if $jmsblog_setting.JMSBLOG_SHOW_PINTEREST}

<span class='st_pinterest_large' displayText='Pinterest'></span>

{/if}

{if $jmsblog_setting.JMSBLOG_SHOW_EMAIL}

<span class='st_email_large' displayText='Email'></span>

{/if}

</div>

{/if}

{if $jmsblog_setting.JMSBLOG_COMMENT_ENABLE}	

<div id="comments">

{if $jmsblog_setting.JMSBLOG_FACEBOOK_COMMENT == 0}

	{if $msg == 1}<div class="success">{l s='Your comment submited' d='Modules.JmsBlog'} ! {if $jmsblog_setting.JMSBLOG_AUTO_APPROVE_COMMENT == 0} {l s='Please waiting approve from Admin' d='Modules.JmsBlog'}.{/if}</div>{/if}

{if $cerrors|@count gt 0}

	<ul>

	{foreach from=$cerrors item=cerror}

		<li class="error">{$cerror nofilter}</li>

	{/foreach}	

	</ul>

{/if}

<div id="accordion" class="panel-group">

	<div class="panel panel-default">

		<div class="comment-heading clearfix">

			<h5><a data-toggle="collapse" data-parent="#accordion" href="#post-comments">{$comments|@count nofilter} {l s='Comments' d='Modules.JmsBlog'}</a></h5>

		</div>		

		<div id="post-comments" class="panel-collapse collapse">

		{if $comments}

			{foreach from=$comments item=comment key = k}

				<div class="post-comment clearfix">

					<div class="post-comment-info">

						<div class="post-comment-img">

							<img class="attachment-widget wp-post-image img-responsive" src="{$image_baseurl nofilter}user.png" />

						</div>

						<div class="post-comment-content">

							<h6>{$comment.customer_name nofilter}</h6>

							<!-- <span class="customer_site">{$comment.customer_site nofilter}</span> -->

							<span class="time_add">{$comment.time_add nofilter}</span>

							<p class="post-comment-content">{$comment.comment nofilter}</p>

						</div>

					</div>

				</div>

			{/foreach}	

		{/if}

		</div>

	</div>

</div>

{if $jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT || (!$jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT && $logged)}	

<div class="commentForm">

	<form id="commentForm" enctype="multipart/form-data" method="post" action="index.php?fc=module&module=jmsblog&controller=post&post_id={$post.post_id nofilter}&action=submitComment">

		<div class="row">

			<div class="col-sm-12">

				<h4 class="heading">Leave a Comment</h4>

				<p class="h-info">{l s='Your email address will not be published' d='Modules.JmsBlog'}.</p>

			</div>

		</div>	

		<div class="form-group">

			<label for="content">{l s='Your Comment' d='Modules.JmsBlog'}<sup class="required">*</sup></label>

			<textarea id="comment" class="form-control" name="comment" rows="8" required></textarea>

		</div>

		<div class="row">

			<div class="col-md-6">

				<div class="form-group">

					<label for="comment_name">{l s='Your Name' d='Modules.JmsBlog'}<sup class="required">*</sup></label>

					<input id="customer_name" class="form-control" name="customer_name" type="text" value="{$customer.firstname} {$customer.lastname}" required />

				</div>	

			</div>

			<div class="col-md-6">

				<div class="form-group">

					<label for="comment_title">{l s='Your Email' d='Modules.JmsBlog'}<sup class="required">*</sup></label>

					<input id="comment_title" class="form-control" name="email" type="text" value="{$customer.email}" required />

				</div>

			</div>

		</div>

		<div class="form-group">

			<label for="comment_title">{l s='Your Website' d='Modules.JmsBlog'}</label>

			<input id="customer_site" class="form-control" name="customer_site" type="text" value=""/></br>

		</div>

		<div id="new_comment_form_footer">

			<input id="item_id_comment_send" name="post_id" type="hidden" value="{$post.post_id nofilter}" />

			<input id="item_id_comment_reply" name="post_id_comment_reply" type="hidden" value="" />

			<p class="">

				<button id="submitComment" class="btn btn-default" name="submitComment" type="submit">{l s='Post Comment' d='Modules.JmsBlog'}</button>

			</p>

		</div>

	</form>

	<script>

	$("#commentForm").validate({

	  rules: {		

		customer_name: "required",		

		email: {

		  required: true,

		  email: true

		}

	  }

	});

	</script>

</div>

{/if}

{if !$jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT && !$logged}

	{l s='Please Login to comment' d='Modules.JmsBlog'}

{/if}

{else}

	<div id="fb-root"></div>

	<script>(function(d, s, id) {

	  var js, fjs = d.getElementsByTagName(s)[0];

	  if (d.getElementById(id)) return;

	  js = d.createElement(s); js.id = id;

	  js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.4";

	  fjs.parentNode.insertBefore(js, fjs);

	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="fb-comments" data-href="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" data-width="100%" data-numposts="5"></div>

	{/if}

	</div>

{/if}

{/block}