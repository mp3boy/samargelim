{*

* 2007-2017 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2017 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}

{extends file='page.tpl'}

{block name="page_content"}

{capture name=path}{$current_category.title nofilter}{/capture}

{if isset($posts) AND $posts}		

	<div class="post-list categorylist post-3item">

		{foreach from=$posts item=post}

			{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}

			{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}				

			<div class="blog-post blog-post1">							
					<div class="post-thumb">
						<a href="{jmsblog::getPageLink('jmsblog-post', $params) nofilter}"><img src="{$image_baseurl nofilter}{$post.image nofilter}" alt="{$post.title nofilter}" class="img-responsive" /></a>			 	
					</div>
					<a href="{jmsblog::getPageLink('jmsblog-post', $params) nofilter}" class="thetitl">{$post.title nofilter}</a>
			</div>				
				

				

		{/foreach}

	</div>

{else}	

{l s='Sorry, dont have any post in this category' d='Modules.JmsBlog'}

{/if}

{/block}



