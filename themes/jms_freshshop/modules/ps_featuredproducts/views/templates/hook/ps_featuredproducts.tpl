<section class="featured-products clearfix" style="margin-top: 50px;">
	<div class="addon-title">
		<h3 class="products-section-title text-uppercase ">
			{l s='Popular Products' d='Shop.Theme.Catalog'}
		</h3>
	</div>
  
  <div class="products customs-carousel-product featured-carousel-products product-box">
    {foreach from=$products item="product"}
	    <div class="item">
	      	{include file="catalog/_partials/miniatures/product.tpl" product=$product}
      	</div>
    {/foreach}
  </div>
</section>
