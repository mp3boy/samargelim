{*
 * @package Jms Drop Megamenu
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}

<div class="jms-megamenu-wrap">
	<a id="open-button" class="open-button hidden-lg hidden-md">
		<i class="fa fa-bars mobile-menu-top" aria-hidden="true"></i>
	</a>

	{$menu_html nofilter}
</div>

<script type="text/javascript">
	var jmmm_event = '{$JMSMM_MOUSEEVENT nofilter}';
	var jmmm_duration = {$JMSMM_DURATION nofilter};	
</script>
<style type="text/css">
.jms-megamenu .dropdown-menu {    
	transition:all {$JMSMM_DURATION nofilter}ms;
}
</style>
