{*
 * @package Jms Adv Search
 * @version 1.1
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}
<div class="jms-advsearch">

		<div class="input-group-addon icon-drop-down advsearch_categories">
			<select name="id_category" id="selector_cat" class="">
			<option value="0">{l s='All Category' mod='jmsadvsearch'}</option>
				{foreach from=$jmsCategTree.children item=child name=jmsCategTree}
					{if $smarty.foreach.jmsCategTree.last}
						{include file="$branche_tpl_path" node=$child last='true'}
					{else}
						{include file="$branche_tpl_path" node=$child}
					{/if}
				{/foreach}
			</select>
		</div>
			<input type="hidden" name="fc" value="module" />
			<input type="hidden" name="module" value="jmsadvsearch" />
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="order" value="product.position.asc" />			
		<div class="input-group keyword-group jms_ajax_search">
			<input type="text" id="ajax_advsearch" name="search_query" placeholder="Cauta ce-ti doresti" class="input-search" />
			<span class="input-reset-search"></span>
			<span class="input-group-addon input-group-search btn_advsearch"></span>

		</div>
	<div id="advsearch_result">
		<div class="samargelim-search-preloader"></div>
		<div class="search_suggestion"></div>
		<div class="search_suggestion_default">
			<span class="search_suggestion_header">Cautari populare pe SaMargelim</span>
			<a class="top_search search_res" href="/cauta?s=cristale+sticla">cristale sticla</a>
			<a class="top_search search_res" href="/cauta?s=ustensile+fimo">ustensile fimo</a>
			<a class="top_search search_res" href="/cauta?s=cercei+sticla">cercei sticla</a>
			<a class="top_search search_res" href="/cauta?s=baza+inel">baza inel</a>
			<a class="top_search search_res" href="/cauta?s=inel+argintiu">inel argintiu</a>
			<a class="top_search search_res" href="/cauta?s=lant+cupru">lant cupru</a>
			<a class="top_search search_res" href="/cauta?s=cristale+opal">cristale opal</a>
			<a class="top_search search_res" href="/cauta?s=pandantiv+stea">pandantiv stea</a>
			<a class="top_search search_res" href="/cauta?s=margele+murano">margele murano</a>
			<a class="top_search search_res" href="/cauta?s=margele+lemn">margele lemn</a>
		</div>
	</div>
	<div class="search-background"></div>
</div>

