{*
 * @package Jms Drop Megamenu
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}

<div class="vermegamenu_box">
	<div class="vermegamenu_title">
		<h3>{l s='Categories' d='Shop.Theme'}</h3>
	</div>

	{$vermenu_html nofilter}
</div>

<script type="text/javascript">
	var jmvmm_event = '{$JMSVMM_MOUSEEVENT nofilter}';
	var jmvmm_duration = {$JMSVMM_DURATION nofilter};	
</script>
<style type="text/css">
.jms-vermegamenu .dropdown-menu {    
	transition:all {$JMSVMM_DURATION nofilter}ms;
}
</style>