{*
 * @package Jms Vertical Megamenu
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}
<div id="mobile-vermegamenu" class="vermegamenu_box">
	<div class="vermegamenu_title">
		<h3>{l s='Categories' d='Shop.Theme'}</h3>
	</div>

	{$vermenu_html nofilter}
</div>