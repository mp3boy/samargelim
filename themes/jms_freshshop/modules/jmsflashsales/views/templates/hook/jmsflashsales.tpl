{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
jQuery(function ($) {
    "use strict";
	var productCarousel = $(".flashsales-carousel");
  });
</script>


<div class="jmsflashsales">
	<div class="hot-deals-content">
		<div class="addon-title hot-deals-title">
			<h3>
				{l s='Deal of the day'  d="Modules.JmsFlashsale"}
			</h3>
		</div>	

		<div class="flashsales-countdown">{$expiretime|escape:'htmlall':'UTF-8'}</div>

		<div class="flashsales-carousel">	
		{foreach from=$products item=product key=k}	
		<div class="product-box-default product-box-1">
			<div class="product-miniature js-product-miniature product-preview item-gutter-{$jpb_gutterwidth}" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}">			
				<div class="preview" data-id-product="{$product.id_product}">
					<a href="{$product.link|escape:'html':'UTF-8'}" class="product-image {if $jpb_phover == 'image_swap'}image_swap{else}image_blur{/if}" data-id-product="{$product.id_product}" itemprop="url">
						<img class="img-responsive product-img1" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:html:'UTF-8'}" itemprop="image" />
					</a>
					{if isset($product.new) && $product.new == 1}
							<span class="label label-new">
								{l s='New'  d="Modules.JmsFlashsale"}
							</span>
					{/if}
					{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$configuration.is_catalog}
							<span class="label label-sale">
								{l s='Sale'  d="Modules.JmsFlashsale"}
							</span>
					{/if}
					{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}

					{block name='product_variants'}
								{if $product.main_variants}
									<div class="color_to_pick_list">
										{include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
									</div>
								{/if}
							{/block}
		            <div class="product_button">
							<a {if $product.quantity < 1}disabled{/if} class="ajax-add-to-cart product-btn cart-button btn-effect btn-default {if $product.quantity < 1}disabled{/if}" data-id-product="{$product.id}" data-minimal-quantity="{$product.minimal_quantity}" data-token="{if isset($static_token) && $static_token}{$static_token}{/if}">
								    <span class="text-addcart">{l s='Buy now' d='Shop.Theme.Actions'}	</span>		
								   <span class="text-outofstock">{l s='Out of stock' d='Shop.Theme.Actions'}</span>
		                            <span class="fa fa-spin fa-spinner"></span>
									<span class="pe pe-7s-check"></span>								   
							</a>

							<a class="addToWishlist product-btn btn-effect btn-default"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|escape:'html'}', false, 1); return false;" data-id-product="{$product.id_product|escape:'html'}" title="{l s='Add to Wishlist'}">
									<span class="icon-heart"></span>
							</a>
											
							<a  data-link-action="quickview" class="quick-view-samargelim product-btn hidden-xs">
							  <span class="icon icon-eye"></span>
							</a>
					</div>	
				</div>

				<div class="product-info">
		        {if isset($jpb_categoryname) && $jpb_categoryname}
				    <span class="categoryname">
						<a href="{url entity='category' id=$product.id_category_default}">
						{$product.category|escape:'html':'UTF-8'}</a>
					</span>
			    {/if}
				{if isset($jpb_wishlist) && $jpb_wishlist}
					<a class="addToWishlist product-btn"  onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|escape:'html'}', false, 1); return false;" data-id-product="{$product.id_product|escape:'html'}" title="{l s='Add to Wishlist'}">
							  <span class="pe pe-7s-like"></span>
					</a>
				{/if}
					{block name='product_name'}
						<a href="{$product.link|escape:'html'}" class="product-link">{$product.name|escape:'html':'UTF-8'}</a>
					{/block}
				  
					{block name='product_price_and_shipping'}
						{if $product.show_price}
						  <div class="content_price">
							{if $product.has_discount}
							  {hook h='displayProductPriceBlock' product=$product type="old_price"}
							  <span class="old price">{$product.regular_price}</span>
							{/if}
							{hook h='displayProductPriceBlock' product=$product type="before_price"}
							<span class="price new">{$product.price}</span>

							{hook h='displayProductPriceBlock' product=$product type='unit_price'}

							{hook h='displayProductPriceBlock' product=$product type='weight'}
						  </div>
						{/if}
					{/block}
					<div class="product-description">
						{$product.description_short|truncate:170:'...' nofilter}
					 </div>
				</div>
			</div>
		</div>
		{/foreach}
		</div>
	</div>
</div>