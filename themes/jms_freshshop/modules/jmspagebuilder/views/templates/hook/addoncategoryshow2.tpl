{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">	
	var ca_items = 3,
	    ca_itemsDesktop = {if $items_show}{$items_show|escape:'htmlall':'UTF-8'}{else}3{/if},
	    ca_itemsDesktopSmall = {if $items_show_md}{$items_show_md|escape:'htmlall':'UTF-8'}{else}2{/if},
	    ca_itemsTablet = {if $items_show_sm}{$items_show_sm|escape:'htmlall':'UTF-8'}{else}2{/if},
	    ca_itemsMobile = {if $items_show_xs}{$items_show_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
		// var p_nav_ca = {if $navigation == 1}true{else}false{/if};
	 //    var p_pag_ca = {if $pagination == 1}true{else}false{/if};
		// var auto_play_ca = {if $autoplay == 1}true{else}false{/if};
</script>

<div class="category-box">
	{if $addon_title}
	<div class="addon-title category-title">
		<h3>{$addon_title|escape:'htmlall':'UTF-8'}</h3>
	</div>
	{/if}
	<div class="home_categories2">
	    {if isset($categories) AND $categories}
	            <div class="categories-carousel">
	            {foreach from=$categories item=category key=k }
	                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
						<div class="categories-wrapper wrapper-banner {if $k%2==0}categories-wrapper-1{else}categories-wrapper-2{/if}">
							{if $show_img==1 }
							<div class="categoy-image">
								<a href="{$categoryLink}">
								<img src="{$link->getCatImageLink('#', $category.id_category, '')}" alt="{$category.name}" title="{$category.name}" class="img-responsive"/>
									<!-- <img src="{$link->getCatImageLink('#', $category.id_category, 'medium_default')}" alt="{$category.name}" title="{$category.name}" class="img-responsive"/> -->
								</a>
							</div>
							{/if}
							<div class="category-info icon-categories-{$k}">
								<a class="cat-name" href="{$categoryLink}">{$category.name}</a>
							</div>
						</div>
						
	            {/foreach}
	            </div>
	    {else}
	        <p>{l s='No categories' d='Shop.Theme.Global'}</p>
	  {/if}
	</div>
</div>