{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="block_myaccount_infos" class="links wrapper myaccount_box">
  <div class="title clearfix hidden-md-up myaccount_title dropdown-toggle" data-toggle="dropdown">
      <span class="h3">{l s='Your account' d='Shop.Theme.CustomerAccount'}</span>
      <i class="fa fa-user"></i>
  </div>

  <ul class="account-list dropdown-menu" id="footer_account_list">

       <!--  <li>
          <a href="{$link->getPageLink('my-account', true)}" title="{l s='View my customer account' d='Shop.Theme.CustomerAccount'}" class="account" rel="nofollow">
            {l s='Account' d='Shop.Theme.CustomerAccount'} 
          </a>
        </li>    
        <li>
          <a href="{$link->getPageLink('order', true)}" title="{l s='View my customer account' d='Shop.Theme.CustomerAccount'}" class="account" rel="nofollow">
            {l s='Checkout' d='Shop.Theme.CustomerAccount'} 
          </a>
        </li>
        <li>
          <a
            class="btn-name" href="{$logout_url}" rel="nofollow" >
            {l s='Log out' d='Shop.Theme.Actions'}
          </a>
        </li> -->


        
      <li>
        <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="header_login_form">
            <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
            <div class="form_content clearfix">
                <div class="content-group login-group active">
                  <h3 class="title">{l s='Login' d='Shop.Theme.CustomerAccount'}</h3>
                  <div class="form-group">
                      <label for="header-email">{l s='Username or email address' d='Shop.Theme.Actions'}</label>
                      <input class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="header-email" name="header-email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" />
                  </div>
                  <div class="form-group">
                      <label for="header-passwd">{l s='Password' d='Shop.Theme.Actions'}</label>
                      <span><input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes}{/if}" autocomplete="off" /></span>
                  </div>
                  <div class="button-group">
                    <div class="button_login">
                      <p class="submit">
                          <button type="button" id="HeaderSubmitLogin" name="HeaderSubmitLogin" class="btn btn-default btn-sm btn-login">
                              <!-- <i class="fa fa-lock left"></i> -->
                                  {l s='Login' d='Shop.Theme.Actions'}
                          </button>
                      </p>
                      <p>
                        <span class="check"></span>
                        {l s='Remember me' d='Shop.Theme.Actions'}
                      </p>
                      <p>
                        <a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">{l s='Lost your password?' d='Shop.Theme.Actions'}</a>
                      </p>
                    </div>

                    <div class="button_create_account">
                      <p>
                        {l s='Not registerde? No problem' d='Shop.Theme.Actions'}
                      </p>
                      <p class="submit">
                        <!-- <a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}" class="create">{l s='Tại tài khoản' d='Shop.Theme.Actions'}</a> -->
                        <a  type="button" id="HeaderSubmitCreateaccount" name="HeaderSubmitCreateaccount" class="btn btn-default btn-sm btn-create-account">
                          {l s='Create account' d='Shop.Theme.Actions'}
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <!-- /////////////////////////////// -->
                <div class="content-group create-account-group">
                  <h3 class="title">{l s='Create account' d='Shop.Theme.CustomerAccount'}</h3>
                  <div class="form-group">
                      <label for="header-email">{l s='Username or email address' d='Shop.Theme.Actions'}</label>
                      <input class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="header-email" name="header-email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" />
                  </div>
                  <div class="form-group">
                      <label for="header-passwd">{l s='Password' d='Shop.Theme.Actions'}</label>
                      <span><input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes}{/if}" autocomplete="off" /></span>
                  </div>
                  <div class="button-group">
                    <div class="button_create_account">
                      <p class="submit">
                          <button class="btn btn-primary form-control-submit pull-xs-right btn-create-account" data-link-action="save-customer" type="submit">
                            {l s='Create account' d='Shop.Theme.Actions'}
                          </button>
                      </p>
                    </div>

                    <div class="button_login">
                      <p>
                        {l s='Already have an account?' d='Shop.Theme.Actions'}
                      </p>
                      <p class="submit">
                        <a type="button" id="HeaderSubmitLogin" name="HeaderSubmitLogin" class="btn btn-default btn-sm btn-login">
                              <!-- <i class="fa fa-lock left"></i> -->
                                  {l s='Login' d='Shop.Theme.Actions'}
                          </a>
                      </p>
                    </div>
                  </div>
                </div>
                <!-- /////////////////////////////// -->
                <!-- /////////////////////////////// -->
                <div class="clearfix">
                  {hook h="displayHeaderLoginButtons"}
                </div>
            </div>
        </form>
      </li>


	</ul>
</div>
