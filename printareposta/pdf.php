<?php
require('/fpdf/fpdf.php');


class PDF_Rotate extends FPDF
{
var $angle=0;

function Rotate($angle, $x=-1, $y=-1)
{
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
    }
}    
class PDF extends PDF_Rotate
{
function RotatedText($x, $y, $txt, $angle)
{
    //Text rotated around its origin
    $this->Rotate($angle, $x, $y);
    $this->Text($x, $y, $txt);
    $this->Rotate(0);
}

function RotatedImage($file, $x, $y, $w, $h, $angle)
{
    //Image rotated around its upper-left corner
    $this->Rotate($angle, $x, $y);
    $this->Image($file, $x, $y, $w, $h);
    $this->Rotate(0);
}
}

$pdf=new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial', '', 20);
$pdf->RotatedImage('circle.png', 85, 60, 40, 16, 45);
$pdf->RotatedText(100, 60, 'Hello!', 45);
$pdf->Output();


$pdf = new FPDF();
$pdf->AddPage();
$pdf->Image('ccc.jpg',0,0,0);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(80);
$pdf->Cell(30,8,'Salut',0);
$pdf->Output();

?>