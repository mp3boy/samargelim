<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<script type="text/Javascript">
          function ch1() {
                         var tt=document.getElementById("cumparator1");
                         document.getElementById("cumparator2").value=tt.value;
                         document.getElementById("cumparator3").value=tt.value;
                         }
          function ch2() {
                         var tt=document.getElementById("nrrc1");
                         document.getElementById("nrrc2").value=tt.value;
                         document.getElementById("nrrc3").value=tt.value;
                         }
          function ch3() {
                         var tt=document.getElementById("cif1");
                         document.getElementById("cif2").value=tt.value;
                         document.getElementById("cif3").value=tt.value;
                         }
          function ch4() {
                         var tt=document.getElementById("sediu1");
                         document.getElementById("sediu2").value=tt.value;
                         document.getElementById("sediu3").value=tt.value;
                         }
          function ch5() {
                         var tt=document.getElementById("judet1");
                         document.getElementById("judet2").value=tt.value;
                         document.getElementById("judet3").value=tt.value;
                         }
          function ch6() {
                         var tt=document.getElementById("cont1");
                         document.getElementById("cont2").value=tt.value;
                         document.getElementById("cont3").value=tt.value;
                         }
          function ch7() {
                         var tt=document.getElementById("banca1");
                         document.getElementById("banca2").value=tt.value;
                         document.getElementById("banca3").value=tt.value;
                         } 
          function ch8() {
                         var tt=document.getElementById("data1");
                         document.getElementById("data2").value=tt.value;
                         document.getElementById("data3").value=tt.value;
                         document.getElementById("data4").value=tt.value;
                         }                                                                                                        
     </script>
<?php
//conectarea la baza de date
include "connect.php";
//conectarea la functia de conversie
function strLei($No, $sp='.', $pct=',' ) {
 
    // numerele literal
    $na = array ( "", "Unu", "Doi", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nb = array ( "", "Un",  "Doua", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nc = array ( "", "Una", "Doua","Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nd = array ( "", "Unu", "Doua", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
 
    // exceptie "saizeci"
    $ex1 = 'Sai';
 
    // unitati
    $ua = array ( "", "Zece", "Zeci", "Zeci","Zeci","Zeci","Zeci","Zeci","Zeci","Zeci");
    $ub = array ( "", "Suta", "Sute", "Sute","Sute","Sute","Sute","Sute","Sute","Sute");
    $uc = array ( "", "Mie", "Mii");
    $ud = array ( "", "Milion", "Milioane");
    $ue = array ( "", "Miliard", "Miliarde");
 
    // legatura intre grupuri
    $lg1 = array ("", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre");
    $lg2 = array ("", "", "Si",  "Si", "Si", "Si", "Si", "Si", "Si", "Si" );
 
    // moneda
    $mon = array ("", " leu", " lei");
    $ban = array ("", " ban ", " bani ");
 
    //se elimina $sp din numar
    $sNo = (string) $No;
    $sNo = str_replace($sp,"",$sNo);
 
    //extrag partea intreaga si o completez cu zerouri la stg.
    $NrI = sprintf("%012s",(string) strtok($sNo,$pct));
 
    // extrag zecimalele
    $Zec = (string) strtok($pct);
    $Zec = substr($Zec . '00',0,2);
 
    // grupul 4 (miliarde)
    $Gr = substr($NrI,0,3);
    $n1 = (integer) $NrI[0];
    $n2 = (integer) $NrI[1];
    $n3 = (integer) $NrI[2];
    $Rez = $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1) ? $Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2] :
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nb[$n3]:$nd[$n3]);
 
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $ue[1]):($Rez . $ue[2]));
 
    // grupul 3 (milioane)
    $Gr = substr($NrI,3,3);
    $n1 = (integer) $NrI[3];
    $n2 = (integer) $NrI[4];
    $n3 = (integer) $NrI[5];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?$Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2]:
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nb[$n3]:$nd[$n3]);
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $ud[1]):($Rez . $ud[2]));
 
    // grupul 2 (mii)
    $Gr = substr($NrI,6,3);
    $n1 = (integer) $NrI[6];
    $n2 = (integer) $NrI[7];
    $n3 = (integer) $NrI[8];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?$Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2]:
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nc[$n3]:$nd[$n3]);
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $uc[1]):($Rez . $uc[2]));
 
    // grupul 1 (unitati)
    $Gr = substr($NrI,9,3);
    $n1 = (integer) $NrI[9];
    $n2 = (integer) $NrI[10];
    $n3 = (integer) $NrI[11];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?($Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2].$mon[2]):($Rez .
       ($n2==6?$ex1:$nc[$n2]). $ua[$n2] .
       ($n3>0?$lg2[$n2]:'') . ($NrI=="000000000001"?($nb[$n3] .$mon[1]):($na[$n3]). $mon[2]));
 
    if ((integer) $NrI == 0) {$Rez = ""; }
 
    // banii
    if ((integer) $Zec>0) {
       $n2 = (integer) substr($Zec,0,1);
       $n3 = (integer) substr($Zec,1,1);
       $Rez .= ' si ';
       $lg22 = ($n3=='0'?'':$lg2[$n2]);
       $Rez = ($n2 == 1)?($Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2].$ban[2]):
       ($Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg22 . ($Zec=="01"?($nb[$n3] .
          $ban[1]):($na[$n3]). $ban[2]));
    }
 
    return $Rez;
 }


$set1=$conn->query("SELECT setare FROM setari_nir WHERE nume='urcf'")->fetch_array();
$set2=$conn->query("SELECT setare FROM setari_nir WHERE nume='serief'")->fetch_array();
$set3=$conn->query("SELECT setare FROM setari_nir WHERE nume='urcc'")->fetch_array();
$set4=$conn->query("SELECT setare FROM setari_nir WHERE nume='seriec'")->fetch_array();


    $urs = $conn->query("SELECT id_customer,id_address_delivery,total_discounts,total_paid,total_shipping FROM ps_orders WHERE id_order='" . $_GET['idz'] . "'")->fetch_array();

    $datat = date("d.m.Y");
//se adaugarea in baza de date daca nu exista


    $theSql1 = "SELECT COUNT(*) FROM f_facturi WHERE order_id='" . $_GET['idz'] . "'";
    $up = $conn->query($theSql1)->fetch_array();
    $numb = $up[0];


    if ($numb > 0) {
        $theSql11 = "SELECT * FROM f_facturi WHERE order_id='" . $_GET['idz'] . "'";
        $row = $conn->query($theSql11)->fetch_assoc();

        $num = $row['client'];
        $nrc = $row['nrrc'];
        $cif = $row['cif'];
        $sediu1 = $row['adresa'];
        $sediu2 = $row['judet'];
        $cont = $row['cont'];
        $banca = $row['banca'];


    } else {


        $sqln = "SELECT a.themessage FROM ps_cart a INNER JOIN ps_orders b ON a.id_cart=b.id_cart AND b.id_order='" . $_GET['idz'] . "'";
        $tos1 = $conn->query($sqln)->fetch_array();

        if ($tos1['themessage'] != '') {
            $rowa = json_decode($tos1['themessage'], true);
        } else {
            $rowa['buyer'] = '';
        }


        if ($rowa['buyer'] != '') {
            $num = $rowa['buyer'];
            $nrc = $rowa['nrreg'];
            $cif = $rowa['cui'];
            $sediu1 = $rowa['sediu'];
            $sediu2 = $rowa['judet'];
            $cont = $rowa['cont'];
            $banca = $rowa['banca'];

        } else {
            //$tsqls="SELECT * FROM ps_customer as a INNER JOIN ps_address as b INNER JOIN ps_state as c ON a.id_customer=b.id_customer AND b.id_state=c.id_state AND a.id_customer='".$urs['id_customer']."'";
            $tsqls = "SELECT * FROM ps_customer WHERE id_customer='" . $urs['id_customer'] . "'";
            $dff = $conn->query($tsqls)->fetch_array();

            $num = $dff['firstname'] . " " . $dff['lastname'];
            $nrc = "";
            $cif = "";


            $tsqls1 = "SELECT * FROM ps_address WHERE id_address='" . $urs['id_address_delivery'] . "'";
            $dff1 = $conn->query($tsqls1)->fetch_array();
            $sediu1 = $dff1['city'] . ", " . $dff1['address1'];


            if ($dff1['id_state'] != 0) {
                $tsqls2 = "SELECT * FROM ps_state WHERE id_state='" . $dff1['id_state'] . "'";
                $dff2 = $conn->query($tsqls1)->fetch_array();
                $sediu2 = $dff2['name'];
            } else {
                $sediu2 = "Bucuresti";
            }

            $cont = "";
            $banca = "";
        }


        $sqlo = "INSERT INTO f_facturi (`order_id`, `nr_curent`, `serie`, `client`,`valoare`, `nrrc`, `cif`, `adresa`, `judet`, `banca`, `cont`, `nr_chitanta`, `chitanta`, `data_factura`, `data_ultima_modificare`) VALUES ('" . $_GET['idz'] . "', '" . $set1['setare'] . "', '" . $set2['setare'] . "', '" . $num . "','0', '" . $nrc . "', '" . $cif . "', '" . $sediu1 . "', '" . $sediu2 . "', '" . $banca . "', '" . $cont . "', '" . $set3['setare'] . "', '" . $_GET['chitanta'] . "', '" . $datat . "', '" . $datat . "')";

        $cc = $conn->query($sqlo);
        $numr1 = $set1['setare'] + 1;
        $setdas1 = $conn->query("UPDATE setari_nir SET setare='" . $numr1 . "' WHERE nume='urcf'");

        ///daca a fost creat deja bonul atunci adun iarasi
        $sport1 = $conn->query("SELECT * FROM ordine_fb WHERE idcomanda='" . $_GET['idz'] . "' AND (buton=1 OR buton=2)")->num_rows;
        if ($sport1 != 0) {
            //update in baza de date a cantitatii din niruri

            $pul1 = $conn->query("SELECT * FROM ps_order_detail WHERE id_order='" . $_GET['idz'] . "'");
            while ($txt1 = $pul1->fetch_assoc()) {

                $polo = $conn->query("SELECT cantitate FROM nir_products WHERE id_nir='99999' AND id_produs='" . $txt1['product_id'] . "'")->fetch_array();
                $canti = $polo['cantitate'] + $txt1['product_quantity'];
                $sffo1 = "UPDATE nir_products SET cantitate='" . $canti . "' WHERE id_nir='99999' AND id_produs='" . $txt1['product_id'] . "'";
                $msd1 = $conn->query($sffo1);
            }
        }

        if ($_GET['chitanta'] == "1") {
            $numr2 = $set3['setare'] + 1;
            $setfsd3 = $conn->query("UPDATE setari_nir SET setare='" . $numr2 . "' WHERE nume='urcc'");
        }

        echo '<script>location.href="?idz=' . $_GET['idz'] . '&chitanta=' . $_GET['chitanta'] . '"</script>';
    }





 ?>

 <style>
.chenar { border:1px solid #000000;}

</style>
<form action="" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
      <td width="35%" valign="top">
         <table width="100%">
              <tr>
                  <td width="70" align="left">Furnizor:</td>
                  <td width="200" align="left"><b>SC Margelia Atelier SRL</b></td>
              </tr>
              <tr>
                  <td width="70" align="left">Nr.RC:</td>
                  <td width="200" align="left">J40/3769/2020</td>
              </tr>
              <tr>
                  <td width="70" align="left">CUI</td>
                  <td width="200" align="left">42401296</td>
              </tr>
              <tr>
                  <td width="70" align="left">Sediul:</td>
                  <td width="200" align="left">Bucuresti, Str. Ernest Juvara, nr 24,sec 6</td>
              </tr>
              <tr>
                  <td width="70" align="left">Contul:</td>
                  <td width="200" align="left">RO85INGB0000999910007550</td>
              </tr>
              <tr>
                  <td width="70" align="left">Banca:</td>
                  <td width="200" align="left">ING Opera</td>
              </tr>
              <tr>
                  <td width="70" align="left">Capital social:</td>
                  <td width="200" align="center"> 200 RON</td>
              </tr>
              <!-- <tr>
                 <td width="70" align="left">Cota TVA:</td>
                  <td width="200" align="left">0%</td>
              </tr>-->
         </table>
      </td>
      
      <td width="30%" valign="bottom">
          <table width="97%" align="center">
              <tr align="center">
                  <td style="font-size:42px; font-weight:bold;">Factura Fiscala</td>
              </tr>
              <tr align="center">
                  <td>
                      <table width="100%" cellpadding="0" cellspacing="0" style="border:2px solid;">
                            <tr>
                                <td width="10%"></td>
                                <td width="40%">Nr. Factura:</td>
                                <td width="50%"><?php echo $row['nr_curent'];?></td>
                            </tr>
                            <tr>
                                <td width="10%"></td>
                                <td width="40%">Serie:</td>
                                <td width="50%"><?php echo $row['serie'];?></td>
                            </tr>
                            <tr>
                                <td width="10%"></td>
                                <td width="40%">Data:</td>
                                <td width="50%"><input type="text" name="data1" id="data1" value="<?php echo $row['data_factura'];?>" style="border:none;padding:0px;" onchange="ch8();"/></td>
                            </tr>
                      </table>
                  </td>
              </tr>
          </table>
      </td>

      <td width="35%" valign="top">
         <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                  <td width="50"></td>
                  <td width="70">Cumparator:</td>
                  <td width="200"><input type="text" id="cumparator1" name="cumparator1" value="<?=$num;?>" size="30" style="border:none;" onchange="ch1();"/></td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70">Nr. R.C:</td>
                  <td width="200"><input type="text" id="nrrc1" name="nrrc1" value="<?=$nrc;?>" size="30" style="border:none;" onchange="ch2();"/></td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70">C.I.F:</td>
                  <td width="200"><input type="text" id="cif1" name="cif1" value="<?=$cif;?>" size="30" style="border:none;" onchange="ch3();"/></td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70" valign="top">Sediul:</td>
                  <td width="200">
                  <!--<input type="text" id="sediu1" name="sediu1" value="<?php //echo $row['adresa'];?>" size="35" style="border:none;" onchange="ch4();"/> -->
                  <textarea id="sediu1" name="sediu1" style="border:none;" onchange="ch4();" style="resize:none;" rows="3" cols="25"spellcheck="false"><?=$sediu1;?></textarea>
                  </td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70">Judetul:</td>
                  <td width="200"><input type="text" id="judet1" name="judet1" value="<?=$sediu2;?>" size="30" style="border:none;" onchange="ch5();"/></td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70">Contul:</td>
                  <td width="200"><input type="text" id="cont1" name="cont1" value="<?=$cont;?>" size="30" style="border:none;"/ onchange="ch6();"></td>
              </tr>
              <tr>
                  <td width="50"></td>
                  <td width="70">Banca:</td>
                  <td width="200"><input type="text" id="banca1" name="banca1" value="<?=$banca;?>" size="30" style="border:none;"/ onchange="ch7();"></td>
              </tr>
         </table>
      </td>
</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
      <tr>
          <td width="5%" class="chenar" align="center">Nr. Crt.</td>
          <td width="35%" class="chenar" align="center">Denumirea produselor</td>
          <td width="5%" class="chenar" align="center">U.M.</td>
          <td width="10%" class="chenar" align="center">Cantitatea</td>
          <td width="15%" class="chenar" align="center">Pretul unitar <!-- (fara T.V.A.) --><br />-lei-</td>
          <td width="15%" class="chenar" align="center">Valoarea<br />-lei-</td>
        <!-- <td width="15%" class="chenar" align="center">Valoarea T.V.A. -lei- <br /></td> -->
      </tr>
      <tr>
          <td width="5%" class="chenar" align="center">0</td>
          <td width="35%" class="chenar" align="center">1</td>
          <td width="5%" class="chenar" align="center">2</td>
          <td width="10%" class="chenar" align="center">3</td>
          <td width="15%" class="chenar" align="center">4</td>
          <td width="15%" class="chenar" align="center">5</td>
       <!--  <td width="15%" class="chenar" align="center">6</td> -->
      </tr>
</table>
      
<?php 
$produs11=$conn->query("SELECT * FROM ps_order_detail WHERE id_order='".$_GET['idz']."'");
$j=0;
$suma1=0;
while($produs1=$produs11->fetch_assoc()){
    
       ///aici intreb daca s-a trecut la rand nou ;)
 $j++;      
       if (($j%27==0) && ($j!=0)) {
       ?>
         <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                 <td width="55%">
                       <table width="100%" cellpadding="0" cellspacing="0">
                               <tr>
                                    <td width="30%" class="chenar" valign="top">
                                          <div style="padding:0 5px 0 0;">Semnatura si stampila furnizorului</div>
                                          <div style="padding:100px 5px 0 0;">Operator:<br /><br /></div>  
                                    </td>
                                    <td width="70%" class="chenar">
                                    <b>Date privind expeditia</b><br />
                                    Numele delegatului:<br />
                                    Buletinul/Cartea de identitate:<br />
                                    Seria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Nr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Elib.<br />
                                    CNP:<br />
                                    Mijlocul de transport:<br />
                                    Nr.:<br />
                                    Expedierea s-a facut in prezenta noastra la data de <input type="text" name="data2" id="data2" value="<?php echo $row['data_factura'];?>" style="border:none;"/>:<br />
                                    Semnatura:<textarea style="border:none;height:5px;"></textarea>
                                    </td>
                               </tr>
                       </table> 
                 </td>   
                 <td width="45%" valign="top" style="height:100%">
                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="33.3%" height="50%" rowspan="2" valign="middle" align="center" class="chenar">Total din care: <br />accize:</td>
                                    <td width="33.3%" valign="middle" align="center" class="chenar"><br /><?php echo number_format($urs['total_paid'],"2",".","");?><br />&nbsp;</td>
                                    <!-- <td width="33.3%" valign="middle" align="center" class="chenar" style="height:25%;"><br /><?php //echo $suma12;?><br />&nbsp;</td> -->
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" class="chenar">&nbsp;<br />&nbsp;</td>
                                    <!-- <td valign="middle" align="center" class="chenar">&nbsp;<br />&nbsp;</td> -->
                                </tr>
                                <tr >
                                    <td rowspan="2" height="50%" valign="middle" align="center" class="chenar">Semnatura de primire:</td>
                                    <td  colspan="2" valign="middle" align="center" class="chenar">
                                    Total de plata <!-- (col.5+col.6) --> <br />
                                    <?php echo number_format($urs['total_paid'],"2",".","");?><br />&nbsp;</td>
                                </tr>
                              
                        </table>
                 </td>
            </tr>
        </table> 
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                  <td width="35%" valign="top">
                     <table width="100%">
                          <tr>
                              <td width="70" align="left">Furnizor:</td>
                              <td width="200" align="left"><b>SC Margelia Handmade SRL</b></td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Nr.RC:</td>
                              <td width="200" align="left">J40/752/2015</td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Sediul:</td>
                              <td width="200" align="left">Bucuresti</td>
                          </tr>
                          <tr>
                              <td width="70" align="left"></td>
                              <td width="200" align="left">Str. Ernest Juvara nr 24 sect 6</td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Contul:</td>
                              <td width="200" align="left">RO31BUCU1291215940576RON</td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Banca:</td>
                              <td width="200" align="left">Alpha Bank - Sucursala Decebal</td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Capital social:</td>
                              <td width="200" align="center">RON</td>
                          </tr>
                          <tr>
                              <td width="70" align="left">Cota TVA:</td>
                              <td width="200" align="left">0%</td>
                          </tr>
                     </table>
                  </td>
                  
                  <td width="30%" valign="bottom">
                      <table width="97%" align="center">
                          <tr align="center">
                              <td style="font-size:42px; font-weight:bold;">Factura Fiscala</td>
                          </tr>
                          <tr align="center">
                              <td>
                                  <table width="100%" cellpadding="0" cellspacing="0" style="border:2px solid;">
                                        <tr>
                                            <td width="10%"></td>
                                            <td width="40%">Nr. Factura:</td>
                                            <td width="50%"><?php echo $row['nr_curent'];?></td>
                                        </tr>
                                        <tr>
                                            <td width="10%"></td>
                                            <td width="40%">Serie:</td>
                                            <td width="50%"><?php echo $row['serie'];?></td>
                                        </tr>
                                        <tr>
                                            <td width="10%"></td>
                                            <td width="40%">Data:</td>
                                            <td width="50%"><input type="text" name="data1" id="data1" value="<?php echo $row['data_factura'];?>" style="border:none;" onchange="ch8();"/></td>
                                        </tr>
                                  </table>
                              </td>
                          </tr>
                      </table>
                  </td>
                  
                  <td width="35%" valign="top">
                     <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                              <td width="50"></td>
                              <td width="70">Cumparator:</td>
                              <td width="200"><input type="text" id="cumparator3" name="cumparatorsss" value="<?php echo $row['client'];?>" size="30" style="border:none;" onchange="ch1();"/></td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70">Nr. R.C:</td>
                              <td width="200"><input type="text" id="nrrc3" name="nrrc1sda" value="<?php echo $row['nrrc'];?>" size="30" style="border:none;" onchange="ch2();"/></td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70">C.I.F:</td>
                              <td width="200"><input type="text" id="cif3" name="cifdas1" value="<?php echo $row['cif'];?>" size="30" style="border:none;" onchange="ch3();"/></td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70" valign="top">Sediul:</td>
                              <td width="200">
                              <!--<input type="text" id="sediu1" name="sediu1" value="<?php //echo $row['adresa'];?>" size="35" style="border:none;" onchange="ch4();"/> -->
                              <textarea id="sediu3" name="sediusad1" style="border:none;" onchange="ch4();" style="resize:none;" rows="3" cols="25"spellcheck="false"><?php echo $row['adresa'];?></textarea>
                              </td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70">Judetul:</td>
                              <td width="200"><input type="text" id="judet3" name="judetsd1" value="<?php echo $row['judet'];?>" size="30" style="border:none;" onchange="ch5();"/></td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70">Contul:</td>
                              <td width="200"><input type="text" id="cont3" name="contdas1" value="<?php echo $row['cont'];?>" size="30" style="border:none;"/ onchange="ch6();"></td>
                          </tr>
                          <tr>
                              <td width="50"></td>
                              <td width="70">Banca:</td>
                              <td width="200"><input type="text" id="banca3" name="bancada1" value="<?php echo $row['banca'];?>" size="30" style="border:none;"/ onchange="ch7();"></td>
                          </tr>
                     </table>
                  </td>
            </tr>
        </table>

        <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                  <td width="5%" class="chenar" align="center">Nr. Crt.</td>
                  <td width="35%" class="chenar" align="center">Denumirea produselor</td>
                  <td width="5%" class="chenar" align="center">U.M.</td>
                  <td width="10%" class="chenar" align="center">Cantitatea</td>
                  <td width="15%" class="chenar" align="center">Pretul unitar <!-- (fara T.V.A.) --><br />-lei-</td>
                  <td width="15%" class="chenar" align="center">Valoarea<br />-lei-</td>
                 <!--  <td width="15%" class="chenar" align="center">Valoarea T.V.A. -lei- <br /> </td> -->
              </tr>
              <tr>
                  <td width="5%" class="chenar" align="center">0</td>
                  <td width="35%" class="chenar" align="center">1</td>
                  <td width="5%" class="chenar" align="center">2</td>
                  <td width="10%" class="chenar" align="center">3</td>
                  <td width="15%" class="chenar" align="center">4</td>
                  <td width="15%" class="chenar" align="center">5</td>
                  <!-- <td width="15%" class="chenar" align="center">6</td> -->
              </tr>
        </table>
       <?php } ?>

    <table width="100%" cellpadding="0" cellspacing="0">
       <tr>
          <td width="5%" class="chenar" align="center" valign="top"><?php echo $j;?></td>
          <td width="35%" class="chenar" align="left"><?php 
//$pupu=$conn->query("SELECT a.value FROM ps_feature_value_lang as a INNER JOIN ps_feature_product as b INNER JOIN ps_feature_lang as c ON a.id_feature_value=b.id_feature_value AND c.name='Material' AND b.id_feature=c.id_feature AND b.id_product='".$produs1['product_id']."'")->fetch_array();
$pupu=$conn->query("SELECT cod2 FROM ps_product_lang WHERE id_product='".$produs1['product_id']."'")->fetch_array();
          echo $pupu['cod2'];?>
              
          </td>
          <td width="5%" class="chenar" align="center">Buc</td>
          <td width="10%" class="chenar" align="right"><?php echo $produs1['product_quantity'];?></td>
          <td width="15%" class="chenar" align="right">
          <?php 
          $price=number_format($produs1['total_price_tax_incl'],"2",".","");
          echo $price;
          ?>
          </td>
          <td width="15%" class="chenar" align="right">
          <?php
          $ppoe=$price*$produs1['product_quantity'];
          $suma1=$suma1+$ppoe;
          echo number_format($ppoe,"2",".","");
          ?>
          </td>
         
      </tr>
      <?php }?>



      <?php if ($urs['total_shipping']!="0.000000") { $j++; ?>
      <tr>
          <td width="5%" class="chenar" align="center" valign="top"><?php echo $j;?></td>
          <td width="35%" class="chenar" align="left">Transport Refacturat</td>
          <td width="5%" class="chenar" align="center">Buc</td>
          <td width="10%" class="chenar" align="right">1</td>
          <td width="15%" class="chenar" align="right">
          <?php 
             echo number_format($urs['total_shipping'],"2",".","");
          ?>
          </td>
          <td width="15%" class="chenar" align="right">
          <?php
            echo number_format($urs['total_shipping'],"2",".","");
          ?>
          </td>
          
      </tr>

      <?php } ?>

<?php

 if ($urs['total_discounts']!="0.000000") { $j++;?>
      <tr>
          <td width="5%" class="chenar" align="center" valign="top"><?php echo $j;?></td>
          <td width="35%" class="chenar" align="left">Discount</td>
          <td width="5%" class="chenar" align="center">Buc</td>
          <td width="10%" class="chenar" align="right">1</td>
          <td width="15%" class="chenar" align="right">
          <?php 
          echo number_format($urs['total_discounts'],"2",".","");
          ?>
          </td>
          <td width="15%" class="chenar" align="right">
          <?php
          echo number_format($urs['total_discounts'],"2",".","");
          ?>
          </td>
         
      </tr>
<?php 
}  //intreb daca sa treaca sau nu la rand nou ;)



$brr="<br />";


?>
    </table>
<table width="100%" cellpadding="0" cellspacing="0">
        <tr>
             <td width="55%">
                   <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                                <td width="30%" class="chenar" valign="top">
                                      <div style="padding:0 5px 0 0;">Semnatura si stampila furnizorului</div>
                                      <div style="padding:100px 5px 0 0;">Operator:<br /><br /></div>  
                                </td>
                                <td width="70%" class="chenar">
                                <b>Date privind expeditia</b><br />
                                Numele delegatului:<br />
                                Buletinul/Cartea de identitate:<br />
                                Seria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Nr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Elib.<br />
                                CNP:<br />
                                Mijlocul de transport: &nbsp;&nbsp;&nbsp;&nbsp; Livrat prin Curier<br />
                                Nr.:<br />
                                Expedierea s-a facut in prezenta noastra la data de <input type="text" name="data2" id="data2" value="<?php echo $row['data_factura'];?>" style="border:none;"/>:<br />
                                Semnatura:
                                </td>
                           </tr>
                   </table> 
             </td>   
             <td width="45%" valign="top" style="height:100%">
                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="33.3%" height="50%" rowspan="2" valign="middle" align="center" class="chenar">Total din care: <br />accize:</td>
                                <td width="33.3%" valign="middle" align="center" class="chenar"><br /><?php echo number_format($urs['total_paid'],"2",".","");?><br />&nbsp;</td>
                                <!-- <td width="33.3%" valign="middle" align="center" class="chenar" style="height:25%;"><br /><?php //echo $suma12;?><br />&nbsp;</td> -->
                            </tr>
                            <tr>
                                <td valign="middle" align="center" class="chenar">&nbsp;<br />&nbsp;</td>
                               <!--  <td valign="middle" align="center" class="chenar">&nbsp;<br />&nbsp;</td> -->
                            </tr>
                            <tr >
                                <td rowspan="2" height="50%" valign="middle" align="center" class="chenar">Semnatura de primire:</td>
                                <td  colspan="2" valign="middle" align="center" class="chenar">
                                Total de plata <!-- (col.5+col.6) --> <br />
                                <?php echo number_format($urs['total_paid'],"2",".","");?><br />&nbsp;</td>
                            </tr>
                          
                    </table>
             </td>
        </tr>
</table>
<textarea style="border:none;overflow:visible;"></textarea>




<?php
if ($row['chitanta']=="1") {
?>
<br /><br />
<div style="page-break-inside: avoid">
      <table width="100%" cellpadding="0" cellspacing="0" style="border-top:2px dotted; border-left:1px solid;border-right:1px solid;border-bottom:1px solid;">
             <tr><td colspan="2">&nbsp;</td></tr>
             <tr>
                <td width="50%" align="left" valign="top">
                      <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                           <td width="5%">&nbsp;</td>
                           <td width="30%" align="left">Furnizor:</td>
                           <td width="65%" align="left">SC Margelia Handamde SRL</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">Nr.ord.reg.com./an:</td>
                           <td align="left">J40/752/2015</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">C.U.I:</td>
                           <td align="left">34011497</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">Capital social:</td>
                           <td align="left">&nbsp;</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">Sediul:</td>
                           <td align="left">Loc. Bucuresti</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">&nbsp;</td>
                           <td align="left">Str. Ernest Juvara nr 24, sect 6</td>
                           </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td align="left">Judetul:</td>
                           <td align="left">Bucuresti</td>
                           </tr>
                      </table>
                </td>
                <td width="50%" align="left" valign="middle">
                <span style="font-size:42px; font-weight:bold;">Chitanta</span><br /><br />
                <table width="70%" align="center">
                     <tr>
                     <td width="50%" align="left">Seria: <?php echo $set4['setare'];?></td>
                     <td width="50%" align="left">Nr.: <?php echo $row['nr_chitanta'];?></td>
                     </tr>
                     <tr>
                     <td width="50%" align="left">Data: <input type="text" name="data3" id="data3" value="<?php echo $row['data_factura'];?>" style="border:none;"></td>
                     <td width="50%" align="left">&nbsp;</td>
                     </tr>
                </table>
                </td>
             </tr>
             <tr><td colspan="2">&nbsp;</td></tr>
             <tr>
             <td colspan="2">
                  <table width="98%" align="right" cellpadding="0" cellspacing="0">
                          <tr>
                               <td width="20%" style="border-bottom:1px solid;" align="left">Am primit de la:</td>
                               <td width="40%" colspan="2" style="border-bottom:1px solid;" align="left"><input type="text" name="cumparator2" id="cumparator2" value="<?php echo $row['client'];?>" style="border:none;" size="48" /></td>
                               <td width="20%" style="border-bottom:1px solid;" align="left">,Nr. R.C.: <input type="text" value="<?php echo $row['nrrc'];?>" style="border:none;" size="10" name="nrrc2" id="nrrc2" /></td>
                               <td width="20%" style="border-bottom:1px solid;" align="left">,C.I.F.: <input type="text" value="<?php echo $row['cif'];?>" style="border:none;" size="10" name="cif2" id="cif2" /></td>
                          </tr>
                          <tr>
                               <td width="20%" style="border-bottom:1px solid;" align="left">Adresa:</td>
                               <td width="40%" colspan="2" style="border-bottom:1px solid;" align="left"><input type="text" value="<?php echo $row['adresa'];?>" style="border:none;" name="sediu2" id="sediu2" size="48"/></td>
                               <td width="20%" style="border-bottom:1px solid;" align="left">,<input type="text" value="<?php echo $row['judet'];?>" style="border:none;" name="judet2" id="judet2" /></td>
                               <td width="20%" style="border-bottom:1px solid;" align="left">&nbsp;</td>
                          </tr>
                           <tr>
                               <td width="20%" style="border-bottom:1px solid;" align="left">Suma de:</td>
                               <td width="20%" style="border-bottom:1px solid;" align="left"><input type="text" value="<?php echo number_format($urs['total_paid'],"2",".",""); ?>" style="border:none;"></td>
                               
                               <td width="60%" colspan="3" style="border-bottom:1px solid;" align="left"><?php echo strLei(number_format($urs['total_paid'],"2",",",""));?></td>
                          </tr>
                          <tr>
                               <td width="20%" style="border-bottom:1px solid;" align="left">Reprezentand:</td>
                               <td width="80%" colspan="4"style="border-bottom:1px solid;" align="left">
                               Plata factura Seria: <?php echo $row['serie'];?>, Nr. <?php echo $row['nr_curent'];?> / <input type="text" value="<?php echo $row['data_factura'];?>" style="border:none;" size="20" name="data4" id="data4"/>
                               </td>
                          </tr>
                          <tr><td colspan="5">&nbsp;</td></tr>
                          <tr><td colspan="5" align="right">Semnatura si stampila:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
                          <tr><td colspan="5">&nbsp;</td></tr>
                          <tr><td colspan="5">&nbsp;</td></tr>
                          <tr><td colspan="5">&nbsp;</td></tr>
                          <tr><td colspan="5">&nbsp;</td></tr>
                  </table>
             </td>
             </tr>
      </table>
</div>


<?php  }
 ?> 
<br />
<input type="submit" name="salveaza_factura" value="salveaza" style="border:none;"/>
<input type="hidden" name="sfr" value="okers"/>
</form>

<?php
//salvarea calculatiei in baza facturii
if ($numb!=0) { } else { $cc1=$conn->query("UPDATE f_facturi set valoare='".number_format($urs['total_paid'],"2",".","")."' WHERE order_id='".$_GET['idz']."'"); }
//modificarea aferanta oricarei facturi
if (!isset($_POST['sfr'])) {}
elseif ($_POST['sfr']=="okers")
{
$theSql = "UPDATE f_facturi SET `client` = '".$_POST['cumparator1']."',`nrrc` = '".$_POST['nrrc1']."',`cif` = '".$_POST['cif1']."',`adresa` = '".$_POST['sediu1']."',`judet` = '".$_POST['judet1']."',`banca` = '".$_POST['banca1']."',`cont` = '".$_POST['cont1']."',`data_factura` = '".$_POST['data1']."',`data_ultima_modificare` = '".$datat."' WHERE `order_id` ='".$_GET['idz']."'";
$pp=$conn->query($theSql);
echo "<script>location.href='?idz=$_GET[idz]&chitanta=$_GET[chitanta]'</script>";                                                
}

?>
