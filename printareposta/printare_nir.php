<?php
session_start();
require('rotation.php');
include "connect.php";

error_reporting(0);


class PDF extends PDF_Rotate
{
function RotatedText($x, $y, $txt, $angle)
{
    //Text rotated around its origin
    $this->Rotate($angle, $x, $y);
    $this->Text($x, $y, $txt);
    $this->Rotate(0);
}

function RotatedImage($file, $x, $y, $w, $h, $angle)
{
    //Image rotated around its upper-left corner
    $this->Rotate($angle, $x, $y);
    $this->Image($file, $x, $y, $w, $h);
    $this->Rotate(0);
}
}
// $puf=mysql_fetch_array(mysql_query("SELECT * FROM nir WHERE id='".$_GET['nir']."'"));
// $ruf=mysql_fetch_array(mysql_query("SELECT * FROM furnizori WHERE id='".$puf['id_furnizor']."'"));

$puf=$conn->query("SELECT * FROM nir WHERE id='".$_GET['nir']."'")->fetch_array();
$ruf=$conn->query("SELECT * FROM furnizori WHERE id='".$puf['id_furnizor']."'")->fetch_array();


$tx0="NOTA DE INTRARE RECEPTIE NR.______";
$tx1="Firma:";    $tx10="SC Edora Corporation SRL";
$tx2="Nr. R.C.:"; $tx20="J40/6125/2006";
$tx3="C.I.F.:";   $tx30="RO18577681";
$tx4="Data.........................";
$tx5="Se receptioneaza marfurile furnizate de ...................................................................din localitatea..........................................................";
$tx6="CUI..........................Nr.Reg.Com............................conform facturii Nr......................................................din data de......................astfel:";

$numefurnizor = $ruf['denumire'];
$localitatefurnizor=$ruf['localitate'];
$cuifurnizor=$ruf['cf'];
$nrcfurnizor=$ruf['nrc'];
$sn=$puf['nr_factura']."  ".$puf['serie'];
$dnir=$puf['data_nir'];

$idnir=$_GET['nir'];
//tipul paginii
$pdf = new PDF('L','mm','A4');
$pdf->AddPage();

//Aici incep datele pentru formular
$pdf->SetFont('Arial', 'B', 27);   $pdf->RotatedText(90,15, $tx0,0); 

$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(10,10, $tx1,0);  $pdf->RotatedText(30,10, $tx10,0);
$pdf->RotatedText(10,15, $tx2,0);  $pdf->RotatedText(30,15, $tx20,0);
$pdf->RotatedText(10,20, $tx3,0);  $pdf->RotatedText(30,20, $tx30,0);

$pdf->SetFont('Arial', 'B', 13);  $pdf->RotatedText(130,23, $tx4,0); $pdf->RotatedText(8,35, $tx5,0);$pdf->RotatedText(8,42, $tx6,0);
$pdf->SetFont('Arial', '', 14);  $pdf->RotatedText(94,34, $numefurnizor,0); $pdf->RotatedText(212,34, $localitatefurnizor,0);
$pdf->RotatedText(17,41, $cuifurnizor,0);$pdf->RotatedText(77,41, $nrcfurnizor,0);$pdf->RotatedText(156,41, $sn,0);
$pdf->RotatedText(247,41, $dnir,0);  $pdf->RotatedText(141,22, $dnir,0);  
$pdf->SetFont('Arial', 'B', 14);
$pdf->RotatedText(255,15, $idnir,0);
//inceputul tabelului
$pdf->Image('table_header.jpg',6,50,285);
//constantele din headerul tabelului
$suma1a=0;
$suma2a=0;
$suma3a=0;
$suma4a=0;
$suma5a=0;
$op1="ARTICOLE";$op2="RECEPTIONAT";$op3="ADAOS";$op4="DENUMIRE";$op5="U.M.";
$op60="Cantitate conf.";$op61="Document";
$op70="Cantitate";$op71="recept.";
$op80="Pret";$op81="unitate";$op82=" ";
$op90="Valoare";$op91="fara tva";
$op10="TVA";
$ops0="Valoare";$ops1="cu tva"; $ops2="%";
$ops3="P.U. cu";$ops4="amanuntul"; $ops5="cu tva";
$ops6="Total";$ops7="tva";
$ops8="Valoare cu";$ops9="amanuntul";$opsp="cu tva";
$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(55,54, $op1,0);  $pdf->RotatedText(150,54, $op2,0); $pdf->RotatedText(213,54, $op3,0);
$pdf->RotatedText(33,63, $op4,0);  $pdf->RotatedText(78,63, $op5,0);
$pdf->RotatedText(91,61, $op60,0); $pdf->RotatedText(94,65, $op61,0);
$pdf->RotatedText(117,61, $op70,0); $pdf->RotatedText(119,65, $op71,0);
$pdf->RotatedText(137,59, $op80,0); $pdf->RotatedText(135,62.5, $op81,0);$pdf->RotatedText(135,66, $op82,0);
$pdf->RotatedText(152,61, $op90,0); $pdf->RotatedText(152,65, $op91,0);
$pdf->RotatedText(172,63, $op10,0);
$pdf->RotatedText(187,61, $ops0,0); $pdf->RotatedText(188,65, $ops1,0);
$pdf->RotatedText(205,63, $ops0,0);
$pdf->RotatedText(226,63, $ops2,0);
$pdf->RotatedText(238,56, $ops3,0); $pdf->RotatedText(235.5,60, $ops4,0); $pdf->RotatedText(239,64, $ops5,0);
$pdf->RotatedText(257,58, $ops6,0); $pdf->RotatedText(259,63, $ops7,0);
$pdf->RotatedText(271,56, $ops8,0); $pdf->RotatedText(271,60, $ops9,0); $pdf->RotatedText(275,64, $opsp,0);

$suma1=0;$suma2=0;$suma3=0;$suma4=0;$suma5=0;
$i=0;
$pdf->SetFont('Arial', 'B', 10);


// $pro=mysql_query("SELECT * FROM nir_products WHERE id_nir='".$_GET['nir']."'");
$pro=$conn->query("SELECT * FROM nir_products WHERE id_nir='".$_GET['nir']."'");

//$tv=mysql_fetch_array(mysql_query("SELECT setare FROM setari_nir WHERE nume='tva1'")); ///extrag tva1
$tv=$conn->query("SELECT setare FROM setari_nir WHERE nume='tva1'")->fetch_array(); ///extrag tva1


while($qq=$pro->fetch_assoc()) {

//$pup=mysql_fetch_array(mysql_query("SELECT cod2 FROM products_to_categories WHERE products_id='".$qq['id_produs']."'"));

$pup=$conn->query("SELECT a.value FROM ps_feature_value_lang as a INNER JOIN ps_feature_product as b INNER JOIN ps_feature_lang as c ON a.id_feature_value=b.id_feature_value AND c.name='Material' AND b.id_feature=c.id_feature AND b.id_product='".$qq['id_produs']."'")->fetch_array();


//$popp=mysql_fetch_array(mysql_query("SELECT * FROM products WHERE products_id='".$qq['id_produs']."'"));
$popp=$conn->query("SELECT * FROM ps_product_lang as a INNER JOIN ps_product as b ON a.id_product=b.id_product AND a.id_product='".$qq['id_produs']."'")->fetch_array();

if ($i==29) { 
            $pdf->AddPage(); 
            }
if ($i>=29)  {           
            //headerul tabelului
            $pdf->Image('table_header.jpg',6,10,285);
            $pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(55,14, $op1,0);  $pdf->RotatedText(150,14, $op2,0); $pdf->RotatedText(213,14, $op3,0);
$pdf->RotatedText(33,23, $op4,0);  $pdf->RotatedText(78,23, $op5,0);
$pdf->RotatedText(91,21, $op60,0); $pdf->RotatedText(94,25, $op61,0);
$pdf->RotatedText(117,21, $op70,0); $pdf->RotatedText(119,25, $op71,0);
$pdf->RotatedText(137,19, $op80,0); $pdf->RotatedText(135,22.5, $op81,0);$pdf->RotatedText(135,26, $op82,0);
$pdf->RotatedText(152,21, $op90,0); $pdf->RotatedText(152,25, $op91,0);
$pdf->RotatedText(172,23, $op10,0);
$pdf->RotatedText(187,21, $ops0,0); $pdf->RotatedText(188,25, $ops1,0);
$pdf->RotatedText(205,23, $ops0,0);
$pdf->RotatedText(226,23, $ops2,0);
$pdf->RotatedText(238,16, $ops3,0); $pdf->RotatedText(235.5,20, $ops4,0); $pdf->RotatedText(239,24, $ops5,0);
$pdf->RotatedText(257,18, $ops6,0); $pdf->RotatedText(259,23, $ops7,0);
$pdf->RotatedText(271,16, $ops8,0); $pdf->RotatedText(271,20, $ops9,0); $pdf->RotatedText(275,24, $opsp,0);

            $it=27.8+(($i-29)*4.7);    //inaltimea randului1
            $it1=31.3+(($i-29)*4.7); //inaltimea textului
            } else {
$it=67.8+($i*4.7);    //inaltimea randului1
$it1=71.3+($i*4.7); //inaltimea textului
                   }
$cantitate=number_format($qq['cantitate'],"2",",",".");
$pft=number_format($popp['piftva'],"2",",",".");

$valftva1=$qq['cantitate']*$popp['piftva'];
$valftva=number_format($valftva1,"2",",",".");

$valcutva1=($tv['setare']*$valftva1/100)+$valftva1;
$valcutva=number_format($valcutva1,"2",",",".");

$resttva1=$valcutva1-$valftva1;
$resttva=number_format($resttva1,"2",",",".");

$pretv=number_format($popp['price'],"2",",",".");

$valtot1=number_format($popp['price'],"2",".","")*$qq['cantitate'];




$valtot=number_format($valtot1,"2",",",".");

$tvaval1=$valtot1-($valtot1/1.24);
$tvaval=number_format($tvaval1,"2",",",".");


$adaos1=100*(number_format($popp['price'],"2",".","")-$popp['picutva'])/$popp['picutva'];
$adaos=number_format($popp['adaos'],"2",",",".");

$sumaadaos1=$valftva1*$popp['adaos']/100;
//$sumaadaos1=$popp['products_id'];
//$sumaadous1=$popp['adaos'];
$sumaadaos=number_format($sumaadaos1,"2",",",".");

//randurile la tabel
$pdf->Image('rand_tabel1.jpg',6,$it,285);
$pdf->RotatedText(6.5,$it1, $pup['value'],0);  //denumire
// $pdf->RotatedText(6.5,$it1, 'TEST',0);  //denumire
$pdf->RotatedText(78,$it1, 'Buc.',0);         //Buc.
$pdf->RotatedText(95,$it1, $cantitate,0);     //Cantitate
$pdf->RotatedText(119,$it1, $cantitate,0);    //Cantitate
$pdf->RotatedText(134,$it1, $pft,0);          //Pret intrare fara tva
$pdf->RotatedText(151,$it1, $valftva,0);          //Valoare fara tva .


$suma1a +=$valftva1;

$suma1=number_format($suma1a,"2",",",".");
$pdf->RotatedText(171,$it1, $resttva,0);           //Diferenta TVA          .
$suma2a=$suma2a+$resttva1;  
$suma2=number_format($suma2a,"2",",",".");        
$pdf->RotatedText(184,$it1, $valcutva,0);         //Total intrare cu tva


$pdf->RotatedText(204,$it1, $sumaadaos,0);      //Suma adaos     .
$suma3a=$suma3a+$sumaadaos1;
$suma3=number_format($suma3a,"2",",",".");
$pdf->RotatedText(221,$it1, $adaos,0);          //Adaos


$pdf->RotatedText(237,$it1, $pretv,0);           //Pret vanzare

$pdf->RotatedText(255,$it1, $tvaval,0);          //TVA vanzare   .
$suma4a=$suma4a+$tvaval1;  
$suma4=number_format($suma4a,"2",",",".");

$pdf->RotatedText(272,$it1, $valtot,0);          //TVA vanzare   .
$suma5a=$suma5a+$valtot1;
$suma5=number_format($suma5a,"2",",",".");
$i++;



                                   }
if ($i<29) {
$its=67.8+($i*4.7); 
$its1=71.3+($i*4.7);
           } else {
                  $its=27.8+(($i-29)*4.7); 
                  $its1=31.3+(($i-29)*4.7);
                  }
                                  
//sumele din partea de jos a tabelului
$pdf->Image('table_bottom.jpg',6,$its,285);
$pdf->RotatedText(151,$its1, $suma1,0);
$pdf->RotatedText(171,$its1, $suma2,0); 
$pdf->RotatedText(204,$its1, $suma3,0);
$pdf->RotatedText(255,$its1, $suma4,0); 
$pdf->RotatedText(272,$its1, $suma5,0);

$summm=$suma1a+$suma2a;
$summm=number_format($summm,"2",",",".");
$toi="Total valoare + TVA:      ".$summm;
$dunga="________________________________________________________________________________________________________________";
$linia1="Comisia de receptie:                                             Semnatura:                                        Data:                                    Primit in gestiune:";
$linia2="Nume si prenume:                                                                                                                                                         Semnatura:";

//total
$pdf->SetFont('Arial', 'B', 13);
$pdf->RotatedText(5,$its1+15, $toi,0);
$pdf->RotatedText(5,$its1+20, $dunga,0);
$pdf->SetFont('Arial', 'B', 11); 
$pdf->RotatedText(5,$its1+25, $linia1,0); 
$pdf->RotatedText(5,$its1+30, $linia2,0);                               
$pdf->Output();

?> 