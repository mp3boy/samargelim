<?php
session_start();
require('rotation.php');

class PDF extends PDF_Rotate
{
function RotatedText($x, $y, $txt, $angle)
{
    //Text rotated around its origin
    $this->Rotate($angle, $x, $y);
    $this->Text($x, $y, $txt);
    $this->Rotate(0);
}

function RotatedImage($file, $x, $y, $w, $h, $angle)
{
    //Image rotated around its upper-left corner
    $this->Rotate($angle, $x, $y);
    $this->Image($file, $x, $y, $w, $h);
    $this->Rotate(0);
}
}

//session_start();
//datele de la formular
$nume=$_SESSION['nume'];
$prenume=$_SESSION['prenume'];
$np=$nume." ".$prenume;
$adresacompleta=$_SESSION['adresacompleta'];
//$valoareincifre=$_SESSION['valincifre'];
$valoareincifre="= ".$_SESSION['valincifre']." =";
$strada=$_SESSION['str'];
$ramburscifre="= ".$_SESSION['ramburscifre']." =";
//$ramburscifre=$_SESSION['ramburscifre'];
$numarul=$_SESSION['nr'];
$valoareinlitere=$_SESSION['valinlitere'];
$bloc=$_SESSION['bloc'];
$rambursinlitere=$_SESSION['rambursinlitere'];
$scara=$_SESSION['scara'];
$etaj=$_SESSION['etaj'];
$sumatotalcifre=$_SESSION['sumatotalcifre'];
$sumatotalalitere=$_SESSION['sumatotalalitere'];
$codpostal=$_SESSION['codpostal'];
$localitate=$_SESSION['localitate'];
$judetul=$_SESSION['judetul'];
$telefon=$_SESSION['telefon'];
$apartament=$_SESSION['apartament'];
$sector=$_SESSION['sector'];
$valo="";
if ($sector!="") {$valo="Sect.".$sector;}
if ($strada!="") {$valo.=" Str.".$strada;}
if ($numarul!="") {$valo.=" Nr.".$numarul;}
if ($bloc!="") {$valo.=" Bl.".$bloc;}
if ($scara!="") {$valo.=" Sc.".$scara;}
if ($etaj!="") {$valo.=" Et.".$etaj;}
if ($apartament!="") {$valo.=" Ap.".$apartament;}
$valo1="";
if ($sector!="") {$valo1="Sect.".$sector;}
if ($bloc!="") {$valo1.=" Bl.".$bloc;}
if ($scara!="") {$valo1.=" Sc.".$scara;}
if ($etaj!="") {$valo1.=" Et.".$etaj;}
if ($apartament!="") {$valo1.=" Ap.".$apartament;}
//date constante
$felultrimiterii="Colet Postal";
$continutul="Accesorii";
$cui="34011497";
$nrcont="RO31BUCU1291215940576RON";
$codcont="";
$deschiscont="Alpha Bank suc Decebal";
//date expeditor
  $dateFirma['denumire'] = 'Margelia Handmade SRL';
  $dateFirma['telefon'] = '0723737752';
  $dateFirma['strada'] = 'Ernest Juvara';
  $dateFirma['numar'] = '24';
  $dateFirma['bloc'] = '';
  $dateFirma['scara'] = '';
  $dateFirma['etaj'] = '';
  $dateFirma['apartament'] = '';
  $dateFirma['localitate'] = 'Bucuresti';
  $dateFirma['sector'] = '6';
  $dateFirma['judet'] = 'Bucuresti';
  $dateFirma['expeditorNume'] = 'Margelia';
  $dateFirma['expeditorPrenume'] = 'Handmade SRL';



$pdf = new PDF('L','mm','A4');
$pdf->AddPage();

//$pdf->RotatedImage('circle.png', 85, 60, 40, 16, 45);
$pdf->Image('pozapdf_juridica00.jpg',0,0,295);

if (!isset($_SESSION['numarordine'])) {$nro="";} else {$nro=$_SESSION['numarordine'];}



//Aici incep datele pentru formular1 stanga sus
$pdf->SetFont('Arial', 'B', 11);
$pdf->RotatedText(1,10, $nro,0);  //Numarul de ordine din borderou


$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(47,32, $felultrimiterii,0);  //Felul trimiterii
$pdf->RotatedText(110,32, $continutul,0);  //Continutul


$pdf->RotatedText(38,47.5, $valoareincifre,0);  //valoare lei cifre
$pdf->RotatedText(60,47.5, $valoareinlitere,0);  //valoare lei litere
$pdf->RotatedText(38,41, $ramburscifre,0);  //ramburs lei cifre
$pdf->RotatedText(60,41, $rambursinlitere,0);  //ramburs lei litere



$pdf->RotatedText(32,53.5, $np,0);  //nume prenume destinatar
$pdf->RotatedText(112,53.5, $telefon,0);  //nume prenume destinatar
$pdf->RotatedText(22,59, $adresacompleta,0);  //completa destinatatttttttttttt destinatar // modificare george
//$pdf->RotatedText(22,59, $strada,0);  //strada destinatar
//$pdf->RotatedText(74,59, $numarul,0);  //nr destinatar
//$pdf->RotatedText(92,59, $bloc,0);  //bloc destinatar
//$pdf->RotatedText(108,59, $scara,0);  //scara destinatar
//$pdf->RotatedText(122,59, $etaj,0);  //etaj destinatar
//$pdf->RotatedText(135,59, $apartament,0);  //apartament destinatar
$pdf->RotatedText(38,65, $codpostal,0);  //cod postal destinatar
$pdf->RotatedText(83,65, $localitate,0);  //localitate destinatar
$pdf->RotatedText(135,65, $sector,0);  //sector destinatar
$pdf->RotatedText(27,75, $judetul,0);  //judet destinatar
$pdf->RotatedText(31,81.5, $dateFirma['denumire'],0);  //denumire expeditor
$pdf->RotatedText(96,81.5, $dateFirma['telefon'],0);  //telefon expeditor
$pdf->RotatedText(22,87, $dateFirma['strada'],0);  //strada expeditor
$pdf->RotatedText(48,87, $dateFirma['numar'],0);  //nr expeditor
$pdf->RotatedText(60,87, $dateFirma['bloc'],0);  //bloc expeditor
$pdf->RotatedText(71,87, $dateFirma['scara'],0);  //scara expeditor
$pdf->RotatedText(82,87, $dateFirma['etaj'],0);  //etaj expeditor
$pdf->RotatedText(94,87, $dateFirma['apartament'],0);  //apartament expeditor
$pdf->RotatedText(109.5,87, $dateFirma['sector'],0);  //sector expeditor
$pdf->RotatedText(59,92, $dateFirma['localitate'],0);  //localitate expeditor
$pdf->RotatedText(98,92, $dateFirma['judet'],0);  //judetul expeditor
///sfarsit formular stanga sus

//inceput formular stanga jos
$pdf->RotatedText(32,135.5, $np,0);  //nume prenume destinatar
$pdf->RotatedText(117,135.5, $telefon,0);  //nume prenume destinatar
$pdf->RotatedText(22,143.5, $adresacompleta,0);  //completa destinatatttttttttttt destinatar / modificare george
//$pdf->RotatedText(22,143.5, $strada,0);  //strada destinatar
//$pdf->RotatedText(86,143.5, $numarul,0);  //nr destinatar
//$pdf->RotatedText(98,143.5, $bloc,0);  //bloc destinatar
//$pdf->RotatedText(111,143.5, $scara,0);  //scara destinatar
//$pdf->RotatedText(122,143.5, $etaj,0);  //etaj destinatar
//$pdf->RotatedText(134,143.5, $apartament,0);  //apartament destinatar
$pdf->RotatedText(98,149, $felultrimiterii,0);  //Felul trimiterii
$pdf->RotatedText(117,155, $valoareincifre,0);  //valoare lei cifre
$pdf->RotatedText(79,155, $ramburscifre,0);  //ramburs lei cifre
$pdf->RotatedText(34,161, $dateFirma['denumire'],0);  //denumire expeditor
$pdf->RotatedText(22,167, $dateFirma['strada'],0);  //strada expeditor
$pdf->RotatedText(70,167, $dateFirma['numar'],0);  //nr expeditor
$pdf->RotatedText(115,167, $dateFirma['localitate'],0);  //localitate expeditor
//sfarsit formular staga jos

//inceput formular dreapta sus
$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(244,4, $np, -90);
//$pdf->RotatedText(240,4, $adresacompleta,-90);  //completa destinatatttttttttttt destinatar / modificare george
//$pdf->RotatedText(240,4, 'Str. '.$strada,-90);  //strada si numar 
$pdf->RotatedText(236,8,$valo1,-90);
$pdf->RotatedText(236,52, $numarul,-90);  //nr 
$pdf->RotatedText(232,10, $codpostal,-90);  //cod postal 
$pdf->RotatedText(232,37, $localitate,-90);  //localitate
$pdf->RotatedText(212,15, $telefon,-90);  //telefon
$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(198,18, $ramburscifre,-90);
//sfarsit formular dreapta sus

//inceput formular dreapta jos
$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(249,70, $dateFirma['denumire'],-90); //nume firma
$pdf->RotatedText(245.3,86, $cui,-90);  //cui firma
$pdf->RotatedText(241.6,75, $dateFirma['strada'],-90);
$pdf->RotatedText(241.6,125, $dateFirma['numar'],-90);
$pdf->RotatedText(237.9,105, $dateFirma['localitate'],-90);
$pdf->RotatedText(234.5,84, $nrcont,-90);
$pdf->RotatedText(231,89, $codcont,-90);
$pdf->RotatedText(227.3,96, $deschiscont,-90);
$pdf->RotatedText(219.9,86, $np,-90);
$pdf->RotatedText(216.4,86, $localitate,-90);
$pdf->SetFont('Arial', 'B', 10);
$pdf->RotatedText(205,90, $ramburscifre,-90);

$findme="si";
$pos = strpos($rambursinlitere, $findme);
if ($pos === false) {
       $pdf->RotatedText(193,68,$rambursinlitere,-90);
} else {
$rrs=explode("si",$rambursinlitere);
$pdf->RotatedText(193,68, $rrs[0],-90);
$pdf->RotatedText(185,68, 'si '.$rrs[1],-90);
}
//sfarsit formular dreapta jos

//pentru eticheta dreapta jos
$pdf->SetFont('Arial', '', 14);
$pdf->RotatedText(276,170, $np,90);
$pdf->RotatedText(276,110, $nro,90);
$pdf->RotatedText(282,205, $adresacompleta,90); //modificare george
//$pdf->RotatedText(282,205, $np,90); 
//$pdf->RotatedText(276,170, $nro,90);
//$pdf->RotatedText(282,205, $np,90);


$pdf->RotatedText(287.8,205, $valo,90); 

$pdf->RotatedText(293.6,205,$localitate.' Jud. '.$judetul.' Tel. '.$telefon,90);
//sfarsit eticheta dreapta jos

//eticheta dreapta sus

$rand1="Buc OP 35                  EXPEDITOR";
$rand2="CP                              Margelia Handmade";
$rand3="Greut.                         Ernest Juvara 24 ";
$rand4="Val. ".$_SESSION['valincifre']." Lei                  Sect. 6, Bucuresti";;
$rand5="Rbs. ".$_SESSION['ramburscifre']." Lei";
$rand6="accesorii";
$pdf->SetFont('Arial', '', 11	);
$pdf->RotatedText(272,70, $rand1,90);
$pdf->RotatedText(276.5,70, $rand2,90);
$pdf->RotatedText(281,70, $rand3,90);
$pdf->RotatedText(285.5,70, $rand4,90);
$pdf->RotatedText(290,70, $rand5,90);
$pdf->RotatedText(295,70, $rand6,90);
//sfarsit eticheta dreapta sus


$pdf->Output();

?> 