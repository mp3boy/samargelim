<?php       
session_start();
include "connect.php";
 function strLei($No, $sp='.', $pct=',' ) {
 
    // numerele literal
    $na = array ( "", "Unu", "Doi", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nb = array ( "", "Un",  "Doua", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nc = array ( "", "Una", "Doua","Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
    $nd = array ( "", "Unu", "Doua", "Trei", "Patru", "Cinci", "Sase", "Sapte", "Opt", "Noua");
 
    // exceptie "saizeci"
    $ex1 = 'Sai';
 
    // unitati
    $ua = array ( "", "Zece", "Zeci", "Zeci","Zeci","Zeci","Zeci","Zeci","Zeci","Zeci");
    $ub = array ( "", "Suta", "Sute", "Sute","Sute","Sute","Sute","Sute","Sute","Sute");
    $uc = array ( "", "Mie", "Mii");
    $ud = array ( "", "Milion", "Milioane");
    $ue = array ( "", "Miliard", "Miliarde");
 
    // legatura intre grupuri
    $lg1 = array ("", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre", "Spre");
    $lg2 = array ("", "", "Si",  "Si", "Si", "Si", "Si", "Si", "Si", "Si" );
 
    // moneda
    $mon = array ("", " leu", " lei");
    $ban = array ("", " ban ", " bani ");
 
    //se elimina $sp din numar
    $sNo = (string) $No;
    $sNo = str_replace($sp,"",$sNo);
 
    //extrag partea intreaga si o completez cu zerouri la stg.
    $NrI = sprintf("%012s",(string) strtok($sNo,$pct));
 
    // extrag zecimalele
    $Zec = (string) strtok($pct);
    $Zec = substr($Zec . '00',0,2);
 
    // grupul 4 (miliarde)
    $Gr = substr($NrI,0,3);
    $n1 = (integer) $NrI[0];
    $n2 = (integer) $NrI[1];
    $n3 = (integer) $NrI[2];
    $Rez = $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1) ? $Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2] :
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nb[$n3]:$nd[$n3]);
 
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $ue[1]):($Rez . $ue[2]));
 
    // grupul 3 (milioane)
    $Gr = substr($NrI,3,3);
    $n1 = (integer) $NrI[3];
    $n2 = (integer) $NrI[4];
    $n3 = (integer) $NrI[5];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?$Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2]:
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nb[$n3]:$nd[$n3]);
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $ud[1]):($Rez . $ud[2]));
 
    // grupul 2 (mii)
    $Gr = substr($NrI,6,3);
    $n1 = (integer) $NrI[6];
    $n2 = (integer) $NrI[7];
    $n3 = (integer) $NrI[8];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?$Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2]:
    $Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg2[$n2] .
       ($Gr=="001"||$Gr=="002"?$nc[$n3]:$nd[$n3]);
    $Rez = ($Gr == "000")?$Rez:(($Gr == "001")?($Rez . $uc[1]):($Rez . $uc[2]));
 
    // grupul 1 (unitati)
    $Gr = substr($NrI,9,3);
    $n1 = (integer) $NrI[9];
    $n2 = (integer) $NrI[10];
    $n3 = (integer) $NrI[11];
    $Rez = $Rez . $sp . $nc[$n1] . $ub[$n1];
    $Rez = ($n2 == 1)?($Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2].$mon[2]):($Rez .
       ($n2==6?$ex1:$nc[$n2]). $ua[$n2] .
       ($n3>0?$lg2[$n2]:'') . ($NrI=="000000000001"?($nb[$n3] .$mon[1]):($na[$n3]). $mon[2]));
 
    if ((integer) $NrI == 0) {$Rez = ""; }
 
    // banii
    if ((integer) $Zec>0) {
       $n2 = (integer) substr($Zec,0,1);
       $n3 = (integer) substr($Zec,1,1);
       $Rez .= ' si ';
       
       //$lg22 = ($n3=='0'?'':$lg2[$n2]); 
       //$Rez = ($n2 == 1)?($Rez . $nb[$n3] . $lg1[$n3] . $ua[$n2].$ban[2]):
       //($Rez . ($n2==6?$ex1:$nc[$n2]) . $ua[$n2] . $lg22 . ($Zec=="01"?($nb[$n3] .
       //   $ban[1]):($na[$n3]). $ban[2]));
       
       if (($n3==0) || ($n3=="")) { $nn=$n2*10; $Rez .=$nn." bani";} else {$Rez .=$n2."".$n3." bani";}
    }
 
    return $Rez;
 }
?>

<style type="text/css">
table.sampler {
	border-width: 1px;
	border-spacing: 2px;
	border-style: solid;
	border-color: gray;
	border-collapse: separate;
	background-color: #ECE3FF;
	border-radius: 15px;
}
input {
border: 0.5pt solid #6B7CFF;
border-collapse: collapse;
border-spacing: 0;
font-size: 10pt;
height: 18pt;
margin: 0;
padding: 0;
space: 0;
width: 200px;
z-index: 1;
}
</style>
<?php


$ord=$conn->query("SELECT * FROM ps_address as a INNER JOIN ps_orders as b ON a.id_address=b.id_address_delivery AND b.id_order='".$_GET['oID']."'")->fetch_array();



//$ord = mysql_query("SELECT delivery_name,delivery_company, delivery_street_address,delivery_city, delivery_postcode, delivery_state, customers_telephone, order_total FROM orders WHERE orders_id='".$_GET['oID']."'");

// echo '<pre>';print_r($ord);
// die();


// list($numeCompletDestinatar, $numeCompanie, $adresaCompletaDestinatar, $localitateDestinatar, $codPostalDestinatar, $judetDestinatar, $telefonDestinatar, $totalsumaComanda) = mysql_fetch_row($ord);
	
//   $rambursValoareComanda = '';
// 	$dateNumeDestinatar = explode(' ',$numeCompletDestinatar);
// 	$numeDestinatar = $dateNumeDestinatar[0];
// 	$prenumeDestinatar = $dateNumeDestinatar[1];

$numeDestinatar=$ord['lastname'];
$prenumeDestinatar=$ord['firstname'];
$numeCompanie=$ord['company'];
$adresaCompletaDestinatar=$ord['address1'];
$localitateDestinatar=$ord['city'];
$codPostalDestinatar=$ord['postcode'];

$telefonDestinatar=$ord['phone'];
$totalsumaComanda=number_format($ord['total_products'],2,"."," ");

$ord1=$conn->query("SELECT name FROM ps_state WHERE id_state='".$ord['id_state']."'")->fetch_array();
$judetDestinatar=$ord1['name'];
	
if (!isset($_GET['vlc'])) { $valoarelei="20";  } else {$valoarelei=$_GET['vlc'];}
$show = strLei($valoarelei,'','.');  
if (!isset($_GET['rlc'])) { $valcom=$totalsumaComanda;  } else {$valcom=$_GET['rlc'];}  
  $show1 = strLei($valcom,'','.');
 
 $sumatotala=$valoarelei+$valcom;
 $show2=strLei($sumatotala,'','.'); 
  	
?>
<form action="" method="post">
<table width="70%" align="center">
<tr>
     <td width="100%" align=center>
                <table class="sampler" cellpadding="4" cellspacing="0">
                      <tr><td colspan="4">&nbsp;</td></tr>
                      
                      <tr>
                            <td width="17%" valign="top">Nume Destinatar</td>
                            <td width="30%" ><input type="text" name="numedest" value="<?php echo $numeDestinatar;?>"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top" rowspan="2">Adresa completa</td>
                            <td width="30%" valign="top" rowspan="2"><textarea name="adresacompleta" style="width:200px;height:80px;"><?php echo $adresaCompletaDestinatar;?></textarea></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top">Prenume Destinatar</td>
                            <td width="30%" ><input type="text" name="prenumedest" value="<?php echo $prenumeDestinatar?>"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top"></td>
                            <td width="30%" valign="top"></td>
                      </tr>
                      <form action="" method="post">
                      <tr>
                            <td width="17%" valign="top">Valoare lei in cifre</td>
                            <td width="30%" ><input type="text" name="valincifre" value="<?php echo $valoarelei;?>" onchange="updateInput(this.value);" /></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Str.</td>
                            <td width="30%" valign="top"><input type="text" name="str" value="" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top">Ramburs lei in cifre</td>
                            <td width="30%" ><input type="text" name="rambursincifre" value="<?php echo $valcom;?>"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Nr.</td>
                            <td width="30%" valign="top"><input type="text" name="nr" value="" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top">Valoare lei in litere</td>
                            <td width="30%" ><input type="text" id="valit" name="valinlitere" value="<?php echo $show;?>"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Bloc</td>
                            <td width="30%" valign="top"><input type="text" name="bloc" value="" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top">Ramburs lei in litere</td>
                            <td width="30%"><input type="text" name="rambursinlitere" value="<?php echo $show1;?>"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Scara</td>
                            <td width="30%" valign="top"><input type="text" name="scara" value="" /></td>
                      </tr>
					  <tr>
                            <td width="17%" valign="top">Greutate (kg)</td>
                            <td width="30%"><input type="text" name="greutate" value="1"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Etaj</td>
                            <td width="30%" valign="top"><input type="text" name="etaj" value="" /></td>
                      </tr>
                      <tr>
                            <td valign="top" colspan="2" align="center"><input type="submit" name="sub1" value="Modificare RON" onmouseover="this.style.cursor='pointer';"/></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Apartament</td>
                            <td width="30%" valign="top"><input type="text" name="apartament" value="" /></td>
                      </tr>
                      </form>
                      <?php 
                      if (!isset($_POST['sub1'])) {}
                      else if ($_POST['sub1']=="Modificare RON") {
                                                                 $magic="?oID=".$_GET['oID']."&vlc=".$_POST['valincifre']."&rlc=".$_POST['rambursincifre']; 
                                                                 echo "<script>location.href='".$magic."'</script>";
                                                                 //header('location: '.$magic);
                                                                 }
                      ?>
                     
                      <tr>
                            <td width="17%" valign="top"></td>
                            <td width="30%"></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Cod postal</td>
                            <td width="30%" valign="top"><input type="text" name="codpostal" value="<?php echo $codPostalDestinatar;?>" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top" colspan="2" align="center"></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Localitate</td>
                            <td width="30%" valign="top"><input type="text" name="localitate" value="<?php echo $localitateDestinatar;?>" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top"></td>
                            <td width="30%" valign="top"></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Judetul</td>
                            <td width="30%" valign="top"><input type="text" name="judetul" value="<?php echo $judetDestinatar;?>" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top"></td>
                            <td width="30%" valign="top"></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Sector</td>
                            <td width="30%" valign="top"><input type="text" name="sector" value="" /></td>
                      </tr>
                      <tr>
                            <td width="17%" valign="top"></td>
                            <td width="30%" valign="top"></td>
                            <td width="6%">&nbsp;</td>
                            <td width="17%" valign="top">Telefon</td>
                            <td width="30%" valign="top"><input type="text" name="telefon" value="<?php echo $telefonDestinatar;?>"/></td>
                      </tr>

                      <tr><td colspan="5" align="center"></td></tr>
                      <tr><td colspan="5" align="center"><input type="submit" name="Printeaza" value="PRINT" onmouseover="this.style.cursor='pointer';"></td></tr>
                </table>
                
    </td>
    
</tr>

<tr>
<td width="100%" align="left">
<div style="text-align:left;margin-top:10px;">
<div style="float:left;width:40px;">
<input type="checkbox" checked name="borderou1" value="1" />
</div>
<div style="float:left;margin-left:70px;">Adauga in borderou (cache)</div>
<div style="clear:both;">&nbsp;</div>
</div>
</td>
</tr>

</table>
</form>

<?php
if (!isset($_POST['Printeaza'])) {}
else if ($_POST['Printeaza']=="PRINT") {
                                       //echo "ok";
                                       $_SESSION['nume']=$_POST['numedest'];
                                       $_SESSION['prenume']=$_POST['prenumedest'];
                                       $_SESSION['adresacompleta']=$_POST['adresacompleta'];
                                       $_SESSION['valincifre']=$_POST['valincifre'];
                                       $_SESSION['str']=$_POST['str'];
                                       $_SESSION['ramburscifre']=$_POST['rambursincifre'];
                                       $_SESSION['nr']=$_POST['nr'];
                                       $_SESSION['valinlitere']=$_POST['valinlitere'];
                                       $_SESSION['bloc']=$_POST['bloc'];
                                       $_SESSION['rambursinlitere']=$_POST['rambursinlitere'];
                                       $_SESSION['scara']=$_POST['scara'];
                                       $_SESSION['etaj']=$_POST['etaj'];
                                       // $_SESSION['sumatotalcifre']=$_POST['sumatotalcifre'];
                                       // $_SESSION['sumatotalalitere']=$_POST['sumatotalalitere'];
                                       $_SESSION['codpostal']=$_POST['codpostal'];
                                       $_SESSION['localitate']=$_POST['localitate'];
                                       $_SESSION['judetul']=$_POST['judetul'];
                                       $_SESSION['telefon']=$_POST['telefon'];
                                       $_SESSION['apartament']=$_POST['apartament'];
                                       $_SESSION['sector']=$_POST['sector'];
                                      // header('location: print1.php');
										
										//echo strLei($_POST['valincifre'],'','.')." ".$_POST['valinlitere'];
									   if (strLei($_POST['rambursincifre'],'','.')!=$_POST['rambursinlitere']) {
									  echo "<script>alert('Atentie la modificarea rambursului!');</script>";
									   } else {



		if ($_POST['borderou1']==1) {

			$orde=$conn->query("SELECT * FROM borderou WHERE idcomanda='".$_GET['oID']."' AND tip=1");
			$mum = $orde->num_rows;
			//	$mum=mysql_num_rows(mysql_query("SELECT * FROM borderou WHERE idcomanda='".$_GET['oID']."'"));									   
									   //se adauga in baza de date
									   
						   
if ($mum==0) {
	$nume=$_POST['numedest']." ".$_POST['prenumedest'];

	///intreb daca e vreun loc gol
	// $sio=mysql_query("SELECT * FROM borderou WHERE idcomanda='0' AND tip='1' LIMIT 0,1");
	// $sipi=mysql_num_rows($sio);

	$sio=$conn->query("SELECT * FROM borderou WHERE idcomanda='0' AND tip='1' LIMIT 0,1");
	$sipi = $sio->num_rows;



	if ($sipi==0) {
		$thesql="INSERT INTO borderou (idcomanda,nume,localitate,valoare,ramburs,kg,tip,data) VALUES ('".$_GET['oID']."','".$nume."','".$_POST['localitate']."','".$_POST['valincifre']."','".$_POST['rambursincifre']."','".$_POST['greutate']."','1','".date('Y-m-d H:i:s')."')";
		$mysq=$conn->query($thesql);
				 
				} else {

		$plm=$sio->fetch_array();
		$lala="UPDATE borderou SET idcomanda='".$_GET['oID']."',nume='".$nume."',localitate='".$_POST['localitate']."',valoare='".$_POST['valincifre']."',ramburs='".$_POST['rambursincifre']."',kg='".$_POST['greutate']."' WHERE id='".$plm['id']."'";
		//echo $lala;die();
		$mysq=$conn->query($lala);
				}	  
					  
			} 
																	}


////extrage numarul de ordine
$ppp1=$conn->query("SELECT idcomanda FROM borderou WHERE tip='1' ORDER BY data ASC");	

$nro=1;
$yoo=$ppp1->fetch_array(MYSQLI_ASSOC);



foreach($yoo as $yu){
	//print_r($yu);

	if ($yu==$_GET['oID']) {$_SESSION['numarordine1']=$nro;}
	$nro++;
}	
//echo $_SESSION['numarordine1'];die();								 									
echo "<script>location.href='print1.php'</script>";
									  
										
       										  }
									   
									   
									   
									   
									   }
?>
